// #include "gtest/gtest.h"
// #include "gmock/gmock.h"
// #include "LowLevelGitApi.h"
// #include "Pipe.h"
// #include "Model_mocks.h"
// #include "TreeViewModel.h"
// #include "StatuserViewModel.h"
//
// using namespace testing;
//
//
// class TreeViewModelTest : public Test
// {
// protected:
//
// 	std::shared_ptr<TerminatorColorizer> m_terminator_colorizer;
// 	std::shared_ptr<Pipe> m_pipe;
// 	std::shared_ptr<NiceMock<MockLibGiter>> m_mock_lib_giter;
// 	std::shared_ptr<NiceMock<MockTerminator>> m_mock_terminator;
//
// 	std::shared_ptr<NiceMock<MockTree>> m_mock_tree;
// 	std::shared_ptr<ITreeViewModel> m_tree_view_model;
//
// 	TreeViewModelTest()
// 	{
// 		m_terminator_colorizer = std::make_shared<TerminatorColorizer>();
// 		m_pipe = std::make_shared<Pipe>();
// 		std::filesystem::path path_to_repo = std::filesystem::current_path().parent_path();
// 		m_mock_lib_giter = std::make_shared<NiceMock<MockLibGiter>>(path_to_repo);
// 		m_mock_terminator = std::make_shared<NiceMock<MockTerminator>>(m_mock_lib_giter.get(), m_terminator_colorizer.get());
//
// 		m_mock_tree = std::make_shared<NiceMock<MockTree>>(m_mock_lib_giter.get(), m_mock_terminator.get());
// 		m_tree_view_model = std::make_shared<TreeViewModel>(m_mock_tree.get());
// 	}
//
// 	void Verify_update_the_tree()
// 	{
// 		unsigned int clear_the_tree_counter = 0;
// 		m_tree_view_model->clear_the_tree.subscribe([&]()
// 		{
// 			clear_the_tree_counter++;
// 		});
//
// 		std::vector<bool> tree_enablement_res;
// 		m_tree_view_model->tree_enablement.subscribe([&](const bool should_enable)
// 		{
// 			tree_enablement_res.emplace_back(should_enable);
// 		});
//
// 		m_tree_view_model->Update_the_tree();
//
// 		ASSERT_TRUE(clear_the_tree_counter == 1);
//
// 		ASSERT_TRUE(tree_enablement_res.size() == 2);
// 		ASSERT_TRUE(tree_enablement_res[0] == false);
// 		ASSERT_TRUE(tree_enablement_res[1] == true);
// 	}
// };
//
// TEST_F(TreeViewModelTest, ctor_test) // NOLINT
// {
// 	const std::shared_ptr<Line> temp_line = std::make_shared<Line>(std::vector<Node>{Node(0, "sha1", 42, COMMIT)}, m_mock_lib_giter.get(), 9);
//
// 	unsigned int counter = 0;
// 	m_tree_view_model->new_line.subscribe([&](const std::shared_ptr<Line>& line)
// 	{
// 		counter++;
//
// 		ASSERT_TRUE(counter == 1);
//
// 		ASSERT_TRUE(line == temp_line);
// 	});
//
// 	m_mock_tree->new_line.notify(temp_line);
// }
//
// TEST_F(TreeViewModelTest, Calc_drawing_coordinates_is_tip_true) // NOLINT
// {
// 	const Node node = Node(1, "sha1", 0, COMMIT, true);
// 	Line line(std::vector<Node>{node}, m_mock_lib_giter.get(), 0);
// 	const std::shared_ptr<CALCULATIONS> calculations = std::make_shared<CALCULATIONS>();
// 	m_tree_view_model->Calc_drawing_coordinates(Node(node), line, 22, 0, 5, calculations);
//
// 	ASSERT_TRUE(calculations->y_center == 11);
// 	ASSERT_TRUE(calculations->x_center == 20);
//
// 	ASSERT_TRUE(calculations->tip_line_x1 == 20);
// 	ASSERT_TRUE(calculations->tip_line_y1 == 11);
// 	ASSERT_TRUE(calculations->tip_line_x2 == 20);
// 	ASSERT_TRUE(calculations->tip_line_y2 == 22);
//
// 	ASSERT_TRUE(calculations->tip_line_first_commit_x1 == 20);
// 	ASSERT_TRUE(calculations->tip_line_first_commit_y1 == 11);
// 	ASSERT_TRUE(calculations->tip_line_first_commit_x2 == 20);
// 	ASSERT_TRUE(calculations->tip_line_first_commit_y2 == 11);
//
// 	ASSERT_TRUE(calculations->rect_x == 14);
// 	ASSERT_TRUE(calculations->rect_y == 5);
// 	ASSERT_TRUE(calculations->rect_width == 12);
// 	ASSERT_TRUE(calculations->rect_height == 12);
//
// 	ASSERT_TRUE(calculations->vertical_x1 == 20);
// 	ASSERT_TRUE(calculations->vertical_y1 == 0);
// 	ASSERT_TRUE(calculations->vertical_x2 == 20);
// 	ASSERT_TRUE(calculations->vertical_y2 == 22);
//
// 	ASSERT_TRUE(calculations->horizontal_x1 = 10);
// 	ASSERT_TRUE(calculations->horizontal_y1 = 21);
// 	ASSERT_TRUE(calculations->horizontal_x2 == 30);
// 	ASSERT_TRUE(calculations->horizontal_y2 == 21);
//
// 	ASSERT_TRUE(calculations->spline_left_to_right_x1 == 10);
// 	ASSERT_TRUE(calculations->spline_left_to_right_y1 == 0);
// 	ASSERT_TRUE(calculations->spline_left_to_right_x2 == 15);
// 	ASSERT_TRUE(calculations->spline_left_to_right_y2 == 11);
// 	ASSERT_TRUE(calculations->spline_left_to_right_x3 == 25);
// 	ASSERT_TRUE(calculations->spline_left_to_right_y3 == 11);
// 	ASSERT_TRUE(calculations->spline_left_to_right_x4 == 30);
// 	ASSERT_TRUE(calculations->spline_left_to_right_y4 == 22);
//
// 	ASSERT_TRUE(calculations->left_to_right_narrow_x1 == 10);
// 	ASSERT_TRUE(calculations->left_to_right_narrow_y1 == 0);
// 	ASSERT_TRUE(calculations->left_to_right_narrow_x2 == 20);
// 	ASSERT_TRUE(calculations->left_to_right_narrow_y2 == 22);
//
// 	ASSERT_TRUE(calculations->spline_right_to_left_x1 == 30);
// 	ASSERT_TRUE(calculations->spline_right_to_left_y1 == 0);
// 	ASSERT_TRUE(calculations->spline_right_to_left_x2 == 25);
// 	ASSERT_TRUE(calculations->spline_right_to_left_y2 == 11);
// 	ASSERT_TRUE(calculations->spline_right_to_left_x3 == 15);
// 	ASSERT_TRUE(calculations->spline_right_to_left_y3 == 11);
// 	ASSERT_TRUE(calculations->spline_right_to_left_x4 == 10);
// 	ASSERT_TRUE(calculations->spline_right_to_left_y4 == 22);
//
// 	ASSERT_TRUE(calculations->right_to_left_narrow_x1 == 20);
// 	ASSERT_TRUE(calculations->right_to_left_narrow_y1 == 0);
// 	ASSERT_TRUE(calculations->right_to_left_narrow_x2 == 10);
// 	ASSERT_TRUE(calculations->right_to_left_narrow_y2 == 22);
//
// 	ASSERT_TRUE(calculations->commit_message_offset == 20);
// }
//
// TEST_F(TreeViewModelTest, Calc_drawing_coordinates_is_tip_false) // NOLINT
// {
// 	const Node node = Node(1, "sha1", 0, COMMIT, false);
// 	Line line(std::vector<Node>{node}, m_mock_lib_giter.get(), 0);
// 	const std::shared_ptr<CALCULATIONS> calculations = std::make_shared<CALCULATIONS>();
// 	m_tree_view_model->Calc_drawing_coordinates(Node(node), line, 22, 0, 5, calculations);
//
// 	ASSERT_TRUE(calculations->y_center == 11);
// 	ASSERT_TRUE(calculations->x_center == 20);
//
// 	ASSERT_TRUE(calculations->tip_line_x1 == 20);
// 	ASSERT_TRUE(calculations->tip_line_y1 == 0);
// 	ASSERT_TRUE(calculations->tip_line_x2 == 20);
// 	ASSERT_TRUE(calculations->tip_line_y2 == 22);
//
// 	ASSERT_TRUE(calculations->tip_line_first_commit_x1 == 20);
// 	ASSERT_TRUE(calculations->tip_line_first_commit_y1 == 0);
// 	ASSERT_TRUE(calculations->tip_line_first_commit_x2 == 20);
// 	ASSERT_TRUE(calculations->tip_line_first_commit_y2 == 11);
//
// 	ASSERT_TRUE(calculations->rect_x == 14);
// 	ASSERT_TRUE(calculations->rect_y == 5);
// 	ASSERT_TRUE(calculations->rect_width == 12);
// 	ASSERT_TRUE(calculations->rect_height == 12);
//
// 	ASSERT_TRUE(calculations->vertical_x1 == 20);
// 	ASSERT_TRUE(calculations->vertical_y1 == 0);
// 	ASSERT_TRUE(calculations->vertical_x2 == 20);
// 	ASSERT_TRUE(calculations->vertical_y2 == 22);
//
// 	ASSERT_TRUE(calculations->horizontal_x1 = 10);
// 	ASSERT_TRUE(calculations->horizontal_y1 = 21);
// 	ASSERT_TRUE(calculations->horizontal_x2 == 30);
// 	ASSERT_TRUE(calculations->horizontal_y2 == 21);
//
// 	ASSERT_TRUE(calculations->spline_left_to_right_x1 == 10);
// 	ASSERT_TRUE(calculations->spline_left_to_right_y1 == 0);
// 	ASSERT_TRUE(calculations->spline_left_to_right_x2 == 15);
// 	ASSERT_TRUE(calculations->spline_left_to_right_y2 == 11);
// 	ASSERT_TRUE(calculations->spline_left_to_right_x3 == 25);
// 	ASSERT_TRUE(calculations->spline_left_to_right_y3 == 11);
// 	ASSERT_TRUE(calculations->spline_left_to_right_x4 == 30);
// 	ASSERT_TRUE(calculations->spline_left_to_right_y4 == 22);
//
// 	ASSERT_TRUE(calculations->left_to_right_narrow_x1 == 10);
// 	ASSERT_TRUE(calculations->left_to_right_narrow_y1 == 0);
// 	ASSERT_TRUE(calculations->left_to_right_narrow_x2 == 20);
// 	ASSERT_TRUE(calculations->left_to_right_narrow_y2 == 22);
//
// 	ASSERT_TRUE(calculations->spline_right_to_left_x1 == 30);
// 	ASSERT_TRUE(calculations->spline_right_to_left_y1 == 0);
// 	ASSERT_TRUE(calculations->spline_right_to_left_x2 == 25);
// 	ASSERT_TRUE(calculations->spline_right_to_left_y2 == 11);
// 	ASSERT_TRUE(calculations->spline_right_to_left_x3 == 15);
// 	ASSERT_TRUE(calculations->spline_right_to_left_y3 == 11);
// 	ASSERT_TRUE(calculations->spline_right_to_left_x4 == 10);
// 	ASSERT_TRUE(calculations->spline_right_to_left_y4 == 22);
//
// 	ASSERT_TRUE(calculations->right_to_left_narrow_x1 == 20);
// 	ASSERT_TRUE(calculations->right_to_left_narrow_y1 == 0);
// 	ASSERT_TRUE(calculations->right_to_left_narrow_x2 == 10);
// 	ASSERT_TRUE(calculations->right_to_left_narrow_y2 == 22);
//
// 	ASSERT_TRUE(calculations->commit_message_offset == 20);
// }
//
// TEST_F(TreeViewModelTest, Calc_drawing_coordinates_empty_nodes) // NOLINT
// {
// 	const Node node = Node(1, "sha1", 0, COMMIT, false);
// 	Line line(std::vector<Node>{}, m_mock_lib_giter.get(), 0);
// 	const std::shared_ptr<CALCULATIONS> calculations = std::make_shared<CALCULATIONS>();
// 	m_tree_view_model->Calc_drawing_coordinates(Node(node), line, 22, 0, 5, calculations);
//
// 	ASSERT_TRUE(calculations->y_center == 11);
// 	ASSERT_TRUE(calculations->x_center == 20);
//
// 	ASSERT_TRUE(calculations->tip_line_x1 == 20);
// 	ASSERT_TRUE(calculations->tip_line_y1 == 0);
// 	ASSERT_TRUE(calculations->tip_line_x2 == 20);
// 	ASSERT_TRUE(calculations->tip_line_y2 == 22);
//
// 	ASSERT_TRUE(calculations->tip_line_first_commit_x1 == 20);
// 	ASSERT_TRUE(calculations->tip_line_first_commit_y1 == 0);
// 	ASSERT_TRUE(calculations->tip_line_first_commit_x2 == 20);
// 	ASSERT_TRUE(calculations->tip_line_first_commit_y2 == 11);
//
// 	ASSERT_TRUE(calculations->rect_x == 14);
// 	ASSERT_TRUE(calculations->rect_y == 5);
// 	ASSERT_TRUE(calculations->rect_width == 12);
// 	ASSERT_TRUE(calculations->rect_height == 12);
//
// 	ASSERT_TRUE(calculations->vertical_x1 == 20);
// 	ASSERT_TRUE(calculations->vertical_y1 == 0);
// 	ASSERT_TRUE(calculations->vertical_x2 == 20);
// 	ASSERT_TRUE(calculations->vertical_y2 == 22);
//
// 	ASSERT_TRUE(calculations->horizontal_x1 = 10);
// 	ASSERT_TRUE(calculations->horizontal_y1 = 21);
// 	ASSERT_TRUE(calculations->horizontal_x2 == 30);
// 	ASSERT_TRUE(calculations->horizontal_y2 == 21);
//
// 	ASSERT_TRUE(calculations->spline_left_to_right_x1 == 10);
// 	ASSERT_TRUE(calculations->spline_left_to_right_y1 == 0);
// 	ASSERT_TRUE(calculations->spline_left_to_right_x2 == 15);
// 	ASSERT_TRUE(calculations->spline_left_to_right_y2 == 11);
// 	ASSERT_TRUE(calculations->spline_left_to_right_x3 == 25);
// 	ASSERT_TRUE(calculations->spline_left_to_right_y3 == 11);
// 	ASSERT_TRUE(calculations->spline_left_to_right_x4 == 30);
// 	ASSERT_TRUE(calculations->spline_left_to_right_y4 == 22);
//
// 	ASSERT_TRUE(calculations->left_to_right_narrow_x1 == 10);
// 	ASSERT_TRUE(calculations->left_to_right_narrow_y1 == 0);
// 	ASSERT_TRUE(calculations->left_to_right_narrow_x2 == 20);
// 	ASSERT_TRUE(calculations->left_to_right_narrow_y2 == 22);
//
// 	ASSERT_TRUE(calculations->spline_right_to_left_x1 == 30);
// 	ASSERT_TRUE(calculations->spline_right_to_left_y1 == 0);
// 	ASSERT_TRUE(calculations->spline_right_to_left_x2 == 25);
// 	ASSERT_TRUE(calculations->spline_right_to_left_y2 == 11);
// 	ASSERT_TRUE(calculations->spline_right_to_left_x3 == 15);
// 	ASSERT_TRUE(calculations->spline_right_to_left_y3 == 11);
// 	ASSERT_TRUE(calculations->spline_right_to_left_x4 == 10);
// 	ASSERT_TRUE(calculations->spline_right_to_left_y4 == 22);
//
// 	ASSERT_TRUE(calculations->right_to_left_narrow_x1 == 20);
// 	ASSERT_TRUE(calculations->right_to_left_narrow_y1 == 0);
// 	ASSERT_TRUE(calculations->right_to_left_narrow_x2 == 10);
// 	ASSERT_TRUE(calculations->right_to_left_narrow_y2 == 22);
//
// 	ASSERT_TRUE(calculations->commit_message_offset == 0);
// }
//
// TEST_F(TreeViewModelTest, Update_the_tree) // NOLINT
// {
// 	Verify_update_the_tree();
// }
//
// TEST_F(TreeViewModelTest, Keyboard_shortcut_was_pressed_no_CTRL) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_tree, Get_tree_from_string()).Times(0);
//
// 	m_tree_view_model->Keyboard_shortcut_was_pressed(false, 'N');
// }
//
// TEST_F(TreeViewModelTest, Keyboard_shortcut_was_pressed_R) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_tree, Get_tree_from_string()).Times(1);
//
// 	unsigned int clear_the_tree_counter = 0;
// 	m_tree_view_model->clear_the_tree.subscribe([&]()
// 	{
// 		clear_the_tree_counter++;
// 	});
//
// 	std::vector<bool> tree_enablement_res;
// 	m_tree_view_model->tree_enablement.subscribe([&](const bool should_enable)
// 	{
// 		tree_enablement_res.emplace_back(should_enable);
// 	});
//
// 	m_tree_view_model->Keyboard_shortcut_was_pressed(true, 'R');
//
// 	ASSERT_TRUE(clear_the_tree_counter == 1);
//
// 	ASSERT_TRUE(tree_enablement_res.size() == 2);
// 	ASSERT_TRUE(tree_enablement_res[0] == false);
// 	ASSERT_TRUE(tree_enablement_res[1] == true);
// }
