#include "gtest/gtest.h"
#include "gmock/gmock.h"
//#include "Tree.h"
#include <fstream>
#include <windows.h>
#include "TerminatorColorizer.h"

using namespace testing;


class TerminatorColorizerTest : public Test
{
protected:
	TerminatorColorizerTest()
	{
		m_terminator_colorizer = std::make_shared<TerminatorColorizer>();
	}

	~TerminatorColorizerTest() = default;

	std::shared_ptr<TerminatorColorizer> m_terminator_colorizer;
};

TEST_F(TerminatorColorizerTest, ctor_test) // NOLINT
{
}

TEST_F(TerminatorColorizerTest, Colorize_test) // NOLINT
{
	std::vector<std::string> test_data;
	test_data.emplace_back("some line");
	test_data.emplace_back("another line");
	test_data.emplace_back("and yet another line");
	test_data.emplace_back(R"(C:\some\path\goes\here (master))");
	test_data.emplace_back("\n");
	test_data.emplace_back("> ");

	std::vector<COLOR_STRING> res = m_terminator_colorizer->Colorize(test_data);

	ASSERT_TRUE(res.size() == test_data.size() + 1);

	for (unsigned int i = 0; i < test_data.size() - 3; ++i)
	{
		ASSERT_TRUE(res[i].data == test_data[i]);
		ASSERT_TRUE(res[i].color.red == m_terminator_colorizer->Get_normal_terminal_color().red);
		ASSERT_TRUE(res[i].color.green == m_terminator_colorizer->Get_normal_terminal_color().green);
		ASSERT_TRUE(res[i].color.blue == m_terminator_colorizer->Get_normal_terminal_color().blue);
	}

	ASSERT_TRUE(res[res.size() - 4].data == std::string("C:\\some\\path\\goes\\here "));
	ASSERT_TRUE(res[res.size() - 4].color.red == m_terminator_colorizer->Get_green_terminal_color().red);
	ASSERT_TRUE(res[res.size() - 4].color.green == m_terminator_colorizer->Get_green_terminal_color().green);
	ASSERT_TRUE(res[res.size() - 4].color.blue == m_terminator_colorizer->Get_green_terminal_color().blue);

	ASSERT_TRUE(res[res.size() - 3].data == std::string("(master)"));
	ASSERT_TRUE(res[res.size() - 3].color.red == m_terminator_colorizer->Get_white_terminal_color().red);
	ASSERT_TRUE(res[res.size() - 3].color.green == m_terminator_colorizer->Get_white_terminal_color().green);
	ASSERT_TRUE(res[res.size() - 3].color.blue == m_terminator_colorizer->Get_white_terminal_color().blue);

	ASSERT_TRUE(res[res.size() - 2].data == std::string("\n"));
	ASSERT_TRUE(res[res.size() - 2].color.red == m_terminator_colorizer->Get_normal_terminal_color().red);
	ASSERT_TRUE(res[res.size() - 2].color.green == m_terminator_colorizer->Get_normal_terminal_color().green);
	ASSERT_TRUE(res[res.size() - 2].color.blue == m_terminator_colorizer->Get_normal_terminal_color().blue);

	ASSERT_TRUE(res[res.size() - 1].data == std::string("> "));
	ASSERT_TRUE(res[res.size() - 1].color.red == m_terminator_colorizer->Get_normal_terminal_color().red);
	ASSERT_TRUE(res[res.size() - 1].color.green == m_terminator_colorizer->Get_normal_terminal_color().green);
	ASSERT_TRUE(res[res.size() - 1].color.blue == m_terminator_colorizer->Get_normal_terminal_color().blue);
}

TEST_F(TerminatorColorizerTest, Colorize_single_diff_test) // NOLINT
{
	std::vector<std::string> test_data = std::vector<std::string>{
		"diff --git a / unit_tester / Model_Test.cpp b / unit_tester / Model_Test.cpp",
		"index d8d3435..06ade26 100644",
		"--- a / unit_tester / Model_Test.cpp",
		"+++ b / unit_tester / Model_Test.cpp",
		"@@ - 56, 3 + 56, 8 @@ TEST_F(ModelTest, Execute_terminal_command) // NOLINT",
		"ASSERT_TRUE(res[0].color.green == 2);",
		"ASSERT_TRUE(res[0].color.blue == 3);",
		"}",
		"+",
		"+TEST_F(ModelTest, some_test) // NOLINT",
		"+ {",
		"+auto res = m_model->Get_diff(\"some_path\");",
		"+}",
		"-Some line that got deleted"
	};

	std::vector<COLOR_STRING> res = m_terminator_colorizer->Colorize_single_diff(test_data);

	ASSERT_TRUE(res.size() == 14);

	ASSERT_TRUE(res[0].color.red == m_terminator_colorizer->Get_diff_header_color().red);
	ASSERT_TRUE(res[0].color.green == m_terminator_colorizer->Get_diff_header_color().green);
	ASSERT_TRUE(res[0].color.blue == m_terminator_colorizer->Get_diff_header_color().blue);
	ASSERT_TRUE(res[0].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
	ASSERT_TRUE(res[0].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
	ASSERT_TRUE(res[0].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);

	ASSERT_TRUE(res[1].color.red == m_terminator_colorizer->Get_diff_header_color().red);
	ASSERT_TRUE(res[1].color.green == m_terminator_colorizer->Get_diff_header_color().green);
	ASSERT_TRUE(res[1].color.blue == m_terminator_colorizer->Get_diff_header_color().blue);
	ASSERT_TRUE(res[1].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
	ASSERT_TRUE(res[1].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
	ASSERT_TRUE(res[1].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);

	ASSERT_TRUE(res[2].color.red == m_terminator_colorizer->Get_diff_header_color().red);
	ASSERT_TRUE(res[2].color.green == m_terminator_colorizer->Get_diff_header_color().green);
	ASSERT_TRUE(res[2].color.blue == m_terminator_colorizer->Get_diff_header_color().blue);
	ASSERT_TRUE(res[2].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
	ASSERT_TRUE(res[2].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
	ASSERT_TRUE(res[2].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);

	ASSERT_TRUE(res[3].color.red == m_terminator_colorizer->Get_diff_header_color().red);
	ASSERT_TRUE(res[3].color.green == m_terminator_colorizer->Get_diff_header_color().green);
	ASSERT_TRUE(res[3].color.blue == m_terminator_colorizer->Get_diff_header_color().blue);
	ASSERT_TRUE(res[3].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
	ASSERT_TRUE(res[3].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
	ASSERT_TRUE(res[3].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);

	ASSERT_TRUE(res[4].color.red == m_terminator_colorizer->Get_diff_turquoise_color().red);
	ASSERT_TRUE(res[4].color.green == m_terminator_colorizer->Get_diff_turquoise_color().green);
	ASSERT_TRUE(res[4].color.blue == m_terminator_colorizer->Get_diff_turquoise_color().blue);
	ASSERT_TRUE(res[4].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
	ASSERT_TRUE(res[4].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
	ASSERT_TRUE(res[4].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);

	ASSERT_TRUE(res[5].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
	ASSERT_TRUE(res[5].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
	ASSERT_TRUE(res[5].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
	ASSERT_TRUE(res[5].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
	ASSERT_TRUE(res[5].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
	ASSERT_TRUE(res[5].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);

	ASSERT_TRUE(res[6].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
	ASSERT_TRUE(res[6].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
	ASSERT_TRUE(res[6].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
	ASSERT_TRUE(res[6].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
	ASSERT_TRUE(res[6].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
	ASSERT_TRUE(res[6].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);

	ASSERT_TRUE(res[7].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
	ASSERT_TRUE(res[7].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
	ASSERT_TRUE(res[7].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
	ASSERT_TRUE(res[7].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
	ASSERT_TRUE(res[7].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
	ASSERT_TRUE(res[7].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);

	ASSERT_TRUE(res[8].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
	ASSERT_TRUE(res[8].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
	ASSERT_TRUE(res[8].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
	ASSERT_TRUE(res[8].background_color.red == m_terminator_colorizer->Get_diff_background_green_color().red);
	ASSERT_TRUE(res[8].background_color.green == m_terminator_colorizer->Get_diff_background_green_color().green);
	ASSERT_TRUE(res[8].background_color.blue == m_terminator_colorizer->Get_diff_background_green_color().blue);

	ASSERT_TRUE(res[9].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
	ASSERT_TRUE(res[9].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
	ASSERT_TRUE(res[9].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
	ASSERT_TRUE(res[9].background_color.red == m_terminator_colorizer->Get_diff_background_green_color().red);
	ASSERT_TRUE(res[9].background_color.green == m_terminator_colorizer->Get_diff_background_green_color().green);
	ASSERT_TRUE(res[9].background_color.blue == m_terminator_colorizer->Get_diff_background_green_color().blue);

	ASSERT_TRUE(res[10].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
	ASSERT_TRUE(res[10].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
	ASSERT_TRUE(res[10].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
	ASSERT_TRUE(res[10].background_color.red == m_terminator_colorizer->Get_diff_background_green_color().red);
	ASSERT_TRUE(res[10].background_color.green == m_terminator_colorizer->Get_diff_background_green_color().green);
	ASSERT_TRUE(res[10].background_color.blue == m_terminator_colorizer->Get_diff_background_green_color().blue);

	ASSERT_TRUE(res[11].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
	ASSERT_TRUE(res[11].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
	ASSERT_TRUE(res[11].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
	ASSERT_TRUE(res[11].background_color.red == m_terminator_colorizer->Get_diff_background_green_color().red);
	ASSERT_TRUE(res[11].background_color.green == m_terminator_colorizer->Get_diff_background_green_color().green);
	ASSERT_TRUE(res[11].background_color.blue == m_terminator_colorizer->Get_diff_background_green_color().blue);

	ASSERT_TRUE(res[12].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
	ASSERT_TRUE(res[12].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
	ASSERT_TRUE(res[12].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
	ASSERT_TRUE(res[12].background_color.red == m_terminator_colorizer->Get_diff_background_green_color().red);
	ASSERT_TRUE(res[12].background_color.green == m_terminator_colorizer->Get_diff_background_green_color().green);
	ASSERT_TRUE(res[12].background_color.blue == m_terminator_colorizer->Get_diff_background_green_color().blue);

	ASSERT_TRUE(res[13].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
	ASSERT_TRUE(res[13].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
	ASSERT_TRUE(res[13].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
	ASSERT_TRUE(res[13].background_color.red == m_terminator_colorizer->Get_diff_background_red_color().red);
	ASSERT_TRUE(res[13].background_color.green == m_terminator_colorizer->Get_diff_background_red_color().green);
	ASSERT_TRUE(res[13].background_color.blue == m_terminator_colorizer->Get_diff_background_red_color().blue);
}

TEST_F(TerminatorColorizerTest, Get_diff_background_green_color_test) // NOLINT
{
	const COLOR res = m_terminator_colorizer->Get_diff_background_green_color();

	ASSERT_TRUE(res.red == 200);
	ASSERT_TRUE(res.green == 255);
	ASSERT_TRUE(res.blue == 200);
}

TEST_F(TerminatorColorizerTest, Get_diff_background_normal_color_test) // NOLINT
{
	const COLOR res = m_terminator_colorizer->Get_diff_background_normal_color();

	ASSERT_TRUE(res.red == 255);
	ASSERT_TRUE(res.green == 255);
	ASSERT_TRUE(res.blue == 255);
}

TEST_F(TerminatorColorizerTest, Get_diff_background_red_color_test) // NOLINT
{
	const COLOR res = m_terminator_colorizer->Get_diff_background_red_color();

	ASSERT_TRUE(res.red == 255);
	ASSERT_TRUE(res.green == 200);
	ASSERT_TRUE(res.blue == 200);
}

TEST_F(TerminatorColorizerTest, Get_diff_header_color_test) // NOLINT
{
	const COLOR res = m_terminator_colorizer->Get_diff_header_color();

	ASSERT_TRUE(res.red == 0);
	ASSERT_TRUE(res.green == 0);
	ASSERT_TRUE(res.blue == 0);
}

TEST_F(TerminatorColorizerTest, Get_diff_normal_color_test) // NOLINT
{
	const COLOR res = m_terminator_colorizer->Get_diff_normal_color();

	ASSERT_TRUE(res.red == 120);
	ASSERT_TRUE(res.green == 120);
	ASSERT_TRUE(res.blue == 120);
}

TEST_F(TerminatorColorizerTest, Get_white_terminal_color_test) // NOLINT
{
	const COLOR res = m_terminator_colorizer->Get_white_terminal_color();

	ASSERT_TRUE(res.red == 255);
	ASSERT_TRUE(res.green == 255);
	ASSERT_TRUE(res.blue == 255);
}

TEST_F(TerminatorColorizerTest, Get_diff_turquoise_color_test) // NOLINT
{
	const COLOR res = m_terminator_colorizer->Get_diff_turquoise_color();

	ASSERT_TRUE(res.red == 40);
	ASSERT_TRUE(res.green == 154);
	ASSERT_TRUE(res.blue == 220);
}
