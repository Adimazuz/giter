// #include "gtest/gtest.h"
// #include "gmock/gmock.h"
// #include "ViewModel.h"
// #include "Pipe.h"
// #include "Model_mocks.h"
// #include "TerminatorViewModel.h"
// #include "TreeViewModel.h"
// #include "StatuserViewModel.h"
// #include "ViewModel_Mocks.h"
//
// using namespace testing;
//
//
// class ViewModelTest : public Test
// {
// protected:
// 	ViewModelTest()
// 	{
// 		m_terminator_colorizer = std::make_shared<TerminatorColorizer>();
// 		m_pipe = std::make_shared<Pipe>();
// 		std::filesystem::path path_to_repo = std::filesystem::current_path().parent_path();
// 		m_mock_lib_giter = std::make_shared<NiceMock<MockLibGiter>>(path_to_repo);
// 		m_mock_terminator = std::make_shared<NiceMock<MockTerminator>>(m_mock_lib_giter.get(), m_terminator_colorizer.get());
//
// 		m_completer = std::make_shared<Completer>(m_mock_terminator.get(), m_mock_lib_giter.get());
// 		m_historer = std::make_shared<Historer>();
// 		m_mock_terminator_view_model = std::make_shared<NiceMock<MockTerminatorViewModel>>();
//
// 		m_mock_tree = std::make_shared<NiceMock<MockTree>>(m_mock_lib_giter.get(), m_mock_terminator.get());
// 		m_mock_tree_view_model = std::make_shared<NiceMock<MockTreeViewModel>>();
//
// 		m_mock_statuser_view_model = std::make_shared<NiceMock<MockStatuserViewModel>>();
// 		m_view_model = std::make_shared<ViewModel>(m_mock_terminator_view_model.get(), m_mock_tree_view_model.get(), m_mock_statuser_view_model.get());
// 	}
//
// 	std::shared_ptr<TerminatorColorizer> m_terminator_colorizer;
// 	std::shared_ptr<Pipe> m_pipe;
// 	std::shared_ptr<NiceMock<MockLibGiter>> m_mock_lib_giter;
// 	std::shared_ptr<NiceMock<MockTerminator>> m_mock_terminator;
//
// 	std::shared_ptr<Completer> m_completer;
// 	std::shared_ptr<Historer> m_historer;
// 	//std::shared_ptr<ITerminatorViewModel> m_terminator_view_model;
// 	std::shared_ptr<NiceMock<MockTerminatorViewModel>> m_mock_terminator_view_model;
//
// 	std::shared_ptr<NiceMock<MockTree>> m_mock_tree;
// 	std::shared_ptr<NiceMock<MockTreeViewModel>> m_mock_tree_view_model;
//
// 	//std::shared_ptr<IStatuserViewModel> m_statuser_view_model;
// 	std::shared_ptr<NiceMock<MockStatuserViewModel>> m_mock_statuser_view_model;
//
// 	std::shared_ptr<ViewModel> m_view_model;
// };
//
// TEST_F(ViewModelTest, ctor_test) // NOLINT
// {
// }
//
// TEST_F(ViewModelTest, Get_terminator_view_model) // NOLINT
// {
// 	const auto res = m_view_model->Get_terminator_view_model();
// 	ASSERT_TRUE(res == m_mock_terminator_view_model.get());
// }
//
// TEST_F(ViewModelTest, Get_tree_view_model) // NOLINT
// {
// 	const auto res = m_view_model->Get_tree_view_model();
// 	ASSERT_TRUE(res == m_mock_tree_view_model.get());
// }
//
// TEST_F(ViewModelTest, Get_statuser_view_model) // NOLINT
// {
// 	const auto res = m_view_model->Get_statuser_view_model();
// 	ASSERT_TRUE(res == m_mock_statuser_view_model.get());
// }
//
// // TEST_F(ViewModelTest, Keyboard_shortcut_was_pressed_no_CTRL) // NOLINT
// // {
// // 	// bool res = m_view_model->Keyboard_shortcut_was_pressed(false, 'R');
// // 	// ASSERT_TRUE(res == true);
// // 	//
// // 	// res = m_view_model->Keyboard_shortcut_was_pressed(false, 'S');
// // 	// ASSERT_TRUE(res == true);
// // 	//
// // 	// res = m_view_model->Keyboard_shortcut_was_pressed(false, 'G');
// // 	// ASSERT_TRUE(res == true);
// // 	//
// // 	// // Some random letter goes here.
// // 	// res = m_view_model->Keyboard_shortcut_was_pressed(false, 'N');
// // 	// ASSERT_TRUE(res == true);
// // }
// //
// // TEST_F(ViewModelTest, Keyboard_shortcut_was_pressed_yes_CTRL) // NOLINT
// // {
// // 	// bool res = m_view_model->Keyboard_shortcut_was_pressed(true, 'R');
// // 	// ASSERT_TRUE(res == false);
// // 	//
// // 	// res = m_view_model->Keyboard_shortcut_was_pressed(true, 'S');
// // 	// ASSERT_TRUE(res == true);
// // 	//
// // 	// res = m_view_model->Keyboard_shortcut_was_pressed(true, 'G');
// // 	// ASSERT_TRUE(res == true);
// // 	//
// // 	// // Some random letter goes here.
// // 	// res = m_view_model->Keyboard_shortcut_was_pressed(true, 'N');
// // 	// ASSERT_TRUE(res == true);
// // }
//
// TEST_F(ViewModelTest, Keyboard_shortcut_was_pressed) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_statuser_view_model, Keyboard_shortcut_was_pressed(true, 42)).Times(1);
// 	EXPECT_CALL(*m_mock_terminator_view_model, Keyboard_shortcut_was_pressed(true, 42)).Times(1);
// 	EXPECT_CALL(*m_mock_tree_view_model, Keyboard_shortcut_was_pressed(true, 42)).Times(1);
//
// 	m_view_model->Keyboard_shortcut_was_pressed(true, 42);
// }
