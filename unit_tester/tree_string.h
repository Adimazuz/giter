#pragma once
#include <string>
#pragma warning( push )
#pragma warning( disable : 4129)
// ReSharper disable StringLiteralTypo
const std::string TREE_STRING =
	"* 2afaf03a340937816026fc347c45ddb98bd679dd Finish with bitology\n"
	"* 8e388f284ef65eb57fcebe7889c0f7cd4822fae9 Prepare bitology skeleton\n"
	"* f32a9c8580a3d1c950ac9b59da22a554b062356d WIP\n"
	"* c6e51d000e7d7063146bd6d5370dfc7d8cd63c1a Make the new General family of functinos\n"
	"* d89269c2ff66099acab0258ec05074b3fc33636b Make read and write behave logically correct\n"
	"* 6fe3212c21140a7dbc51beb7688c2e6c898998a1 Fix event hooking\n"
	"* 5d5ab5b878b74620757fe7f8798880c50379d652 Rename a method\n"
	"* 8b7e8d9c4291799ac60862e97c05832911ee870d Hook the events\n"
	"* ec6dc2343d52a6177fea9306d1e9fd59083c0725 Add the new radio boxes\n"
	"* f336f4b7ab69fe0b34ccd2d25c67a2bc3eed995c Prepare MVVM skeleton for JtagDialog\n"
	"*   b680b86340f49999cf063db4f0a3d6c2da98f359 Merge branch 'feature-msr1'\n"
	"|\\ \n"
	"| * 99ba42caad8fa7f2428b5cdbd69dc840cfea2ae2 Add the 0x16 IR write for OPen Drain Mode.\n"
	"| * 1768279129c238080df3079a4da5d31f4a1b7ea6 Turn off MTAP functionality, always, for MSR1 (by feature)\n"
	"| * 33a05fd9c128378f343e1afa0c2daa5e8419d5a8 Add a check for no MTAP in MSR1\n"
	"| * a2fe2e30a8b2aa827fcbdfc483c7f757aee46c91 Fix: Fix: Add new project: MSR1\n"
	"| * 118527fff01728e91982e0a6f7884161f62e4423 Fix: Add new project: MSR1\n"
	"| * 2827f9bdb411706de0c56b6060242cc03c42e0db Add new project: MSR1\n"
	"| | * 91d1dc979af4e3b716def11513b1689e89545fb5 Add MSR3 MTAP device ID to TLDataBase\n"
	"| | * 689b91460c31e5bdfba5127bd89777e6226eac79 Add MSR3 MTAP device ID\n"
	"| | * 8427d0e95e3e29e4c7d8b8d9b300e84ef1cc973e WIP adding the class\n"
	"| |/  \n"
	"|/|   \n"
	"* | ba5deff5c4f5730938bb7613c062f495c4effb4a Fix release build\n"
	"* | ed4e44c575642c9d1d975e4e3850cb7ed3379fcb Kockwork\n"
	"* | 976141cb7f846031e1fed55fa82ea1bba15767b5 Kockwork\n"
	"* |   1844dfd9311a3c227864ab6dd4b4b83965251bc3 Merge branch 'feature-update-ftdi-driver'\n"
	"|\\ \\  \n"
	"| * | e70f7058c4f14aa98ceb54ed8aedf46b5622d776 Update FTDI drivers to latest version\n"
	"| * | a7d7579e92ee2783b53599d80919f7b46110ff45 Revert \"Remove 32 bits from FTDI driver\"\n"
	"| * | 1c5dace4801c0c6a1399bd0848bbb3bb09df18c0 Revert \"Delete 32 bits FTDI driver\"\n"
	"| * | dbba9aeb7087daadfac25e3dffe3f2654f8980f4 Revert \"Update FTDI driver to latest version\"\n"
	"| * | 5e08c8ffc78451d48996ad611824fd46946a2be5 Remove 32 bits from FTDI driver\n"
	"| * | fd5aef103a19219157660e0108b2d74adea972f6 Delete 32 bits FTDI driver\n"
	"| * | 1215ba14b4de363bb1a42ce3e7eb4c3ed10c8173 Update FTDI driver to latest version\n"
	"* | | 98828fc63690545d3f004df57f1c685bf08e135a Improve logging in unknonw device case.\n"
	"* | | 0d76141aedac4974a49b6d3c36e7d4260b6b1409 Improve logging in unknonw device case.\n"
	"| | | * 6f5cad87bdbdd7ed38e95d9ba5a018850bb167f5 Update pictures\n"
	"| | | * 115b4cf8c190cf08abee56bfb7d443ce71371926 Update pictures\n"
	"| | | * f2367ff975870f9b23aef7793569344642b57eaa Change the icon an all sub forms\n"
	"| | | * e4095fb75184e9b48bb1464c9783fed589fb7790 Update the icon of the about form.\n"
	"| | | * 7e10841d13f0e232ef7fb8204b7a2ee75c100259 Revert some changes related to SDK\n"
	"| | | * c2e94fd1ee3bd3ecee335ea2197e98566b1dfd60 More tenlira --> tdt changes\n"
	"| | | * 46a19eac4185f6d3bfd9010b1533aab19dee8dcc Update the docs with TDT instaed of TenLira\n"
	"| | | * be66c7b7b6de1df468f17bbb7bdbd9466503d0fb Update main exe icon\n"
	"| | | * 54b9b098151bdb7d1293a345b20b6f88fb0772f7 Update icon of main GUI\n"
	"| | | * 741f019ba73efa1d221202ec07121f3f5e11f934 Try and change the icon of RunCMDAsAdministrator.exe\n"
	"| | | * 4d040a0c8d3e078970064379ab6584aedda028a9 Some last minute update to the main icon\n"
	"| | | *   05e5c0a8de77f52d6414001685c5711d568ae1c9 Merge branch 'feature-name-change' into feature-images-names-all-new\n"
	"| | | |\\  \n"
	"| | | | * 0445f1cd0103a57a6aaa7456f92a76209bb9be83 Find some more instances of tenlira string\n"
	"| | | | * 9c3b33a4b44ca1c9d42fa01c9ed508d87b64cef9 CTRL+ALT+L a unit test\n"
	"| | | | * 83c1b8b24ed9c9e94c54a7194f3b672d4d9df5cd Fix rebase\n"
	"| | | | * 62ce3c31756bc2e89aecf29128c25835cf587f4f WIP after rebase\n"
	"| | | | * 53b6a331a8b63fd2dbb7e58155388edc4e90cf83 WIP after rebase\n"
	"| | | | * 97a1a7c356468d96c751c193009c5c08bd8fbe80 WIP after rebase\n"
	"| | | | * ea016b921f48d8604b1df8e744f69d830d3e5878 WIP\n"
	"| | | | * edd626398ec7ad15deec369914cfbad0683d9d1a WIP\n"
	"| | | | * e15fbd7a6b48f54b78875f9b2a6689723c07f66d WIP\n"
	"| | | | * 7df903e25271c3b5382724786c36f83ff8b1153e WIP\n"
	"| | | | * 84c80d136e237d94c2ef741503b549ea6014824e WIP\n"
	"| | | | * 47d459a289761f764cde70ca8667da59f9c941d5 WIP\n"
	"| | | | * 6738fcda1ee4ade2b2a31ee8d981f65fa2c7e209 WIP\n"
	"| | | | * be7be0779732ea95c4cf9af72b578ab89851c5da WIP\n"
	"| | | | * 2e6cc1f66cc9597d69bb0b68fd898b6cc2d695b0 WIP\n"
	"| | | | * f782e5bbf53b27bfa1c242869d41ad734ddcadd3 WIP\n"
	"| | | | * 8d04a580650477d5517ceee7aedebeed2f8f5fcf WIP\n"
	"| | | | * 834015a1491f3ba09a2b827c4f3a7e5ed1f6314b WIP\n"
	"| | | | * 3b31126ef4f8ecf25645f65e023b6b54e09e4e60 WIP\n"
	"| | | | * ccaef5db3a53754257e0376f6ca6442ea34d6402 WIP\n"
	"| | | | * f8d004cd8e3a1827970523631503b7e5cf7b099c WIP\n"
	"| | | | * 89199035e101f85e4006437669cb83d0021a9d37 WIP\n"
	"| | | | * 851a3e1bc5d80f3cd42583a2e3729ea79fe6e6d7 WIP\n"
	"| | | | * 853baeceea3843a8b26bd424c458886d586e0f56 WIP\n"
	"| | | | * e5cd876eaf5f8e04fca5091f3d8ba05774bbbe75 WIP\n"
	"| | | | * 52277e8828ceb4f95056732b16e46557da67e452 WIP\n"
	"| |_|_|/  \n"
	"|/| | |   \n"
	"| | | *   d3b5fe65c2ddb8a6cc071d49dde612a59ec5719e Merge branch 'feature-new-images' into feature-images-names-all-new\n"
	"| | | |\\  \n"
	"| |_|/ /  \n"
	"|/| | |   \n"
	"| | | * e5ceff3d21a15f64242640b32506e205bbf121e0 Change the main icon to be the blue one\n"
	"| | | * 8fe22c7c155ae6ff818efd457346ad3846729c78 Change tenlira.exe icon\n"
	"| | | * 683bb17225ba72b014e5bba0bf3860bc32f16036 Change the main icon in the installer also\n"
	"| | | * 3cf05ffafdd9381cb72a8ce19fb6ed4142bf965c Change the main icon\n"
	"| | | * 7ad7ff85d32eb63efb2aae0247e79cb7a41575a9 Update all icons\n"
	"| | | * fdaf6f54187e9d99796e0a77a890ad5feec838c7 Update some more icons\n"
	"| | | * 9b6690a115a980b7ce98118ccc295af4c37e1df6 Add many more icons\n"
	"| | | * 1de181a00eef9b7fe9cb7f5ce37554cd7c38cef5 Resharper for a bit to make TC happy\n"
	"| | | * 10c8395f3b699c7d6d550179a77004ad1430a740 Remove dead icons for proj file\n"
	"| | | * 1ac1e7480ddbe9eb785fe10ca6f9c7b71d52e3ff Update the tools icon\n"
	"| | | * b4cc1920657ba6ae585fc976f13776f547d38eec Update some more icons\n"
	"| | | | * ddce992a37cd2f4e23510b9edeafb655cfe0b4df Fixed issue with driver installation on both YFL's\n"
	"| | | | * be057f09368bd39717e3789c536c3d1f60f65e25 changes for compiling\n"
	"| | | | | * 21bae2735b11b84eb0a5520669ed1f2c2b71ca12 Add some more missing registers\n"
	"| | | | | * f66eed6663470cd772f745a83d1b06411d0277fc Add POC reset register to TBTDevice_GR class\n"
	"| |_|_|_|/  \n"
	"|/| | | |   \n"
	"* | | | | 29c99f8be885b216a3b83abcb3a421c89f0b74cb Kockwork\n"
	"* | | | | c3139667d8f8617ae244b07e03f35522b5ee3554 Kockwork\n"
	"* | | | | 693938b0955a591da41cfd08934b12a64a914360 Kockwork\n"
	"* | | | | 7e312243ed655fff8a57de7cc726ead88ce48717 Kockwork\n"
	"* | | | | 286fff721ce4dc7907bc1aa014d6fdd76e505207 Kockwork\n"
	"* | | | | 6af0a4524e9af68161c3424af98f08da6a225920 Kockwork\n"
	"* | | | | cec423b76b2d778d7d55db17bcc35a3c1414a360 Kockwork\n"
	"* | | | | 29a6b0e47d43f9848b88776660a2a95bdda1661d Kockwork\n"
	"* | | | | b160dd0e610ee97a134c647554f4f3556f7703fa Kockwork\n"
	"| |_|/ /  \n"
	"|/| | |   \n"
	"|\n"
	"*   607443446bb08a137a5aa44d2ae75cc6fce9a123 Merge branch 'feature-dp-support-for-gr'\n"
	"|\\  \n"
	"| * 7cebdaab8941e755c2075e03e01508e43e6ac06f Reformat the code\n"
	"| * 6589a592ad07f264a3c230e8bf4a01ab5ffbda36 Make all the needed changes for DP support in GR\n"
	"| * d48cc77902b1b7cd7d430421317b45b00afffa44 Add some more important code comments\n"
	"| * e789f3911f9ac8980e12466a7d83f1426a87db14 Add important code remark\n"
	"| * b8497c4b509ed26a7107901c784d6cfe64f61ca0 WIP\n"
	"|/  \n"
	"* 2803dc4f5de2979add66f2cd4926579d591b3851 Update the README\n"
	"* 36fb2870dc0ac2059fd07dcf05ac53c5a43034dc Fix named arguments\n"
	"|\\  \n"
	"| * 817ade9f3443ac7d1c99aea4e70c15f7c5fb9e69 1st WIP\n"
	"* |   2e99a8427ee2db2344db415ef535872408671157 Merge branch 'master' of ssh://gitlab.devtools.intel.com:29418/rsheink/TenLira\n"
	"|\\ \\  \n"
	"| * \\   f90f1af3cabbcb31e591ed8e59c280b86f1e7032 Merge branch 'mfg-tools-add-YFL-to-test' into 'master'\n"
	"| |\\ \\  \n"
	"|/ / /  \n"
	"| * | 8091d39c316f6f33c400b1af07a6425bb9617af9 when testing YFL , work with driver ready instead of CM pass through mode\n"
	"| |/  \n"
	"| | * a4e740011db00a845706394555ee3679a5ef371e Change to non unicode some more\n"
	"| | * 178d92e6c6c7597b94ffe7187b9b5fd72fc613d3 Change to non unicode\n"
	"| |/  \n"
	"|/|   \n"
	"* | 5b9ad4adeeab44616ed0a2ee2d44cb6a93fd2fad Resharper for a bit\n"
	"|/  \n"
	"* f76834e3f83e07b770a352a35f4359ab06cbbc13 Change the winblows sdk thingy to be more general\n"
	"| * 9beef30a4c76b594ad47de5f717f1c0b7bb32655 Change the winblows sdk thingy to be more general\n"
	"|/  \n"
	"* 1002ad67684c5ef4e9095beede5a08bc7b151f5e Update gtest\n"
	"| * e98c39c5c9b6635d9649a659a304a0e2830e29a2 WIP\n"
	"|/  \n"
	"| * 3d217893c72323907190ff39bd6870d85966f89b fix last rebase issue\n"
	"|/  \n"
	"| *   b1c376cb071708148ec979c773ba11f7946a28ee Merge branch 'feature-make-kmgl-wrapper-sdl-compliant' of ssh://gitlab.devtools.intel.com:29418/rsheink/TenLira into feature-make-kmgl-wrapper-sdl-compliant\n"
	"| |\\  \n"
	"| | *   f638cf75212ef3e4438c165c8c9de1aa2be884fd Merge branch 'feature-make-kmgl-wrapper-sdl-compliant' of ssh://gitlab.devtools.intel.com:29418/rsheink/TenLira into feature-make-kmgl-wrapper-sdl-compliant\n"
	"| | |\\  \n"
	"| | |/  \n"
	"| |/|   \n"
	"| * | 5e3fab681c49653297c8b0e1118ae6c1975f1729 WIP\n"
	"| * |   8d432e0d154f66606643352ad257b5106c01967b Merge branch 'feature-make-kmgl-wrapper-sdl-compliant' of ssh://gitlab.devtools.intel.com:29418/rsheink/TenLira into feature-make-kmgl-wrapper-sdl-compliant\n"
	"| |\\ \\  \n"
	"| * \\ \\   4306a303aba07ab11a816a86eece0fdd809666af Merge branch 'feature-make-kmgl-wrapper-sdl-compliant' of ssh://gitlab.devtools.intel.com:29418/rsheink/TenLira into feature-make-kmgl-wrapper-sdl-compliant\n"
	"| |\\ \\ \\  \n"
	"| * \\ \\ \\   88c27fa85ae66341b6ce2d3d152adb5a76ad432b Merge branch 'feature-make-kmgl-wrapper-sdl-compliant' of ssh://gitlab.devtools.intel.com:29418/rsheink/TenLira into feature-make-kmgl-wrapper-sdl-compliant\n"
	"| |\\ \\ \\ \\  \n"
	"| | | | | * 2635835e6015cfaea96925a780e05e823abcc441 WIP\n"
	"| | | | |/  \n"
	"| | | | * 0b0d2790f7175f712a21a659a48732aabad6d093 a\n"
	"| | | |/  \n"
	"| | | * 88f2d591736b831d979ddecd740d6aa985f2b115 a\n"
	"| | |/  \n"
	"| | * 9868684fb6c07030a3e7a7595412f23a9cda4564 a\n"
	"| |/  \n"
	"| * cb0f770d5d641669b5259338ffbca9cb9e5a4a50 a\n"
	"| | * d23171cd3144a39a2868d2ba13b3f1758aec14c0 a\n"
	"| |/  \n"
	"|/|   \n"
	"* | 1d00c4924ef587a05641075b0fe867eab653eea5 a\n"
	"|/  \n"
	"* aa15c393866d32f370776dc19eef6a21ce36ab55 a\n"
	"|\\  \n"
	"| *   c29ad43faea89fa2ea356e5f23a0183d471926cb Merge branch 'feature-devcon' into 'master'\n"
	"| |\\  \n"
	"|/ /  \n"
	"| | *   786a7605976d26af63431345a8971c4bf0547962 Merge branch 'feature-devcon' of ssh://gitlab.devtools.intel.com:29418/rsheink/TenLira into feature-devcon\n"
	"| | |\\  \n"
	"| | |/  \n"
	"| |/|   \n"
	"| * |   2323483710fae0d18f31c604127aa040cc4378ba Merge branch 'master' into 'feature-devcon'\n"
	"| |\\ \\  \n"
	"| |/ /  \n"
	"|/| /   \n"
	"| |/    \n"
	"* |   5bbee1b491f98ba4d6a09664852b054f3ccc93c8 Merge branch 'feature-devcon'\n"
	"|/\n"
	"* dce4f41cc6792eba2070c7d61cc7aba2c5ace9a1 Comment the uninstall kmgl thingy\n"
	"| \n"
	"| \n"
	"|  \n"
	"| | \n"
	"* | dce4f41cc6792eba2070c7d61cc7aba2ckdje9a1 Comment the uninstall kmgl thingy\n"
	"|\\ \\ \n"
	"| * | dck4j41cc6792eba2070c7d61cc7aba2c5ace9a1 Comment thesd uninstall kmgl thingy\n"
	"|/ /  \n"
	"* | dce4pw9cc6792eba2070c7d61cc7aba2c5ace9a1 Comment the uninstafdll kmgl thingy\n"
	"|/\n"
	"* ls74pw9cc6792eba2070c7d61cc7aba2c5aceld7 Comment the uninstall kmgl thingy\n";
// ReSharper restore StringLiteralTypo
#pragma warning(pop)
