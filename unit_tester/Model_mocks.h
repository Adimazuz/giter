#pragma once
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "ITerminator.h"
#include <windows.h>
#include "IPipe.h"


// class MockTerminator : public ITerminator
// {
// public:
// 	explicit MockTerminator(ILibGiter* lib_giter, TerminatorColorizer* colorizer)
// 	{
// 	}
//
// 	~MockTerminator() = default;
//
// 	MOCK_CONST_METHOD1(Execute_terminator_command, std::vector<COLOR_STRING>(const std::wstring& data));
// 	MOCK_CONST_METHOD1(Execute_shell_command, std::vector<std::string>(const std::wstring& data));
// 	MOCK_CONST_METHOD0(Get_string_tree, std::string());
// };

// class MockTree : public ITree
// {
// public:
// 	explicit MockTree(ILibGiter* lib_giter, ITerminator* terminator)
// 	{
// 	}
//
// 	~MockTree() = default;
//
// 	// ReSharper disable CppOverridingFunctionWithoutOverrideSpecifier
// 	MOCK_CONST_METHOD1(Get_sha1, std::string(std::string line));
// 	MOCK_METHOD0(Refresh_the_tree, void());
// 	// ReSharper restore CppOverridingFunctionWithoutOverrideSpecifier
// };

// class MockLowLevelGitApi : public ILowLevelGitApi
// {
// public:
// 	explicit MockLowLevelGitApi() = default;
// 	~MockLowLevelGitApi() = default;
//
// 	// ReSharper disable CppOverridingFunctionWithoutOverrideSpecifier
// 	MOCK_CONST_METHOD3(Git_reference_lookup, int(git_reference** out, git_repository* repo, const char* name));
// 	MOCK_CONST_METHOD1(Git_reference_type, int(const git_reference* ref));
// 	MOCK_CONST_METHOD2(Git_oid_fmt, void(char* out, const git_oid* id));
// 	MOCK_CONST_METHOD1(Git_reference_target, const git_oid*(const git_reference* ref));
// 	MOCK_CONST_METHOD1(Git_reference_is_tag, int(const git_reference* ref));
// 	MOCK_CONST_METHOD1(Git_reference_shorthand, const char* (const git_reference* ref));
// 	MOCK_CONST_METHOD1(Git_reference_is_remote, int(const git_reference* ref));
// 	MOCK_CONST_METHOD1(Git_reference_is_branch, int(const git_reference* ref));
// 	MOCK_CONST_METHOD1(Git_reference_free, void(git_reference* ref));
// 	MOCK_METHOD2(Get_commit_message, std::string(const std::string& sha1, git_repository* repo));
// 	MOCK_METHOD0(Git_libgit2_init, int());
// 	MOCK_METHOD2(Git_repository_open, int(git_repository** out, const char* path));
// 	MOCK_METHOD1(Git_repository_free, void(git_repository* repo));
// 	MOCK_METHOD0(Git_libgit2_shutdown, int());
// 	MOCK_METHOD2(Git_reference_list, int(git_strarray *array, git_repository *repo));
// 	MOCK_METHOD1(Git_strarray_free, void(git_strarray *array));
// 	MOCK_METHOD2(Git_oid_fromstr, int(git_oid* out, const char* str));
// 	MOCK_METHOD3(Git_commit_lookup, int(git_commit** commit, git_repository* repo, git_oid* id));
// 	MOCK_METHOD1(Git_commit_parentcount, unsigned int(git_commit* commit));
// 	MOCK_METHOD3(Git_commit_parent, int(git_commit** out, const git_commit* commit, unsigned n));
// 	MOCK_METHOD1(Git_commit_id, const git_oid* (git_commit* commit));
// 	MOCK_METHOD3(Git_oid_tostr, char* (char* out, size_t n, const git_oid* id));
// 	MOCK_METHOD3(Git_status_list_new, int(git_status_list** out, git_repository* repo, git_status_options* opts));
// 	MOCK_METHOD1(Git_status_list_entrycount, size_t(git_status_list* status_list));
// 	MOCK_METHOD2(Git_status_byindex, const git_status_entry* (git_status_list* status_list, size_t idx));
// 	MOCK_METHOD1(Git_commit_time, git_time_t(const git_commit *commit));
// 	MOCK_METHOD1(Git_status_list_free, void(git_status_list* status_list));
// 	// ReSharper restore CppOverridingFunctionWithoutOverrideSpecifier
// };
//
// class MockLibGiter : public ILibGiter
// {
// public:
// 	explicit MockLibGiter(std::filesystem::path& repo_path)
// 	{
// 	}
//
// 	~MockLibGiter() = default;
//
// 	// ReSharper disable CppOverridingFunctionWithoutOverrideSpecifier
// 	MOCK_METHOD0(Update_all_refs, void());
// 	MOCK_METHOD1(Get_local_refs, std::vector<std::string>(const std::string& sha1));
// 	MOCK_METHOD1(Get_remote_refs, std::vector<std::string>(const std::string& sha1));
// 	MOCK_METHOD1(Get_tag_refs, std::vector<std::string>(const std::string& sha1));
// 	MOCK_METHOD0(Get_all_local_refs, std::vector<std::string>());
// 	MOCK_METHOD1(Get_number_of_parents, unsigned int(const std::string& sha1));
// 	MOCK_CONST_METHOD1(Get_commit_message, std::string(const std::string& sha1));
// 	MOCK_CONST_METHOD0(Get_current_branch, std::string());
// 	MOCK_CONST_METHOD0(Get_staged, std::vector<std::pair<std::string, std::string>>());
// 	MOCK_CONST_METHOD0(Get_unstaged, std::vector<std::pair<std::string, std::string>>());
// 	MOCK_CONST_METHOD0(Get_untracked, std::vector<std::pair<std::string, std::string>>());
// 	MOCK_CONST_METHOD1(Get_date, std::string(const std::string sha1));
// 	// ReSharper restore CppOverridingFunctionWithoutOverrideSpecifier
// };
//
// class MockPipe : public IPipe
// {
// public:
// 	explicit MockPipe()
// 	{
// 	}
//
// 	~MockPipe() = default;
//
// 	MOCK_CONST_METHOD4(Create_pipe, BOOL(PHANDLE h_read_pipe, PHANDLE h_write_pipe, LPSECURITY_ATTRIBUTES lp_pipe_attributes, DWORD n_size));
// 	MOCK_CONST_METHOD10(
// 		Create_process,
// 		BOOL(LPCWSTR lp_application_name, LPWSTR lp_command_line, LPSECURITY_ATTRIBUTES lp_process_attributes, LPSECURITY_ATTRIBUTES lp_thread_attributes,BOOL b_inherit_handles,
// 			DWORD dw_creation_flags, LPVOID lp_environment, LPCWSTR lp_current_directory, LPSTARTUPINFOW lp_startup_info, LPPROCESS_INFORMATION lp_process_information));
// 	MOCK_CONST_METHOD1(Close_handle, BOOL(HANDLE h_object));
// 	MOCK_CONST_METHOD2(Wait_for_single_object, DWORD(HANDLE h_handle, DWORD dw_milliseconds));
// 	MOCK_CONST_METHOD6(
// 		Peek_named_pipe, BOOL(HANDLE h_named_pipe, LPVOID lp_buffer, DWORD n_buffer_size, LPDWORD lp_bytes_read, LPDWORD lp_total_bytes_avail, LPDWORD lp_bytes_left_this_message));
// 	MOCK_CONST_METHOD5(Read_file, BOOL(HANDLE h_file, LPVOID lp_buffer, DWORD n_number_of_bytes_to_read, LPDWORD lp_number_of_bytes_read, LPOVERLAPPED lp_overlapped));
// };
