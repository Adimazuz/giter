//#include "gtest/gtest.h"
//#include "gmock/gmock.h"
//#include "Tree.h"
//#include <fstream>
//#include "Model_mocks.h"
//#include <windows.h>
//
//
//using namespace testing;
//
//class LineTest : public Test
//{
//protected:
//	LineTest()
//	{
//		std::filesystem::path path_to_repo = std::filesystem::current_path().parent_path();
//		m_mock_lib_giter = std::make_shared<NiceMock<MockLibGiter>>(path_to_repo);
//
//		ON_CALL(*m_mock_lib_giter, Get_commit_message(StrEq("sha0"))).WillByDefault(Return("commit_message0"));
//		ON_CALL(*m_mock_lib_giter, Get_commit_message(StrEq("sha1"))).WillByDefault(Return("commit_message1"));
//
//		ON_CALL(*m_mock_lib_giter, Get_local_refs(StrEq("sha0"))).WillByDefault(Return(std::vector<std::string>{"local_ref"}));
//		ON_CALL(*m_mock_lib_giter, Get_remote_refs(StrEq("sha1"))).WillByDefault(Return(std::vector<std::string>{"remote_ref"}));
//		ON_CALL(*m_mock_lib_giter, Get_tag_refs(StrEq("sha1"))).WillByDefault(Return(std::vector<std::string>{"tag_ref"}));
//
//		ON_CALL(*m_mock_lib_giter, Get_date("sha1")).WillByDefault(Return("some_date"));
//
//		m_line_empty = std::make_shared<Line>(std::vector<Node>{}, m_mock_lib_giter.get(), 42);
//		m_line_with_single_commit_only = std::make_shared<Line>(std::vector<Node>{Node(0, "sha1", 2, COMMIT, false)}, m_mock_lib_giter.get(), 0);
//		m_line_with_single_line_only = std::make_shared<Line>(std::vector<Node>{Node(6, "", 5, LINE, false)}, m_mock_lib_giter.get(), 0);
//		m_line_with_multiple_various_nodes = std::make_shared<Line>(std::vector<Node>{
//			                                                            Node(2, "", 4, RIGHT_TO_LEFT, false),
//			                                                            Node(0, "sha0", 0, COMMIT, true),
//			                                                            Node(2, "", 1, LINE, false)
//		                                                            }, m_mock_lib_giter.get(), 0);
//	}
//
//	bool Compare(Node lhs, Node rhs)
//	{
//		if ( //this->m_color == rhs.m_color &&
//			lhs.Get_color() == rhs.Get_color() &&
//			//this->m_sha1 == rhs.m_sha1 &&
//			lhs.Get_sha1() == rhs.Get_sha1() &&
//			//this->m_type == rhs.m_type &&
//			lhs.Get_type() == rhs.Get_type() &&
//			//this->m_is_tip == rhs.m_is_tip &&
//			lhs.Is_tip() == rhs.Is_tip() &&
//			//this->m_column == rhs.m_column
//			lhs.Get_column() == rhs.Get_column())
//		{
//			return true;
//		}
//	
//		return false;
//	}
//
//	~LineTest() = default;
//
//	std::shared_ptr<NiceMock<MockLibGiter>> m_mock_lib_giter;
//
//	std::shared_ptr<Line> m_line_empty;
//	std::shared_ptr<Line> m_line_with_single_line_only;
//	std::shared_ptr<Line> m_line_with_single_commit_only;
//	std::shared_ptr<Line> m_line_with_multiple_various_nodes;
//};
//
//TEST_F(LineTest, ctor_test) // NOLINT
//{
//}
//
//TEST_F(LineTest, the_equals_operator_self_assignment) // NOLINT
//{
//	// ReSharper disable once CppIdenticalOperandsInBinaryExpression
//	const Line res = *m_line_empty = *m_line_empty;
//
//	ASSERT_TRUE(res.Get_commit_message() == m_line_empty->Get_commit_message());
//	ASSERT_TRUE(res.Get_sha1() == m_line_empty->Get_sha1());
//	ASSERT_TRUE(res.Has_at_least_one_ref() == m_line_empty->Has_at_least_one_ref());
//	ASSERT_TRUE(res.Get_is_first_line() == m_line_empty->Get_is_first_line());
//	// !!! Rewrite this line in Rust. !!!
//	//ASSERT_TRUE(res.Get_nodes() == m_line_empty->Get_nodes());
//}
//
//TEST_F(LineTest, the_equals_operator) // NOLINT
//{
//	*m_line_empty = *m_line_with_multiple_various_nodes;
//
//	ASSERT_TRUE(m_line_empty->Get_commit_message() == m_line_with_multiple_various_nodes->Get_commit_message());
//	ASSERT_TRUE(m_line_empty->Get_sha1() == m_line_with_multiple_various_nodes->Get_sha1());
//	ASSERT_TRUE(m_line_empty->Has_at_least_one_ref() == m_line_with_multiple_various_nodes->Has_at_least_one_ref());
//	ASSERT_TRUE(m_line_empty->Get_is_first_line() == m_line_with_multiple_various_nodes->Get_is_first_line());
//	// !!! Rewrite this line in Rust. !!!
//	//ASSERT_TRUE(m_line_empty->Get_nodes() == m_line_with_multiple_various_nodes->Get_nodes());
//}
//
//TEST_F(LineTest, Get_sha1_empy) // NOLINT
//{
//	const std::string res = m_line_empty->Get_sha1();
//
//	ASSERT_TRUE(res.empty() == true);
//}
//
//TEST_F(LineTest, Get_sha1) // NOLINT
//{
//	const std::string res = m_line_with_multiple_various_nodes->Get_sha1();
//
//	ASSERT_TRUE(res == "sha0");
//}
//
//TEST_F(LineTest, Get_local_refs) // NOLINT
//{
//	const std::vector<std::string> res = m_line_with_multiple_various_nodes->Get_local_refs();
//
//	ASSERT_TRUE(res.size() == 1);
//	ASSERT_TRUE(res[0] == "local_ref");
//}
//
//TEST_F(LineTest, Get_remote_refs) // NOLINT
//{
//	const std::vector<std::string> res = m_line_with_single_commit_only->Get_remote_refs();
//
//	ASSERT_TRUE(res.size() == 1);
//	ASSERT_TRUE(res[0] == "remote_ref");
//}
//
//TEST_F(LineTest, Get_tag_refs) // NOLINT
//{
//	const std::vector<std::string> res = m_line_with_single_commit_only->Get_tag_refs();
//
//	ASSERT_TRUE(res.size() == 1);
//	ASSERT_TRUE(res[0] == "tag_ref");
//}
//
//TEST_F(LineTest, Has_any_ref_commit_positive) // NOLINT
//{
//	const bool res = m_line_with_multiple_various_nodes->Has_at_least_one_ref();
//
//	ASSERT_TRUE(res == true);
//}
//
//TEST_F(LineTest, Has_any_ref_commit_negative) // NOLINT
//{
//	const bool res = m_line_with_single_line_only->Has_at_least_one_ref();
//
//	ASSERT_TRUE(res == false);
//}
//
//TEST_F(LineTest, Get_commit_message_no_commit_node_in_line) // NOLINT
//{
//	const std::string res = m_line_with_single_line_only->Get_commit_message();
//
//	ASSERT_TRUE(res.empty() == true);
//}
//
//TEST_F(LineTest, Get_commit_message) // NOLINT
//{
//	const std::string res = m_line_with_multiple_various_nodes->Get_commit_message();
//
//	ASSERT_TRUE(res == "commit_message0");
//}
//
//TEST_F(LineTest, Has_at_least_one_ref_positive) // NOLINT
//{
//	const bool res = m_line_with_single_commit_only->Has_at_least_one_ref();
//
//	ASSERT_TRUE(res == true);
//}
//
//TEST_F(LineTest, Has_at_least_one_ref_negative) // NOLINT
//{
//	const bool res = m_line_with_single_line_only->Has_at_least_one_ref();
//
//	ASSERT_TRUE(res == false);
//}
//
//TEST_F(LineTest, Get_is_first_line_positive) // NOLINT
//{
//	const bool res = m_line_with_single_commit_only->Get_is_first_line();
//
//	ASSERT_TRUE(res == true);
//}
//
//TEST_F(LineTest, Get_is_first_line_negative) // NOLINT
//{
//	const bool res = m_line_empty->Get_is_first_line();
//
//	ASSERT_TRUE(res == false);
//}
//
//TEST_F(LineTest, Get_nodes) // NOLINT
//{
//	const std::vector<Node> res = m_line_with_multiple_various_nodes->Get_nodes();
//
//	ASSERT_TRUE(res.size() == 3);
//
//	 //ASSERT_TRUE(res[0] == Node(2, "", 4, RIGHT_TO_LEFT, false));
//	//Node(2, "", 4, RIGHT_TO_LEFT, false);
//	//ASSERT_TRUE(res[0].Get_column() == 2);
//	//ASSERT_TRUE(res[0].Get_color() == 2);
//	//ASSERT_TRUE(res[0].Get_type() == RIGHT_TO_LEFT);
//	//ASSERT_TRUE(res[0].Is_tip() == false);
//	// ASSERT_TRUE(res[1] == Node(0, "sha0", 0, COMMIT, true));
//	// ASSERT_TRUE(res[2] == Node(2, "", 1, LINE, false));
//
//	ASSERT_TRUE(Compare(res[0], Node(2, "", 4, RIGHT_TO_LEFT, false)));
//	ASSERT_TRUE(Compare(res[1], Node(0, "sha0", 0, COMMIT, true)) == true);
//	ASSERT_TRUE(Compare(res[2] , Node(2, "", 1, LINE, false)) == true);
//}
//
//TEST_F(LineTest, Get_date_empty) // NOLINT
//{
//	const std::string res = m_line_with_single_line_only->Get_date();
//
//	ASSERT_TRUE(res.empty() == true);
//}
//
//TEST_F(LineTest, Get_date) // NOLINT
//{
//	const std::string res = m_line_with_single_commit_only->Get_date();
//
//	ASSERT_TRUE(res == "some_date");
//}
