// #include "gtest/gtest.h"
// #include "gmock/gmock.h"
// #include "Tree.h"
// #include "LowLevelGitApi.h"
// #include <fstream>
// #include "Model_mocks.h"
// #include "tree_string.h"
// #include <windows.h>
//
// using namespace testing;
//
//
// class TreeTest : public Test
// {
// protected:
// 	TreeTest()
// 	{
// 		std::filesystem::path path_to_repo = std::filesystem::current_path().parent_path();
// 		m_mock_lib_giter = std::make_shared<NiceMock<MockLibGiter>>(path_to_repo);
//
// 		m_terminator_colorizer = std::make_shared<NiceMock<TerminatorColorizer>>();
// 		m_mock_terminator = std::make_shared<NiceMock<MockTerminator>>(m_mock_lib_giter.get(), m_terminator_colorizer.get());
//
// 		m_tree = std::make_shared<Tree>(m_mock_lib_giter.get(), m_mock_terminator.get());
//
// 		m_res = std::make_shared<std::vector<Line>>();
// 		m_tree->new_line.subscribe([this](std::shared_ptr<Line> line)
// 		{
// 			m_res->emplace_back(*line);
// 		});
//
// 		ON_CALL(*m_mock_terminator, Get_string_tree).WillByDefault(Return(TREE_STRING));
// 	}
//
// 	~TreeTest() = default;
//
// 	std::shared_ptr<NiceMock<MockLibGiter>> m_mock_lib_giter;
// 	std::shared_ptr<NiceMock<TerminatorColorizer>> m_terminator_colorizer;
// 	std::shared_ptr<NiceMock<MockTerminator>> m_mock_terminator;
//
// 	std::shared_ptr<Tree> m_tree;
//
// 	std::shared_ptr<std::vector<Line>> m_res;
// };
//
// TEST_F(TreeTest, Get_tree_from_string_observable_test) // NOLINT
// {
// 	bool at_least_once_not_100 = false;
// 	bool at_least_once_100 = false;
// 	m_tree->tree_getting.subscribe([&](const int current, const int total, const double percentage)
// 	{
// 		if (percentage < 100)
// 		{
// 			at_least_once_not_100 = true;
// 		}
//
// 		if (percentage == 100)
// 		{
// 			at_least_once_100 = true;
// 		}
// 	});
//
// 	m_tree->Get_tree_from_string();
//
// 	ASSERT_TRUE(at_least_once_not_100 == true);
// 	ASSERT_TRUE(at_least_once_100 == true);
// }
//
// TEST_F(TreeTest, Get_tree_from_string_size) // NOLINT
// {
// 	m_tree->Get_tree_from_string();
//
// 	ASSERT_TRUE(m_res->size() == 199);
// }
//
// TEST_F(TreeTest, Get_tree_from_string_is_first_line) // NOLINT
// {
// 	m_tree->Get_tree_from_string();
//
// 	for (int i = 0; i < m_res->size() - 1; ++i)
// 	{
// 		ASSERT_TRUE(m_res->at(0).Get_is_first_line() == false);
// 	}
//
// 	ASSERT_TRUE(m_res->back().Get_is_first_line() == true);
// }
//
// TEST_F(TreeTest, Get_tree_from_string_is_tip) // NOLINT
// {
// 	m_tree->Get_tree_from_string();
//
// 	ASSERT_TRUE(m_res->at(0).Get_nodes()[0].Is_tip() == true);
// 	ASSERT_TRUE(m_res->at(18).Get_nodes()[2].Is_tip() == true);
// 	ASSERT_TRUE(m_res->at(37).Get_nodes()[3].Is_tip() == true);
// 	ASSERT_TRUE(m_res->at(93).Get_nodes()[4].Is_tip() == true);
// 	ASSERT_TRUE(m_res->at(95).Get_nodes()[5].Is_tip() == true);
// 	ASSERT_TRUE(m_res->at(130).Get_nodes()[2].Is_tip() == true);
// 	ASSERT_TRUE(m_res->at(137).Get_nodes()[1].Is_tip() == true);
// 	ASSERT_TRUE(m_res->at(140).Get_nodes()[1].Is_tip() == true);
// 	ASSERT_TRUE(m_res->at(142).Get_nodes()[1].Is_tip() == true);
// 	ASSERT_TRUE(m_res->at(144).Get_nodes()[1].Is_tip() == true);
// 	ASSERT_TRUE(m_res->at(166).Get_nodes()[2].Is_tip() == true);
// 	ASSERT_TRUE(m_res->at(176).Get_nodes()[2].Is_tip() == true);
//
// 	for (unsigned int i = 0; i < static_cast<unsigned int>(m_res->size()); ++i)
// 	{
// 		if (i == 0 || i == 18 || i == 37 || i == 93 || i == 95 || i == 130 || i == 137 || i == 140 || i == 142 || i == 144 || i == 166 || i == 176)
// 		{
// 			continue;
// 		}
//
// 		for (const auto& node : m_res->at(i).Get_nodes())
// 		{
// 			ASSERT_TRUE(node.Is_tip() == false);
// 		}
// 	}
// }
//
// TEST_F(TreeTest, Get_tree_from_string_commit_was_detected) // NOLINT
// {
// 	ON_CALL(*m_mock_lib_giter, Get_commit_message(StrEq("8e388f284ef65eb57fcebe7889c0f7cd4822fae9"))).WillByDefault(Return("some_commit_message"));
// 	ON_CALL(*m_mock_lib_giter, Get_local_refs(StrEq("8e388f284ef65eb57fcebe7889c0f7cd4822fae9"))).WillByDefault(Return(std::vector<std::string>{"local_ref"}));
// 	ON_CALL(*m_mock_lib_giter, Get_remote_refs(StrEq("8e388f284ef65eb57fcebe7889c0f7cd4822fae9"))).WillByDefault(Return(std::vector<std::string>{"remote_ref"}));
//
// 	m_tree->Get_tree_from_string();
//
// 	ASSERT_TRUE(m_res->at(1).Get_commit_message() == "some_commit_message");
// 	ASSERT_TRUE(m_res->at(1).Get_sha1() == "8e388f284ef65eb57fcebe7889c0f7cd4822fae9");
// 	ASSERT_TRUE(m_res->at(1).Get_is_first_line() == false);
// 	// ASSERT_TRUE(m_res->at(1).Get_local_refs() == "local_ref ");
// 	// ASSERT_TRUE(m_res->at(1).Get_remote_refs() == "remote_ref ");
// 	ASSERT_TRUE(m_res->at(1).Get_nodes().size() == 1);
// 	ASSERT_TRUE(m_res->at(1).Get_local_refs() == std::vector<std::string>{"local_ref"});
// 	ASSERT_TRUE(m_res->at(1).Get_remote_refs() == std::vector<std::string>{"remote_ref"});
// 	ASSERT_TRUE(m_res->at(1).Get_nodes()[0].Get_sha1() == "8e388f284ef65eb57fcebe7889c0f7cd4822fae9");
// 	ASSERT_TRUE(m_res->at(1).Get_nodes()[0].Get_color() == 0);
// 	ASSERT_TRUE(m_res->at(1).Get_nodes()[0].Get_column() == 1);
// 	ASSERT_TRUE(m_res->at(1).Get_nodes()[0].Get_type() == COMMIT);
// 	ASSERT_TRUE(m_res->at(1).Get_nodes()[0].Is_tip() == false);
// }
//
// TEST_F(TreeTest, Get_tree_from_string_merge_test) // NOLINT
// {
// 	m_tree->Get_tree_from_string();
//
// 	// Merge in general
// 	ASSERT_TRUE(m_res->at(10).Get_nodes()[0].Get_type() == COMMIT);
// 	ASSERT_TRUE(m_res->at(11).Get_nodes()[0].Get_type() == LINE);
// 	ASSERT_TRUE(m_res->at(11).Get_nodes()[1].Get_type() == LEFT_TO_RIGHT);
//
// 	// Merge colors
// 	const unsigned int merge_commit_color = m_res->at(10).Get_nodes()[0].Get_color();
// 	const unsigned int below_commit_color = m_res->at(11).Get_nodes()[0].Get_color();
// 	const unsigned int below_and_to_the_right_color = m_res->at(11).Get_nodes()[1].Get_color();
// 	ASSERT_TRUE(merge_commit_color != below_commit_color &&
// 		below_commit_color != below_and_to_the_right_color &&
// 		merge_commit_color != below_and_to_the_right_color);
// 	const unsigned int below_below_commit_color = m_res->at(12).Get_nodes()[0].Get_color();
// 	const unsigned int below_below_and_to_the_right_color = m_res->at(12).Get_nodes()[1].Get_color();
// 	ASSERT_TRUE(below_commit_color == below_below_commit_color);
// 	ASSERT_TRUE(below_and_to_the_right_color == below_below_and_to_the_right_color);
// 	ASSERT_TRUE(below_below_commit_color != below_below_and_to_the_right_color);
// }
//
// TEST_F(TreeTest, Get_tree_from_string_criss_cross_test) // NOLINT
// {
// 	m_tree->Get_tree_from_string();
//
// 	ASSERT_TRUE(m_res->at(21).Get_nodes()[1].Get_type() == LINE);
// 	ASSERT_TRUE(m_res->at(21).Get_nodes()[2].Get_type() == RIGHT_TO_LEFT);
// 	ASSERT_TRUE(m_res->at(22).Get_nodes()[1].Get_type() == RIGHT_TO_LEFT);
// 	ASSERT_TRUE(m_res->at(22).Get_nodes()[2].Get_type() == LINE);
// 	ASSERT_TRUE(m_res->at(21).Get_nodes()[1].Get_color() == m_res->at(22).Get_nodes()[2].Get_color());
// 	ASSERT_TRUE(m_res->at(21).Get_nodes()[2].Get_color() == m_res->at(22).Get_nodes()[1].Get_color());
// }
//
// TEST_F(TreeTest, Get_tree_from_string_right_to_left_narrow) // NOLINT
// {
// 	m_tree->Get_tree_from_string();
//
// 	ASSERT_TRUE(m_res->at(182).Get_nodes()[3].Get_type() == RIGHT_TO_LEFT);
// 	ASSERT_TRUE(m_res->at(183).Get_nodes()[3].Get_type() == RIGHT_TO_LEFT_NARROW);
// 	ASSERT_TRUE(m_res->at(184).Get_nodes()[2].Get_type() == RIGHT_TO_LEFT_NARROW);
// }
//
// TEST_F(TreeTest, Get_tree_from_string_left_to_right_narrow) // NOLINT
// {
// 	m_tree->Get_tree_from_string();
//
// 	ASSERT_TRUE(m_res->at(152).Get_nodes()[3].Get_type() == LEFT_TO_RIGHT_NARROW);
// 	ASSERT_TRUE(m_res->at(153).Get_nodes()[3].Get_type() == LEFT_TO_RIGHT_NARROW);
// 	ASSERT_TRUE(m_res->at(154).Get_nodes()[4].Get_type() == LEFT_TO_RIGHT_NARROW);
// 	ASSERT_TRUE(m_res->at(155).Get_nodes()[4].Get_type() == LEFT_TO_RIGHT_NARROW);
// 	ASSERT_TRUE(m_res->at(156).Get_nodes()[5].Get_type() == LEFT_TO_RIGHT);
// }
//
// TEST_F(TreeTest, Get_tree_from_string_horizonatl_on_the_right_end) // NOLINT
// {
// 	m_tree->Get_tree_from_string();
//
// 	// Structure.
// 	ASSERT_TRUE(m_res->at(75).Get_nodes()[4].Get_type() == COMMIT);
// 	ASSERT_TRUE(m_res->at(76).Get_nodes()[6].Get_type() == RIGHT_TO_LEFT);
// 	ASSERT_TRUE(m_res->at(76).Get_nodes()[4].Get_type() == HORIZONTAL);
// 	ASSERT_TRUE(m_res->at(76).Get_nodes()[2].Get_type() == HORIZONTAL);
// 	ASSERT_TRUE(m_res->at(77).Get_nodes()[1].Get_type() == RIGHT_TO_LEFT);
// 	ASSERT_TRUE(m_res->at(78).Get_nodes()[0].Get_type() == LINE);
//
// 	// Color.
// 	const unsigned int start_color = m_res->at(74).Get_nodes()[4].Get_color();
// 	ASSERT_TRUE(m_res->at(76).Get_nodes()[6].Get_color() == start_color);
// 	ASSERT_TRUE(m_res->at(76).Get_nodes()[4].Get_color() == start_color);
// 	ASSERT_TRUE(m_res->at(76).Get_nodes()[2].Get_color() == start_color);
// 	ASSERT_TRUE(m_res->at(77).Get_nodes()[1].Get_color() == start_color);
// 	ASSERT_TRUE(m_res->at(78).Get_nodes()[0].Get_color() != start_color);
// }
//
// TEST_F(TreeTest, Get_tree_from_string_horizonatl_in_the_middle) // NOLINT
// {
// 	m_tree->Get_tree_from_string();
//
// 	// Structure.
// 	ASSERT_TRUE(m_res->at(107).Get_nodes()[3].Get_type() == LINE);
// 	ASSERT_TRUE(m_res->at(108).Get_nodes()[4].Get_type() == RIGHT_TO_LEFT);
// 	ASSERT_TRUE(m_res->at(108).Get_nodes()[2].Get_type() == HORIZONTAL);
// 	ASSERT_TRUE(m_res->at(109).Get_nodes()[1].Get_type() == RIGHT_TO_LEFT);
// 	ASSERT_TRUE(m_res->at(110).Get_nodes()[0].Get_type() == LINE);
//
// 	// Color.
// 	const unsigned int start_color = m_res->at(107).Get_nodes()[3].Get_color();
// 	ASSERT_TRUE(m_res->at(108).Get_nodes()[4].Get_color() == start_color);
// 	ASSERT_TRUE(m_res->at(108).Get_nodes()[2].Get_color() == start_color);
// 	ASSERT_TRUE(m_res->at(109).Get_nodes()[1].Get_color() == start_color);
// 	ASSERT_TRUE(m_res->at(110).Get_nodes()[0].Get_color() != start_color);
// }
//
// TEST_F(TreeTest, Get_tree_from_string_split_with_commit) // NOLINT
// {
// 	m_tree->Get_tree_from_string();
//
// 	// Structure.
// 	ASSERT_TRUE(m_res->at(20).Get_nodes()[2].Get_type() == COMMIT);
// 	ASSERT_TRUE(m_res->at(21).Get_nodes()[1].Get_type() == LINE);
// 	ASSERT_TRUE(m_res->at(21).Get_nodes()[2].Get_type() == RIGHT_TO_LEFT);
//
// 	// Color.
// 	const unsigned int color = m_res->at(20).Get_nodes()[2].Get_color();
// 	ASSERT_TRUE(m_res->at(21).Get_nodes()[1].Get_color() != color);
// 	ASSERT_TRUE(m_res->at(21).Get_nodes()[2].Get_color() == color);
// }
//
// TEST_F(TreeTest, Get_tree_from_string_blablabla) // NOLINT
// {
// 	m_tree->Get_tree_from_string();
//
// 	ASSERT_TRUE(m_res->at(196).Get_nodes()[1].Get_type() == LINE);
// 	ASSERT_TRUE(m_res->at(195).Get_nodes()[2].Get_type() == RIGHT_TO_LEFT);
// 	ASSERT_TRUE(m_res->at(194).Get_nodes()[2].Get_type() == LINE);
//
// 	const auto color = m_res->at(196).Get_nodes()[1].Get_color();
//
// 	ASSERT_TRUE(m_res->at(196).Get_nodes()[1].Get_color() == color);
// 	ASSERT_TRUE(m_res->at(195).Get_nodes()[2].Get_color() == color);
// 	ASSERT_TRUE(m_res->at(194).Get_nodes()[2].Get_color() == color);
// }
