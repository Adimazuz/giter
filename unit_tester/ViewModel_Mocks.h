#pragma once
#include "gtest/gtest.h"
#include "gmock/gmock.h"


// class MockTerminatorViewModel : public ITerminatorViewModel
// {
// public:
// 	explicit MockTerminatorViewModel() = default;
//
// 	~MockTerminatorViewModel() = default;
//
// 	MOCK_CONST_METHOD1(Execute_terminator_command, std::vector<COLOR_STRING>(const std::wstring& data));
// 	MOCK_CONST_METHOD1(Execute_shell_command, std::vector<std::string>(const std::wstring& data));
// 	MOCK_CONST_METHOD0(Get_string_tree, std::string());
//
// 	// ReSharper disable CppOverridingFunctionWithoutOverrideSpecifier
// 	MOCK_METHOD1(Press_enter, void(std::string input_line_text));
// 	MOCK_METHOD1(Append_to_input_line_and_press_enter, void(const std::string data_to_append));
// 	MOCK_METHOD1(Append_illegal_to_input_line_and_press_enter, void(const std::string& data_to_append));
// 	MOCK_METHOD0(Set_focus_to_the_input_line, void());
// 	MOCK_METHOD1(Append_data_to_terminator, void(std::string data_to_append));
// 	MOCK_METHOD0(Go_earlier_in_history, void());
// 	MOCK_METHOD0(Go_later_in_history, void());
// 	MOCK_METHOD0(Get_normal_terminator_color, COLOR());
// 	MOCK_METHOD2(Keyboard_shortcut_was_pressed, void(const bool was_ctrl_pressed, const unsigned key_code));
// 	// ReSharper restore CppOverridingFunctionWithoutOverrideSpecifier
// };

// class MockStatuserViewModel : public IStatuserViewModel
// {
// public:
// 	explicit MockStatuserViewModel() = default;
//
// 	~MockStatuserViewModel() = default;
//
// 	// ReSharper disable CppOverridingFunctionWithoutOverrideSpecifier
// 	MOCK_METHOD0(Stage_button_was_clicked, void());
// 	MOCK_METHOD1(Reset_button_was_clicked, void(const std::string& file_path));
// 	MOCK_METHOD0(Unstage_button_was_clicked, void());
// 	MOCK_METHOD1(Get_diff, std::vector<COLOR_STRING>(const std::string& file_path));
// 	MOCK_CONST_METHOD0(Update_statuser, void());
// 	MOCK_METHOD1(Unstaged_double_click, void(const std::string& file_path));
// 	MOCK_METHOD1(Staged_double_click, void(const std::string& file_path));
// 	MOCK_METHOD1(Commit_button_was_clicked, void(const std::string& commit_message));
// 	MOCK_METHOD0(Push_button_was_clicked, void());
// 	MOCK_METHOD2(Keyboard_shortcut_was_pressed, void(const bool was_ctrl_pressed, const unsigned key_code));
// 	// ReSharper restore CppOverridingFunctionWithoutOverrideSpecifier
// };

// class MockTreeViewModel : public ITreeViewModel
// {
// public:
// 	explicit MockTreeViewModel() = default;
//
// 	~MockTreeViewModel() = default;
//
// 	// ReSharper disable CppOverridingFunctionWithoutOverrideSpecifier
// 	MOCK_CONST_METHOD6(Calc_drawing_coordinates,
// 	                   void(const Node& node, Line& line, const unsigned int cell_height, const unsigned int cell_y, const unsigned radius, const std::shared_ptr<CALCULATIONS>&
// 		                   calculations));
// 	MOCK_CONST_METHOD0(Update_the_tree, void());
// 	MOCK_METHOD2(Keyboard_shortcut_was_pressed, void(const bool was_ctrl_pressed, const unsigned key_code));
// 	// ReSharper restore CppOverridingFunctionWithoutOverrideSpecifier
// };
