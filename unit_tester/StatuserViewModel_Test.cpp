// #include "gtest/gtest.h"
// #include "gmock/gmock.h"
// #include "LowLevelGitApi.h"
// #include "Pipe.h"
// #include "Model_mocks.h"
// #include "TerminatorViewModel.h"
// #include "StatuserViewModel.h"
// #include "ViewModel_Mocks.h"
//
// using namespace testing;
//
//
// class StatuserViewModelTest : public Test
// {
// protected:
//
// 	std::vector<std::vector<std::pair<std::string, std::string>>> m_res_staged;
// 	std::vector<std::vector<std::pair<std::string, std::string>>> m_res_unstaged;
// 	std::vector<std::vector<std::pair<std::string, std::string>>> m_res_untracked;
//
// 	std::shared_ptr<TerminatorColorizer> m_terminator_colorizer;
// 	std::shared_ptr<Pipe> m_pipe;
// 	std::shared_ptr<NiceMock<MockLibGiter>> m_mock_lib_giter;
// 	std::shared_ptr<NiceMock<MockTerminator>> m_mock_terminator;
//
// 	std::shared_ptr<Completer> m_completer;
// 	std::shared_ptr<Historer> m_historer;
//
// 	std::shared_ptr<NiceMock<MockTree>> m_mock_tree;
//
// 	std::shared_ptr<NiceMock<MockTerminatorViewModel>> m_mock_terminator_view_model;
//
// 	std::shared_ptr<IStatuserViewModel> m_statuser_view_model;
//
// 	StatuserViewModelTest()
// 	{
// 		m_terminator_colorizer = std::make_shared<TerminatorColorizer>();
// 		m_pipe = std::make_shared<Pipe>();
// 		std::filesystem::path path_to_repo = std::filesystem::current_path().parent_path();
// 		m_mock_lib_giter = std::make_shared<NiceMock<MockLibGiter>>(path_to_repo);
// 		m_mock_terminator = std::make_shared<NiceMock<MockTerminator>>(m_mock_lib_giter.get(), m_terminator_colorizer.get());
//
// 		m_completer = std::make_shared<Completer>(m_mock_terminator.get(), m_mock_lib_giter.get());
// 		m_historer = std::make_shared<Historer>();
//
// 		m_mock_tree = std::make_shared<NiceMock<MockTree>>(m_mock_lib_giter.get(), m_mock_terminator.get());
//
// 		m_mock_terminator_view_model = std::make_shared<NiceMock<MockTerminatorViewModel>>();
//
// 		m_statuser_view_model = std::make_shared<StatuserViewModel>(m_mock_lib_giter.get(), m_mock_terminator.get(), m_terminator_colorizer.get(),
// 		                                                            m_mock_terminator_view_model.get());
//
// 		Setup_update_statuser();
// 	}
//
// 	[[nodiscard]] bool Was_update_statuser_called() const
// 	{
// 		if (m_res_staged.empty() == true && m_res_unstaged.empty() == true && m_res_untracked.empty() == true)
// 		{
// 			return false;
// 		}
// 		else
// 		{
// 			return true;
// 		}
// 	}
//
// 	bool Is_update_statuser_valid()
// 	{
// 		if (m_res_staged.size() == 1 &&
// 			m_res_unstaged.size() == 1 &&
// 			m_res_untracked.size() == 1 &&
// 			m_res_staged[0][0].first == "1" &&
// 			m_res_staged[0][0].second == "2" &&
// 			m_res_unstaged[0][0].first == "3" &&
// 			m_res_unstaged[0][0].second == "4" &&
// 			m_res_untracked[0][0].first == "5" &&
// 			m_res_untracked[0][0].second == "6")
// 		{
// 			return true;
// 		}
// 		else
// 		{
// 			return false;
// 		}
// 	}
//
// private:
//
// 	void Setup_update_statuser()
// 	{
// 		ON_CALL(*m_mock_lib_giter, Get_staged()).WillByDefault(Return(std::vector<std::pair<std::string, std::string>>{std::make_pair("1", "2")}));
// 		m_statuser_view_model->staged.subscribe([this](std::vector<std::pair<std::string, std::string>> staged)
// 		{
// 			m_res_staged.emplace_back(staged);
// 		});
//
// 		ON_CALL(*m_mock_lib_giter, Get_unstaged()).WillByDefault(Return(std::vector<std::pair<std::string, std::string>>{std::make_pair("3", "4")}));
// 		m_statuser_view_model->unstaged.subscribe([this](std::vector<std::pair<std::string, std::string>> unstaged)
// 		{
// 			m_res_unstaged.emplace_back(unstaged);
// 		});
//
// 		ON_CALL(*m_mock_lib_giter, Get_untracked()).WillByDefault(Return(std::vector<std::pair<std::string, std::string>>{std::make_pair("5", "6")}));
// 		m_statuser_view_model->untracked.subscribe([this](std::vector<std::pair<std::string, std::string>> untracked)
// 		{
// 			m_res_untracked.emplace_back(untracked);
// 		});
// 	}
// };
//
// TEST_F(StatuserViewModelTest, Stage_button_was_clicked) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_terminator_view_model, Append_to_input_line_and_press_enter("git add .")).Times(1);
//
// 	m_statuser_view_model->Stage_button_was_clicked();
//
// 	ASSERT_TRUE(Is_update_statuser_valid() == true);
// }
//
// TEST_F(StatuserViewModelTest, Unstage_button_was_clicked) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_terminator_view_model, Append_to_input_line_and_press_enter("git reset")).Times(1);
//
// 	m_statuser_view_model->Unstage_button_was_clicked();
//
// 	ASSERT_TRUE(Is_update_statuser_valid() == true);
// }
//
// TEST_F(StatuserViewModelTest, Reset_button_was_clicked) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_terminator_view_model, Append_to_input_line_and_press_enter("git checkout some_file_path")).Times(1);
//
// 	m_statuser_view_model->Reset_button_was_clicked("some_file_path");
//
// 	ASSERT_TRUE(Is_update_statuser_valid() == true);
// }
//
// TEST_F(StatuserViewModelTest, Reset_button_was_clicked_empty) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_terminator_view_model, Append_to_input_line_and_press_enter(_)).Times(0);
//
// 	m_statuser_view_model->Reset_button_was_clicked("");
//
// 	ASSERT_TRUE(Was_update_statuser_called() == false);
// }
//
// TEST_F(StatuserViewModelTest, Get_diff_empty) // NOLINT
// {
// 	const auto res = m_statuser_view_model->Get_diff("");
//
// 	ASSERT_TRUE(res.empty() == true);
// }
//
// TEST_F(StatuserViewModelTest, Get_diff) // NOLINT
// {
// 	ON_CALL(*m_mock_terminator, Execute_shell_command(StrEq(L"git diff some_file_path"))).WillByDefault(Return(std::vector<std::string>{
// 		"Nothing to show dude. It's an untracked or staged file. Move along, nothing to see."
// 	}));
//
// 	auto res = m_statuser_view_model->Get_diff("some_file_path");
//
// 	ASSERT_TRUE(res.size() == 1);
// 	ASSERT_TRUE(res[0].data == "Nothing to show dude. It's an untracked or staged file. Move along, nothing to see.");
// }
//
// TEST_F(StatuserViewModelTest, Get_diff_no_diff) // NOLINT
// {
// 	ON_CALL(*m_mock_terminator, Execute_shell_command(StrEq(L"git diff some_file_path"))).WillByDefault(Return(std::vector<std::string>{}));
//
// 	auto res = m_statuser_view_model->Get_diff("some_file_path");
//
// 	ASSERT_TRUE(res.size() == 1);
// 	ASSERT_TRUE(res[0].data == "Nothing to show dude. It's an untracked or staged file. Move along, nothing to see.");
// }
//
// TEST_F(StatuserViewModelTest, Get_diff_wrong_file_path) // NOLINT
// {
// 	ON_CALL(*m_mock_terminator, Execute_shell_command(StrEq(L"git diff wrong_file_path"))).WillByDefault(Return(std::vector<std::string>{
// 		"unknown revision or path not in the working tree"
// 	}));
//
// 	auto res = m_statuser_view_model->Get_diff("wrong_file_path");
//
// 	ASSERT_TRUE(res.size() == 1);
// 	ASSERT_TRUE(res[0].data == "Nothing to show dude. It's a deleted file. Move along, nothing to see.");
// }
//
// TEST_F(StatuserViewModelTest, Update_statuser) // NOLINT
// {
// 	m_statuser_view_model->Update_statuser();
//
// 	ASSERT_TRUE(Is_update_statuser_valid() == true);
// }
//
// TEST_F(StatuserViewModelTest, Unstaged_double_click) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_terminator_view_model, Append_to_input_line_and_press_enter(StrEq("git add some_file_path")));
//
// 	m_statuser_view_model->Unstaged_double_click("some_file_path");
//
// 	ASSERT_TRUE(Is_update_statuser_valid() == true);
// }
//
// TEST_F(StatuserViewModelTest, Staged_double_click) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_terminator_view_model, Append_to_input_line_and_press_enter(StrEq("git reset HEAD some_file_path")));
//
// 	m_statuser_view_model->Staged_double_click("some_file_path");
//
// 	ASSERT_TRUE(Is_update_statuser_valid() == true);
// }
//
// TEST_F(StatuserViewModelTest, Commit_button_was_clicked) // NOLINT
// {
// 	std::vector<std::string> res_commit_message_set_text;
// 	m_statuser_view_model->commit_message_set_text.subscribe([&](const std::string& data)
// 	{
// 		res_commit_message_set_text.emplace_back(data);
// 	});
//
// 	EXPECT_CALL(*m_mock_terminator_view_model, Append_to_input_line_and_press_enter(StrEq("git commit -m \"some_commit_message\"")));
//
// 	m_statuser_view_model->Commit_button_was_clicked("some_commit_message");
//
// 	ASSERT_TRUE(res_commit_message_set_text.size() == 1);
// 	ASSERT_TRUE(res_commit_message_set_text[0].empty() == true);
//
// 	ASSERT_TRUE(Is_update_statuser_valid() == true);
// }
//
// TEST_F(StatuserViewModelTest, Push_button_was_clicked) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_terminator_view_model, Append_to_input_line_and_press_enter(StrEq("git push")));
//
// 	m_statuser_view_model->Push_button_was_clicked();
//
// 	ASSERT_TRUE(Is_update_statuser_valid() == true);
// }
//
// TEST_F(StatuserViewModelTest, Keyboard_shortcut_was_pressed_no_CTRL) // NOLINT
// {
// 	m_statuser_view_model->Keyboard_shortcut_was_pressed(false, 'N');
//
// 	ASSERT_TRUE(Was_update_statuser_called() == false);
// }
//
// TEST_F(StatuserViewModelTest, Keyboard_shortcut_was_pressed_S) // NOLINT
// {
// 	m_statuser_view_model->Keyboard_shortcut_was_pressed(true, 'S');
//
// 	ASSERT_TRUE(Is_update_statuser_valid() == true);
// }
//
// TEST_F(StatuserViewModelTest, Keyboard_shortcut_was_pressed_R) // NOLINT
// {
// 	m_statuser_view_model->Keyboard_shortcut_was_pressed(true, 'R');
//
// 	ASSERT_TRUE(Is_update_statuser_valid() == true);
// }
