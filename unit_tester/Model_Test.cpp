// #include "gtest/gtest.h"
// #include "gmock/gmock.h"
// #include "LibGiter.h"
// #include "Tree.h"
// #include "Model.h"
// #include "Model_mocks.h"
// #include <windows.h>
//
//
// using namespace testing;
//
// class ModelTest : public testing::Test
// {
// protected:
// 	ModelTest()
// 	{
// 		m_terminator_colorizer = std::make_shared<TerminatorColorizer>();
// 		std::filesystem::path path_to_repo = std::filesystem::current_path().parent_path();
// 		m_mock_lib_giter = std::make_shared<NiceMock<MockLibGiter>>(path_to_repo);
// 		m_mock_terminator = std::make_shared<MockTerminator>(m_mock_lib_giter.get(), m_terminator_colorizer.get());
// 		m_mock_tree = std::make_shared<MockTree>(m_mock_lib_giter.get(), m_mock_terminator);
// 		m_model = std::make_shared<Model>(m_mock_lib_giter.get(), m_mock_tree.get(), m_mock_terminator.get(), m_terminator_colorizer.get());
// 	}
//
// 	std::shared_ptr<TerminatorColorizer> m_terminator_colorizer;
// 	std::shared_ptr<NiceMock<MockLibGiter>> m_mock_lib_giter;
// 	std::shared_ptr<MockTerminator> m_mock_terminator;
// 	std::shared_ptr<MockTree> m_mock_tree;
// 	std::shared_ptr<Model> m_model;
// };
//
// TEST_F(ModelTest, ctor_test) // NOLINT
// {
// 	m_model->talking_to_shell.subscribe([=](const int current, const int total, const double percentage)
// 	{
// 		ASSERT_TRUE(current == 1);
// 		ASSERT_TRUE(total == 2);
// 		ASSERT_TRUE(percentage == 3);
// 	});
// 	m_mock_terminator->talking_to_shell.notify(1, 2, 3);
//
// 	m_model->tree_getting.subscribe([=](const int current, const int total, const double percentage)
// 	{
// 		ASSERT_TRUE(current == 1);
// 		ASSERT_TRUE(total == 2);
// 		ASSERT_TRUE(percentage == 3);
// 	});
// 	m_mock_tree->tree_getting.notify(1, 2, 3);
//
// 	m_model->tree_parsing.subscribe([=](const int current, const int total, const double percentage)
// 	{
// 		ASSERT_TRUE(current == 1);
// 		ASSERT_TRUE(total == 2);
// 		ASSERT_TRUE(percentage == 3);
// 	});
// 	m_mock_tree->tree_parsing.notify(1, 2, 3);
// }
//
// TEST_F(ModelTest, Get_normal_terminal_color_test) // NOLINT
// {
// 	const COLOR res = m_model->Get_normal_terminal_color();
//
// 	ASSERT_TRUE(res.red == m_terminator_colorizer->Get_normal_terminal_color().red);
// 	ASSERT_TRUE(res.green == m_terminator_colorizer->Get_normal_terminal_color().green);
// 	ASSERT_TRUE(res.blue == m_terminator_colorizer->Get_normal_terminal_color().blue);
// }
//
// TEST_F(ModelTest, Execute_terminator_command) // NOLINT
// {
// 	const std::wstring data = L"some command goes here";
// 	EXPECT_CALL(*m_mock_terminator, Execute_terminator_command(data)).Times(1).WillOnce(testing::Return(std::vector<COLOR_STRING>{
// 		COLOR_STRING{"some command goes here", COLOR{1, 2, 3}}
// 	}));
//
// 	std::vector<COLOR_STRING> res = m_model->Execute_terminal_command(L"some command goes here");
//
// 	ASSERT_TRUE(res.size() == 1);
// 	ASSERT_TRUE(res[0].data == "some command goes here");
// 	ASSERT_TRUE(res[0].color.red == 1);
// 	ASSERT_TRUE(res[0].color.green == 2);
// 	ASSERT_TRUE(res[0].color.blue == 3);
// }
//
// // TEST_F(ModelTest, Get_diff) // NOLINT
// // {
// // 	const std::wstring data = L"git diff some_path";
// // 	EXPECT_CALL(*m_mock_terminator, Execute_shell_command(data)).Times(1).WillOnce(testing::Return(std::vector<std::string>{
// // 		"diff --git a / unit_tester / Model_Test.cpp b / unit_tester / Model_Test.cpp",
// // 		"index d8d3435..06ade26 100644",
// // 		"--- a / unit_tester / Model_Test.cpp",
// // 		"+++ b / unit_tester / Model_Test.cpp",
// // 		"@@ - 56, 3 + 56, 8 @@ TEST_F(ModelTest, Execute_terminator_command) // NOLINT",
// // 		"ASSERT_TRUE(res[0].color.green == 2);",
// // 		"ASSERT_TRUE(res[0].color.blue == 3);",
// // 		"}",
// // 		"+",
// // 		"+TEST_F(ModelTest, some_test) // NOLINT",
// // 		"+ {",
// // 		"+auto res = m_model->Get_diff(\"some_path\");",
// // 		"+}",
// // 		"-Some line that got deleted"
// // 	}));
// //
// // 	auto res = m_model->Get_diff("some_path");
// //
// // 	ASSERT_TRUE(res.size() == 14);
// //
// // 	ASSERT_TRUE(res[0].color.red == m_terminator_colorizer->Get_diff_header_color().red);
// // 	ASSERT_TRUE(res[0].color.green == m_terminator_colorizer->Get_diff_header_color().green);
// // 	ASSERT_TRUE(res[0].color.blue == m_terminator_colorizer->Get_diff_header_color().blue);
// // 	ASSERT_TRUE(res[0].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
// // 	ASSERT_TRUE(res[0].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
// // 	ASSERT_TRUE(res[0].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);
// //
// // 	ASSERT_TRUE(res[1].color.red == m_terminator_colorizer->Get_diff_header_color().red);
// // 	ASSERT_TRUE(res[1].color.green == m_terminator_colorizer->Get_diff_header_color().green);
// // 	ASSERT_TRUE(res[1].color.blue == m_terminator_colorizer->Get_diff_header_color().blue);
// // 	ASSERT_TRUE(res[1].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
// // 	ASSERT_TRUE(res[1].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
// // 	ASSERT_TRUE(res[1].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);
// //
// // 	ASSERT_TRUE(res[2].color.red == m_terminator_colorizer->Get_diff_header_color().red);
// // 	ASSERT_TRUE(res[2].color.green == m_terminator_colorizer->Get_diff_header_color().green);
// // 	ASSERT_TRUE(res[2].color.blue == m_terminator_colorizer->Get_diff_header_color().blue);
// // 	ASSERT_TRUE(res[2].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
// // 	ASSERT_TRUE(res[2].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
// // 	ASSERT_TRUE(res[2].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);
// //
// // 	ASSERT_TRUE(res[3].color.red == m_terminator_colorizer->Get_diff_header_color().red);
// // 	ASSERT_TRUE(res[3].color.green == m_terminator_colorizer->Get_diff_header_color().green);
// // 	ASSERT_TRUE(res[3].color.blue == m_terminator_colorizer->Get_diff_header_color().blue);
// // 	ASSERT_TRUE(res[3].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
// // 	ASSERT_TRUE(res[3].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
// // 	ASSERT_TRUE(res[3].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);
// //
// // 	ASSERT_TRUE(res[4].color.red == m_terminator_colorizer->Get_diff_turquoise_color().red);
// // 	ASSERT_TRUE(res[4].color.green == m_terminator_colorizer->Get_diff_turquoise_color().green);
// // 	ASSERT_TRUE(res[4].color.blue == m_terminator_colorizer->Get_diff_turquoise_color().blue);
// // 	ASSERT_TRUE(res[4].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
// // 	ASSERT_TRUE(res[4].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
// // 	ASSERT_TRUE(res[4].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);
// //
// // 	ASSERT_TRUE(res[5].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
// // 	ASSERT_TRUE(res[5].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
// // 	ASSERT_TRUE(res[5].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
// // 	ASSERT_TRUE(res[5].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
// // 	ASSERT_TRUE(res[5].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
// // 	ASSERT_TRUE(res[5].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);
// //
// // 	ASSERT_TRUE(res[6].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
// // 	ASSERT_TRUE(res[6].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
// // 	ASSERT_TRUE(res[6].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
// // 	ASSERT_TRUE(res[6].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
// // 	ASSERT_TRUE(res[6].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
// // 	ASSERT_TRUE(res[6].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);
// //
// // 	ASSERT_TRUE(res[7].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
// // 	ASSERT_TRUE(res[7].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
// // 	ASSERT_TRUE(res[7].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
// // 	ASSERT_TRUE(res[7].background_color.red == m_terminator_colorizer->Get_diff_background_normal_color().red);
// // 	ASSERT_TRUE(res[7].background_color.green == m_terminator_colorizer->Get_diff_background_normal_color().green);
// // 	ASSERT_TRUE(res[7].background_color.blue == m_terminator_colorizer->Get_diff_background_normal_color().blue);
// //
// // 	ASSERT_TRUE(res[8].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
// // 	ASSERT_TRUE(res[8].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
// // 	ASSERT_TRUE(res[8].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
// // 	ASSERT_TRUE(res[8].background_color.red == m_terminator_colorizer->Get_diff_background_green_color().red);
// // 	ASSERT_TRUE(res[8].background_color.green == m_terminator_colorizer->Get_diff_background_green_color().green);
// // 	ASSERT_TRUE(res[8].background_color.blue == m_terminator_colorizer->Get_diff_background_green_color().blue);
// //
// // 	ASSERT_TRUE(res[9].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
// // 	ASSERT_TRUE(res[9].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
// // 	ASSERT_TRUE(res[9].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
// // 	ASSERT_TRUE(res[9].background_color.red == m_terminator_colorizer->Get_diff_background_green_color().red);
// // 	ASSERT_TRUE(res[9].background_color.green == m_terminator_colorizer->Get_diff_background_green_color().green);
// // 	ASSERT_TRUE(res[9].background_color.blue == m_terminator_colorizer->Get_diff_background_green_color().blue);
// //
// // 	ASSERT_TRUE(res[10].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
// // 	ASSERT_TRUE(res[10].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
// // 	ASSERT_TRUE(res[10].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
// // 	ASSERT_TRUE(res[10].background_color.red == m_terminator_colorizer->Get_diff_background_green_color().red);
// // 	ASSERT_TRUE(res[10].background_color.green == m_terminator_colorizer->Get_diff_background_green_color().green);
// // 	ASSERT_TRUE(res[10].background_color.blue == m_terminator_colorizer->Get_diff_background_green_color().blue);
// //
// // 	ASSERT_TRUE(res[11].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
// // 	ASSERT_TRUE(res[11].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
// // 	ASSERT_TRUE(res[11].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
// // 	ASSERT_TRUE(res[11].background_color.red == m_terminator_colorizer->Get_diff_background_green_color().red);
// // 	ASSERT_TRUE(res[11].background_color.green == m_terminator_colorizer->Get_diff_background_green_color().green);
// // 	ASSERT_TRUE(res[11].background_color.blue == m_terminator_colorizer->Get_diff_background_green_color().blue);
// //
// // 	ASSERT_TRUE(res[12].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
// // 	ASSERT_TRUE(res[12].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
// // 	ASSERT_TRUE(res[12].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
// // 	ASSERT_TRUE(res[12].background_color.red == m_terminator_colorizer->Get_diff_background_green_color().red);
// // 	ASSERT_TRUE(res[12].background_color.green == m_terminator_colorizer->Get_diff_background_green_color().green);
// // 	ASSERT_TRUE(res[12].background_color.blue == m_terminator_colorizer->Get_diff_background_green_color().blue);
// //
// // 	ASSERT_TRUE(res[13].color.red == m_terminator_colorizer->Get_diff_normal_color().red);
// // 	ASSERT_TRUE(res[13].color.green == m_terminator_colorizer->Get_diff_normal_color().green);
// // 	ASSERT_TRUE(res[13].color.blue == m_terminator_colorizer->Get_diff_normal_color().blue);
// // 	ASSERT_TRUE(res[13].background_color.red == m_terminator_colorizer->Get_diff_background_red_color().red);
// // 	ASSERT_TRUE(res[13].background_color.green == m_terminator_colorizer->Get_diff_background_red_color().green);
// // 	ASSERT_TRUE(res[13].background_color.blue == m_terminator_colorizer->Get_diff_background_red_color().blue);
// // }
//
// //TEST_F(ModelTest, Refresh_the_tree) // NOLINT
// //{
// //	ON_CALL(*m_mock_lib_giter, Get_commit_message(StrEq("sha1"))).WillByDefault(Return("commit message"));
// //	ON_CALL(*m_mock_lib_giter, Get_local_refs(StrEq("sha1"))).WillByDefault(Return(std::vector<std::string>{"local ref"}));
// //	ON_CALL(*m_mock_lib_giter, Get_remote_refs(StrEq("sha1"))).WillByDefault(Return(std::vector<std::string>{"remote ref"}));
// //
// //	EXPECT_CALL(*m_mock_tree, Refresh_the_tree()).Times(1).WillOnce(testing::Return(
// //		std::make_shared<std::vector<Line>>(std::vector<Line>{
// //			Line(std::vector<Node>{Node(1, "sha1", 2, COMMIT, true)}, m_mock_lib_giter.get(), 0)
// //		})
// //	));
// //
// //	const std::shared_ptr<std::vector<Line>> res = m_model->Refresh_the_tree();
// //
// //	ASSERT_TRUE(res->size() == 1);
// //	ASSERT_TRUE(res->at(0).Get_commit_message() == "commit message");
// //	ASSERT_TRUE(res->at(0).Has_at_least_one_ref() == true);
// //	ASSERT_TRUE(res->at(0).Get_sha1() == "sha1");
// //	ASSERT_TRUE(res->at(0).Get_nodes().size() == 1);
// //	ASSERT_TRUE(res->at(0).Get_nodes()[0].Get_column() == 2);
// //	ASSERT_TRUE(res->at(0).Get_nodes()[0].Get_color() == 2);
// //	ASSERT_TRUE(res->at(0).Get_nodes()[0].Is_tip() == true);
// //	ASSERT_TRUE(res->at(0).Get_nodes()[0].Get_type() == NODE_TYPE::COMMIT);
// //}
//
// TEST_F(ModelTest, Get_staged) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_lib_giter, Get_staged()).Times(1).WillOnce(testing::Return(
// 		std::make_shared<std::vector<std::pair<std::string, std::string>>>(
// 			std::vector<std::pair<std::string, std::string>>{std::make_pair("5", "6")})
// 	));
//
// 	std::shared_ptr<std::vector<std::pair<std::string, std::string>>> res = m_model->Get_staged();
//
// 	ASSERT_TRUE(res->size() == 1);
// 	ASSERT_TRUE(res->at(0).first == "5");
// 	ASSERT_TRUE(res->at(0).second == "6");
// }
//
// TEST_F(ModelTest, Get_unstaged) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_lib_giter, Get_unstaged()).Times(1).WillOnce(testing::Return(
// 		std::make_shared<std::vector<std::pair<std::string, std::string>>>(
// 			std::vector<std::pair<std::string, std::string>>{std::make_pair("5", "6")})
// 	));
//
// 	std::shared_ptr<std::vector<std::pair<std::string, std::string>>> res = m_model->Get_unstaged();
//
// 	ASSERT_TRUE(res->size() == 1);
// 	ASSERT_TRUE(res->at(0).first == "5");
// 	ASSERT_TRUE(res->at(0).second == "6");
// }
//
// TEST_F(ModelTest, Get_untracked) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_lib_giter, Get_untracked()).Times(1).WillOnce(testing::Return(
// 		std::make_shared<std::vector<std::pair<std::string, std::string>>>(
// 			std::vector<std::pair<std::string, std::string>>{std::make_pair("5", "6")})
// 	));
//
// 	std::shared_ptr<std::vector<std::pair<std::string, std::string>>> res = m_model->Get_untracked();
//
// 	ASSERT_TRUE(res->size() == 1);
// 	ASSERT_TRUE(res->at(0).first == "5");
// 	ASSERT_TRUE(res->at(0).second == "6");
// }
