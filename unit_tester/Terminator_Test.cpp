#include "gtest/gtest.h"
#include "gmock/gmock.h"
//#include "Tree.h"
#include <fstream>
#include "Model_mocks.h"
#include <windows.h>
#include "Terminator.h"

using namespace testing;
using ::testing::NiceMock;


class TerminatorTest : public Test
{
protected:
	TerminatorTest()
	{
		std::filesystem::path path_to_repo = std::filesystem::current_path().parent_path();
		m_mock_lib_giter = std::make_shared<NiceMock<MockLibGiter>>(path_to_repo);

		m_mock_pipe = std::make_shared<NiceMock<MockPipe>>();

		m_terminator_colorizer = std::make_shared<TerminatorColorizer>();

		m_terminator = std::make_shared<Terminator>(m_mock_pipe.get(), m_mock_lib_giter.get(), m_terminator_colorizer.get());
	}

	~TerminatorTest() = default;

	std::shared_ptr<NiceMock<MockPipe>> m_mock_pipe;
	std::shared_ptr<NiceMock<MockLibGiter>> m_mock_lib_giter;
	std::shared_ptr<TerminatorColorizer> m_terminator_colorizer;
	std::shared_ptr<Terminator> m_terminator;
};

TEST_F(TerminatorTest, ctor_test) // NOLINT
{
}

TEST_F(TerminatorTest, Execute_shell_command_could_not_create_pipe) // NOLINT
{
	ON_CALL(*m_mock_pipe, Create_pipe).WillByDefault(Return(false));

	try
	{
		std::vector<std::string> res = m_terminator->Execute_shell_command(L"");
	}
	catch (std::exception& exc)
	{
		const char* message = exc.what();
		const std::string expected = "Panic! Could not create a pipe for shell execution!";

		ASSERT_TRUE(std::string(message) == expected);

		SUCCEED();

		return;
	}

	FAIL();
}

TEST_F(TerminatorTest, Execute_shell_command_empty_input) // NOLINT
{
	ON_CALL(*m_mock_pipe, Create_pipe).WillByDefault(Return(true));
	ON_CALL(*m_mock_pipe, Create_process).WillByDefault(Return(false));

	std::vector<std::string> res = m_terminator->Execute_shell_command(L"");

	ASSERT_TRUE(res.empty() == true);
}

TEST_F(TerminatorTest, Execute_shell_command_no_availible_data) // NOLINT
{
	ON_CALL(*m_mock_pipe, Create_pipe).WillByDefault(Return(true));
	ON_CALL(*m_mock_pipe, Create_process).WillByDefault(Return(true));

	ON_CALL(*m_mock_pipe, Peek_named_pipe(_, nullptr, 0, nullptr, _, nullptr)).WillByDefault(DoAll(SetArgPointee < 4 > (0), Return(true)));

	std::vector<std::string> res = m_terminator->Execute_shell_command(L"some command goes here");

	ASSERT_TRUE(res.size() == 0);
}

TEST_F(TerminatorTest, Execute_shell_command_bad_command_or_file_name) // NOLINT
{
	ON_CALL(*m_mock_pipe, Create_pipe).WillByDefault(Return(true));
	ON_CALL(*m_mock_pipe, Create_process).WillByDefault(Return(false));

	std::vector<std::string> res = m_terminator->Execute_shell_command(L"some bad command goes here");

	ASSERT_TRUE(res.size() == 1);
	ASSERT_TRUE(res[0] == std::string("'some bad command goes here' is not recognized as an internal or external command, operable program or batch file.\n"));
}

TEST_F(TerminatorTest, Execute_shell_command_Read_file_failure) // NOLINT
{
	ON_CALL(*m_mock_pipe, Create_pipe).WillByDefault(Return(true));
	ON_CALL(*m_mock_pipe, Create_process).WillByDefault(Return(true));

	ON_CALL(*m_mock_pipe, Peek_named_pipe(_, nullptr, 0, nullptr, _, nullptr)).WillByDefault(DoAll(SetArgPointee < 4 > (13), Return(true)));

	const char* buf = "git version 1";
	ON_CALL(*m_mock_pipe, Read_file(_, _, _, _, _)).WillByDefault(Return(false));

	try
	{
		std::vector<std::string> res = m_terminator->Execute_shell_command(L"git --version");
		FAIL();
	}
	catch (std::exception& exc)
	{
		ASSERT_TRUE(std::string(exc.what()) == std::string("Panic! Read_file() failed!"));
	}
}

// ReSharper disable once CppInconsistentNaming
ACTION_P(Set_res, value)
{
	for (int i = 0; i < 13; ++i)
	{
		static_cast<char*>(arg1)[i] = value[i];
	}
}

TEST_F(TerminatorTest, Execute_shell_command) // NOLINT
{
	int times = 0;
	m_terminator->talking_to_shell.subscribe([&](const int current, const int total, const double counter)
	{
		times++;

		ASSERT_TRUE(counter == 1);
	});

	ON_CALL(*m_mock_pipe, Create_pipe).WillByDefault(Return(true));
	ON_CALL(*m_mock_pipe, Create_process).WillByDefault(Return(true));

	EXPECT_CALL(*m_mock_pipe, Peek_named_pipe(_, nullptr, 0, nullptr, _, nullptr)).WillOnce(DoAll(SetArgPointee < 4 > (13), Return(true)))
	                                                                              .WillOnce(Return(false));

	const char* buf = "git version 1";
	ON_CALL(*m_mock_pipe, Read_file(_, _, _, _, _)).WillByDefault(DoAll(Set_res(buf), SetArgPointee < 3 > (13), Return(true)));

	std::vector<std::string> res = m_terminator->Execute_shell_command(L"git --version");

	ASSERT_TRUE(res.size() == 1);
	ASSERT_TRUE(res[0] == std::string("git version 1\n"));

	ASSERT_TRUE(times == 1);
}

TEST_F(TerminatorTest, Execute_terminator_command_bad_command_or_file_name) // NOLINT
{
	ON_CALL(*m_mock_pipe, Create_pipe).WillByDefault(Return(true));
	ON_CALL(*m_mock_pipe, Create_process).WillByDefault(Return(false));

	EXPECT_CALL(*m_mock_lib_giter, Get_current_branch()).Times(1).WillOnce(Return("master"));

	std::vector<COLOR_STRING> res = m_terminator->Execute_terminator_command(L"some bad command goes here");

	ASSERT_TRUE(res.size() == 6);

	ASSERT_TRUE(res[0].data == std::string("'some bad command goes here' is not recognized as an internal or external command, operable program or batch file.\n"));
	ASSERT_TRUE(res[0].color.red == 203);
	ASSERT_TRUE(res[0].color.green == 203);
	ASSERT_TRUE(res[0].color.blue == 203);

	ASSERT_TRUE(res[1].data == std::string("\n"));
	ASSERT_TRUE(res[1].color.red == 203);
	ASSERT_TRUE(res[1].color.green == 203);
	ASSERT_TRUE(res[1].color.blue == 203);

	ASSERT_TRUE(res[2].data == std::filesystem::current_path().string() + " ");
	ASSERT_TRUE(res[2].color.red == 141);
	ASSERT_TRUE(res[2].color.green == 208);
	ASSERT_TRUE(res[2].color.blue == 6);

	ASSERT_TRUE(res[3].data == std::string("(master)"));
	ASSERT_TRUE(res[3].color.red == m_terminator_colorizer->Get_white_terminal_color().red);
	ASSERT_TRUE(res[3].color.green == m_terminator_colorizer->Get_white_terminal_color().green);
	ASSERT_TRUE(res[3].color.blue == m_terminator_colorizer->Get_white_terminal_color().blue);

	ASSERT_TRUE(res[4].data == std::string("\n"));
	ASSERT_TRUE(res[4].color.red == 203);
	ASSERT_TRUE(res[4].color.green == 203);
	ASSERT_TRUE(res[4].color.blue == 203);

	ASSERT_TRUE(res[5].data == std::string("> "));
	ASSERT_TRUE(res[5].color.red == 203);
	ASSERT_TRUE(res[5].color.green == 203);
	ASSERT_TRUE(res[5].color.blue == 203);
}

TEST_F(TerminatorTest, Execute_terminator_command) // NOLINT
{
	int times = 0;
	m_terminator->talking_to_shell.subscribe([&](const int current, const int total, const double counter)
	{
		times++;

		ASSERT_TRUE(counter == 1);
	});

	ON_CALL(*m_mock_lib_giter, Get_current_branch()).WillByDefault(Return("master"));

	ON_CALL(*m_mock_pipe, Create_pipe).WillByDefault(Return(true));
	ON_CALL(*m_mock_pipe, Create_process).WillByDefault(Return(true));

	EXPECT_CALL(*m_mock_pipe, Peek_named_pipe(_, nullptr, 0, nullptr, _, nullptr)).WillOnce(DoAll(SetArgPointee < 4 > (13), Return(true)))
	                                                                              .WillOnce(Return(false));

	const char* buf = "git version 1";
	ON_CALL(*m_mock_pipe, Read_file(_, _, _, _, _)).WillByDefault(DoAll(Set_res(buf), SetArgPointee < 3 > (13), Return(true)));

	std::vector<COLOR_STRING> res = m_terminator->Execute_terminator_command(L"git --version");

	ASSERT_TRUE(res.size() == 6);

	ASSERT_TRUE(res[0].data.find("git version 1") != std::string::npos);
	ASSERT_TRUE(res[0].color.red == 203);
	ASSERT_TRUE(res[0].color.green == 203);
	ASSERT_TRUE(res[0].color.blue == 203);

	ASSERT_TRUE(res[1].data == std::string("\n"));
	ASSERT_TRUE(res[1].color.red == 203);
	ASSERT_TRUE(res[1].color.green == 203);
	ASSERT_TRUE(res[1].color.blue == 203);

	ASSERT_TRUE(res[2].data == std::filesystem::current_path().string() + " ");
	ASSERT_TRUE(res[2].color.red == 141);
	ASSERT_TRUE(res[2].color.green == 208);
	ASSERT_TRUE(res[2].color.blue == 6);

	ASSERT_TRUE(res[3].data == std::string("(master)"));
	ASSERT_TRUE(res[3].color.red == m_terminator_colorizer->Get_white_terminal_color().red);
	ASSERT_TRUE(res[3].color.green == m_terminator_colorizer->Get_white_terminal_color().green);
	ASSERT_TRUE(res[3].color.blue == m_terminator_colorizer->Get_white_terminal_color().blue);

	ASSERT_TRUE(res[4].data == std::string("\n"));
	ASSERT_TRUE(res[4].color.red == 203);
	ASSERT_TRUE(res[4].color.green == 203);
	ASSERT_TRUE(res[4].color.blue == 203);

	ASSERT_TRUE(res[5].data == std::string("> "));
	ASSERT_TRUE(res[5].color.red == 203);
	ASSERT_TRUE(res[5].color.green == 203);
	ASSERT_TRUE(res[5].color.blue == 203);

	ASSERT_TRUE(times == 1);
}

ACTION_P(Set_res2, value)
{
	for (int i = 0; i < 13; ++i)
	{
		static_cast<char*>(arg1)[i] = value[i];
	}
}

TEST_F(TerminatorTest, Get_string_tree_test) // NOLINT
{
	int times = 0;
	m_terminator->talking_to_shell.subscribe([&](const int current, const int total, const double counter)
	{
		times++;

		ASSERT_TRUE(counter == 1);
	});

	ON_CALL(*m_mock_pipe, Create_pipe).WillByDefault(Return(true));
	ON_CALL(*m_mock_pipe, Create_process).WillByDefault(Return(true));

	EXPECT_CALL(*m_mock_pipe, Peek_named_pipe(_, nullptr, 0, nullptr, _, nullptr)).WillOnce(DoAll(SetArgPointee < 4 > (13), Return(true)))
	                                                                              .WillOnce(Return(false));

	const char* buf = "* some_sha1AB";
	ON_CALL(*m_mock_pipe, Read_file(_, _, _, _, _)).WillByDefault(DoAll(Set_res2(buf), SetArgPointee < 3 > (13), Return(true)));

	std::string res = m_terminator->Get_string_tree();

	ASSERT_TRUE(res == std::string(buf));
	ASSERT_TRUE(times == 1);
}
