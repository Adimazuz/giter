// #include "gtest/gtest.h"
// #include "gmock/gmock.h"
// #include "Model_mocks.h"
// #include "TerminatorViewModel.h"
// #include "StatuserViewModel.h"
// #include "git_sub_commands_string.h"
//
// using namespace testing;
//
//
// class TerminatorViewModelTest : public Test
// {
// protected:
// 	TerminatorViewModelTest()
// 	{
// 		m_terminator_colorizer = std::make_shared<TerminatorColorizer>();
// 		std::filesystem::path path_to_repo = std::filesystem::current_path().parent_path();
// 		m_mock_lib_giter = std::make_shared<NiceMock<MockLibGiter>>(path_to_repo);
// 		m_mock_terminator = std::make_shared<NiceMock<MockTerminator>>(m_mock_lib_giter.get(), m_terminator_colorizer.get());
//
// 		ON_CALL(*m_mock_terminator, Execute_shell_command(StrEq(L"git help -a"))).WillByDefault(Return(Break_into_lines(GIT_SUB_COMMANDS_STRING)));
// 		m_completer = std::make_shared<Completer>(m_mock_terminator.get(), m_mock_lib_giter.get());
// 		m_historer = std::make_shared<Historer>();
// 		m_terminator_view_model = std::make_shared<TerminatorViewModel>(m_mock_terminator.get(), m_terminator_colorizer.get(), m_completer.get(), m_historer.get());
// 	}
//
// 	std::shared_ptr<TerminatorColorizer> m_terminator_colorizer;
// 	std::shared_ptr<NiceMock<MockLibGiter>> m_mock_lib_giter;
// 	std::shared_ptr<NiceMock<MockTerminator>> m_mock_terminator;
//
// 	std::shared_ptr<Completer> m_completer;
// 	std::shared_ptr<Historer> m_historer;
// 	std::shared_ptr<ITerminatorViewModel> m_terminator_view_model;
//
// private:
//
// 	// ReSharper disable once CppMemberFunctionMayBeStatic
// 	[[nodiscard]] std::vector<std::string> Break_into_lines(const std::string& data) const
// 	{
// 		// This code is faster the using streams.
// 		const char separator = '\n';
// 		std::string::size_type b = 0;
// 		std::vector<std::string> lines;
//
// 		while ((b = data.find_first_not_of(separator, b)) != std::string::npos)
// 		{
// 			const auto e = data.find_first_of(separator, b);
// 			std::string temp = data.substr(b, e - b);
// 			lines.push_back(temp + "\n");
// 			b = e;
// 		}
//
// 		return lines;
// 	}
// };
//
// TEST_F(TerminatorViewModelTest, Press_enter_empty_string) // NOLINT
// {
// 	std::vector<std::vector<COLOR_STRING>> terminal_data_res;
// 	m_terminator_view_model->terminal_data.subscribe([&](const std::vector<COLOR_STRING>& data)
// 	{
// 		terminal_data_res.emplace_back(data);
// 	});
//
// 	std::vector<bool> terminator_enablement_res;
// 	m_terminator_view_model->terminator_enablement.subscribe([&](const bool should_enable)
// 	{
// 		terminator_enablement_res.emplace_back(should_enable);
// 	});
//
// 	unsigned int terminator_set_focus_res = 0;
// 	m_terminator_view_model->terminator_set_focus.subscribe([&]()
// 	{
// 		terminator_set_focus_res++;
// 	});
//
// 	m_terminator_view_model->Press_enter("");
//
// 	ASSERT_TRUE(terminal_data_res.size() == 1);
// 	ASSERT_TRUE(terminal_data_res[0].empty() == true);
//
// 	ASSERT_TRUE(terminator_enablement_res.size() == 2);
// 	ASSERT_TRUE(terminator_enablement_res[0] == false);
// 	ASSERT_TRUE(terminator_enablement_res[1] == true);
//
// 	ASSERT_TRUE(terminator_set_focus_res == 1);
//
// 	ASSERT_TRUE(m_historer->Get_earlier().empty() == true);
// 	ASSERT_TRUE(m_historer->Get_later().empty() == true);
// }
//
// TEST_F(TerminatorViewModelTest, Press_enter) // NOLINT
// {
// 	std::vector<std::vector<COLOR_STRING>> terminal_data_res;
// 	m_terminator_view_model->terminal_data.subscribe([&](const std::vector<COLOR_STRING>& data)
// 	{
// 		terminal_data_res.emplace_back(data);
// 	});
//
// 	std::vector<bool> terminator_enablement_res;
// 	m_terminator_view_model->terminator_enablement.subscribe([&](const bool should_enable)
// 	{
// 		terminator_enablement_res.emplace_back(should_enable);
// 	});
//
// 	unsigned int terminator_set_focus_res = 0;
// 	m_terminator_view_model->terminator_set_focus.subscribe([&]()
// 	{
// 		terminator_set_focus_res++;
// 	});
//
// 	ON_CALL(*m_mock_terminator, Execute_terminator_command(_)).WillByDefault(Return(std::vector<COLOR_STRING>{COLOR_STRING{"response to some command"}}));
// 	m_terminator_view_model->Press_enter("> git status");
//
// 	ASSERT_TRUE(terminal_data_res.size() == 1);
// 	ASSERT_TRUE(terminal_data_res[0][0].data == "\n");
// 	ASSERT_TRUE(terminal_data_res[0][1].data == "response to some command");
//
// 	ASSERT_TRUE(terminator_enablement_res.size() == 2);
// 	ASSERT_TRUE(terminator_enablement_res[0] == false);
// 	ASSERT_TRUE(terminator_enablement_res[1] == true);
//
// 	ASSERT_TRUE(terminator_set_focus_res == 1);
//
// 	ASSERT_TRUE(m_historer->Get_earlier() == "git status");
// 	ASSERT_TRUE(m_historer->Get_later().empty() == true);
// }
//
// TEST_F(TerminatorViewModelTest, Append_to_input_line_and_press_enter) // NOLINT
// {
// 	unsigned int clear_input_line_res = 0;
// 	m_terminator_view_model->clear_input_line.subscribe([&]()
// 	{
// 		clear_input_line_res++;
// 	});
//
// 	std::vector<std::vector<COLOR_STRING>> terminal_data_res;
// 	m_terminator_view_model->terminal_data.subscribe([&](const std::vector<COLOR_STRING>& data)
// 	{
// 		terminal_data_res.emplace_back(data);
// 	});
//
// 	std::vector<bool> terminator_enablement_res;
// 	m_terminator_view_model->terminator_enablement.subscribe([&](const bool should_enable)
// 	{
// 		terminator_enablement_res.emplace_back(should_enable);
// 	});
//
// 	unsigned int terminator_set_focus_res = 0;
// 	m_terminator_view_model->terminator_set_focus.subscribe([&]()
// 	{
// 		terminator_set_focus_res++;
// 	});
//
// 	ON_CALL(*m_mock_terminator, Execute_terminator_command(_)).WillByDefault(Return(std::vector<COLOR_STRING>{COLOR_STRING{"response to some command"}}));
// 	m_terminator_view_model->Append_to_input_line_and_press_enter("git status");
//
// 	ASSERT_TRUE(clear_input_line_res == 1);
//
// 	ASSERT_TRUE(terminal_data_res.size() == 2);
// 	ASSERT_TRUE(terminal_data_res[0][0].data == "git status");
// 	ASSERT_TRUE(terminal_data_res[1][0].data == "\n");
//
// 	ASSERT_TRUE(terminator_enablement_res.size() == 2);
// 	ASSERT_TRUE(terminator_enablement_res[0] == false);
// 	ASSERT_TRUE(terminator_enablement_res[1] == true);
//
// 	ASSERT_TRUE(terminator_set_focus_res == 1);
//
// 	ASSERT_TRUE(m_historer->Get_earlier() == "git status");
// 	ASSERT_TRUE(m_historer->Get_later().empty() == true);
// }
//
// TEST_F(TerminatorViewModelTest, Append_illegal_to_input_line_and_press_enter) // NOLINT
// {
// 	std::vector<std::vector<COLOR_STRING>> terminal_data_res;
// 	m_terminator_view_model->terminal_data.subscribe([&](const std::vector<COLOR_STRING>& data)
// 	{
// 		terminal_data_res.emplace_back(data);
// 	});
//
// 	std::vector<bool> terminator_enablement_res;
// 	m_terminator_view_model->terminator_enablement.subscribe([&](const bool should_enable)
// 	{
// 		terminator_enablement_res.emplace_back(should_enable);
// 	});
//
// 	unsigned int terminator_set_focus_res = 0;
// 	m_terminator_view_model->terminator_set_focus.subscribe([&]()
// 	{
// 		terminator_set_focus_res++;
// 	});
//
// 	ON_CALL(*m_mock_terminator, Execute_terminator_command(_)).WillByDefault(Return(std::vector<COLOR_STRING>{COLOR_STRING{"response to some command"}}));
// 	m_terminator_view_model->Append_illegal_to_input_line_and_press_enter("git status");
//
// 	ASSERT_TRUE(terminal_data_res.size() == 2);
// 	ASSERT_TRUE(terminal_data_res[0][0].data == "git status");
// 	ASSERT_TRUE(terminal_data_res[1][0].data == "response to some command");
//
// 	ASSERT_TRUE(terminator_enablement_res.size() == 2);
// 	ASSERT_TRUE(terminator_enablement_res[0] == false);
// 	ASSERT_TRUE(terminator_enablement_res[1] == true);
//
// 	ASSERT_TRUE(terminator_set_focus_res == 1);
// }
//
// TEST_F(TerminatorViewModelTest, Set_focus_to_the_input_line) // NOLINT
// {
// 	unsigned int terminator_set_focus_res = 0;
// 	m_terminator_view_model->terminator_set_focus.subscribe([&]()
// 	{
// 		terminator_set_focus_res++;
// 	});
//
// 	m_terminator_view_model->Set_focus_to_the_input_line();
//
// 	ASSERT_TRUE(terminator_set_focus_res == 1);
// }
//
// TEST_F(TerminatorViewModelTest, Append_data_to_terminator_empty) // NOLINT
// {
// 	std::vector<std::vector<COLOR_STRING>> terminal_data_res;
// 	m_terminator_view_model->terminal_data.subscribe([&](const std::vector<COLOR_STRING>& data)
// 	{
// 		terminal_data_res.emplace_back(data);
// 	});
//
// 	m_terminator_view_model->Append_data_to_terminator("git status");
//
// 	ASSERT_TRUE(terminal_data_res.empty() == true);
// }
//
// TEST_F(TerminatorViewModelTest, Append_data_to_terminator_single) // NOLINT
// {
// 	std::vector<std::vector<COLOR_STRING>> terminal_data_res;
// 	m_terminator_view_model->terminal_data.subscribe([&](const std::vector<COLOR_STRING>& data)
// 	{
// 		terminal_data_res.emplace_back(data);
// 	});
//
// 	m_terminator_view_model->Append_data_to_terminator("git stat");
//
// 	ASSERT_TRUE(terminal_data_res.size() == 1);
// 	ASSERT_TRUE(terminal_data_res[0][0].data == "us");
// }
//
// TEST_F(TerminatorViewModelTest, Append_data_to_terminator_many) // NOLINT
// {
// 	std::vector<std::vector<COLOR_STRING>> terminal_data_res;
// 	m_terminator_view_model->terminal_data.subscribe([&](const std::vector<COLOR_STRING>& data)
// 	{
// 		terminal_data_res.emplace_back(data);
// 	});
//
// 	m_terminator_view_model->Append_data_to_terminator("git check");
//
// 	ASSERT_TRUE(terminal_data_res.size() == 9);
//
// 	ASSERT_TRUE(terminal_data_res[1][0].data == "check-attr\n");
// 	ASSERT_TRUE(terminal_data_res[1][0].color.red = m_terminator_colorizer->Get_white_terminal_color().red);
// 	ASSERT_TRUE(terminal_data_res[1][0].color.green = m_terminator_colorizer->Get_white_terminal_color().green);
// 	ASSERT_TRUE(terminal_data_res[1][0].color.blue = m_terminator_colorizer->Get_white_terminal_color().blue);
//
// 	ASSERT_TRUE(terminal_data_res[2][0].data == "check-ignore\n");
// 	ASSERT_TRUE(terminal_data_res[2][0].color.red = m_terminator_colorizer->Get_white_terminal_color().red);
// 	ASSERT_TRUE(terminal_data_res[2][0].color.green = m_terminator_colorizer->Get_white_terminal_color().green);
// 	ASSERT_TRUE(terminal_data_res[2][0].color.blue = m_terminator_colorizer->Get_white_terminal_color().blue);
//
// 	ASSERT_TRUE(terminal_data_res[3][0].data == "check-mailmap\n");
// 	ASSERT_TRUE(terminal_data_res[3][0].color.red = m_terminator_colorizer->Get_white_terminal_color().red);
// 	ASSERT_TRUE(terminal_data_res[3][0].color.green = m_terminator_colorizer->Get_white_terminal_color().green);
// 	ASSERT_TRUE(terminal_data_res[3][0].color.blue = m_terminator_colorizer->Get_white_terminal_color().blue);
//
// 	ASSERT_TRUE(terminal_data_res[4][0].data == "check-ref-format\n");
// 	ASSERT_TRUE(terminal_data_res[4][0].color.red = m_terminator_colorizer->Get_white_terminal_color().red);
// 	ASSERT_TRUE(terminal_data_res[4][0].color.green = m_terminator_colorizer->Get_white_terminal_color().green);
// 	ASSERT_TRUE(terminal_data_res[4][0].color.blue = m_terminator_colorizer->Get_white_terminal_color().blue);
//
// 	ASSERT_TRUE(terminal_data_res[5][0].data == "checkout\n");
// 	ASSERT_TRUE(terminal_data_res[5][0].color.red = m_terminator_colorizer->Get_white_terminal_color().red);
// 	ASSERT_TRUE(terminal_data_res[5][0].color.green = m_terminator_colorizer->Get_white_terminal_color().green);
// 	ASSERT_TRUE(terminal_data_res[5][0].color.blue = m_terminator_colorizer->Get_white_terminal_color().blue);
//
// 	ASSERT_TRUE(terminal_data_res[6][0].data == "checkout-index\n");
// 	ASSERT_TRUE(terminal_data_res[6][0].color.red = m_terminator_colorizer->Get_white_terminal_color().red);
// 	ASSERT_TRUE(terminal_data_res[6][0].color.green = m_terminator_colorizer->Get_white_terminal_color().green);
// 	ASSERT_TRUE(terminal_data_res[6][0].color.blue = m_terminator_colorizer->Get_white_terminal_color().blue);
// }
//
// TEST_F(TerminatorViewModelTest, Go_earlier_in_history_no_history) // NOLINT
// {
// 	unsigned int clear_input_line_res = 0;
// 	m_terminator_view_model->clear_input_line.subscribe([&]()
// 	{
// 		clear_input_line_res++;
// 	});
//
// 	std::vector<std::vector<COLOR_STRING>> terminal_data_res;
// 	m_terminator_view_model->terminal_data.subscribe([&](const std::vector<COLOR_STRING>& data)
// 	{
// 		terminal_data_res.emplace_back(data);
// 	});
//
// 	m_terminator_view_model->Go_earlier_in_history();
//
// 	ASSERT_TRUE(clear_input_line_res == 0);
//
// 	ASSERT_TRUE(terminal_data_res.empty());
// }
//
// TEST_F(TerminatorViewModelTest, Go_earlier_in_history) // NOLINT
// {
// 	unsigned int clear_input_line_res = 0;
// 	m_terminator_view_model->clear_input_line.subscribe([&]()
// 	{
// 		clear_input_line_res++;
// 	});
//
// 	std::vector<std::vector<COLOR_STRING>> terminal_data_res;
// 	m_terminator_view_model->terminal_data.subscribe([&](const std::vector<COLOR_STRING>& data)
// 	{
// 		terminal_data_res.emplace_back(data);
// 	});
//
// 	m_historer->Add("bla");
// 	m_terminator_view_model->Go_earlier_in_history();
//
// 	ASSERT_TRUE(clear_input_line_res == 1);
//
// 	ASSERT_TRUE(terminal_data_res.size() == 1);
// 	ASSERT_TRUE(terminal_data_res[0][0].data == "bla");
// }
//
// TEST_F(TerminatorViewModelTest, Go_later_in_history_no_history) // NOLINT
// {
// 	unsigned int clear_input_line_res = 0;
// 	m_terminator_view_model->clear_input_line.subscribe([&]()
// 	{
// 		clear_input_line_res++;
// 	});
//
// 	std::vector<std::vector<COLOR_STRING>> terminal_data_res;
// 	m_terminator_view_model->terminal_data.subscribe([&](const std::vector<COLOR_STRING>& data)
// 	{
// 		terminal_data_res.emplace_back(data);
// 	});
//
// 	m_terminator_view_model->Go_later_in_history();
//
// 	ASSERT_TRUE(clear_input_line_res == 0);
//
// 	ASSERT_TRUE(terminal_data_res.empty());
// }
//
// TEST_F(TerminatorViewModelTest, Go_later_in_history_end) // NOLINT
// {
// 	unsigned int clear_input_line_res = 0;
// 	m_terminator_view_model->clear_input_line.subscribe([&]()
// 	{
// 		clear_input_line_res++;
// 	});
//
// 	std::vector<std::vector<COLOR_STRING>> terminal_data_res;
// 	m_terminator_view_model->terminal_data.subscribe([&](const std::vector<COLOR_STRING>& data)
// 	{
// 		terminal_data_res.emplace_back(data);
// 	});
//
// 	m_historer->Add("bla");
// 	m_terminator_view_model->Go_later_in_history();
//
// 	ASSERT_TRUE(clear_input_line_res == 0);
//
// 	ASSERT_TRUE(terminal_data_res.empty() == true);
// }
//
// TEST_F(TerminatorViewModelTest, Go_later_in_history_middle) // NOLINT
// {
// 	unsigned int clear_input_line_res = 0;
// 	m_terminator_view_model->clear_input_line.subscribe([&]()
// 	{
// 		clear_input_line_res++;
// 	});
//
// 	std::vector<std::vector<COLOR_STRING>> terminal_data_res;
// 	m_terminator_view_model->terminal_data.subscribe([&](const std::vector<COLOR_STRING>& data)
// 	{
// 		terminal_data_res.emplace_back(data);
// 	});
//
// 	m_historer->Add("bla1");
// 	m_historer->Add("bla2");
// 	m_historer->Add("bla3");
// #pragma warning( push )
// #pragma warning( disable : 4834)
// 	// ReSharper disable CppNoDiscardExpression
// 	// ReSharper disable CppExpressionWithoutSideEffects
// 	m_historer->Get_earlier();
// 	m_historer->Get_earlier();
// 	// ReSharper restore CppExpressionWithoutSideEffects
// 	// ReSharper restore CppNoDiscardExpression
// #pragma warning(pop)
// 	m_terminator_view_model->Go_later_in_history();
//
// 	ASSERT_TRUE(clear_input_line_res == 1);
//
// 	ASSERT_TRUE(terminal_data_res.size() == 1);
// 	ASSERT_TRUE(terminal_data_res[0][0].data == "bla3");
// }
//
// TEST_F(TerminatorViewModelTest, Get_normal_terminator_color) // NOLINT
// {
// 	const COLOR res = m_terminator_view_model->Get_normal_terminator_color();
//
// 	ASSERT_TRUE(res.red == m_terminator_colorizer->Get_normal_terminal_color().red);
// 	ASSERT_TRUE(res.green == m_terminator_colorizer->Get_normal_terminal_color().green);
// 	ASSERT_TRUE(res.blue == m_terminator_colorizer->Get_normal_terminal_color().blue);
// }
//
// TEST_F(TerminatorViewModelTest, Keyboard_shortcut_was_pressed_no_CTRL) // NOLINT
// {
// 	std::vector<std::vector<COLOR_STRING>> terminal_data_res;
// 	m_terminator_view_model->terminal_data.subscribe([&](const std::vector<COLOR_STRING>& data)
// 	{
// 		terminal_data_res.emplace_back(data);
// 	});
//
// 	m_terminator_view_model->Keyboard_shortcut_was_pressed(false, 'N');
//
// 	ASSERT_TRUE(terminal_data_res.empty() == true);
// }
//
// TEST_F(TerminatorViewModelTest, Keyboard_shortcut_was_pressed_S) // NOLINT
// {
// 	unsigned int clear_input_line_res = 0;
// 	m_terminator_view_model->clear_input_line.subscribe([&]()
// 	{
// 		clear_input_line_res++;
// 	});
//
// 	std::vector<std::vector<COLOR_STRING>> terminal_data_res;
// 	m_terminator_view_model->terminal_data.subscribe([&](const std::vector<COLOR_STRING>& data)
// 	{
// 		terminal_data_res.emplace_back(data);
// 	});
//
// 	std::vector<bool> terminator_enablement_res;
// 	m_terminator_view_model->terminator_enablement.subscribe([&](const bool should_enable)
// 	{
// 		terminator_enablement_res.emplace_back(should_enable);
// 	});
//
// 	unsigned int terminator_set_focus_res = 0;
// 	m_terminator_view_model->terminator_set_focus.subscribe([&]()
// 	{
// 		terminator_set_focus_res++;
// 	});
//
// 	ON_CALL(*m_mock_terminator, Execute_terminator_command(_)).WillByDefault(Return(std::vector<COLOR_STRING>{COLOR_STRING{"response to some command"}}));
// 	m_terminator_view_model->Keyboard_shortcut_was_pressed(true, 'S');
//
// 	ASSERT_TRUE(clear_input_line_res == 1);
//
// 	ASSERT_TRUE(terminal_data_res.size() == 2);
// 	ASSERT_TRUE(terminal_data_res[0][0].data == "git status");
// 	ASSERT_TRUE(terminal_data_res[1].size() == 2);
// 	ASSERT_TRUE(terminal_data_res[1][0].data == "\n");
// 	ASSERT_TRUE(terminal_data_res[1][1].data == "response to some command");
//
// 	ASSERT_TRUE(terminator_enablement_res.size() == 2);
// 	ASSERT_TRUE(terminator_enablement_res[0] == false);
// 	ASSERT_TRUE(terminator_enablement_res[1] == true);
//
// 	ASSERT_TRUE(terminator_set_focus_res == 1);
//
// 	ASSERT_TRUE(m_historer->Get_earlier() == "git status");
// 	ASSERT_TRUE(m_historer->Get_later().empty() == true);
// }
//
// TEST_F(TerminatorViewModelTest, Keyboard_shortcut_was_pressed_R) // NOLINT
// {
// 	std::vector<std::vector<COLOR_STRING>> terminal_data_res;
// 	m_terminator_view_model->terminal_data.subscribe([&](const std::vector<COLOR_STRING>& data)
// 	{
// 		terminal_data_res.emplace_back(data);
// 	});
//
// 	std::vector<bool> terminator_enablement_res;
// 	m_terminator_view_model->terminator_enablement.subscribe([&](const bool should_enable)
// 	{
// 		terminator_enablement_res.emplace_back(should_enable);
// 	});
//
// 	unsigned int terminator_set_focus_res = 0;
// 	m_terminator_view_model->terminator_set_focus.subscribe([&]()
// 	{
// 		terminator_set_focus_res++;
// 	});
//
// 	ON_CALL(*m_mock_terminator, Execute_terminator_command(_)).WillByDefault(Return(std::vector<COLOR_STRING>{COLOR_STRING{"response to some command"}}));
// 	m_terminator_view_model->Keyboard_shortcut_was_pressed(true, 'R');
//
// 	ASSERT_TRUE(terminal_data_res.size() == 2);
// 	ASSERT_TRUE(
// 		terminal_data_res[0][0].data == std::string("git log") + "\n" +
// 		"I will not print it out here, its kinda long, but please do be welcomed to take a look at the Forester tab.\n");
// 	ASSERT_TRUE(terminal_data_res[0][0].data == std::string("git log") + "\n" +
// 		"I will not print it out here, its kinda long, but please do be welcomed to take a look at the Forester tab.\n");
// 	ASSERT_TRUE(terminal_data_res[1][0].data == "response to some command");
//
// 	ASSERT_TRUE(terminator_enablement_res.size() == 2);
// 	ASSERT_TRUE(terminator_enablement_res[0] == false);
// 	ASSERT_TRUE(terminator_enablement_res[1] == true);
//
// 	ASSERT_TRUE(terminator_set_focus_res == 1);
// }
//
// TEST_F(TerminatorViewModelTest, Keyboard_shortcut_was_pressed_G) // NOLINT
// {
// 	unsigned int terminator_set_focus_res = 0;
// 	m_terminator_view_model->terminator_set_focus.subscribe([&]()
// 	{
// 		terminator_set_focus_res++;
// 	});
//
// 	m_terminator_view_model->Keyboard_shortcut_was_pressed(true, 'G');
//
// 	ASSERT_TRUE(terminator_set_focus_res == 1);
// }
