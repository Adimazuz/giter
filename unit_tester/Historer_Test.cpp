// #include "gtest/gtest.h"
// #include "gmock/gmock.h"
// #include "Model_mocks.h"
// #include <windows.h>
// #include "../model/Historer.h"
//
// using namespace testing;
//
//
// class HistorerTest : public Test
// {
// protected:
// 	HistorerTest()
// 	{
// 		m_historer_empty = std::make_shared<Historer>();
//
// 		m_historer = std::make_shared<Historer>();
// 		m_historer->Add("ancient");
// 		m_historer->Add("middle");
// 		m_historer->Add("young");
// 	}
//
// 	~HistorerTest() = default;
//
// 	std::shared_ptr<Historer> m_historer_empty;
//
// 	std::shared_ptr<Historer> m_historer;
// };
//
// TEST_F(HistorerTest, Get_earlier_empty) // NOLINT
// {
// 	std::string res = m_historer_empty->Get_earlier();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer_empty->Get_earlier();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer_empty->Get_earlier();
// 	ASSERT_TRUE(res.empty() == true);
// }
//
// TEST_F(HistorerTest, Get_later_empty) // NOLINT
// {
// 	std::string res = m_historer_empty->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer_empty->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer_empty->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
// }
//
// TEST_F(HistorerTest, Get_earlier_Get_later) // NOLINT
// {
// 	std::string res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer->Get_earlier();
// 	ASSERT_TRUE(res == "young");
//
// 	res = m_historer->Get_earlier();
// 	ASSERT_TRUE(res == "middle");
//
// 	res = m_historer->Get_earlier();
// 	ASSERT_TRUE(res == "ancient");
//
// 	res = m_historer->Get_earlier();
// 	ASSERT_TRUE(res == "ancient");
//
// 	res = m_historer->Get_earlier();
// 	ASSERT_TRUE(res == "ancient");
//
// 	res = m_historer->Get_earlier();
// 	ASSERT_TRUE(res == "ancient");
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res == "middle");
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res == "young");
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer->Get_earlier();
// 	ASSERT_TRUE(res == "young");
//
// 	res = m_historer->Get_earlier();
// 	ASSERT_TRUE(res == "middle");
//
// 	res = m_historer->Get_earlier();
// 	ASSERT_TRUE(res == "ancient");
//
// 	res = m_historer->Get_earlier();
// 	ASSERT_TRUE(res == "ancient");
//
// 	res = m_historer->Get_earlier();
// 	ASSERT_TRUE(res == "ancient");
//
// 	res = m_historer->Get_earlier();
// 	ASSERT_TRUE(res == "ancient");
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res == "middle");
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res == "young");
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
// }
//
// TEST_F(HistorerTest, Add) // NOLINT
// {
// 	m_historer->Add("new");
//
// 	std::string res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer->Get_earlier();
// 	ASSERT_TRUE(res == "new");
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_historer->Get_later();
// 	ASSERT_TRUE(res.empty() == true);
// }
