// #include "gtest/gtest.h"
// #include "gmock/gmock.h"
// #include "LibGiter.h"
// #include "LowLevelGitApi.h"
// #include <fstream>
// #include "Model_mocks.h"
// #include <windows.h>
//
//
// using namespace testing;
//
// class LibGiterTest : public Test
// {
// protected:
// 	LibGiterTest()
// 	{
// 		std::filesystem::path path_to_repo = std::filesystem::current_path().parent_path();
// 		m_mock_low_level_git_api = std::make_shared<NiceMock<MockLowLevelGitApi>>();
//
// 		// Constructor.
// 		EXPECT_CALL(*m_mock_low_level_git_api, Git_libgit2_init()).Times(1).WillOnce(Return(1));
// 		EXPECT_CALL(*m_mock_low_level_git_api, Git_repository_open(_, StrEq(path_to_repo.string()))).Times(1).WillOnce(Return(1));
//
// 		git_strarray ref_list;
// 		ref_list.count = 0;
// 		ON_CALL(*m_mock_low_level_git_api, Git_reference_list(_, _)).WillByDefault(DoAll(SetArgPointee<0>(ref_list), Return(1)));
//
// 		ON_CALL(*m_mock_low_level_git_api, Git_strarray_free(_)).WillByDefault(Return());
//
// 		// Destructor.
// 		EXPECT_CALL(*m_mock_low_level_git_api, Git_repository_free(_)).Times(1);
// 		EXPECT_CALL(*m_mock_low_level_git_api, Git_libgit2_shutdown()).Times(1).WillOnce(Return(1));
//
// 		// This is the beginning of complete tree mocking.
// 		git_oid oid;
// 		oid.id[0] = 42;
// 		oid.id[1] = 43;
// 		oid.id[2] = 44;
// 		ON_CALL(*m_mock_low_level_git_api, Git_oid_fromstr(NotNull(), StrEq("2a444003c954a61e342bb323051534b7cb39c002")))
// 			.WillByDefault(DoAll(SetArgPointee<0>(oid), Return(1)));
//
// 		m_lib_giter = std::make_shared<LibGiter>(path_to_repo, m_mock_low_level_git_api.get());
// 	}
//
// 	~LibGiterTest()
// 	{
// 		try
// 		{
// 			std::filesystem::remove_all(R"(.git)");
// 		}
// 		catch (...)
// 		{
// 		}
// 	}
//
// 	std::shared_ptr<NiceMock<MockLowLevelGitApi>> m_mock_low_level_git_api;
// 	std::shared_ptr<LibGiter> m_lib_giter;
// };
//
// TEST_F(LibGiterTest, ctor_test) // NOLINT
// {
// }
//
// TEST_F(LibGiterTest, Update_all_refs_no_refs) // NOLINT
// {
// 	git_strarray ref_list;
// 	ref_list.count = 0;
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_list(_, _)).WillByDefault(DoAll(SetArgPointee<0>(ref_list), Return(1)));
//
// 	ON_CALL(*m_mock_low_level_git_api, Git_strarray_free(_)).WillByDefault(Return());
//
// 	m_lib_giter->Update_all_refs();
//
// 	const std::vector<std::string> res_local = m_lib_giter->Get_local_refs("");
// 	ASSERT_TRUE(res_local.empty() == true);
//
// 	const std::vector<std::string> res_remote = m_lib_giter->Get_remote_refs("");
// 	ASSERT_TRUE(res_remote.empty() == true);
//
// 	const std::vector<std::string> res_tag = m_lib_giter->Get_tag_refs("");
// 	ASSERT_TRUE(res_tag.empty() == true);
//
// 	const std::vector<std::string> res_local_all = m_lib_giter->Get_all_local_refs();
// 	ASSERT_TRUE(res_local_all.empty() == true);
// }
//
// TEST_F(LibGiterTest, Update_all_refs_local_only) // NOLINT
// {
// 	git_strarray ref_list;
// 	char* a[1];
// 	ref_list.strings = a;
// 	ref_list.count = 1;
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_list(_, _)).WillByDefault(DoAll(SetArgPointee<0>(ref_list), Return(1)));
//
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_lookup(_, _, _)).WillByDefault(Return(1));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_type(_)).WillByDefault(Return(GIT_REF_OID));
// 	const char* bla = "s000000000000000000000000000000000000000";
// 	ON_CALL(*m_mock_low_level_git_api, Git_oid_fmt(_, _)).WillByDefault(DoAll(SetArgPointee<0>(*bla), Return()));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_is_tag(_)).WillByDefault(Return(0));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_shorthand(_)).WillByDefault(Return("some_ref_name"));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_is_branch(_)).WillByDefault(Return(1));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_is_remote(_)).WillByDefault(Return(0));
//
// 	ON_CALL(*m_mock_low_level_git_api, Git_strarray_free(_)).WillByDefault(Return());
//
// 	m_lib_giter->Update_all_refs();
//
// 	const std::vector<std::string> res_local = m_lib_giter->Get_local_refs("s000000000000000000000000000000000000000");
// 	ASSERT_TRUE(res_local.size() == 1);
// 	ASSERT_TRUE(res_local[0] == "some_ref_name");
//
// 	const std::vector<std::string> res_remote = m_lib_giter->Get_remote_refs("s000000000000000000000000000000000000000");
// 	ASSERT_TRUE(res_remote.empty() == true);
//
// 	const std::vector<std::string> res_tag = m_lib_giter->Get_tag_refs("s000000000000000000000000000000000000000");
// 	ASSERT_TRUE(res_tag.empty() == true);
//
// 	const std::vector<std::string> res_local_all = m_lib_giter->Get_all_local_refs();
// 	ASSERT_TRUE(res_local_all.size() == 1);
// 	ASSERT_TRUE(res_local_all[0] == "some_ref_name");
// }
//
// TEST_F(LibGiterTest, Update_all_refs_remote_only) // NOLINT
// {
// 	git_strarray ref_list;
// 	char* a[1];
// 	ref_list.strings = a;
// 	ref_list.count = 1;
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_list(_, _)).WillByDefault(DoAll(SetArgPointee<0>(ref_list), Return(1)));
//
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_lookup(_, _, _)).WillByDefault(Return(1));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_type(_)).WillByDefault(Return(GIT_REF_OID));
// 	const char* bla = "s000000000000000000000000000000000000000";
// 	ON_CALL(*m_mock_low_level_git_api, Git_oid_fmt(_, _)).WillByDefault(DoAll(SetArgPointee<0>(*bla), Return()));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_is_tag(_)).WillByDefault(Return(0));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_shorthand(_)).WillByDefault(Return("remote ref"));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_is_branch(_)).WillByDefault(Return(0));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_is_remote(_)).WillByDefault(Return(1));
//
// 	ON_CALL(*m_mock_low_level_git_api, Git_strarray_free(_)).WillByDefault(Return());
//
// 	m_lib_giter->Update_all_refs();
//
// 	const std::vector<std::string> res_local = m_lib_giter->Get_local_refs("s000000000000000000000000000000000000000");
// 	ASSERT_TRUE(res_local.empty() == true);
//
// 	const std::vector<std::string> res_remote = m_lib_giter->Get_remote_refs("s000000000000000000000000000000000000000");
// 	ASSERT_TRUE(res_remote.size() == 1);
// 	ASSERT_TRUE(res_remote[0] == "remote ref");
//
// 	const std::vector<std::string> res_tag = m_lib_giter->Get_tag_refs("s000000000000000000000000000000000000000");
// 	ASSERT_TRUE(res_tag.empty() == true);
//
// 	const std::vector<std::string> res_local_all = m_lib_giter->Get_all_local_refs();
// 	ASSERT_TRUE(res_local_all.empty() == true);
// }
//
// TEST_F(LibGiterTest, Update_all_refs_all_types) // NOLINT
// {
// 	git_strarray ref_list;
// 	char* a[1];
// 	ref_list.strings = a;
// 	ref_list.count = 3;
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_list(_, _)).WillByDefault(DoAll(SetArgPointee<0>(ref_list), Return(1)));
//
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_lookup(_, _, _)).WillByDefault(Return(1));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_type(_)).WillByDefault(Return(GIT_REF_OID));
// 	const char* bla = "s000000000000000000000000000000000000000";
// 	ON_CALL(*m_mock_low_level_git_api, Git_oid_fmt(_, _)).WillByDefault(DoAll(SetArgPointee<0>(*bla), Return()));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_is_tag(_)).WillByDefault(Return(0));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_shorthand(_)).WillByDefault(Return("some_ref_name"));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_is_branch(_)).WillByDefault(Return(1));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_is_remote(_)).WillByDefault(Return(1));
// 	ON_CALL(*m_mock_low_level_git_api, Git_reference_is_tag(_)).WillByDefault(Return(1));
//
// 	ON_CALL(*m_mock_low_level_git_api, Git_strarray_free(_)).WillByDefault(Return());
//
// 	m_lib_giter->Update_all_refs();
//
// 	const std::vector<std::string> res_local = m_lib_giter->Get_local_refs("s000000000000000000000000000000000000000");
// 	ASSERT_TRUE(res_local.size() == 1);
// 	ASSERT_TRUE(res_local[0] == "some_ref_name");
//
// 	const std::vector<std::string> res_remote = m_lib_giter->Get_remote_refs("s000000000000000000000000000000000000000");
// 	ASSERT_TRUE(res_remote.size() == 1);
// 	ASSERT_TRUE(res_remote[0] == "some_ref_name");
//
// 	const std::vector<std::string> res_tag = m_lib_giter->Get_tag_refs("s000000000000000000000000000000000000000");
// 	ASSERT_TRUE(res_tag.size() == 1);
// 	ASSERT_TRUE(res_tag[0] == "some_ref_name");
//
// 	const std::vector<std::string> res_local_all = m_lib_giter->Get_all_local_refs();
// 	ASSERT_TRUE(res_local_all.size() == 1);
// 	ASSERT_TRUE(res_local_all[0] == "some_ref_name");
// }
//
// TEST_F(LibGiterTest, Get_number_of_parents_empty_sha1) // NOLINT
// {
// 	const unsigned int res = m_lib_giter->Get_number_of_parents("");
//
// 	ASSERT_TRUE(res == 0);
// }
//
// TEST_F(LibGiterTest, Get_number_of_parents) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_oid_fromstr(_, _));
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_commit_lookup(_, _, _));
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_commit_parentcount(_)).WillOnce(Return(42));
//
// 	const unsigned int res = m_lib_giter->Get_number_of_parents("2a444003c954a61e342bb323051534b7cb39c002");
//
// 	ASSERT_TRUE(res == 42);
// }
//
// TEST_F(LibGiterTest, Get_commit_message) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_low_level_git_api, Get_commit_message(StrEq("sha1_0"), _)).WillRepeatedly(Return("commit_message_0"));
//
// 	const std::string res = m_lib_giter->Get_commit_message("sha1_0");
//
// 	ASSERT_TRUE(res == "commit_message_0");
// }
//
// TEST_F(LibGiterTest, Get_current_branch) // NOLINT
// {
// 	std::filesystem::create_directories(R"(.git)");
//
// 	std::ofstream head_file;
// 	head_file.open(R"(.git\HEAD)");
// 	head_file << "ref: refs/heads/master\n";
// 	head_file.close();
//
// 	const std::string res = m_lib_giter->Get_current_branch();
//
// 	ASSERT_TRUE(res == "master");
// }
//
// TEST_F(LibGiterTest, Get_staged_failure) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_list_new(_, _, _)).WillOnce(Return(42));
//
// 	try
// 	{
// 		std::vector<std::pair<std::string, std::string>> res = m_lib_giter->Get_staged();
// 	}
// 	catch (std::exception& exc)
// 	{
// 		ASSERT_TRUE(std::string(exc.what()) == "Catastrophic failure!");
// 		SUCCEED();
// 	}
// 	catch (...)
// 	{
// 		FAIL();
// 	}
// }
//
// TEST_F(LibGiterTest, Get_unstaged_failure) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_list_new(_, _, _)).WillOnce(Return(42));
//
// 	try
// 	{
// 		std::vector<std::pair<std::string, std::string>> res = m_lib_giter->Get_unstaged();
// 	}
// 	catch (std::exception& exc)
// 	{
// 		ASSERT_TRUE(std::string(exc.what()) == "Catastrophic failure!");
// 		SUCCEED();
// 	}
// 	catch (...)
// 	{
// 		FAIL();
// 	}
// }
//
// TEST_F(LibGiterTest, Get_untracked_failure) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_list_new(_, _, _)).WillOnce(Return(42));
//
// 	try
// 	{
// 		std::vector<std::pair<std::string, std::string>> res = m_lib_giter->Get_untracked();
// 	}
// 	catch (std::exception& exc)
// 	{
// 		ASSERT_TRUE(std::string(exc.what()) == "Catastrophic failure!");
// 		SUCCEED();
// 	}
// 	catch (...)
// 	{
// 		FAIL();
// 	}
// }
//
// TEST_F(LibGiterTest, Get_staged) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_list_new(_, _, _)).WillOnce(Return(0));
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_list_entrycount(_)).WillOnce(Return(7));
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_list_free(_));
//
// 	git_status_entry bla{};
// 	bla.status = GIT_STATUS_INDEX_MODIFIED;
// 	git_diff_delta head_to_index{};
// 	bla.head_to_index = &head_to_index;
// 	bla.head_to_index->old_file.path = "old_file";
// 	bla.index_to_workdir = &head_to_index;
// 	bla.index_to_workdir->new_file.path = "new_file";
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_byindex(_, Le(1))).WillRepeatedly(Return(&bla));
//
// 	git_status_entry bla2{};
// 	bla2.status = GIT_STATUS_INDEX_NEW;
// 	bla2.head_to_index = &head_to_index;
// 	bla2.head_to_index->old_file.path = "old_file";
// 	bla2.index_to_workdir = &head_to_index;
// 	bla2.index_to_workdir->new_file.path = "new_file";
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_byindex(_, Eq(2))).WillRepeatedly(Return(&bla2));
//
// 	git_status_entry bla3{};
// 	bla3.status = GIT_STATUS_INDEX_DELETED;
// 	bla3.head_to_index = &head_to_index;
// 	bla3.head_to_index->old_file.path = "old_file";
// 	bla3.index_to_workdir = &head_to_index;
// 	bla3.index_to_workdir->new_file.path = "new_file";
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_byindex(_, Eq(3))).WillRepeatedly(Return(&bla3));
//
// 	git_status_entry bla4{};
// 	bla4.status = GIT_STATUS_INDEX_RENAMED;
// 	bla4.head_to_index = &head_to_index;
// 	bla4.head_to_index->old_file.path = "old_file";
// 	bla4.index_to_workdir = &head_to_index;
// 	bla4.index_to_workdir->new_file.path = "new_file";
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_byindex(_, Eq(4))).WillRepeatedly(Return(&bla4));
//
// 	git_status_entry bla5{};
// 	bla5.status = GIT_STATUS_INDEX_TYPECHANGE;
// 	bla5.head_to_index = &head_to_index;
// 	bla5.head_to_index->old_file.path = "old_file";
// 	bla5.index_to_workdir = &head_to_index;
// 	bla5.index_to_workdir->new_file.path = "new_file";
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_byindex(_, Eq(5))).WillRepeatedly(Return(&bla5));
//
// 	git_status_entry bla6{};
// 	bla6.status = GIT_STATUS_CURRENT;
// 	bla6.head_to_index = &head_to_index;
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_byindex(_, Eq(6))).WillRepeatedly(Return(&bla6));
//
// 	const std::vector<std::pair<std::string, std::string>> res = m_lib_giter->Get_staged();
//
// 	ASSERT_TRUE(res.size() == 6);
// 	ASSERT_TRUE(res.at(0).first == "modified");
// 	ASSERT_TRUE(res.at(0).second == "new_file");
// 	ASSERT_TRUE(res.at(1).first == "modified");
// 	ASSERT_TRUE(res.at(1).second == "new_file");
// 	ASSERT_TRUE(res.at(2).first == "new file");
// 	ASSERT_TRUE(res.at(2).second == "new_file");
// 	ASSERT_TRUE(res.at(3).first == "deleted");
// 	ASSERT_TRUE(res.at(3).second == "new_file");
// 	ASSERT_TRUE(res.at(4).first == "renamed");
// 	ASSERT_TRUE(res.at(4).second == "new_file");
// 	ASSERT_TRUE(res.at(5).first == "typechange");
// 	ASSERT_TRUE(res.at(5).second == "new_file");
// }
//
// TEST_F(LibGiterTest, Get_unstaged) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_list_new(_, _, _)).WillOnce(Return(0));
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_list_entrycount(_)).WillOnce(Return(7));
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_list_free(_));
//
// 	git_status_entry bla{};
// 	bla.status = GIT_STATUS_WT_MODIFIED;
// 	git_diff_delta head_to_index{};
// 	bla.head_to_index = &head_to_index;
// 	bla.head_to_index->old_file.path = "old_file";
// 	bla.index_to_workdir = &head_to_index;
// 	bla.index_to_workdir->new_file.path = "new_file";
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_byindex(_, Le(1))).WillRepeatedly(Return(&bla));
//
// 	git_status_entry bla2{};
// 	bla2.status = GIT_STATUS_CURRENT;
// 	git_diff_delta head_to_index2{};
// 	bla2.head_to_index = &head_to_index2;
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_byindex(_, Eq(2))).WillRepeatedly(Return(&bla2));
//
// 	git_status_entry bla3{};
// 	bla3.status = GIT_STATUS_CONFLICTED;
// 	git_diff_delta head_to_index3{};
// 	bla3.head_to_index = &head_to_index3;
// 	bla3.index_to_workdir = nullptr;
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_byindex(_, Eq(3))).WillRepeatedly(Return(&bla3));
//
// 	git_status_entry bla4{};
// 	bla4.status = GIT_STATUS_WT_DELETED;
// 	bla4.head_to_index = &head_to_index;
// 	bla4.index_to_workdir = &head_to_index;;
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_byindex(_, Eq(4))).WillRepeatedly(Return(&bla4));
//
// 	git_status_entry bla5{};
// 	bla5.status = GIT_STATUS_WT_RENAMED;
// 	bla5.head_to_index = &head_to_index;
// 	bla5.index_to_workdir = &head_to_index;;
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_byindex(_, Eq(5))).WillRepeatedly(Return(&bla5));
//
// 	git_status_entry bla6{};
// 	bla6.status = GIT_STATUS_WT_TYPECHANGE;
// 	bla6.head_to_index = &head_to_index;
// 	bla6.index_to_workdir = &head_to_index;;
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_byindex(_, Eq(6))).WillRepeatedly(Return(&bla6));
//
// 	const std::vector<std::pair<std::string, std::string>> res = m_lib_giter->Get_unstaged();
//
// 	ASSERT_TRUE(res.size() == 5);
// 	ASSERT_TRUE(res.at(0).first == "modified");
// 	ASSERT_TRUE(res.at(0).second == "new_file");
// 	ASSERT_TRUE(res.at(1).first == "modified");
// 	ASSERT_TRUE(res.at(1).second == "new_file");
// 	ASSERT_TRUE(res.at(2).first == "deleted");
// 	ASSERT_TRUE(res.at(2).second == "new_file");
// 	ASSERT_TRUE(res.at(3).first == "renamed");
// 	ASSERT_TRUE(res.at(3).second == "new_file");
// 	ASSERT_TRUE(res.at(4).first == "typechange");
// 	ASSERT_TRUE(res.at(4).second == "new_file");
// }
//
// TEST_F(LibGiterTest, Get_untracked) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_list_new(_, _, _)).WillOnce(Return(0));
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_list_entrycount(_)).WillOnce(Return(2));
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_list_free(_));
// 	git_status_entry bla{};
// 	bla.status = GIT_STATUS_WT_NEW;
// 	git_diff_delta head_to_index{};
// 	bla.head_to_index = &head_to_index;
// 	bla.head_to_index->old_file.path = "old_file";
// 	bla.index_to_workdir = &head_to_index;
// 	bla.index_to_workdir->new_file.path = "new_file";
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_status_byindex(_, _)).WillRepeatedly(Return(&bla));
//
// 	const std::vector<std::pair<std::string, std::string>> res = m_lib_giter->Get_untracked();
//
// 	ASSERT_TRUE(res.size() == 2);
// 	ASSERT_TRUE(res.at(0).first == "untracked");
// 	ASSERT_TRUE(res.at(0).second == "old_file");
// 	ASSERT_TRUE(res.at(1).first == "untracked");
// 	ASSERT_TRUE(res.at(1).second == "old_file");
// }
//
// TEST_F(LibGiterTest, Get_date_empty) // NOLINT
// {
// 	const std::string res = m_lib_giter->Get_date("");
//
// 	ASSERT_TRUE(res.empty() == true);
// }
//
// TEST_F(LibGiterTest, Get_date) // NOLINT
// {
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_oid_fromstr(_, StrEq("sha1"))).Times(1);
// 	EXPECT_CALL(*m_mock_low_level_git_api, Git_commit_lookup(_, _, _)).Times(1);
//
// 	ON_CALL(*m_mock_low_level_git_api, Git_commit_time(_)).WillByDefault(Return(0));
//
// 	const std::string res = m_lib_giter->Get_date("sha1");
//
// 	ASSERT_TRUE(res.find("Thu, 01 Jan 1970 ") != std::string::npos);
// }
