#pragma once
#include <string>
#pragma warning( push )
#pragma warning( disable : 4129)
// ReSharper disable StringLiteralTypo
const std::string GIT_SUB_COMMANDS_STRING =
	"See 'git help <command>' to read about a specific subcommand                                  \n"
	"Main Porcelain Commands                                                                       \n"
	"   add                  Add file contents to the index                                        \n"
	"   am                   Apply a series of patches from a mailbox                              \n"
	"   archive              Create an archive of files from a named tree                          \n"
	"   bisect               Use binary search to find the commit that introduced a bug            \n"
	"   branch               List, create, or delete branches                                      \n"
	"   bundle               Move objects and refs by archive                                      \n"
	"   checkout             Switch branches or restore working tree files                         \n"
	"   cherry-pick          Apply the changes introduced by some existing commits                 \n"
	"   citool               Graphical alternative to git-commit                                   \n"
	"   clean                Remove untracked files from the working tree                          \n"
	"   clone                Clone a repository into a new directory                               \n"
	"   commit               Record changes to the repository                                      \n"
	"   describe             Give an object a human readable name based on an available ref        \n"
	"   diff                 Show changes between commits, commit and working tree, etc            \n"
	"   fetch                Download objects and refs from another repository                     \n"
	"   format-patch         Prepare patches for e-mail submission                                 \n"
	"   gc                   Cleanup unnecessary files and optimize the local repository           \n"
	"   gitk                 The Git repository browser                                            \n"
	"   grep                 Print lines matching a pattern                                        \n"
	"   gui                  A portable graphical interface to Git                                 \n"
	"   init                 Create an empty Git repository or reinitialize an existing one        \n"
	"   log                  Show commit logs                                                      \n"
	"   merge                Join two or more development histories together                       \n"
	"   mv                   Move or rename a file, a directory, or a symlink                      \n"
	"   notes                Add or inspect object notes                                           \n"
	"   pull                 Fetch from and integrate with another repository or a local branch    \n"
	"   push                 Update remote refs along with associated objects                      \n"
	"   range-diff           Compare two commit ranges (e.g. two versions of a branch)             \n"
	"   rebase               Reapply commits on top of another base tip                            \n"
	"   reset                Reset current HEAD to the specified state                             \n"
	"   revert               Revert some existing commits                                          \n"
	"   rm                   Remove files from the working tree and from the index                 \n"
	"   shortlog             Summarize 'git log' output                                            \n"
	"   show                 Show various types of objects                                         \n"
	"   stash                Stash the changes in a dirty working directory away                   \n"
	"   status               Show the working tree status                                          \n"
	"   submodule            Initialize, update or inspect submodules                              \n"
	"   tag                  Create, list, delete or verify a tag object signed with GPG           \n"
	"   worktree             Manage multiple working trees                                         \n"
	"Ancillary Commands / Manipulators                                                             \n"
	"   config               Get and set repository or global options                              \n"
	"   fast-export          Git data exporter                                                     \n"
	"   fast-import          Backend for fast Git data importers                                   \n"
	"   filter-branch        Rewrite branches                                                      \n"
	"   mergetool            Run merge conflict resolution tools to resolve merge conflicts        \n"
	"   pack-refs            Pack heads and tags for efficient repository access                   \n"
	"   prune                Prune all unreachable objects from the object database                \n"
	"   reflog               Manage reflog information                                             \n"
	"   remote               Manage set of tracked repositories                                    \n"
	"   repack               Pack unpacked objects in a repository                                 \n"
	"   replace              Create, list, delete refs to replace objects                          \n"
	"Ancillary Commands / Interrogators                                                            \n"
	"   annotate             Annotate file lines with commit information                           \n"
	"   blame                Show what revision and author last modified each line of a file       \n"
	"   count-objects        Count unpacked number of objects and their disk consumption           \n"
	"   difftool             Show changes using common diff tools                                  \n"
	"   fsck                 Verifies the connectivity and validity of the objects in the database \n"
	"   gitweb               Git web interface (web frontend to Git repositories)                  \n"
	"   help                 Display help information about Git                                    \n"
	"   instaweb             Instantly browse your working repository in gitweb                    \n"
	"   merge-tree           Show three-way merge without touching index                           \n"
	"   rerere               Reuse recorded resolution of conflicted merges                        \n"
	"   show-branch          Show branches and their commits                                       \n"
	"   verify-commit        Check the GPG signature of commits                                    \n"
	"   verify-tag           Check the GPG signature of tags                                       \n"
	"   whatchanged          Show logs with difference each commit introduces                      \n"
	"Interacting with Others                                                                       \n"
	"   archimport           Import a GNU Arch repository into Git                                 \n"
	"   cvsexportcommit      Export a single commit to a CVS checkout                              \n"
	"   cvsimport            Salvage your data out of another SCM people love to hate              \n"
	"   cvsserver            A CVS server emulator for Git                                         \n"
	"   imap-send            Send a collection of patches from stdin to an IMAP folder             \n"
	"   p4                   Import from and submit to Perforce repositories                       \n"
	"   quiltimport          Applies a quilt patchset onto the current branch                      \n"
	"   request-pull         Generates a summary of pending changes                                \n"
	"   send-email           Send a collection of patches as emails                                \n"
	"   svn                  Bidirectional operation between a Subversion repository and Git       \n"
	"Low-level Commands / Manipulators                                                             \n"
	"   apply                Apply a patch to files and/or to the index                            \n"
	"   checkout-index       Copy files from the index to the working tree                         \n"
	"   commit-graph         Write and verify Git commit-graph files                               \n"
	"   commit-tree          Create a new commit object                                            \n"
	"   hash-object          Compute object ID and optionally creates a blob from a file           \n"
	"   index-pack           Build pack index file for an existing packed archive                  \n"
	"   merge-file           Run a three-way file merge                                            \n"
	"   merge-index          Run a merge for files needing merging                                 \n"
	"   mktag                Creates a tag object                                                  \n"
	"   mktree               Build a tree-object from ls-tree formatted text                       \n"
	"   multi-pack-index     Write and verify multi-pack-indexes                                   \n"
	"   pack-objects         Create a packed archive of objects                                    \n"
	"   prune-packed         Remove extra objects that are already in pack files                   \n"
	"   read-tree            Reads tree information into the index                                 \n"
	"   symbolic-ref         Read, modify and delete symbolic refs                                 \n"
	"   unpack-objects       Unpack objects from a packed archive                                  \n"
	"   update-index         Register file contents in the working tree to the index               \n"
	"   update-ref           Update the object name stored in a ref safely                         \n"
	"   write-tree           Create a tree object from the current index                           \n"
	"Low-level Commands / Interrogators                                                            \n"
	"   cat-file             Provide content or type and size information for repository objects   \n"
	"   cherry               Find commits yet to be applied to upstream                            \n"
	"   diff-files           Compares files in the working tree and the index                      \n"
	"   diff-index           Compare a tree to the working tree or index                           \n"
	"   diff-tree            Compares the content and mode of blobs found via two tree objects     \n"
	"   for-each-ref         Output information on each ref                                        \n"
	"   get-tar-commit-id    Extract commit ID from an archive created using git-archive           \n"
	"   ls-files             Show information about files in the index and the working tree        \n"
	"   ls-remote            List references in a remote repository                                \n"
	"   ls-tree              List the contents of a tree object                                    \n"
	"   merge-base           Find as good common ancestors as possible for a merge                 \n"
	"   name-rev             Find symbolic names for given revs                                    \n"
	"   pack-redundant       Find redundant pack files                                             \n"
	"   rev-list             Lists commit objects in reverse chronological order                   \n"
	"   rev-parse            Pick out and massage parameters                                       \n"
	"   show-index           Show packed archive index                                             \n"
	"   show-ref             List references in a local repository                                 \n"
	"   unpack-file          Creates a temporary file with a blob's contents                       \n"
	"   var                  Show a Git logical variable                                           \n"
	"   verify-pack          Validate packed Git archive files                                     \n"
	"Low-level Commands / Synching Repositories                                                    \n"
	"   daemon               A really simple server for Git repositories                           \n"
	"   fetch-pack           Receive missing objects from another repository                       \n"
	"   http-backend         Server side implementation of Git over HTTP                           \n"
	"   send-pack            Push objects over Git protocol to another repository                  \n"
	"   update-server-info   Update auxiliary info file to help dumb servers                       \n"
	"Low-level Commands / Internal Helpers                                                         \n"
	"   check-attr           Display gitattributes information                                     \n"
	"   check-ignore         Debug gitignore / exclude files                                       \n"
	"   check-mailmap        Show canonical names and email addresses of contacts                  \n"
	"   check-ref-format     Ensures that a reference name is well formed                          \n"
	"   column               Display data in columns                                               \n"
	"   credential           Retrieve and store user credentials                                   \n"
	"   credential-store     Helper to store credentials on disk                                   \n"
	"   fmt-merge-msg        Produce a merge commit message                                        \n"
	"   interpret-trailers   Add or parse structured information in commit messages                \n"
	"   mailinfo             Extracts patch and authorship from a single e-mail message            \n"
	"   mailsplit            Simple UNIX mbox splitter program                                     \n"
	"   merge-one-file       The standard helper program to use with git-merge-index               \n"
	"   patch-id             Compute unique ID for a patch                                         \n"
	"   sh-i18n              Git's i18n setup code for shell scripts                               \n"
	"   sh-setup             Common Git shell script setup code                                    \n"
	"   stripspace           Remove unnecessary whitespace                                         \n"
	"External commands                                                                             \n"
	"   askyesno\n"
	"   credential-helper-selector\n"
	"   flow\n"
	"   lfs\n";
