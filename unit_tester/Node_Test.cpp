// #include "gtest/gtest.h"
// #include "gmock/gmock.h"
// #include "Tree.h"
// #include <fstream>
// #include "Model_mocks.h"
// #include <windows.h>
//
// using namespace testing;
//
//
// class NodeTest : public Test
// {
// protected:
// 	NodeTest()
// 	{
// 		std::filesystem::path path_to_repo = std::filesystem::current_path().parent_path();
// 		m_mock_lib_giter = std::make_shared<NiceMock<MockLibGiter>>(path_to_repo);
//
// 		ON_CALL(*m_mock_lib_giter, Get_commit_message(StrEq("sha1"))).WillByDefault(Return("commit_message"));
// 		ON_CALL(*m_mock_lib_giter, Get_local_refs(StrEq("sha1"))).WillByDefault(Return(std::vector<std::string>{"local_ref_1", "local_ref_2"}));
// 		ON_CALL(*m_mock_lib_giter, Get_remote_refs(StrEq("sha1"))).WillByDefault(Return(std::vector<std::string>{"remote_ref_1", "remote_ref_2"}));
//
// 		m_node = std::make_shared<Node>(0, "sha1", 2, COMMIT, true);
// 		m_node_commit_no_ref = std::make_shared<Node>(0, "some_sha1_hash", 2, COMMIT, true);
// 	}
//
// 	~NodeTest() = default;
//
// 	std::shared_ptr<NiceMock<MockLibGiter>> m_mock_lib_giter;
//
// 	std::shared_ptr<Node> m_node;
// 	std::shared_ptr<Node> m_node_commit_no_ref;
// };
//
// TEST_F(NodeTest, ctor_test) // NOLINT
// {
// 	ASSERT_TRUE(m_node->Get_column() == 1);
// 	ASSERT_TRUE(m_node->Get_sha1() == "sha1");
// 	ASSERT_TRUE(m_node->Get_type() == COMMIT);
// 	ASSERT_TRUE(m_node->Is_tip() == true);
// }
//
// //TEST_F(NodeTest, operator_double_equals_negative) // NOLINT
// //{
// //	const auto temp_node = std::make_shared<Node>(7, "sha2", 42, LINE, false);
// //
// //	ASSERT_FALSE(*m_node == *temp_node);
// //}
// //
// //TEST_F(NodeTest, operator_double_equals_positive) // NOLINT
// //{
// //	const auto temp_node = std::make_shared<Node>(0, "sha1", 2, COMMIT, true);
// //
// //	ASSERT_TRUE(*m_node == *temp_node);
// //}
//
// TEST_F(NodeTest, Get_column)
// {
// 	ASSERT_TRUE(m_node->Get_column() == 1);
// }
//
// TEST_F(NodeTest, Is_tip)
// {
// 	ASSERT_TRUE(m_node->Is_tip() == true);
// }
//
// TEST_F(NodeTest, Get_sha1)
// {
// 	ASSERT_TRUE(m_node->Get_sha1() == "sha1");
// }
//
// TEST_F(NodeTest, Get_type)
// {
// 	ASSERT_TRUE(m_node->Get_type() == COMMIT);
// }
//
// TEST_F(NodeTest, Get_color)
// {
// 	ASSERT_TRUE(m_node->Get_color() == 2);
// }
