// #include "gtest/gtest.h"
// #include "gmock/gmock.h"
// //#include "Tree.h"
// #include <fstream>
// #include "Model_mocks.h"
// #include <windows.h>
// #include "../model/Completer.h"
// #include "git_sub_commands_string.h"
//
// using namespace testing;
//
//
// class CompleterTest : public Test
// {
// protected:
// 	CompleterTest()
// 	{
// 		std::filesystem::path path_to_repo = std::filesystem::current_path().parent_path();
// 		m_mock_lib_giter = std::make_shared<NiceMock<MockLibGiter>>(path_to_repo);
// 		m_mock_terminator = std::make_shared<NiceMock<MockTerminator>>(m_mock_lib_giter.get(), m_terminator_colorizer.get());
//
// 		ON_CALL(*m_mock_terminator, Execute_shell_command(StrEq(L"git help -a"))).WillByDefault(Return(Break_into_lines(GIT_SUB_COMMANDS_STRING)));
// 		ON_CALL(*m_mock_lib_giter, Get_all_local_refs()).WillByDefault(Return(std::vector<std::string>{
// 			"master", "feature-bla", "feature-one", "feature-two", "feature-three", "random_string"
// 		}));
//
// 		m_completer = std::make_shared<Completer>(m_mock_terminator.get(), m_mock_lib_giter.get());
// 	}
//
// 	~CompleterTest() = default;
//
// 	std::shared_ptr<NiceMock<MockLibGiter>> m_mock_lib_giter;
// 	std::shared_ptr<NiceMock<TerminatorColorizer>> m_terminator_colorizer;
// 	std::shared_ptr<NiceMock<MockTerminator>> m_mock_terminator;
//
// 	std::shared_ptr<Completer> m_completer;
//
// private:
//
// 	// ReSharper disable once CppMemberFunctionMayBeStatic
// 	[[nodiscard]] std::vector<std::string> Break_into_lines(const std::string& data) const
// 	{
// 		// This code is faster the using streams.
// 		const char separator = '\n';
// 		std::string::size_type b = 0;
// 		std::vector<std::string> lines;
//
// 		while ((b = data.find_first_not_of(separator, b)) != std::string::npos)
// 		{
// 			const auto e = data.find_first_of(separator, b);
// 			std::string temp = data.substr(b, e - b);
// 			lines.push_back(temp + "\n");
// 			b = e;
// 		}
//
// 		return lines;
// 	}
// };
//
// TEST_F(CompleterTest, Get_completion) // NOLINT
// {
// 	const std::vector<std::string> res = m_completer->Get_completion("git checko");
//
// 	ASSERT_TRUE(res.size() == 1);
// 	ASSERT_TRUE(res[0] == "ut");
// }
//
// TEST_F(CompleterTest, Get_completion_test_empty_string) // NOLINT
// {
// 	const std::vector<std::string> res = m_completer->Get_completion("");
//
// 	ASSERT_TRUE(res.empty() == true);
// }
//
// TEST_F(CompleterTest, Get_completion_test_git_only) // NOLINT
// {
// 	const std::vector<std::string> res = m_completer->Get_completion("git");
//
// 	ASSERT_TRUE(res.empty() == true);
// }
//
// TEST_F(CompleterTest, Get_completion_one_branch) // NOLINT
// {
// 	ON_CALL(*m_mock_lib_giter, Get_all_local_refs()).WillByDefault(Return(std::vector<std::string>{"master"}));
//
// 	std::vector<std::string> res = m_completer->Get_completion("git checkout mas");
// 	ASSERT_TRUE(res.size() == 1);
// 	ASSERT_TRUE(res[0] == "ter");
// }
//
// TEST_F(CompleterTest, Get_completion_test_branch_completion) // NOLINT
// {
// 	//"master", "feature-bla", "feature-one", "feature-two", "feature-three", "random_string"
// 	std::vector<std::string> res = m_completer->Get_completion("git checkout mas");
// 	ASSERT_TRUE(res.size() == 1);
// 	ASSERT_TRUE(res[0] == "ter");
//
// 	res = m_completer->Get_completion("git checkout master");
// 	ASSERT_TRUE(res.empty() == true);
//
// 	res = m_completer->Get_completion("git checkout feature");
// 	ASSERT_TRUE(res.size() == 1);
// 	ASSERT_TRUE(res[0] == "-");
//
// 	res = m_completer->Get_completion("git checkout feature-");
// 	ASSERT_TRUE(res.size() == 4);
// 	ASSERT_TRUE(res[0] == "feature-bla");
// 	ASSERT_TRUE(res[1] == "feature-one");
// 	ASSERT_TRUE(res[2] == "feature-three");
// 	ASSERT_TRUE(res[3] == "feature-two");
//
// 	res = m_completer->Get_completion("git checkout feature-b");
// 	ASSERT_TRUE(res.size() == 1);
// 	ASSERT_TRUE(res[0] == "la");
//
// 	res = m_completer->Get_completion("git checkout feature-o");
// 	ASSERT_TRUE(res.size() == 1);
// 	ASSERT_TRUE(res[0] == "ne");
//
// 	res = m_completer->Get_completion("git checkout feature-t");
// 	ASSERT_TRUE(res.size() == 2);
// 	ASSERT_TRUE(res[0] == "feature-three");
// 	ASSERT_TRUE(res[1] == "feature-two");
//
// 	res = m_completer->Get_completion("git checkout feature-tw");
// 	ASSERT_TRUE(res.size() == 1);
// 	ASSERT_TRUE(res[0] == "o");
//
// 	res = m_completer->Get_completion("git checkout feature-th");
// 	ASSERT_TRUE(res.size() == 1);
// 	ASSERT_TRUE(res[0] == "ree");
// }
