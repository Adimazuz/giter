#pragma once
#include <observable/subject.hpp>


typedef struct filer_view_model FILER_VIEW_MODEL_T;

// ReSharper disable CppInconsistentNaming

inline observable::subject<void()> refresh;
extern "C" void filer_view_model_notify_refresh();

extern "C" void filer_view_model_get_history(FILER_VIEW_MODEL_T*, const char* file_path);
extern "C" unsigned int filer_view_model_history_amount(FILER_VIEW_MODEL_T*);
extern "C" char* filer_view_model_history_commit_message(FILER_VIEW_MODEL_T*, unsigned int line_index);
extern "C" char* filer_view_model_history_date(FILER_VIEW_MODEL_T*, unsigned int line_index);
extern "C" char* filer_view_model_history_sha1(FILER_VIEW_MODEL_T*, unsigned int line_index);

extern "C" void filer_view_model_get_diff(FILER_VIEW_MODEL_T*, const char* sha1, const char* sha1_previous, const char* file_path);
extern "C" unsigned int filer_view_model_get_diff_amount(FILER_VIEW_MODEL_T*);
extern "C" const char* filer_view_model_get_diff_data(FILER_VIEW_MODEL_T*, unsigned int);
extern "C" unsigned int filer_view_model_get_diff_red(FILER_VIEW_MODEL_T*, unsigned int);
extern "C" unsigned int filer_view_model_get_diff_green(FILER_VIEW_MODEL_T*, unsigned int);
extern "C" unsigned int filer_view_model_get_diff_blue(FILER_VIEW_MODEL_T*, unsigned int);
extern "C" unsigned int filer_view_model_get_diff_background_red(FILER_VIEW_MODEL_T*, unsigned int);
extern "C" unsigned int filer_view_model_get_diff_background_green(FILER_VIEW_MODEL_T*, unsigned int);
extern "C" unsigned int filer_view_model_get_diff_background_blue(FILER_VIEW_MODEL_T*, unsigned int);

extern "C" const char* filer_view_model_get_repo_path(FILER_VIEW_MODEL_T*);

// ReSharper restore CppInconsistentNaming
