#include "TerminatorViewModelFFI.h"


// ReSharper disable once CppInconsistentNaming
void terminator_view_model_notify_terminator_enablement(const bool enabled)
{
	terminator_enablement.notify(enabled);
}

// ReSharper disable once CppInconsistentNaming
void terminator_view_model_notify_terminal_data()
{
	terminal_data.notify();
}

// ReSharper disable once CppInconsistentNaming
void terminator_view_model_notify_terminator_set_focus()
{
	terminator_set_focus.notify();
}

// ReSharper disable once CppInconsistentNaming
void terminator_view_model_notify_clear_input_line()
{
	clear_input_line.notify();
}
