#pragma once
#include <observable/subject.hpp>


/**
 * \brief Denotes the type of a node.
 */
enum NODE_TYPE { COMMIT, LINE, HORIZONTAL, LEFT_TO_RIGHT, LEFT_TO_RIGHT_NARROW, RIGHT_TO_LEFT, RIGHT_TO_LEFT_NARROW };

inline observable::subject<void(unsigned int line)> new_line;
inline observable::subject<void()> clear_the_tree;
inline observable::subject<void(bool)> tree_enablement;
inline observable::subject<void()> tree_selection;
inline observable::subject<void()> copy_paste_was_pressed;

typedef struct tree_view_model TREE_VIEW_MODEL_T;

// ReSharper disable CppInconsistentNaming

extern "C" void tree_view_model_update_the_tree(TREE_VIEW_MODEL_T*);
//extern "C" void tree_view_model_keyboard_shortcut_was_pressed(TREE_VIEW_MODEL_T*, const bool was_ctrl_pressed, const unsigned key_code);
extern "C" unsigned int tree_view_model_get_nodes_amount(TREE_VIEW_MODEL_T*, unsigned int line_index);
extern "C" unsigned int tree_view_model_get_commit_message_offset(TREE_VIEW_MODEL_T*, unsigned int line_index);

extern "C" unsigned int tree_view_model_get_local_refs_amount(TREE_VIEW_MODEL_T*, unsigned int line_index);
extern "C" char* tree_view_model_get_local_ref(TREE_VIEW_MODEL_T*, unsigned int line_index, unsigned int ref_index);
extern "C" unsigned int tree_view_model_get_remote_refs_amount(TREE_VIEW_MODEL_T*, unsigned int line_index);
extern "C" char* tree_view_model_get_remote_ref(TREE_VIEW_MODEL_T*, unsigned int line_index, unsigned int ref_index);
extern "C" unsigned int tree_view_model_get_tag_refs_amount(TREE_VIEW_MODEL_T*, unsigned int line_index);
extern "C" char* tree_view_model_get_tag_ref(TREE_VIEW_MODEL_T*, unsigned int line_index, unsigned int ref_index);

extern "C" char* tree_view_model_get_commit_message(TREE_VIEW_MODEL_T*, unsigned int line_index);
extern "C" unsigned int tree_view_model_get_color(TREE_VIEW_MODEL_T*, unsigned int line_index, unsigned int node_index);
extern "C" unsigned int tree_view_model_get_node_type(TREE_VIEW_MODEL_T*, unsigned int line_index, unsigned int node_index);
extern "C" bool tree_view_model_get_is_first_line(TREE_VIEW_MODEL_T*, unsigned int line_index);
extern "C" bool tree_view_model_has_at_least_one_ref(TREE_VIEW_MODEL_T*, unsigned int line_index);
extern "C" int tree_view_model_get_column(TREE_VIEW_MODEL_T*, unsigned int line_index, unsigned int node_index);
extern "C" bool tree_view_model_is_tip(TREE_VIEW_MODEL_T*, unsigned int line_index, unsigned int node_index);
extern "C" char* tree_view_model_get_date(TREE_VIEW_MODEL_T*, unsigned int line_index);
extern "C" char* tree_view_model_get_sha1(TREE_VIEW_MODEL_T*, unsigned int line_index);

extern "C" void tree_view_model_notify_new_line(unsigned int bla);
extern "C" void tree_view_model_notify_clear_the_tree();
extern "C" void tree_view_model_notify_tree_enablement(bool);
extern "C" void tree_view_model_notify_tree_selection();
extern "C" void tree_view_model_copy_paste_was_pressed();

// ReSharper restore CppInconsistentNaming
