#pragma once
#include <string>
#include <observable/subject.hpp>


// ReSharper disable CppInconsistentNaming
inline observable::subject<void()> unstaged;
extern "C" void statuser_view_model_notify_unstaged();

inline observable::subject<void()> staged;
extern "C" void statuser_view_model_notify_staged();

inline observable::subject<void()> untracked;
extern "C" void statuser_view_model_notify_untracked();

inline observable::subject<void()> commit_message_clear;
extern "C" void statuser_view_model_notify_clear_commit_message();
// ReSharper restore CppInconsistentNaming

typedef struct statuser_view_model STATUSER_VIEW_MODEL_T;
// ReSharper disable CppInconsistentNaming
extern "C" unsigned int statuser_view_model_get_unstaged_amount(STATUSER_VIEW_MODEL_T*);
extern "C" const char* statuser_view_model_get_unstaged_file_name(STATUSER_VIEW_MODEL_T*, unsigned int);
extern "C" const char* statuser_view_model_get_unstaged_file_status(STATUSER_VIEW_MODEL_T*, unsigned int);

extern "C" unsigned int statuser_view_model_get_staged_amount(STATUSER_VIEW_MODEL_T*);
extern "C" const char* statuser_view_model_get_staged_file_name(STATUSER_VIEW_MODEL_T*, unsigned int);
extern "C" const char* statuser_view_model_get_staged_file_status(STATUSER_VIEW_MODEL_T*, unsigned int);

extern "C" unsigned int statuser_view_model_get_untracked_amount(STATUSER_VIEW_MODEL_T*);
extern "C" const char* statuser_view_model_get_untracked_file_name(STATUSER_VIEW_MODEL_T*, unsigned int);
extern "C" const char* statuser_view_model_get_untracked_file_status(STATUSER_VIEW_MODEL_T*, unsigned int);

extern "C" void statuser_view_model_get_diff(STATUSER_VIEW_MODEL_T*, const char*);
extern "C" unsigned int statuser_view_model_get_diff_amount(STATUSER_VIEW_MODEL_T*);
extern "C" const char* statuser_view_model_get_diff_data(STATUSER_VIEW_MODEL_T*, unsigned int);
extern "C" unsigned int statuser_view_model_get_diff_red(STATUSER_VIEW_MODEL_T*, unsigned int);
extern "C" unsigned int statuser_view_model_get_diff_green(STATUSER_VIEW_MODEL_T*, unsigned int);
extern "C" unsigned int statuser_view_model_get_diff_blue(STATUSER_VIEW_MODEL_T*, unsigned int);
extern "C" unsigned int statuser_view_model_get_diff_background_red(STATUSER_VIEW_MODEL_T*, unsigned int);
extern "C" unsigned int statuser_view_model_get_diff_background_green(STATUSER_VIEW_MODEL_T*, unsigned int);
extern "C" unsigned int statuser_view_model_get_diff_background_blue(STATUSER_VIEW_MODEL_T*, unsigned int);

extern "C" void statuser_view_model_stage_button_was_clicked(STATUSER_VIEW_MODEL_T*);
extern "C" void statuser_view_model_reset_button_was_clicked(STATUSER_VIEW_MODEL_T*, const char*);
extern "C" void statuser_view_model_unstage_button_was_clicked(STATUSER_VIEW_MODEL_T*);
extern "C" void statuser_view_model_unstaged_double_click(STATUSER_VIEW_MODEL_T*, const char*);
extern "C" void statuser_view_model_staged_double_click(STATUSER_VIEW_MODEL_T*, const char*);
extern "C" void statuser_view_model_commit_button_was_clicked(STATUSER_VIEW_MODEL_T*, const char*);
extern "C" void statuser_view_model_push_button_was_clicked(STATUSER_VIEW_MODEL_T*);
// ReSharper restore CppInconsistentNaming
