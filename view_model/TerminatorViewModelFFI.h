#pragma once
#include <string>
#include <observable/subject.hpp>


// ReSharper disable CppInconsistentNaming
inline observable::subject<void(bool)> terminator_enablement;
extern "C" void terminator_view_model_notify_terminator_enablement(bool);

//inline observable::subject<void(unsigned int line_number)> terminal_data;
inline observable::subject<void()> terminal_data;
extern "C" void terminator_view_model_notify_terminal_data();

inline observable::subject<void()> terminator_set_focus;
extern "C" void terminator_view_model_notify_terminator_set_focus();

inline observable::subject<void()> clear_input_line;
extern "C" void terminator_view_model_notify_clear_input_line();
// ReSharper restore CppInconsistentNaming

typedef struct terminator_view_model TERMINATOR_VIEW_MODEL_T;
// ReSharper disable CppInconsistentNaming
extern "C" unsigned int terminator_view_model_get_res_amount(TERMINATOR_VIEW_MODEL_T*);
extern "C" const char* terminator_view_model_get_res(TERMINATOR_VIEW_MODEL_T*, unsigned int);
extern "C" unsigned int terminator_view_model_get_res_red(TERMINATOR_VIEW_MODEL_T*, unsigned int);
extern "C" unsigned int terminator_view_model_get_res_green(TERMINATOR_VIEW_MODEL_T*, unsigned int);
extern "C" unsigned int terminator_view_model_get_res_blue(TERMINATOR_VIEW_MODEL_T*, unsigned int);

extern "C" unsigned int terminator_view_model_get_normal_terminator_color_red(TERMINATOR_VIEW_MODEL_T*);
extern "C" unsigned int terminator_view_model_get_normal_terminator_color_green(TERMINATOR_VIEW_MODEL_T*);
extern "C" unsigned int terminator_view_model_get_normal_terminator_color_blue(TERMINATOR_VIEW_MODEL_T*);
extern "C" void terminator_view_model_press_enter(TERMINATOR_VIEW_MODEL_T*, const char* data);
extern "C" void terminator_view_model_go_earlier_in_history(TERMINATOR_VIEW_MODEL_T*);
extern "C" void terminator_view_model_go_later_in_history(TERMINATOR_VIEW_MODEL_T*);
extern "C" void terminator_view_model_append_data_to_terminator(TERMINATOR_VIEW_MODEL_T*, const char* data);

// ReSharper restore CppInconsistentNaming

// ReSharper disable once CppInconsistentNaming
struct COLOR final
{
	// ReSharper disable CppInconsistentNaming
	unsigned int red;
	unsigned int green;
	unsigned int blue;
	// ReSharper restore CppInconsistentNaming
};

// ReSharper disable once CppInconsistentNaming
struct COLOR_STRING final
{
	// ReSharper disable CppInconsistentNaming
	std::string data;
	COLOR color{0, 0, 0};
	COLOR background_color{255, 255, 255};
	// ReSharper restore CppInconsistentNaming
};
