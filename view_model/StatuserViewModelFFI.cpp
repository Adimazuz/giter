#include "StatuserViewModelFFI.h"


// ReSharper disable once CppInconsistentNaming
void statuser_view_model_notify_unstaged()
{
	unstaged.notify();
}

// ReSharper disable once CppInconsistentNaming
void statuser_view_model_notify_staged()
{
	staged.notify();
}

// ReSharper disable once CppInconsistentNaming
void statuser_view_model_notify_untracked()
{
	untracked.notify();
}

// ReSharper disable once CppInconsistentNaming
void statuser_view_model_notify_clear_commit_message()
{
	commit_message_clear.notify();
}
