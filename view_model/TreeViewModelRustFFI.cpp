#include "TreeViewModelRustFFI.h"


// ReSharper disable once CppInconsistentNaming
void tree_view_model_notify_new_line(const unsigned int bla)
{
	new_line.notify(bla);
}

// ReSharper disable once CppInconsistentNaming
void tree_view_model_notify_clear_the_tree()
{
	clear_the_tree.notify();
}

// ReSharper disable once CppInconsistentNaming
void tree_view_model_notify_tree_enablement(const bool enable)
{
	tree_enablement.notify(enable);
}

// ReSharper disable once CppInconsistentNaming
void tree_view_model_notify_tree_selection()
{
	tree_selection.notify();
}

// ReSharper disable once CppInconsistentNaming
void tree_view_model_copy_paste_was_pressed()
{
	copy_paste_was_pressed.notify();
}
