#pragma once
#include "TreeViewModelRustFFI.h"
#include "TerminatorViewModelFFI.h"
#include "FilerViewModelFFI.h"


typedef struct view_model VIEW_MODEL_T;

// ReSharper disable CppInconsistentNaming

extern "C" VIEW_MODEL_T* view_model_new(const char*);
extern "C" void view_model_free(VIEW_MODEL_T*);

extern "C" TREE_VIEW_MODEL_T* view_model_get_tree_view_model(VIEW_MODEL_T*);
extern "C" TERMINATOR_VIEW_MODEL_T* view_model_get_terminator_view_model(VIEW_MODEL_T*);
extern "C" STATUSER_VIEW_MODEL_T* view_model_get_statuser_view_model(VIEW_MODEL_T*);
extern "C" FILER_VIEW_MODEL_T* view_model_get_filer_view_model(VIEW_MODEL_T*);
extern "C" void view_model_keyboard_shortcut_was_pressed(VIEW_MODEL_T*, bool, unsigned int, bool);

// ReSharper restore CppInconsistentNaming
