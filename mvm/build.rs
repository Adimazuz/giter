use std::env;
use std::path::Path;
use std::env::VarError;

fn main() {
    let mut lib_path = String::new();
    let profile = std::env::var("PROFILE").unwrap();
    match profile.as_str() {
        "debug" => { lib_path = String::from("..\\x64\\Debug"); }
        "release" => { lib_path = String::from("..\\x64\\Release"); }
        _ => (),
    }

    println!("cargo:rustc-link-search=native={}", lib_path);

    let test_build = env::var("TEST_BUILD");//.unwrap();
    match test_build {
        Ok(value) => {
            if value == "1".to_string() {
                println!("cargo:rustc-link-lib=static=view_model");
            }
        }
        Err(_) => {}
    }
}