use std::cell::RefCell;
use std::rc::Rc;
use crate::terminator::ITerminator;
use std::collections::BTreeSet;
use crate::model::lib_giter::ilib_giter::ILibGiter;


pub(crate) struct Completer {
    m_terminator: Rc<RefCell<dyn ITerminator>>,
    m_lib_giter: Rc<RefCell<dyn ILibGiter>>,
    m_completions: BTreeSet<String>,
}

impl Completer {
    pub(crate) fn new(terminator: Rc<RefCell<dyn ITerminator>>, lib_giter: Rc<RefCell<dyn ILibGiter>>) -> Completer {
        let mut completer = Completer {
            m_terminator: terminator,
            m_lib_giter: lib_giter,
            m_completions: Default::default(),
        };

        let res = completer.m_terminator.borrow_mut().execute_shell_command("git help -a");

        let git_sub_commands = completer.parse_git_sub_commands(&res);
        for git_sub_command in git_sub_commands {
            completer.m_completions.insert(git_sub_command);
        }

        completer.m_lib_giter.borrow_mut().update_all_refs();
        let local_refs = completer.m_lib_giter.borrow().get_all_local_refs();
        for local_ref in local_refs {
            completer.m_completions.insert(local_ref);
        }

        completer
    }

    fn parse_git_sub_commands(&self, data: &Vec<String>) -> Vec<String> {
        let mut res = vec![];

        for datum in data {
            match datum.find("   ") {
                None => continue,
                Some(_) => {
                    let trimmed: String = datum.chars().skip(3).collect();

                    let left_overs_position = trimmed.find(' ');
                    match left_overs_position {
                        None => {}
                        Some(_) => {
                            let bla = trimmed.find(' ');
                            match bla {
                                None => {}
                                Some(pos) => {
                                    let bbb: String = trimmed.chars().take(pos).collect();

                                    res.push(bbb);
                                }
                            }
                        }
                    }
                }
            }
        }

        res
    }

    pub fn get_completion(&self, value: &str) -> Vec<String> {
        let mut data = value.to_string();
        let mut res = vec![];

        if data.is_empty() == true || data.find(' ').is_none() {
            return res;
        }

        let mut parsed_value = String::new();
        match data.rfind(' ') {
            None => {}
            Some(pos) => {
                let bla: String = data.chars().skip(pos).collect();
                data = bla;
                parsed_value = data.chars().skip(1).collect();
            }
        }

        if self.m_completions.contains(parsed_value.as_str()) == true {
            return res;
        }

        let mut starts_with = self.get_everything_that_starts_with(String::from(&parsed_value));

        if starts_with.len() == 1 {
            let bla = starts_with[0].chars().skip(parsed_value.chars().count()).collect();
            starts_with[0] = bla;

            res.push(starts_with[0].to_owned());
        }

        if starts_with.len() > 1 {
            let mut the_completion: String = "".to_string();

            let mut index = parsed_value.chars().count();
            while self.is_every_character_equal(&starts_with, index) == true {
                the_completion = the_completion + starts_with[0].chars().nth(index).unwrap().to_string().as_str();

                index = index + 1;

                starts_with = self.get_everything_that_starts_with(parsed_value.to_owned() + the_completion.as_str());
            }

            if the_completion.is_empty() == true {
                for value in starts_with {
                    res.push(value);
                }
            } else {
                res.push(the_completion);
            }
        }

        res
    }

    fn get_everything_that_starts_with(&self, data: String) -> Vec<String> {
        let mut res = vec![];

        for completion in &self.m_completions {
            match completion.find(&data) {
                None => {}
                Some(pos) => {
                    if pos == 0 {
                        res.push(String::from(completion));
                    }
                }
            }
        }

        res
    }

    fn is_every_character_equal(&self, data: &Vec<String>, index: usize) -> bool {
        let das_character: char = char::from(0);
//        if index >= data[0].chars().count() {
//            let das_character = 0;
//        } else {
//            let das_character = data[0].chars().nth(index).unwrap();
//        }

        for datum in data {
            if datum.chars().nth(index).unwrap() != das_character {
                return false;
            }
        }

        return true;
    }
}