use mockall::*;
use std::collections::HashMap;
use std::path::Path;


#[automock]
pub trait ILibGiter {
    fn update_all_refs(&mut self);
    fn get_local_refs(&self, sha1: &str) -> Vec<String>;
    fn get_refs(&self, hash_map: &HashMap<String, String>, sha1: &str) -> Vec<String>;
    fn get_remote_refs(&self, sha1: &str) -> Vec<String>;
    fn get_tag_refs(&self, sha1: &str) -> Vec<String>;
    fn get_all_local_refs(&self) -> Vec<String>;
    fn get_commit_message(&self, sha1: &str) -> String;
    fn get_current_branch(&self) -> String;
    fn get_number_of_parents(&self, sha1: &str) -> u64;
    fn get_date(&self, sha1: &str) -> String;
    fn get_staged(&self) -> Vec<(String, String)>;
    fn get_unstaged(&self) -> Vec<(String, String)>;
    fn get_untracked(&self) -> Vec<(String, String)>;
    fn get_repo_path(&self) -> &Path;
}