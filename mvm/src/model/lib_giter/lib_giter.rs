extern crate libc;

use std::collections::HashMap;
use crate::low_level_git_api::ILowLevelGitApi;
use mockall::predicate::*;
use std::path::Path;
use crate::model::lib_giter::ilib_giter::ILibGiter;


pub struct LibGiter {
    m_local_refs: HashMap<String, String>,
    m_remote_refs: HashMap<String, String>,
    m_tag_refs: HashMap<String, String>,
    m_low_level_git_api: Box<dyn ILowLevelGitApi>,
}

impl LibGiter {
//    pub(crate) fn default() -> Rc<RefCell<dyn ILibGiter>> {
//        let lib_giter = LibGiter {
//            m_low_level_git_api: Box::new(LowLevelGitApi::default()),
//            m_local_refs: Default::default(),
//            m_remote_refs: Default::default(),
//            m_tag_refs: Default::default(),
//        };
//
//        Rc::new(RefCell::new(lib_giter))
//    }

    pub(crate) fn new(low_level_git_api: Box<dyn ILowLevelGitApi>) -> LibGiter {
        LibGiter {
            m_low_level_git_api: low_level_git_api,
            m_local_refs: Default::default(),
            m_remote_refs: Default::default(),
            m_tag_refs: Default::default(),
        }
    }
}

impl ILibGiter for LibGiter {
    fn update_all_refs(&mut self) {
        self.m_local_refs.clear();
        self.m_remote_refs.clear();
        self.m_tag_refs.clear();

        let all_refs = self.m_low_level_git_api.find_all_refs();

        self.m_local_refs = all_refs.0;
        self.m_remote_refs = all_refs.1;
        self.m_tag_refs = all_refs.2;
    }

    fn get_local_refs(&self, sha1: &str) -> Vec<String> {
        LibGiter::get_refs(self, &self.m_local_refs, sha1)
    }

    fn get_refs(&self, hash_map: &HashMap<String, String>, sha1: &str) -> Vec<String> {
        let mut res: Vec<String>;
        res = Default::default();

        for reference in hash_map {
            if reference.1 == sha1 {
                res.push(String::from(reference.0));
            }
        }

        res
    }

    fn get_remote_refs(&self, sha1: &str) -> Vec<String> {
        LibGiter::get_refs(&self, &self.m_remote_refs, sha1)
    }

    fn get_tag_refs(&self, sha1: &str) -> Vec<String> {
        LibGiter::get_refs(&self, &self.m_tag_refs, sha1)
    }

    fn get_all_local_refs(&self) -> Vec<String> {
        let mut res: Vec<String> = Default::default();

        for bla in self.m_local_refs.keys() {
            res.push(String::from(bla));
        }

        res
    }

    fn get_commit_message(&self, sha1: &str) -> String {
        self.m_low_level_git_api.get_commit_message(sha1)
    }

    fn get_current_branch(&self) -> String {
        self.m_low_level_git_api.get_current_branch()
    }

    fn get_number_of_parents(&self, sha1: &str) -> u64 {
        self.m_low_level_git_api.get_number_of_parents(sha1)
    }

    fn get_date(&self, sha1: &str) -> String {
        self.m_low_level_git_api.get_date(sha1)
    }

    fn get_staged(&self) -> Vec<(String, String)> {
        let data = self.m_low_level_git_api.get_statuses();

        let mut res = vec![];

        for (k, v) in data.0.iter() {
            res.push((k.to_owned(), v.to_owned()));
        }

        res
    }

    fn get_unstaged(&self) -> Vec<(String, String)> {
        let data = self.m_low_level_git_api.get_statuses();

        let mut res = vec![];

        for (k, v) in data.1.iter() {
            res.push((k.to_owned(), v.to_owned()));
        }

        res
    }

    fn get_untracked(&self) -> Vec<(String, String)> {
        let data = self.m_low_level_git_api.get_statuses();

        let mut res = vec![];

        for (k, v) in data.2.iter() {
            res.push((k.to_owned(), v.to_owned()));
        }

        res
    }

//    fn get_history_diff(&self, commit_sha1: &str, file_path: &Path) -> Vec<String> {
//        self.m_low_level_git_api.get_history_diff(commit_sha1, file_path)
//    }

    fn get_repo_path(&self) -> &Path {
        self.m_low_level_git_api.get_repo_path()
    }
}

//#[cfg(test)]
//mod tests {
//    use super::*;
//
//    struct MockLowLevelGitApi {}
//
//    impl ILowLevelGitApi for MockLowLevelGitApi {
//        fn find_all_refs(&self) -> (HashMap<String, String>, HashMap<String, String>, HashMap<String, String>) {
//            (Default::default(), Default::default(), Default::default())
//        }
//
//        fn get_commit_message(&self, sha1: &str) -> String {
//            "".to_string()
//        }
//
//        fn get_current_branch(&self) -> String {
//            unimplemented!()
//        }
//
//        fn get_number_of_parents(&self, sha1: &str) -> u64 {
//            unimplemented!()
//        }
//
//        fn get_statuses(&self) -> (HashMap<String, String>, HashMap<String, String>, HashMap<String, String>) {
//            unimplemented!()
//        }
//
//        fn get_date(&self, sha1: &str) -> String {
//            unimplemented!()
//        }
//    }
//
//    fn setup() -> LibGiter {
//        let mock_low_level_git_api = Box::new(MockLowLevelGitApi {});
//        LibGiter::new(mock_low_level_git_api)
//    }
//
//    #[test]
//    fn update_all_refs_no_refs() {
//        let mut lib_giter = setup();
//
//        lib_giter.update_all_refs();
//
//        let local_refs = lib_giter.get_local_refs("");
//        assert_eq!(local_refs.is_empty(), true);
//
//        let remote_refs = lib_giter.get_remote_refs("");
//        assert_eq!(remote_refs.is_empty(), true);
//
//        let tag_refs = lib_giter.get_tag_refs("");
//        assert_eq!(tag_refs.is_empty(), true);
//
//        let all_local_refs = lib_giter.get_all_local_refs();
//        assert_eq!(all_local_refs.is_empty(), true);
//    }
//}