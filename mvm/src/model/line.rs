use crate::node::Node;
use std::cell::RefCell;
use std::rc::Rc;
use crate::node::NodeType::Commit;
use crate::model::lib_giter::ilib_giter::ILibGiter;


pub(crate) struct Line {
    m_nodes: Vec<Node>,
    m_lib_giter: Rc<RefCell<dyn ILibGiter>>,
    m_sha1: String,
    m_index: usize,
    m_commit_message: String,
    m_ref_local: Vec<String>,
    m_ref_remote: Vec<String>,
    m_ref_tag: Vec<String>,
    m_has_ref: bool,
    m_date: String,
    m_is_first_line: bool,
}

impl Line {
//    pub(crate) fn default() -> Line {
//        Line {
//            m_nodes: vec![],
//            m_lib_giter: LibGiter::default(),
//            m_sha1: "".to_string(),
//            m_index: 0,
//            m_commit_message: "".to_string(),
//            m_ref_local: vec![],
//            m_ref_remote: vec![],
//            m_ref_tag: vec![],
//            m_has_ref: false,
//            m_date: "".to_string(),
//            m_is_first_line: false,
//        }
//    }

    pub(crate) fn new(nodes: Vec<Node>, lib_giter: Rc<RefCell<dyn ILibGiter>>, index: u64) -> Line {
        let mut line = Line {
            m_nodes: nodes,
            m_lib_giter: lib_giter,
            m_sha1: "".to_string(),
            m_index: index as usize,
            m_commit_message: "".to_string(),
            m_ref_local: vec![],
            m_ref_remote: vec![],
            m_ref_tag: vec![],
            m_has_ref: false,
            m_date: "".to_string(),
            m_is_first_line: false,
        };

        line.m_sha1 = String::from(line.search_nodes_for_sha1());

        line.m_commit_message = line.m_lib_giter.borrow().get_commit_message(&line.m_sha1);

        line.m_ref_local = line.m_lib_giter.borrow().get_local_refs(&line.m_sha1);
        line.m_ref_remote = line.m_lib_giter.borrow().get_remote_refs(&line.m_sha1);
        line.m_ref_tag = line.m_lib_giter.borrow().get_tag_refs(&line.m_sha1);

        line.m_has_ref = line.determine_whether_has_any_branch_ref();

        line.m_date = line.m_lib_giter.borrow().get_date(&line.m_sha1);

        line.m_is_first_line = line.determine_whether_first_line(index);

        line
    }

    pub(crate) fn from(line: &Line) -> Line {
        let mut nodes: Vec<Node> = vec![];

        for node in line.get_nodes() {
            nodes.push(Node::from(node));
        }

        Line::new(nodes, Rc::clone(&line.m_lib_giter), line.get_index() as u64)
    }

    fn search_nodes_for_sha1(&self) -> &str {
        for i in 0..self.m_nodes.len() {
            if *self.m_nodes[i].get_type() == Commit {
                return &self.m_nodes[i].get_sha1();
            }
        }

        ""
    }

    fn determine_whether_has_any_branch_ref(&self) -> bool {
        if self.m_ref_local.is_empty() == false || self.m_ref_remote.is_empty() == false {
            return true;
        } else {
            return false;
        }
    }

    fn determine_whether_first_line(&self, index: u64) -> bool {
        if index == 0 {
            return true;
        } else {
            return false;
        }
    }

    fn get_index(&self) -> usize {
        self.m_index
    }

    pub(crate) fn get_local_refs(&self) -> &Vec<String> {
        &self.m_ref_local
    }

    pub(crate) fn get_remote_refs(&self) -> &Vec<String> {
        &self.m_ref_remote
    }

    pub(crate) fn get_tag_refs(&self) -> &Vec<String> {
        &self.m_ref_tag
    }

    pub(crate) fn get_sha1(&self) -> &str {
        &self.m_sha1
    }

    pub(crate) fn get_commit_message(&self) -> &str {
        &self.m_commit_message
    }

    pub(crate) fn has_at_least_one_ref(&self) -> bool {
        self.m_has_ref
    }

    pub(crate) fn get_is_first_line(&self) -> bool {
        self.m_is_first_line
    }

    pub(crate) fn get_nodes(&self) -> &Vec<Node> {
        &self.m_nodes
    }

    pub(crate) fn get_date(&self) -> &str {
        &self.m_date
    }
}