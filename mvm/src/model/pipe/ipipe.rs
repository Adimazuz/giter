use winapi::shared::minwindef::{DWORD, LPVOID, BOOL, LPDWORD};
use winapi::um::minwinbase::{LPSECURITY_ATTRIBUTES, LPOVERLAPPED};
use winapi::um::winnt::{PHANDLE, LPCWSTR, LPWSTR, HANDLE};
use winapi::um::processthreadsapi::{LPPROCESS_INFORMATION, LPSTARTUPINFOW};


pub(crate) trait IPipe {
    fn create_pipe(&self, h_read_pipe: PHANDLE, h_write_pipe: PHANDLE, lp_pipe_attributes: LPSECURITY_ATTRIBUTES, n_size: DWORD) -> bool;
    fn create_process(&self, lp_application_name: LPCWSTR, lp_command_line: LPWSTR, lp_process_attributes: LPSECURITY_ATTRIBUTES, lp_thread_attributes: LPSECURITY_ATTRIBUTES, b_inherit_handles: BOOL, dw_creation_flags: DWORD, lp_environment: LPVOID, lp_current_directory: LPCWSTR, lp_startup_info: LPSTARTUPINFOW, lp_process_information: LPPROCESS_INFORMATION) -> bool;
    fn close_handle(&self, h_object: HANDLE) -> bool;
    fn wait_for_single_object(&self, h_handle: HANDLE, dw_milliseconds: DWORD) -> u32;
    fn peek_named_pipe(&self, h_named_pipe: HANDLE, lp_buffer: LPVOID, n_buffer_size: DWORD, lp_bytes_read: LPDWORD, lp_total_bytes_avail: LPDWORD, lp_bytes_left_this_message: LPDWORD) -> bool;
    fn read_file(&self, h_file: HANDLE, lp_buffer: LPVOID, n_number_of_bytes_to_read: DWORD, lp_number_of_bytes_read: LPDWORD, lp_overlapped: LPOVERLAPPED) -> bool;
}