use winapi::um::namedpipeapi::{CreatePipe, PeekNamedPipe};
use winapi::shared::minwindef::{DWORD, LPVOID, BOOL, LPDWORD};
use winapi::um::minwinbase::{LPSECURITY_ATTRIBUTES, LPOVERLAPPED};
use winapi::um::winnt::{PHANDLE, LPCWSTR, LPWSTR, HANDLE};
use winapi::um::processthreadsapi::{CreateProcessW, LPPROCESS_INFORMATION, LPSTARTUPINFOW};
use winapi::um::handleapi::CloseHandle;
use winapi::um::synchapi::WaitForSingleObject;
use winapi::um::fileapi::ReadFile;
use winapi::ctypes::c_void;
use crate::model::pipe::ipipe::IPipe;


pub(crate) struct Pipe {}

impl Pipe {
    pub(crate) fn new() -> Pipe {
        Pipe {}
    }
}

impl IPipe for Pipe {
    fn create_pipe(&self, h_read_pipe: PHANDLE, h_write_pipe: PHANDLE, lp_pipe_attributes: LPSECURITY_ATTRIBUTES, n_size: DWORD) -> bool {
        unsafe {
            CreatePipe(h_read_pipe, h_write_pipe, lp_pipe_attributes, n_size) != 0
        }
    }

    fn create_process(&self, lp_application_name: LPCWSTR, lp_command_line: LPWSTR, lp_process_attributes: LPSECURITY_ATTRIBUTES,
                      lp_thread_attributes: LPSECURITY_ATTRIBUTES, b_inherit_handles: BOOL, dw_creation_flags: DWORD, lp_environment: LPVOID,
                      lp_current_directory: LPCWSTR, lp_startup_info: LPSTARTUPINFOW, lp_process_information: LPPROCESS_INFORMATION) -> bool {
        unsafe {
            CreateProcessW(lp_application_name, lp_command_line, lp_process_attributes, lp_thread_attributes,
                           b_inherit_handles, dw_creation_flags, lp_environment, lp_current_directory, lp_startup_info, lp_process_information) != 0
        }
    }

    fn close_handle(&self, h_object: *mut c_void) -> bool {
        unsafe {
            CloseHandle(h_object) != 0
        }
    }

    fn wait_for_single_object(&self, h_handle: *mut c_void, dw_milliseconds: u32) -> u32 {
        unsafe {
            WaitForSingleObject(h_handle, dw_milliseconds)
        }
    }

    fn peek_named_pipe(&self, h_named_pipe: *mut c_void, lp_buffer: *mut c_void, n_buffer_size: u32, lp_bytes_read: *mut u32,
                       lp_total_bytes_avail: *mut u32, lp_bytes_left_this_message: *mut u32) -> bool {
        unsafe {
            PeekNamedPipe(h_named_pipe, lp_buffer, n_buffer_size, lp_bytes_read, lp_total_bytes_avail, lp_bytes_left_this_message) != 0
        }
    }

    fn read_file(&self, h_file: HANDLE, lp_buffer: LPVOID, n_number_of_bytes_to_read: DWORD, lp_number_of_bytes_read: LPDWORD, lp_overlapped: LPOVERLAPPED) -> bool {
        unsafe {
            ReadFile(h_file, lp_buffer, n_number_of_bytes_to_read, lp_number_of_bytes_read, lp_overlapped) != 0
        }
    }
}