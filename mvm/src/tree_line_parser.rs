pub(crate) struct TreeLineParser {
    m_line: String,
}

impl TreeLineParser {
    pub(crate) fn new(line: &str) -> TreeLineParser {
        TreeLineParser {
            m_line: line.to_string()
        }
    }

    pub(crate) fn get_sha1_from_line(&self) -> String {
        let sha1_position = self.find_sha1_position();

        if sha1_position == 0 {
            String::new()
        } else {
            self.m_line.chars().skip(sha1_position).take(40).collect()
        }
    }

    fn find_sha1_position(&self) -> usize {
        let mut sha1_position = 0;

        for (i, c) in self.m_line.chars().enumerate() {
            if self.is_legal_graph_character(c.to_string().as_str()) == false {
                sha1_position = i;
                break;
            }
        }

        sha1_position
    }

    fn is_legal_graph_character(&self, character: &str) -> bool {
        if character == "|" || character == "/" || character == "\\" || character == "_" || character == " " || character == "*"
            || character == "-" || character == "." // Very rare case with large number of branches (for example: Linux kernel).
        {
            return true;
        }

        return false;
    }
}