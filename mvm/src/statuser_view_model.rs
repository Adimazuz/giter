use std::rc::Rc;
use std::cell::{RefCell};
use crate::terminator::ITerminator;
use crate::terminator_colorizer::{TerminatorColorizer, ColorString};
use std::ffi::{CString, CStr};
use std::os::raw::c_char;
use crate::terminator_view_model::ITerminatorViewModel;
use crate::model::lib_giter::ilib_giter::ILibGiter;


pub(crate) trait IStatiserViewModel {
    fn stage_button_was_clicked(&mut self);
    fn reset_button_was_clicked(&mut self, file_path: &str);
    fn unstage_button_was_clicked(&mut self);
    fn get_diff(&mut self, file_path: &str) -> &Vec<ColorString>;
    fn update_statuser(&mut self);
    fn unstaged_double_click(&mut self, file_path: &str);
    fn staged_double_click(&mut self, file_path: &str);
    fn commit_button_was_clicked(&mut self, commit_message: &str);
    fn push_button_was_clicked(&mut self);
    fn keyboard_shortcut_was_pressed(&mut self, was_ctrl_pressed: bool, key_code: usize);
}

pub struct StatuserViewModel {
    m_lib_giter: Rc<RefCell<dyn ILibGiter>>,
    m_terminator: Rc<RefCell<dyn ITerminator>>,
    m_terminator_colorizer: Rc<RefCell<TerminatorColorizer>>,
    m_terminator_view_model: Rc<RefCell<dyn ITerminatorViewModel>>,
    m_unstaged: Vec<(String, String)>,
    m_staged: Vec<(String, String)>,
    m_untracked: Vec<(String, String)>,
    m_diff: Vec<ColorString>,
}

impl StatuserViewModel {
    pub(crate) fn new(lib_giter: Rc<RefCell<dyn ILibGiter>>, terminator: Rc<RefCell<dyn ITerminator>>, terminator_colorizer: Rc<RefCell<TerminatorColorizer>>, terminator_view_model: Rc<RefCell<dyn ITerminatorViewModel>>) -> StatuserViewModel {
        StatuserViewModel {
            m_lib_giter: lib_giter,
            m_terminator: terminator,
            m_terminator_colorizer: terminator_colorizer,
            m_terminator_view_model: terminator_view_model,
            m_unstaged: Default::default(),
            m_staged: Default::default(),
            m_untracked: Default::default(),
            m_diff: vec![],
        }
    }

    fn get_diff_text(&mut self, file_path: &str) -> &Vec<ColorString> {
        let bla = self.m_terminator.borrow();
        let data = bla.execute_shell_command(("git diff ".to_owned() + file_path).as_str());

        self.m_diff = self.m_terminator_colorizer.borrow().colorize_single_diff(&data);

        if self.m_diff.is_empty() == true {
            self.m_diff.clear();

            self.m_diff.push(ColorString::new("Nothing to show dude. It's an untracked or staged file. Move along, nothing to see.",
                                              self.m_terminator_colorizer.borrow().get_diff_normal_color(), self.m_terminator_colorizer.borrow().get_diff_background_normal_color()));
            return &self.m_diff;
        }

        if self.m_diff[0].data.find("unknown revision or path not in the working tree").is_some() == true {
            self.m_diff.clear();

            self.m_diff.push(ColorString::new("Nothing to show dude. It's a deleted file. Move along, nothing to see.",
                                              self.m_terminator_colorizer.borrow().get_diff_normal_color(), self.m_terminator_colorizer.borrow().get_diff_background_normal_color()));
            return &self.m_diff;
        }

        &self.m_diff
    }
}

impl IStatiserViewModel for StatuserViewModel {
    fn stage_button_was_clicked(&mut self) {
        let mut bla = String::from("git add .");
        let bla2 = bla.as_mut_str();
        self.m_terminator_view_model.borrow_mut().append_to_input_line_and_press_enter(bla2);

        self.update_statuser();
    }

    fn reset_button_was_clicked(&mut self, file_path: &str) {
        if file_path.is_empty() == true {
            return;
        }

        let mut bla = String::from("git checkout ".to_string() + file_path);
        let bla2 = bla.as_mut_str();
        self.m_terminator_view_model.borrow_mut().append_to_input_line_and_press_enter(bla2);

        self.update_statuser();
    }

    fn unstage_button_was_clicked(&mut self) {
        let mut bla = String::from("git reset");
        let bla2 = bla.as_mut_str();
        self.m_terminator_view_model.borrow_mut().append_to_input_line_and_press_enter(bla2);

        self.update_statuser();
    }

    fn get_diff(&mut self, file_path: &str) -> &Vec<ColorString> {
        if file_path.is_empty() == true {
            self.m_diff.clear();
            return &self.m_diff;
        }

        self.get_diff_text(file_path)
    }

    fn update_statuser(&mut self) {
        self.m_untracked = self.m_lib_giter.borrow().get_untracked();
        self.m_unstaged = self.m_lib_giter.borrow().get_unstaged();
        self.m_staged = self.m_lib_giter.borrow().get_staged();
        unsafe {
            statuser_view_model_notify_unstaged();
            statuser_view_model_notify_untracked();
            statuser_view_model_notify_staged();
        }
    }

    fn unstaged_double_click(&mut self, file_path: &str) {
        let mut bla = String::from("git add ".to_string() + file_path);
        let bla2 = bla.as_mut_str();
        self.m_terminator_view_model.borrow_mut().append_to_input_line_and_press_enter(bla2);

        self.update_statuser();
    }

    fn staged_double_click(&mut self, file_path: &str) {
        let mut bla = String::from("git reset HEAD ".to_string() + file_path);
        let bla2 = bla.as_mut_str();
        self.m_terminator_view_model.borrow_mut().append_to_input_line_and_press_enter(bla2);

        self.update_statuser();
    }

    fn commit_button_was_clicked(&mut self, commit_message: &str) {
        let mut bla;
//        if commit_message == "" {
//            bla = String::from("git commit -m");
//        } else {
//            bla = String::from(String::from("git commit -m \"") + commit_message + "\"");
//        }
        bla = String::from(String::from("git commit -m \"") + commit_message + "\"");
        let bla2 = bla.as_mut_str();
        self.m_terminator_view_model.borrow_mut().append_to_input_line_and_press_enter(bla2);

        unsafe {
            statuser_view_model_notify_clear_commit_message();
        }

        self.update_statuser();
    }

    fn push_button_was_clicked(&mut self) {
        let mut bla = String::from("git push");
        let bla2 = bla.as_mut_str();
        self.m_terminator_view_model.borrow_mut().append_to_input_line_and_press_enter(bla2);

        self.update_statuser();
    }

    fn keyboard_shortcut_was_pressed(&mut self, was_ctrl_pressed: bool, key_code: usize) {
        if was_ctrl_pressed == true && key_code == 0x53 { // The letter S.
            self.update_statuser();
        }
        if was_ctrl_pressed == true && key_code == 0x52 { // The letter R.
            self.update_statuser();
        }
    }
}

extern "C" {
    fn statuser_view_model_notify_unstaged();
}

extern "C" {
    fn statuser_view_model_notify_staged();
}

extern "C" {
    fn statuser_view_model_notify_untracked();
}

extern "C" {
    fn statuser_view_model_notify_clear_commit_message();
}

#[no_mangle]
pub extern fn statuser_view_model_get_unstaged_amount(ptr: *mut StatuserViewModel) -> usize {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    statuser_view_model.m_unstaged.len()
}

#[no_mangle]
pub extern fn statuser_view_model_get_unstaged_file_name(ptr: *mut StatuserViewModel, index: usize) -> *const c_char {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let res = statuser_view_model.m_unstaged[index].0.as_str();

    let s = CString::new(res).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn statuser_view_model_get_unstaged_file_status(ptr: *mut StatuserViewModel, index: usize) -> *const c_char {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let res = statuser_view_model.m_unstaged[index].1.as_str();

    let s = CString::new(res).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn statuser_view_model_get_staged_amount(ptr: *mut StatuserViewModel) -> usize {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    statuser_view_model.m_staged.len()
}

#[no_mangle]
pub extern fn statuser_view_model_get_staged_file_name(ptr: *mut StatuserViewModel, index: usize) -> *const c_char {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let res = statuser_view_model.m_staged[index].0.as_str();

    let s = CString::new(res).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn statuser_view_model_get_staged_file_status(ptr: *mut StatuserViewModel, index: usize) -> *const c_char {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let res = statuser_view_model.m_staged[index].1.as_str();

    let s = CString::new(res).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn statuser_view_model_get_untracked_amount(ptr: *mut StatuserViewModel) -> usize {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    statuser_view_model.m_untracked.len()
}

#[no_mangle]
pub extern fn statuser_view_model_get_untracked_file_name(ptr: *mut StatuserViewModel, index: usize) -> *const c_char {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let res = statuser_view_model.m_untracked[index].0.as_str();

    let s = CString::new(res).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn statuser_view_model_get_untracked_file_status(ptr: *mut StatuserViewModel, index: usize) -> *const c_char {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let res = statuser_view_model.m_untracked[index].1.as_str();

    let s = CString::new(res).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn statuser_view_model_get_diff(ptr: *mut StatuserViewModel, s: *const c_char) {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let c_str = unsafe {
        assert!(!s.is_null());
        CStr::from_ptr(s)
    };
    let mut bla = String::from(c_str.to_str().unwrap());
    let r_str = bla.as_mut_str();

    statuser_view_model.get_diff(r_str);
}

#[no_mangle]
pub extern fn statuser_view_model_get_diff_amount(ptr: *mut StatuserViewModel) -> usize {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    statuser_view_model.m_diff.len()
}

#[no_mangle]
pub extern fn statuser_view_model_get_diff_data(ptr: *mut StatuserViewModel, index: usize) -> *const c_char {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let res = statuser_view_model.m_diff[index].data.as_str();

    let s = CString::new(res).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn statuser_view_model_get_diff_red(ptr: *mut StatuserViewModel, index: usize) -> usize {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    statuser_view_model.m_diff[index].color.red
}

#[no_mangle]
pub extern fn statuser_view_model_get_diff_green(ptr: *mut StatuserViewModel, index: usize) -> usize {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    statuser_view_model.m_diff[index].color.green
}

#[no_mangle]
pub extern fn statuser_view_model_get_diff_blue(ptr: *mut StatuserViewModel, index: usize) -> usize {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    statuser_view_model.m_diff[index].color.blue
}

#[no_mangle]
pub extern fn statuser_view_model_get_diff_background_red(ptr: *mut StatuserViewModel, index: usize) -> usize {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    statuser_view_model.m_diff[index]._background_color.red
}

#[no_mangle]
pub extern fn statuser_view_model_get_diff_background_green(ptr: *mut StatuserViewModel, index: usize) -> usize {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    statuser_view_model.m_diff[index]._background_color.green
}

#[no_mangle]
pub extern fn statuser_view_model_get_diff_background_blue(ptr: *mut StatuserViewModel, index: usize) -> usize {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    statuser_view_model.m_diff[index]._background_color.blue
}

#[no_mangle]
pub extern fn statuser_view_model_stage_button_was_clicked(ptr: *mut StatuserViewModel) {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    statuser_view_model.stage_button_was_clicked();
}

#[no_mangle]
pub extern fn statuser_view_model_reset_button_was_clicked(ptr: *mut StatuserViewModel, s: *const c_char) {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let c_str = unsafe {
        assert!(!s.is_null());
        CStr::from_ptr(s)
    };
    let mut bla = String::from(c_str.to_str().unwrap());
    let r_str = bla.as_mut_str();

    statuser_view_model.reset_button_was_clicked(r_str);
}

#[no_mangle]
pub extern fn statuser_view_model_unstage_button_was_clicked(ptr: *mut StatuserViewModel) {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    statuser_view_model.unstage_button_was_clicked();
}

#[no_mangle]
pub extern fn statuser_view_model_unstaged_double_click(ptr: *mut StatuserViewModel, s: *const c_char) {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let c_str = unsafe {
        assert!(!s.is_null());
        CStr::from_ptr(s)
    };
    let mut bla = String::from(c_str.to_str().unwrap());
    let r_str = bla.as_mut_str();

    statuser_view_model.unstaged_double_click(r_str);
}

#[no_mangle]
pub extern fn statuser_view_model_staged_double_click(ptr: *mut StatuserViewModel, s: *const c_char) {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let c_str = unsafe {
        assert!(!s.is_null());
        CStr::from_ptr(s)
    };
    let mut bla = String::from(c_str.to_str().unwrap());
    let r_str = bla.as_mut_str();

    statuser_view_model.staged_double_click(r_str);
}

#[no_mangle]
pub extern fn statuser_view_model_commit_button_was_clicked(ptr: *mut StatuserViewModel, s: *const c_char) {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let c_str = unsafe {
        assert!(!s.is_null());
        CStr::from_ptr(s)
    };
    let mut bla = String::from(c_str.to_str().unwrap());
    let r_str = bla.as_mut_str();

    statuser_view_model.commit_button_was_clicked(r_str);
}

#[no_mangle]
pub extern fn statuser_view_model_push_button_was_clicked(ptr: *mut StatuserViewModel) {
    let statuser_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    statuser_view_model.push_button_was_clicked();
}