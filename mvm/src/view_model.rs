use crate::tree_view_model::TreeViewModel;
use crate::low_level_git_api::LowLevelGitApi;
use crate::model::lib_giter::ilib_giter::ILibGiter;
use crate::model::lib_giter::lib_giter::LibGiter;
use crate::tree::{Tree, ITree};
use crate::model::pipe::pipe::Pipe;
use crate::terminator::{Terminator, ITerminator};
use std::cell::RefCell;
use std::rc::Rc;
use crate::terminator_colorizer::TerminatorColorizer;
use crate::terminator_view_model::TerminatorViewModel;
use crate::terminator_view_model::ITerminatorViewModel;
use crate::completer::Completer;
use crate::historer::Historer;
use crate::statuser_view_model::{StatuserViewModel, IStatiserViewModel};
use std::ffi::CStr;
use std::os::raw::c_char;
use crate::tree_view_model_ffi::{ITreeViewModelFFI, TreeViewModelFFI};
use crate::filer_view_model::FilerViewModel;
use crate::filer_view_model_ffi::{FilerViewModelFFI, IFilerViewModelFFI};


pub struct ViewModel {
    //m_lib_giter: Rc<RefCell<dyn ILibGiter>>,
    //m_terminator: Rc<RefCell<dyn ITerminator>>,
    //m_tree: Rc<RefCell<dyn ITree>>,
    m_terminator_view_model: Rc<RefCell<dyn ITerminatorViewModel>>,
    m_tree_view_model: TreeViewModel,
    m_statuser_view_model: StatuserViewModel,
    m_filer_view_model: FilerViewModel,
}

impl ViewModel {
    fn new(path_to_repo: String) -> ViewModel {
        let low_level_git_api = LowLevelGitApi::new(path_to_repo.as_ref());
        let lib_giter: Rc<RefCell<dyn ILibGiter>> = Rc::new(RefCell::new(LibGiter::new(Box::new(low_level_git_api))));
        let terminator_colorizer: Rc<RefCell<TerminatorColorizer>> = Rc::new(RefCell::new(TerminatorColorizer::new()));
        let historer = Rc::new(RefCell::new(Historer::new()));
        let terminator: Rc<RefCell<dyn ITerminator>> = Rc::new(RefCell::new(Terminator::new(Box::new(Pipe::new()), Rc::clone(&lib_giter), Rc::clone(&terminator_colorizer))));
        let completer: Rc<RefCell<Completer>> = Rc::new(RefCell::new(Completer::new(Rc::clone(&terminator), Rc::clone(&lib_giter))));
        let tree: Rc<RefCell<dyn ITree>> = Rc::new(RefCell::new(Tree::new(Rc::clone(&lib_giter), Rc::clone(&terminator))));
        let terminator_view_model = TerminatorViewModel::new(Rc::clone(&terminator), Rc::clone(&terminator_colorizer), Rc::clone(&completer), Rc::clone(&historer));
        let terminator_view_model_ref: Rc<RefCell<dyn ITerminatorViewModel>> = Rc::new(RefCell::new(terminator_view_model));
        let tree_view_model_ffi: Rc<RefCell<dyn ITreeViewModelFFI>> = Rc::new(RefCell::new(TreeViewModelFFI::new()));
        let filer_view_model_ffi: Rc<RefCell<dyn IFilerViewModelFFI>> = Rc::new(RefCell::new(FilerViewModelFFI::new()));

        ViewModel {
            //m_lib_giter: lib_giter,
            //m_terminator: terminator,
            //m_tree: tree,
            m_terminator_view_model: Rc::clone(&terminator_view_model_ref),
            m_tree_view_model: TreeViewModel::new(Rc::clone(&tree), tree_view_model_ffi),
            m_statuser_view_model: StatuserViewModel::new(Rc::clone(&lib_giter), Rc::clone(&terminator), Rc::clone(&terminator_colorizer), Rc::clone(&terminator_view_model_ref)),
            m_filer_view_model: FilerViewModel::new(Rc::clone(&lib_giter), Rc::clone(&terminator), Rc::clone(&terminator_colorizer), filer_view_model_ffi),
        }
    }

    fn keyboard_shortcut_was_pressed(&mut self, was_ctrl_pressed: bool, key_code: usize, is_tree_selected: bool) {
        self.m_terminator_view_model.borrow_mut().keyboard_shortcut_was_pressed(was_ctrl_pressed, key_code);
        self.m_tree_view_model.keyboard_shortcut_was_pressed(was_ctrl_pressed, key_code, is_tree_selected);
        self.m_statuser_view_model.keyboard_shortcut_was_pressed(was_ctrl_pressed, key_code);
        self.m_filer_view_model.keyboard_shortcut_was_pressed(was_ctrl_pressed, key_code);
    }
}

#[no_mangle]
pub extern fn view_model_new(s: *const c_char) -> *mut ViewModel {
    let c_str = unsafe {
        assert!(!s.is_null());
        CStr::from_ptr(s)
    };

    let r_str = c_str.to_str().unwrap();

    Box::into_raw(Box::new(ViewModel::new(r_str.to_string())))
}

#[no_mangle]
pub extern fn view_model_free(ptr: *mut ViewModel) {
    if ptr.is_null() {
        return;
    }

    unsafe {
        Box::from_raw(ptr);
    }
}

#[no_mangle]
pub extern fn view_model_get_tree_view_model(ptr: *mut ViewModel) -> *mut TreeViewModel {
    let view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    &mut view_model.m_tree_view_model
}

#[no_mangle]
pub extern fn view_model_get_terminator_view_model(ptr: *mut ViewModel) -> *mut TerminatorViewModel {
    let view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    //&mut view_model.m_terminator_view_model
    view_model.m_terminator_view_model.as_ptr() as *mut TerminatorViewModel
}

#[no_mangle]
pub extern fn view_model_get_statuser_view_model(ptr: *mut ViewModel) -> *mut StatuserViewModel {
    let view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    &mut view_model.m_statuser_view_model
}

#[no_mangle]
pub extern fn view_model_get_filer_view_model(ptr: *mut ViewModel) -> *mut FilerViewModel {
    let view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    &mut view_model.m_filer_view_model
}

#[no_mangle]
pub extern fn view_model_keyboard_shortcut_was_pressed(ptr: *mut ViewModel, was_ctrl_pressed: bool, key_code: usize, is_tree_selected: bool) {
    let view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    view_model.keyboard_shortcut_was_pressed(was_ctrl_pressed, key_code, is_tree_selected);
}