use crate::node::NodeType::{Commit, Horizontal, LeftToRight, LeftToRightNarrow, RightToLeft, RightToLeftNarrow};

#[derive(Debug, PartialEq)]
pub(crate) enum NodeType { Commit, Line, Horizontal, LeftToRight, LeftToRightNarrow, RightToLeft, RightToLeftNarrow }

#[derive(Debug)]
pub struct Node {
    m_column: usize,
    m_sha1: String,
    m_type: NodeType,
    m_is_tip: bool,
    m_color: usize,
}

impl Node {
    pub(crate) fn new(column: usize, sha1: &str, color: usize, node_type: NodeType, is_tip: bool) -> Node {
        Node {
            m_column: column + 1,
            m_sha1: sha1.to_string(),
            m_type: node_type,
            m_is_tip: is_tip,
            m_color: color,
        }
    }

    pub(crate) fn from(node: &Node) -> Node {
        let node_type = match node.get_type() {
            NodeType::Commit => Commit,
            NodeType::Line => NodeType::Line,
            NodeType::Horizontal => Horizontal,
            NodeType::LeftToRight => LeftToRight,
            NodeType::LeftToRightNarrow => LeftToRightNarrow,
            NodeType::RightToLeft => RightToLeft,
            NodeType::RightToLeftNarrow => RightToLeftNarrow,
        };
        Node::new(node.get_column() - 1, node.get_sha1(), node.get_color(), node_type, node.is_tip())
    }

    pub(crate) fn get_column(&self) -> usize {
        self.m_column
    }

    pub(crate) fn is_tip(&self) -> bool {
        self.m_is_tip
    }

    pub(crate) fn get_sha1(&self) -> &str {
        &self.m_sha1
    }

    pub(crate) fn get_type(&self) -> &NodeType {
        &self.m_type
    }

    pub(crate) fn get_color(&self) -> usize {
        self.m_color
    }

    pub(crate) fn set_color(&mut self, color: usize) {
        self.m_color = color;
    }
}

//#[cfg(test)]
//mod tests {
//    use super::*;
//    use crate::node::NodeType::Commit;
//
//    fn get_node() -> Node {
//        Node {
//            m_column: 1,
//            m_sha1: "sha1".to_string(),
//            m_type: Commit,
//            m_is_tip: true,
//            m_color: 2,
//        }
//    }
//
//    #[test]
//    fn ctor_test() {
//        let node = Node::new(0, "sha1", 2, Commit, true);
//
////        assert_eq!(node.get_column(), 1);
////        assert_eq!(node.get_sha1(), "sha1");
////        assert_eq!(node.get_type(), 0);
////        assert_eq!(node.is_tip(), true);
//    }
//
////    #[test]
////    fn operator_double_equals_negative() {
////        //const auto temp_node = std::make_shared<Node>(7, "sha2", 42, LINE, false);
////        let node_left = get_node();
////
////        let node_right = Node {
////            m_column: 7,
////            m_sha1: "sha2".to_string(),
////            m_type: 1,
////            m_is_tip: false,
////            m_color: 42,
////        };
////
////        assert_ne!(node_left, node_right);
////    }
////
////    #[test]
////    fn operator_double_equals_positive() {
////        //const auto temp_node = std::make_shared<Node>(7, "sha2", 42, LINE, false);
////        let node_left = get_node();
////
////        let node_right = Node {
////            m_column: 1,
////            m_sha1: "sha1".to_string(),
////            m_type: 0,
////            m_is_tip: true,
////            m_color: 2,
////        };
////
////        assert_eq!(node_left, node_right);
////    }
//
//    #[test]
//    fn get_column() {
//        let node = get_node();
//
//        assert_eq!(node.get_column(), 1);
//    }
//
//    #[test]
//    fn is_tip() {
//        let node = get_node();
//
//        assert_eq!(node.is_tip(), true);
//    }
//
//    #[test]
//    fn get_sha1() {
//        let node = get_node();
//
//        assert_eq!(node.get_sha1(), "sha1");
//    }
//
//    #[test]
//    fn get_type() {
//        let node = get_node();
//
//        //assert_eq!(node.get_type(), 0);
//    }
//
//    #[test]
//    fn get_color() {
//        let node = get_node();
//
//        assert_eq!(node.get_color(), 2);
//    }
//}