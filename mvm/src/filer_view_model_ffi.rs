use mockall::*;
use mockall::predicate::*;


#[automock]
pub(crate) trait IFilerViewModelFFI {
    //    fn tree_view_model_notify_new_line(&self, input: usize);
//    fn tree_view_model_notify_clear_the_tree(&self);
//    fn tree_view_model_notify_tree_enablement(&self, enabled: bool);
//    fn tree_view_model_notify_tree_selection(&self);
//    fn tree_view_model_copy_paste_was_pressed(&self);
    fn filer_view_model_notify_refresh(&self);
}

pub struct FilerViewModelFFI {}

impl FilerViewModelFFI {
    pub(crate) fn new() -> FilerViewModelFFI {
        FilerViewModelFFI {}
    }
}

impl IFilerViewModelFFI for FilerViewModelFFI {
    fn filer_view_model_notify_refresh(&self) {
        unsafe {
            filer_view_model_notify_refresh();
        }
    }
}

extern "C" {
    fn filer_view_model_notify_refresh();
}