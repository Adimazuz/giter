use crate::model::line::Line;
use crate::terminator::ITerminator;
use crate::tree_line_parser::TreeLineParser;
use crate::node::{Node, NodeType};
use crate::node::NodeType::{Horizontal, LeftToRightNarrow, LeftToRight, RightToLeftNarrow, RightToLeft, Commit};
use std::cell::RefCell;
use std::rc::Rc;
use crate::model::lib_giter::ilib_giter::ILibGiter;


pub(crate) trait ITree {
    fn refresh_the_tree(&mut self);
    fn get_tree_line(&self, line_number: usize) -> &Line;
    fn get_tree_size(&self) -> usize;
}

pub struct Tree {
    m_lib_giter: Rc<RefCell<dyn ILibGiter>>,
    m_terminator: Rc<RefCell<dyn ITerminator>>,
    m_string_lines: Vec<String>,
    m_lines: Vec<Line>,
    m_previous_line: Line,
    m_colors: Vec<usize>,
}

impl ITree for Tree {
    fn refresh_the_tree(&mut self) {
        self.m_lib_giter.borrow_mut().update_all_refs();

        self.m_lines.clear();

        let tree_string = self.m_terminator.borrow_mut().get_string_tree();

        self.m_string_lines = self.break_string_into_lines(tree_string);

        self.m_colors = vec![];

        for i in 0..self.m_string_lines.len() {
            if i > 0 {
                self.colorize_merge(String::from(&self.m_string_lines[i]).as_str(), String::from(&self.m_string_lines[i - 1]).as_str());
                self.colorize_split_with_commit(String::from(&self.m_string_lines[i]).as_str(), String::from(&self.m_string_lines[i - 1]).as_str());
            }

            let single_line = self.process_single_line(i);
            self.m_previous_line = Line::new(single_line, Rc::clone(&self.m_lib_giter), (self.m_string_lines.len() - 1 - i) as u64);

            //let line = Line::new(single_line, Rc::clone(&self.m_lib_giter), (self.m_string_lines.len() - 1 - i) as u64);
            let line = Line::from(&self.m_previous_line);
            self.m_lines.push(line);
        }
    }

    fn get_tree_line(&self, line_number: usize) -> &Line {
        &self.m_lines[line_number]
    }

    fn get_tree_size(&self) -> usize {
        self.m_lines.len()
    }
}

impl Tree {
    pub(crate) fn new(lib_giter: Rc<RefCell<dyn ILibGiter>>, terminator: Rc<RefCell<dyn ITerminator>>) -> Tree {
        let line = Line::new(vec![], Rc::clone(&lib_giter), 0);

        Tree {
            m_lib_giter: lib_giter,
            m_terminator: terminator,
            m_string_lines: vec![],
            m_lines: vec![],
            m_previous_line: line,
            m_colors: vec![],
        }
    }

    fn break_string_into_lines(&self, tree_string: String) -> Vec<String> {
        let mut res = vec![];

        let lines = tree_string.lines();
        for line in lines {
            res.push(line.to_string())
        }

        res
    }

    fn colorize_merge(&mut self, line: &str, previous_line: &str) {
        match line.find("|\\") {
            None => {}
            Some(res) => {
                // To make sure its a real merge. Maybe its juts branches crossing each other?
                if previous_line.chars().nth(res).unwrap() == '*'
                {
                    self.m_colors[res] = self.m_colors[res] + 1;
                    self.m_colors[res + 1] = self.m_colors[res] + 1;
                    self.m_colors[res + 2] = self.m_colors[res + 1];
                }
            }
        }
    }

    fn colorize_split_with_commit(&mut self, line: &str, previous_line: &str) {
        match line.find("|/") {
            None => {}
            Some(res) => {
                // To make sure its a real merge. Maybe its juts branches crossing each other?
                if previous_line.chars().nth(res + 2).unwrap() == '*'
                {
                    self.m_colors[res + 1] = self.get_color_from_previous_line(res + 3);
                }
            }
        }
    }

    fn get_color_from_previous_line(&self, column: usize) -> usize {
        let mut res = 0;

        for node in self.m_previous_line.get_nodes() {
            if node.get_column() == column {
                res = node.get_color();
            }
        }

        res
    }

    fn process_single_line(&mut self, i: usize) -> Vec<Node> {
        let mut single_line = vec![];

        let graph_line_width = self.get_graph_line_width(&self.m_string_lines[i]);

        for k in 0..graph_line_width {
            if k + 1 > self.m_colors.len()
            {
                // Start a new column with color zero.
                self.m_colors.push(0);
            }

            match self.process_single_character(i, k) {
                None => {}
                Some(node) => { single_line.push(node) }
            }
        }

        self.colorize_horizontal_line_and_ending(&mut single_line);

        single_line
    }

    fn get_graph_line_width(&self, line: &str) -> usize {
        let sha1_position = self.find_sha1_position(line);

        let graph_line_width;

        if sha1_position == 0
        {
            graph_line_width = line.len();
        } else {
            graph_line_width = sha1_position;
        }

        graph_line_width
    }

    fn find_sha1_position(&self, line: &str) -> usize {
        let mut sha1_position = 0;

        for (i, c) in line.chars().enumerate() {
            if self.is_legal_graph_character(c.to_string().as_str()) == false {
                sha1_position = i;
                break;
            }
        }

        sha1_position
    }

    fn is_legal_graph_character(&self, character: &str) -> bool {
        if character == "|" || character == "/" || character == "\\" || character == "_" || character == " " || character == "*"
            || character == "-" || character == "." // Very rare case with large number of branches (for example: Linux kernel).
        {
            return true;
        }

        return false;
    }

    fn process_single_character(&mut self, index: usize, column: usize) -> Option<Node> {
        if self.m_string_lines[index].chars().nth(column).unwrap() == '|'
        {
            self.colorize_vertical_line(String::from(&self.m_string_lines[index - 1]).as_str(), column);

            return Some(Node::new(column, "", self.m_colors[column], NodeType::Line, false));
        }

        if self.m_string_lines[index].chars().nth(column).unwrap() == '_'
        {
            return Some(Node::new(column, "", 0, Horizontal, false));
        }

        if self.m_string_lines[index].chars().nth(column).unwrap() == '\\'
        {
            self.colorize_left_to_right_line(String::from(&self.m_string_lines[index - 1]).as_str(), column);

            let node_type = self.determine_left_to_right_structure(index, column);

            return Some(Node::new(column, "", self.m_colors[column], node_type, false));
        }

        if self.m_string_lines[index].chars().nth(column).unwrap() == '/'
        {
            self.colorize_right_to_left_line(String::from(&self.m_string_lines[index - 1]).as_str(), column);

            let node_type = self.determine_right_to_left_structure(index, column);

            return Some(Node::new(column, "", self.m_colors[column], node_type, false));
        }

        if self.m_string_lines[index].chars().nth(column).unwrap() == '*'
        {
            let commit_node = self.process_commit_node(index, column);
            //return std::make_shared < Node > (commit_node);
            //return Some(Node::new(column, "", self.m_colors[column], node_type, false));
            return Some(commit_node);
        }

        return None;
    }

    fn colorize_horizontal_line_and_ending(&self, line: &mut Vec<Node>) {
        if self.has_at_least_one_horizontal_line(line) == true {
            let mut color: usize = 0;

            for i in 0..line.len() {
                if *line[i].get_type() == RightToLeft && *line[i - 2].get_type() == Horizontal {
                    color = line[i].get_color();
                }
            }

            for node in line {
                if *node.get_type() == Horizontal {
                    node.set_color(color);
                }
            }
        }
    }

    fn has_at_least_one_horizontal_line(&self, line: &Vec<Node>) -> bool {
        for node in line {
            if *node.get_type() == Horizontal {
                return true;
            }
        }

        return false;
    }

    fn colorize_vertical_line(&mut self, previous_line: &str, column: usize) {
        if column > 0
        {
            if previous_line.chars().nth(column - 1).unwrap() == '\\'
            {
                self.m_colors[column] = self.get_color_from_previous_line(column);
            }

            if previous_line.chars().nth(column + 1).unwrap() == '/'
            {
                if previous_line.chars().nth(column - 1).unwrap() == '/'
                {
                    self.m_colors[column] = self.get_color_from_previous_line(column + 2);
                }
            }
        }
    }

    fn colorize_left_to_right_line(&mut self, previous_line: &str, column: usize) {
        if previous_line.chars().nth(column - 1).unwrap() == '|' ||
            previous_line.chars().nth(column - 1).unwrap() == '\\'
        {
            self.m_colors[column] = self.get_color_from_previous_line(column);
        }
    }

    fn determine_left_to_right_structure(&self, line_number: usize, column: usize) -> NodeType {
        if self.m_string_lines[line_number + 1].chars().nth(column + 1).unwrap() == '\\' &&
            self.m_string_lines[line_number + 2].chars().nth(column + 2).unwrap() == '\\'
        {
            return LeftToRightNarrow;
        }

        if self.m_string_lines[line_number - 1].chars().nth(column - 1).unwrap() == '\\' &&
            self.m_string_lines[line_number + 1].chars().nth(column + 1).unwrap() == '\\'
        {
            return LeftToRightNarrow;
        }

        LeftToRight
    }

    fn colorize_right_to_left_line(&mut self, previous_line: &str, column: usize) {
        if previous_line.len() > column // This is just a precaution for index out of bounds thingy.
        {
            if previous_line.chars().nth(column + 1).unwrap() == '|'
            {
                // Maybe its just branches crossing each other?
                if previous_line.chars().nth(column + 2).unwrap() != '/'
                {
                    self.m_colors[column] = self.get_color_from_previous_line(column + 2);
                } else {
                    self.m_colors[column] = self.get_color_from_previous_line(column + 3);
                }
            }

            if previous_line.chars().nth(column + 1).unwrap() == '/'
            {
                self.m_colors[column] = self.get_color_from_previous_line(column + 2);
            }

            if (previous_line.len() > column + 2) == true // This is just a precaution for index out of bounds thingy.
            {
                if previous_line.chars().nth(column + 2).unwrap() == '_'
                {
                    self.m_colors[column] = self.get_color_from_previous_line(column + 3);
                }
            }
        }
    }

    fn determine_right_to_left_structure(&self, line_number: usize, column: usize) -> NodeType {
        if self.m_string_lines[line_number - 1].chars().nth(column + 1).unwrap() == '/' &&
            self.m_string_lines[line_number + 1].chars().nth(column - 1).unwrap() == '/'
        {
            return RightToLeftNarrow;
        }
        if self.m_string_lines[line_number - 1].chars().nth(column + 1).unwrap() == '/' &&
            self.m_string_lines[line_number + 1].chars().nth(column - 1).unwrap() != '/'
        {
            return RightToLeftNarrow;
        }

        return RightToLeft;
    }

    fn process_commit_node(&mut self, index: usize, column: usize) -> Node {
        let is_tip;

        // Well, the first commit is definitely a tip.
        if index == 0
        {
            is_tip = true;
        } else {
            is_tip = self.determine_if_tip(&self.m_string_lines[index - 1], column);

            if is_tip == true {
                self.m_colors[column] = self.m_colors[column] + 1;
            }
        }

        //let sha1 = self.get_sha1_from_line(&self.m_string_lines[index]);
        let sha1 = TreeLineParser::new(&self.m_string_lines[index]).get_sha1_from_line();
        Node::new(column, sha1.as_str(), self.m_colors[column], Commit, is_tip)
    }

    fn determine_if_tip(&self, previous_line: &str, column: usize) -> bool {
        // A tip definitely can't be at the left most column.
        if column <= 1
        {
            return false;
        }

        if column - 1 < previous_line.len() &&
            (previous_line.chars().nth(column - 1).unwrap() == '\\' ||
                previous_line.chars().nth(column - 1).unwrap() == '|' ||
                previous_line.chars().nth(column - 1).unwrap() == '_')
        {
            return false;
        }

        if column < previous_line.len() &&
            (previous_line.chars().nth(column).unwrap() == '*' ||
                previous_line.chars().nth(column).unwrap() == '\\' ||
                previous_line.chars().nth(column).unwrap() == '/' ||
                previous_line.chars().nth(column).unwrap() == '|' ||
                previous_line.chars().nth(column).unwrap() == '_')
        {
            return false;
        }

        if previous_line.len() > column &&
            (previous_line.chars().nth(column + 1).unwrap() == '/' ||
                previous_line.chars().nth(column + 1).unwrap() == '|' ||
                previous_line.chars().nth(column + 1).unwrap() == '_')
        {
            return false;
        }

        return true;
    }
}