extern crate libc;


pub struct Historer {
    pub m_index: i32,
    pub m_history: Vec<String>,
}

impl Historer {
    pub(crate) fn new() -> Historer {
        Historer {
            m_index: -1,
            m_history: Vec::new(),
        }
    }

    pub(crate) fn get_earlier(&mut self) -> String {
        if self.m_index == -1 {
            return String::new();
        }

        self.m_index = self.m_index - 1;

        if self.m_index == -1 {
            self.m_index = 0;
        }

        let res = &self.m_history[self.m_index as usize];

        String::from(res)
    }

    pub(crate) fn get_later(&mut self) -> String {
        if self.m_index == -1 {
            return String::new();
        }

        self.m_index = self.m_index + 1;

        if self.m_index as usize > self.m_history.len() - 1 {
            self.m_index = self.m_history.len() as i32;

            return String::new();
        }

        let res = &self.m_history[self.m_index as usize];

        String::from(res)
    }

    pub(crate) fn add(&mut self, data: String) {
        self.m_history.push(data);

        self.m_index = self.m_history.len() as i32;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn setup_empty() -> Historer {
        Historer {
            m_index: -1,
            m_history: Vec::new(),
        }
    }

    fn setup() -> Historer {
        let mut historer = Historer {
            m_index: -1,
            m_history: Vec::new(),
        };

        historer.add("ancient".to_string());
        historer.add("middle".to_string());
        historer.add("young".to_string());

        return historer;
    }

    #[test]
    fn get_earlier_empty() {
        let mut historer = setup_empty();

        let res = historer.get_earlier();
        assert_eq!(res.is_empty(), true);

        let res = historer.get_earlier();
        assert_eq!(res.is_empty(), true);

        let res = historer.get_earlier();
        assert_eq!(res.is_empty(), true);
    }

    #[test]
    fn get_later_empty() {
        let mut historer = setup_empty();

        let res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        let res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        let res = historer.get_later();
        assert_eq!(res.is_empty(), true);
    }

    #[test]
    fn get_earlier_get_later() {
        let mut historer = setup();

        let mut res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        res = historer.get_earlier();
        assert_eq!(res, "young");

        res = historer.get_earlier();
        assert_eq!(res, "middle");

        res = historer.get_earlier();
        assert_eq!(res, "ancient");

        res = historer.get_earlier();
        assert_eq!(res, "ancient");

        res = historer.get_earlier();
        assert_eq!(res, "ancient");

        res = historer.get_earlier();
        assert_eq!(res, "ancient");

        res = historer.get_later();
        assert_eq!(res, "middle");

        res = historer.get_later();
        assert_eq!(res, "young");

        res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        res = historer.get_earlier();
        assert_eq!(res, "young");

        res = historer.get_earlier();
        assert_eq!(res, "middle");

        res = historer.get_earlier();
        assert_eq!(res, "ancient");

        res = historer.get_earlier();
        assert_eq!(res, "ancient");

        res = historer.get_earlier();
        assert_eq!(res, "ancient");

        res = historer.get_earlier();
        assert_eq!(res, "ancient");

        res = historer.get_later();
        assert_eq!(res, "middle");

        res = historer.get_later();
        assert_eq!(res, "young");

        res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        res = historer.get_later();
        assert_eq!(res.is_empty(), true);
    }

    #[test]
    fn add() {
        let mut historer = setup();
        historer.add("new".to_string());

        let mut res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        res = historer.get_earlier();
        assert_eq!(res, "new");

        res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        res = historer.get_later();
        assert_eq!(res.is_empty(), true);

        res = historer.get_later();
        assert_eq!(res.is_empty(), true);
    }
}
