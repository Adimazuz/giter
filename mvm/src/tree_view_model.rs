use crate::node::NodeType::Commit;
use std::ffi::CString;
use std::os::raw::c_char;
use crate::tree::ITree;
use crate::tree_view_model_ffi::ITreeViewModelFFI;
use crate::node::NodeType;
use std::cell::RefCell;
use std::rc::Rc;


pub struct TreeViewModel {
    m_tree: Rc<RefCell<dyn ITree>>,
    m_tree_view_model_ffi: Rc<RefCell<dyn ITreeViewModelFFI>>,
}

impl TreeViewModel {
    pub(crate) fn new(tree: Rc<RefCell<dyn ITree>>, tree_view_model_ffi: Rc<RefCell<dyn ITreeViewModelFFI>>) -> TreeViewModel {
        TreeViewModel {
            m_tree: tree,
            m_tree_view_model_ffi: tree_view_model_ffi,
        }
    }

    fn update_the_tree(&mut self) {
        self.m_tree.borrow_mut().refresh_the_tree();

        self.m_tree_view_model_ffi.borrow().tree_view_model_notify_clear_the_tree();

        self.m_tree_view_model_ffi.borrow().tree_view_model_notify_tree_enablement(false);

        let tree_size = self.m_tree.borrow_mut().get_tree_size();
        for i in 0..tree_size {
            self.m_tree_view_model_ffi.borrow().tree_view_model_notify_new_line(i);
        }

        self.m_tree_view_model_ffi.borrow().tree_view_model_notify_tree_enablement(true);
    }

    fn get_nodes_amount(&self, line_index: usize) -> usize {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        line.get_nodes().len()
    }

    fn get_commit_message_offset(&self, line_index: usize) -> usize {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        if line.get_nodes().is_empty() == true {
            return 0;
        } else {
            return (line.get_nodes().last().unwrap().get_column() + 1) / 2 * 4/*cell_width*/ * 5/*radius*/;
        }
    }

    fn get_local_refs_amount(&self, line_index: usize) -> usize {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        line.get_local_refs().len()
    }

    fn get_local_ref(&self, line_index: usize, ref_index: usize) -> String {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        String::from(&line.get_local_refs()[ref_index])
    }

    fn get_remote_refs_amount(&self, line_index: usize) -> usize {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        line.get_remote_refs().len()
    }

    fn get_remote_ref(&self, line_index: usize, ref_index: usize) -> String {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        String::from(&line.get_remote_refs()[ref_index])
    }

    fn get_tag_refs_amount(&self, line_index: usize) -> usize {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        line.get_tag_refs().len()
    }

    fn get_tag_ref(&self, line_index: usize, ref_index: usize) -> String {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        String::from(&line.get_tag_refs()[ref_index])
    }

    fn get_commit_message(&self, line_index: usize) -> String {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        String::from(line.get_commit_message())
    }

    fn get_color(&self, line_index: usize, node_index: usize) -> usize {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        line.get_nodes()[node_index].get_color()
    }

    fn get_node_type(&self, line_index: usize, node_index: usize) -> &NodeType {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        match line.get_nodes()[node_index].get_type() {
            Commit => &Commit,
            NodeType::Line => &NodeType::Line,
            NodeType::Horizontal => &NodeType::Horizontal,
            NodeType::LeftToRight => &NodeType::LeftToRight,
            NodeType::LeftToRightNarrow => &NodeType::LeftToRightNarrow,
            NodeType::RightToLeft => &NodeType::RightToLeft,
            NodeType::RightToLeftNarrow => &NodeType::RightToLeftNarrow,
        }
    }

    fn get_is_first_line(&self, line_index: usize) -> bool {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        line.get_is_first_line()
    }

    fn has_at_least_one_ref(&self, line_index: usize) -> bool {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        line.has_at_least_one_ref()
    }

    fn get_column(&self, line_index: usize, node_index: usize) -> usize {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        line.get_nodes()[node_index].get_column()
    }

    fn is_tip(&self, line_index: usize, node_index: usize) -> bool {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        line.get_nodes()[node_index].is_tip()
    }

    fn get_date(&self, line_index: usize) -> String {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        String::from(line.get_date())
    }

    fn get_sha1(&self, line_index: usize) -> String {
        let bla = self.m_tree.borrow_mut();
        let line = bla.get_tree_line(line_index);

        String::from(line.get_sha1())
    }

    pub(crate) fn keyboard_shortcut_was_pressed(&mut self, was_ctrl_pressed: bool, key_code: usize, is_tree_selected: bool) {
        if was_ctrl_pressed == true && key_code == 0x52 { // The letter R.
            self.update_the_tree();
        }

        if was_ctrl_pressed == true && key_code == 0x54 { // The letter T.
            self.m_tree_view_model_ffi.borrow().tree_view_model_notify_tree_selection();
        }

        if was_ctrl_pressed == true && key_code == 0x43 && is_tree_selected == true { // The letter C.
            self.m_tree_view_model_ffi.borrow().tree_view_model_copy_paste_was_pressed();
        }
    }
}

#[no_mangle]
pub extern fn tree_view_model_update_the_tree(ptr: *mut TreeViewModel) {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    tree_view_model.update_the_tree();
}

#[no_mangle]
pub extern fn tree_view_model_get_nodes_amount(ptr: *mut TreeViewModel, line_index: usize) -> usize {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    tree_view_model.get_nodes_amount(line_index)
}

#[no_mangle]
pub extern fn tree_view_model_get_commit_message_offset(ptr: *mut TreeViewModel, line_index: usize) -> usize {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    tree_view_model.get_commit_message_offset(line_index)
}

#[no_mangle]
pub extern fn tree_view_model_get_local_refs_amount(ptr: *mut TreeViewModel, line_index: usize) -> usize {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    tree_view_model.get_local_refs_amount(line_index)
}

#[no_mangle]
pub extern fn tree_view_model_get_local_ref(ptr: *mut TreeViewModel, line_index: usize, ref_index: usize) -> *const c_char {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let reference = tree_view_model.get_local_ref(line_index, ref_index);

    let s = CString::new(reference).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn tree_view_model_get_remote_refs_amount(ptr: *mut TreeViewModel, line_index: usize) -> usize {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    tree_view_model.get_remote_refs_amount(line_index)
}

#[no_mangle]
pub extern fn tree_view_model_get_remote_ref(ptr: *mut TreeViewModel, line_index: usize, ref_index: usize) -> *const c_char {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let reference = tree_view_model.get_remote_ref(line_index, ref_index);

    let s = CString::new(reference).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn tree_view_model_get_tag_refs_amount(ptr: *mut TreeViewModel, line_index: usize) -> usize {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    tree_view_model.get_tag_refs_amount(line_index)
}

#[no_mangle]
pub extern fn tree_view_model_get_tag_ref(ptr: *mut TreeViewModel, line_index: usize, ref_index: usize) -> *const c_char {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let reference = tree_view_model.get_tag_ref(line_index, ref_index);

    let s = CString::new(reference).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn tree_view_model_get_commit_message(ptr: *mut TreeViewModel, line_index: usize) -> *const c_char {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let reference = tree_view_model.get_commit_message(line_index);

    let s = CString::new(reference).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn tree_view_model_get_color(ptr: *mut TreeViewModel, line_index: usize, node_index: usize) -> usize {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    tree_view_model.get_color(line_index, node_index)
}

#[no_mangle]
pub extern fn tree_view_model_get_node_type(ptr: *mut TreeViewModel, line_index: usize, node_index: usize) -> usize {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    match tree_view_model.get_node_type(line_index, node_index) {
        Commit => 0,
        NodeType::Line => 1,
        NodeType::Horizontal => 2,
        NodeType::LeftToRight => 3,
        NodeType::LeftToRightNarrow => 4,
        NodeType::RightToLeft => 5,
        NodeType::RightToLeftNarrow => 6,
    }
}

#[no_mangle]
pub extern fn tree_view_model_get_is_first_line(ptr: *mut TreeViewModel, line_index: usize) -> bool {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    tree_view_model.get_is_first_line(line_index)
}

#[no_mangle]
pub extern fn tree_view_model_has_at_least_one_ref(ptr: *mut TreeViewModel, line_index: usize) -> bool {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    tree_view_model.has_at_least_one_ref(line_index)
}

#[no_mangle]
pub extern fn tree_view_model_get_column(ptr: *mut TreeViewModel, line_index: usize, node_index: usize) -> i32 {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    tree_view_model.get_column(line_index, node_index) as i32
}

#[no_mangle]
pub extern fn tree_view_model_is_tip(ptr: *mut TreeViewModel, line_index: usize, node_index: usize) -> bool {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    tree_view_model.is_tip(line_index, node_index)
}

#[no_mangle]
pub extern fn tree_view_model_get_date(ptr: *mut TreeViewModel, line_index: usize) -> *const c_char {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let date = tree_view_model.get_date(line_index);

    let s = CString::new(date).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn tree_view_model_get_sha1(ptr: *mut TreeViewModel, line_index: usize) -> *const c_char {
    let tree_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let sha1 = tree_view_model.get_sha1(line_index);

    let s = CString::new(sha1).unwrap();
    s.into_raw()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tree::Tree;
    use crate::low_level_git_api::LowLevelGitApi;
    use crate::terminator::{Terminator, ITerminator};
    use crate::tree_view_model::TreeViewModel;
    use crate::terminator_colorizer::TerminatorColorizer;
    use crate::tree_view_model_ffi::MockITreeViewModelFFI;
    use std::env;
    use crate::model::lib_giter::ilib_giter::ILibGiter;
    use crate::model::pipe::pipe::Pipe;
    use crate::model::lib_giter::lib_giter::LibGiter;


    fn setup(mock_tree_view_model_ffi: MockITreeViewModelFFI) -> TreeViewModel {
        let low_level_git_api = LowLevelGitApi::new(env::current_dir().unwrap().parent().unwrap());
        let lib_giter: Rc<RefCell<dyn ILibGiter>> = Rc::new(RefCell::new(LibGiter::new(Box::new(low_level_git_api))));
        let terminator_colorizer: Rc<RefCell<TerminatorColorizer>> = Rc::new(RefCell::new(TerminatorColorizer::new()));
        let terminator: Rc<RefCell<dyn ITerminator>> = Rc::new(RefCell::new(Terminator::new(Box::new(Pipe::new()), Rc::clone(&lib_giter), Rc::clone(&terminator_colorizer))));
        let tree: Rc<RefCell<dyn ITree>> = Rc::new(RefCell::new(Tree::new(Rc::clone(&lib_giter), Rc::clone(&terminator))));

        TreeViewModel::new(tree, Rc::new(RefCell::new(mock_tree_view_model_ffi)))
    }

    #[test]
    fn keyboard_shortcut_was_pressed_ctrl_plus_t() {
        let mut mock_tree_view_model_ffi = MockITreeViewModelFFI::new();
        mock_tree_view_model_ffi.expect_tree_view_model_notify_tree_selection().times(2).return_const(());
        let mut tree_view_model = setup(mock_tree_view_model_ffi);

        tree_view_model.keyboard_shortcut_was_pressed(true, 0x54 /* The letter T*/, true);
        tree_view_model.keyboard_shortcut_was_pressed(true, 0x54 /* The letter T*/, false);
    }

    #[test]
    fn keyboard_shortcut_was_pressed_ctrl_plus_c() {
        let mut mock_tree_view_model_ffi = MockITreeViewModelFFI::new();
        mock_tree_view_model_ffi.expect_tree_view_model_copy_paste_was_pressed().times(1).return_const(());
        let mut tree_view_model = setup(mock_tree_view_model_ffi);

        tree_view_model.keyboard_shortcut_was_pressed(true, 0x43 /* The letter C*/, true);
        tree_view_model.keyboard_shortcut_was_pressed(true, 0x43 /* The letter C*/, false);
    }
}