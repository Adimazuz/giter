pub(crate) struct Color {
    pub(crate) red: usize,
    pub(crate) green: usize,
    pub(crate) blue: usize,
}

impl Color {
    pub(crate) fn new(red: usize, green: usize, blue: usize) -> Color {
        Color {
            red,
            green,
            blue,
        }
    }

    pub(crate) fn from(color: &Color) -> Color {
        Color::new(color.red, color.green, color.blue)
    }
}

pub(crate) struct ColorString {
    pub(crate) data: String,
    pub(crate) color: Color,
    pub(crate) _background_color: Color,
}

impl ColorString {
    pub(crate) fn new(data: &str, color: &Color, background_color: &Color) -> ColorString {
        ColorString {
            data: data.to_string(),
            color: Color::new(color.red, color.green, color.blue),
            _background_color: Color::from(background_color),
        }
    }
}

pub(crate) struct TerminatorColorizer {
    m_normal_color: Color,
    m_green_color: Color,
    m_white_color: Color,

    // Colors for diff view.
    _m_diff_background_green_color: Color,
    m_diff_background_red_color: Color,
    m_diff_header_color: Color,
    m_diff_normal_color: Color,
    m_diff_background_normal_color: Color,
    m_diff_turquoise_color: Color,
}

impl TerminatorColorizer {
    pub(crate) fn new() -> TerminatorColorizer {
        TerminatorColorizer {
            m_normal_color: Color::new(203, 203, 203),
            m_green_color: Color::new(141, 208, 6),
            m_white_color: Color::new(255, 255, 255),

            // Diff view colors.
            _m_diff_background_green_color: Color::new(200, 255, 200),
            m_diff_background_red_color: Color::new(255, 200, 200),
            m_diff_header_color: Color::new(0, 0, 0),
            m_diff_normal_color: Color::new(120, 120, 120),
            m_diff_background_normal_color: Color::new(255, 255, 255),
            m_diff_turquoise_color: Color::new(40, 154, 220),
        }
    }

    pub(crate) fn get_normal_terminal_color(&self) -> &Color {
        &self.m_normal_color
    }

    pub(crate) fn get_white_terminal_color(&self) -> &Color {
        &self.m_white_color
    }

    pub fn get_diff_background_normal_color(&self) -> &Color {
        &self.m_diff_background_normal_color
    }

    pub fn get_diff_normal_color(&self) -> &Color {
        &self.m_diff_normal_color
    }

    pub(crate) fn colorize(&self, data: &Vec<String>) -> Vec<ColorString> {
        let mut res = vec![];

        for i in 0..data.len() {
            // Special case - the line with the current working directory.
            if i == data.len() - 3 {
                let position = data[i].find("(").unwrap();

                let left = &data[i][0..position];
                let right = &data[i][position..data[i].chars().count()];

                res.push(ColorString::new(left, &self.m_green_color, &Color::new(255, 255, 255)));
                res.push(ColorString::new(right, &self.m_white_color, &Color::new(255, 255, 255)));
            } else {
                res.push(ColorString::new(&data[i], &self.m_normal_color, &Color::new(255, 255, 255)));
            }
        }

        return res;
    }

    pub(crate) fn colorize_single_diff(&self, data: &Vec<String>) -> Vec<ColorString> {
        let mut res = vec![];

        let mut color;
        let mut background_color;

        for line in data {
            //if (line.len() >= 5 && &line[0..4] == "diff ") ||
            if (self.sub_string(line, 5) == "diff ".to_string()) ||
                (self.sub_string(line, 6) == "index ".to_string()) ||
                (self.sub_string(line, 4) == "--- ".to_string()) ||
                (self.sub_string(line, 4) == "+++ ".to_string()) {
                color = Color::from(&self.m_diff_header_color);
                background_color = Color::from(&self.m_diff_background_normal_color);
            } else if self.sub_string(line, 3) == "@@ ".to_string() {
                color = Color::from(&self.m_diff_turquoise_color);
                background_color = Color::from(&self.m_diff_background_normal_color);
            } else if self.sub_string(line, 1) == "+".to_string() {
                color = Color::from(&self.m_diff_normal_color);
                background_color = Color::from(&self._m_diff_background_green_color);
            } else if self.sub_string(line, 1) == "-".to_string() {
                color = Color::from(&self.m_diff_normal_color);
                background_color = Color::from(&self.m_diff_background_red_color);
            } else {
                color = Color::from(&self.m_diff_normal_color);
                background_color = Color::from(&self.m_diff_background_normal_color);
            }

            res.push(ColorString::new(line, &color, &background_color));
        }

        res
    }

    fn sub_string(&self, data: &String, length: usize) -> String {
        let mut extent = length;
        if length > data.chars().count() {
            extent = data.chars().count();
        }

        data.chars().take(extent).collect()
    }
}