extern crate chrono;

use git2::{Repository, Oid, StatusOptions, StatusShow};
use std::path::Path;
use std::collections::HashMap;
use std::time::{UNIX_EPOCH, Duration};
use chrono::prelude::DateTime;
use chrono::Utc;


pub(crate) trait ILowLevelGitApi {
    fn find_all_refs(&self) -> (HashMap<String, String>, HashMap<String, String>, HashMap<String, String>);
    fn get_commit_message(&self, sha1: &str) -> String;
    fn get_current_branch(&self) -> String;
    fn get_number_of_parents(&self, sha1: &str) -> u64;
    fn get_statuses(&self) -> (HashMap<String, String>, HashMap<String, String>, HashMap<String, String>);
    //fn get_history_diff(&self, commit_sha1: &str, file_path: &Path) -> Vec<String>;
    fn get_date(&self, sha1: &str) -> String;
    fn get_repo_path(&self) -> &Path;
}

pub(crate) struct LowLevelGitApi {
    m_repo: Repository,
}

impl LowLevelGitApi {
    pub(crate) fn new(repo_path: &Path) -> LowLevelGitApi {
        LowLevelGitApi {
            m_repo: match Repository::open(repo_path) {
                Ok(repo) => repo,
                Err(e) => panic!("Failed to open repo: {}.", e),
            }
        }
    }
}

impl ILowLevelGitApi for LowLevelGitApi {
    fn find_all_refs(&self) -> (HashMap<String, String>, HashMap<String, String>, HashMap<String, String>) {
        let mut local_refs: HashMap<String, String> = Default::default();
        let mut remote_refs: HashMap<String, String> = Default::default();
        let mut tag_refs: HashMap<String, String> = Default::default();

        let references = self.m_repo.references().unwrap();

        for a_reference in references {
            let reference = a_reference.unwrap();

            let sha1 = match reference.target() {
                Some(oid) => format!("{}", oid),
                None => continue,
            };

            let full_ref_name = reference.name().unwrap();
            let ref_name = full_ref_name.split("/").last().unwrap();

            if reference.is_branch() == true {
                local_refs.insert(String::from(ref_name), String::from(&sha1));
            }

            if reference.is_remote() == true {
                remote_refs.insert(String::from(ref_name), String::from(&sha1));
            }

            if reference.is_tag() == true {
                tag_refs.insert(String::from(ref_name), String::from(&sha1));
            }
        }

        (local_refs, remote_refs, tag_refs)
    }

    fn get_commit_message(&self, sha1: &str) -> String {
        if sha1.is_empty() == true {
            return "".to_string();
        }

        let oid = Oid::from_str(sha1).unwrap();

        let commit = self.m_repo.find_commit(oid);

        let c = commit.unwrap();

        let message = c.message().unwrap();

        String::from(message.trim())
    }

    fn get_current_branch(&self) -> String {
        String::from(self.m_repo.head().unwrap().name().unwrap().split("/").last().unwrap())
    }

    fn get_number_of_parents(&self, sha1: &str) -> u64 {
        let commit = self.m_repo.find_commit(Oid::from_str(sha1).unwrap()).unwrap();

        commit.parent_count() as u64
    }

    fn get_statuses(&self) -> (HashMap<String, String>, HashMap<String, String>, HashMap<String, String>) {
        let mut staged: HashMap<String, String> = Default::default();
        let mut unstaged: HashMap<String, String> = Default::default();
        let mut untracked: HashMap<String, String> = Default::default();

        let mut options = StatusOptions::new();
        options.include_untracked(true);
        options.show(StatusShow::IndexAndWorkdir);

        let statuses = self.m_repo.statuses(Some(&mut options)).unwrap();

        for status in statuses.iter() {
            if status.status().is_index_new() == true {
                staged.insert(status.path().unwrap().to_string(), "new file".to_string());
            }

            if status.status().is_index_modified() == true {
                staged.insert(status.path().unwrap().to_string(), "modified".to_string());
            }

            if status.status().is_index_deleted() == true {
                staged.insert(status.path().unwrap().to_string(), "deleted".to_string());
            }

            if status.status().is_index_renamed() == true {
                staged.insert(status.path().unwrap().to_string(), "renamed".to_string());
            }

            if status.status().is_index_typechange() == true {
                staged.insert(status.path().unwrap().to_string(), "typechange".to_string());
            }

            if status.status().is_wt_modified() == true {
                unstaged.insert(status.path().unwrap().to_string(), "modified".to_string());
            }

            if status.status().is_wt_deleted() == true {
                unstaged.insert(status.path().unwrap().to_string(), "deleted".to_string());
            }

            if status.status().is_wt_renamed() == true {
                unstaged.insert(status.path().unwrap().to_string(), "renamed".to_string());
            }

            if status.status().is_wt_typechange() == true {
                unstaged.insert(status.path().unwrap().to_string(), "typechange".to_string());
            }

            if status.status().is_wt_new() == true {
                untracked.insert(status.path().unwrap().to_string(), "untracked".to_string());
            }
        }

        (staged, unstaged, untracked)
    }

//    fn get_history_diff(&self, sha1: &str, file_path: &Path) -> Vec<String> {
//        let oid = Oid::from_str(sha1).unwrap();
//        let commit = self.m_repo.find_commit(oid).unwrap();
//        let parent_commit = commit.parent(0).unwrap();
//
//        let commit_tree = commit.tree().unwrap();
//        let parent_commit_tree = parent_commit.tree().unwrap();
//
//        let mut bla = DiffOptions::new();
//        let diff = self.m_repo.diff_tree_to_tree(Some(&parent_commit_tree), Some(&commit_tree), Some(&mut bla)).unwrap();
//
//        let mut res = vec![];
//
//        diff.print(Patch, |delta, _hunk, line| {
//            let mut prefix = String::new();
//            match line.origin() {
//                '+' | '-' => prefix = line.origin().to_string(),
//                _ => {}
//            }
//
//            if delta.new_file().path().unwrap() == file_path {
//                res.push(prefix.to_string() + std::str::from_utf8(line.content()).unwrap());
//            }
//
//            true
//        });
//
//        res
//    }

    fn get_date(&self, sha1: &str) -> String {
        if sha1.is_empty() == true {
            return String::new();
        }

        let commit = self.m_repo.find_commit(Oid::from_str(sha1).unwrap()).unwrap();

        let d = UNIX_EPOCH + Duration::from_secs(commit.time().seconds() as u64);
        let datetime = DateTime::<Utc>::from(d);

        datetime.format("%Y-%m-%d %H:%M:%S").to_string()
    }

    fn get_repo_path(&self) -> &Path {
        self.m_repo.path().parent().unwrap()
    }
}