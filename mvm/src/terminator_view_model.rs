use crate::terminator::ITerminator;
use crate::historer::Historer;
use crate::completer::Completer;
use crate::terminator_colorizer::Color;
use std::cell::RefCell;
use std::rc::Rc;
use crate::terminator_colorizer::{TerminatorColorizer, ColorString};
use std::os::raw::c_char;
use std::ffi::{CString, CStr};
//use crate::{terminator_view_model_notify_terminal_data, terminator_view_model_notify_terminator_enablement, terminator_view_model_notify_terminator_set_focus, terminator_view_model_notify_clear_input_line};


pub(crate) trait ITerminatorViewModel {
    fn press_enter(&mut self, data: &mut str);
    fn append_to_input_line_and_press_enter(&mut self, data_to_write: &mut str);
    fn append_illegal_to_input_line_and_press_enter(&mut self, data_to_write: &mut str);
    fn set_focus_to_the_input_line(&self);
    fn append_data_to_terminator(&mut self, data: &mut str);
    fn go_earlier_in_history(&mut self);
    fn go_later_in_history(&mut self);
    fn get_normal_terminator_color(&mut self) -> Color;
    fn keyboard_shortcut_was_pressed(&mut self, was_ctrl_pressed: bool, key_code: usize);
}

pub struct TerminatorViewModel {
    m_terminator: Rc<RefCell<dyn ITerminator>>,
    m_terminator_colorizer: Rc<RefCell<TerminatorColorizer>>,
    m_completer: Rc<RefCell<Completer>>,
    m_historer: Rc<RefCell<Historer>>,
    m_res: Vec<ColorString>,
}

impl TerminatorViewModel {
    pub(crate) fn new(terminator: Rc<RefCell<dyn ITerminator>>, terminator_colorizer: Rc<RefCell<TerminatorColorizer>>, completer: Rc<RefCell<Completer>>, historer: Rc<RefCell<Historer>>) -> TerminatorViewModel {
        TerminatorViewModel {
            m_terminator: terminator,
            m_terminator_colorizer: terminator_colorizer,
            m_completer: completer,
            m_historer: historer,
            m_res: vec![],
        }
    }

    fn remove_terminator_prefix(&self, data: &str) -> String {
        match data.find("> ") {
            None => String::from(data),
            Some(_) => {
                match data.char_indices().skip(2).next() {
                    Some((_, _)) => String::from(data).as_str()[2..].to_string(),
                    None => String::new(),
                }
            }
        }
    }

    fn get_res_amount(&self) -> usize {
        self.m_res.len()
    }

    fn get_res(&self, line_index: usize) -> &str {
        &self.m_res[line_index].data
    }

    fn get_res_red(&self, line_index: usize) -> usize {
        self.m_res[line_index].color.red
    }
    fn get_res_green(&self, line_index: usize) -> usize {
        self.m_res[line_index].color.green
    }
    fn get_res_blue(&self, line_index: usize) -> usize {
        self.m_res[line_index].color.blue
    }

    fn write_data_to_terminator(&mut self, data: &mut str, color: Color) {
        let data_to_write = &self.remove_terminator_prefix(data);

        let mut color_to_write_with = Color::from(self.m_terminator_colorizer.as_ref().borrow().get_normal_terminal_color());

        if color.red != 0 || color.green != 0 || color.blue != 0
        {
            color_to_write_with.red = color.red;
            color_to_write_with.green = color.green;
            color_to_write_with.blue = color.blue;
        }

        //std::vector<COLOR_STRING>{COLOR_STRING{data_to_write, color_to_write_with, COLOR()}};
        self.m_res.clear();
        self.m_res = vec![ColorString::new(data_to_write, &color_to_write_with, &Color::new(0, 0, 0))];

        self.notify_the_terminal();
    }

    fn notify_the_terminal(&self) {
        unsafe {
            terminator_view_model_notify_terminal_data();
        }
    }
}

impl ITerminatorViewModel for TerminatorViewModel {
    fn press_enter(&mut self, data: &mut str) {
        let input_line_text = &self.remove_terminator_prefix(data);

        unsafe {
            terminator_view_model_notify_terminator_enablement(false);
        }

        self.m_res.clear();
        self.m_res = self.m_terminator.as_ref().borrow().execute_terminator_command(input_line_text);

        if input_line_text.is_empty() == false
        {
            //res.insert(res.begin(), COLOR_STRING { "\n" });
            self.m_res.insert(0, ColorString::new("\n", &Color::new(0, 0, 0), &Color::new(0, 0, 0)))
        }

        self.notify_the_terminal();

        unsafe {
            terminator_view_model_notify_terminator_enablement(true);
        }

        unsafe {
            terminator_view_model_notify_terminator_set_focus();
        }

        if input_line_text.is_empty() == false
        {
            self.m_historer.as_ref().borrow_mut().add(input_line_text.to_string());
        }
    }

    fn append_to_input_line_and_press_enter(&mut self, data_to_write: &mut str) {
        unsafe {
            terminator_view_model_notify_clear_input_line();
        }

        self.write_data_to_terminator(data_to_write, Color::new(0, 0, 0));

        self.press_enter(data_to_write);
    }

    fn append_illegal_to_input_line_and_press_enter(&mut self, data_to_write: &mut str) {
        self.write_data_to_terminator(data_to_write, Color::new(0, 0, 0));

        self.press_enter(String::new().as_mut_str());
    }

    fn set_focus_to_the_input_line(&self) {
        unsafe {
            terminator_view_model_notify_terminator_set_focus();
        }
    }

    fn append_data_to_terminator(&mut self, data: &mut str) {
        let data_to_append = &self.remove_terminator_prefix(data);

        let completion = self.m_completer.as_ref().borrow().get_completion(data_to_append);

        if completion.is_empty() == true {
            return;
        }

        if completion.len() == 1 {
            self.m_res.clear();
            self.m_res = vec![ColorString::new(&completion[0], self.m_terminator_colorizer.as_ref().borrow().get_normal_terminal_color(), &Color::new(0, 0, 0))];

            self.notify_the_terminal();
        } else {
            self.write_data_to_terminator(String::from("\n").as_mut_str(), Color::new(0, 0, 0));

            for data in completion {
                let bla = Color::from(self.m_terminator_colorizer.as_ref().borrow().get_white_terminal_color());
                self.write_data_to_terminator(String::from(data + "\n").as_mut_str(), bla);
            }

            self.press_enter(String::new().as_mut_str());

            self.m_res.clear();
            self.m_res = vec![ColorString::new(data_to_append, self.m_terminator_colorizer.as_ref().borrow().get_normal_terminal_color(), &Color::new(0, 0, 0))];

            self.notify_the_terminal();
        }
    }

    fn go_earlier_in_history(&mut self) {
        let res = self.m_historer.as_ref().borrow_mut().get_earlier();

        if res.is_empty() == false {
            unsafe {
                terminator_view_model_notify_clear_input_line();
            }

            self.m_res.clear();
            self.m_res = vec![ColorString::new(&res, self.m_terminator_colorizer.as_ref().borrow().get_normal_terminal_color(), &Color::new(0, 0, 0))];

            self.notify_the_terminal();
        }
    }

    fn go_later_in_history(&mut self) {
        let res = self.m_historer.as_ref().borrow_mut().get_later();

        if res.is_empty() == false {
            unsafe {
                terminator_view_model_notify_clear_input_line();
            }

            self.m_res.clear();
            self.m_res = vec![ColorString::new(&res, self.m_terminator_colorizer.as_ref().borrow().get_normal_terminal_color(), &Color::new(0, 0, 0))];

            self.notify_the_terminal();
        }
    }

    fn get_normal_terminator_color(&mut self) -> Color {
        let bla = self.m_terminator_colorizer.as_ref().borrow();
        let color = Color::from(bla.get_normal_terminal_color());

        color
    }

    fn keyboard_shortcut_was_pressed(&mut self, was_ctrl_pressed: bool, key_code: usize) {
        if was_ctrl_pressed == true && key_code == 0x53 { // The letter S.
            self.append_to_input_line_and_press_enter(String::from("git status").as_mut_str());
        }
        if was_ctrl_pressed == true && key_code == 0x52 { // The letter R.
            self.append_illegal_to_input_line_and_press_enter(String::from(
                "git log\nI will not print it out here, its kinda long, but please do be welcomed to take a look at the Forester tab.\n").as_mut_str());
        }
        if was_ctrl_pressed == true && key_code == 0x47 {// The letter G.
            self.set_focus_to_the_input_line();
        }
    }
}

extern "C" {
    fn terminator_view_model_notify_terminator_enablement(enabled: bool);
}

extern "C" {
    fn terminator_view_model_notify_terminal_data();
}

extern "C" {
    fn terminator_view_model_notify_terminator_set_focus();
}

extern "C" {
    fn terminator_view_model_notify_clear_input_line();
}

#[no_mangle]
pub extern fn terminator_view_model_get_res_amount(ptr: *mut TerminatorViewModel) -> usize {
    let terminator_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    terminator_view_model.get_res_amount()
}

#[no_mangle]
pub extern fn terminator_view_model_get_res(ptr: *mut TerminatorViewModel, line_index: usize) -> *const c_char {
    let terminator_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let res = terminator_view_model.get_res(line_index);

    let s = CString::new(res).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn terminator_view_model_get_res_red(ptr: *mut TerminatorViewModel, line_index: usize) -> usize {
    let terminator_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    terminator_view_model.get_res_red(line_index)
}

#[no_mangle]
pub extern fn terminator_view_model_get_res_green(ptr: *mut TerminatorViewModel, line_index: usize) -> usize {
    let terminator_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    terminator_view_model.get_res_green(line_index)
}

#[no_mangle]
pub extern fn terminator_view_model_get_res_blue(ptr: *mut TerminatorViewModel, line_index: usize) -> usize {
    let terminator_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    terminator_view_model.get_res_blue(line_index)
}

#[no_mangle]
pub extern fn terminator_view_model_get_normal_terminator_color_red(ptr: *mut TerminatorViewModel) -> usize {
    let terminator_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    terminator_view_model.get_normal_terminator_color().red
}

#[no_mangle]
pub extern fn terminator_view_model_get_normal_terminator_color_green(ptr: *mut TerminatorViewModel) -> usize {
    let terminator_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    terminator_view_model.get_normal_terminator_color().green
}

#[no_mangle]
pub extern fn terminator_view_model_get_normal_terminator_color_blue(ptr: *mut TerminatorViewModel) -> usize {
    let terminator_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    terminator_view_model.get_normal_terminator_color().blue
}

#[no_mangle]
pub extern fn terminator_view_model_press_enter(ptr: *mut TerminatorViewModel, s: *const c_char) {
    let terminator_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let c_str = unsafe {
        assert!(!s.is_null());

        CStr::from_ptr(s)
    };
    let mut bla = String::from(c_str.to_str().unwrap());
    let r_str = bla.as_mut_str();

    terminator_view_model.press_enter(r_str);
}

#[no_mangle]
pub extern fn terminator_view_model_go_earlier_in_history(ptr: *mut TerminatorViewModel) {
    let terminator_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    terminator_view_model.go_earlier_in_history();
}

#[no_mangle]
pub extern fn terminator_view_model_go_later_in_history(ptr: *mut TerminatorViewModel) {
    let terminator_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    terminator_view_model.go_later_in_history();
}

#[no_mangle]
pub extern fn terminator_view_model_append_data_to_terminator(ptr: *mut TerminatorViewModel, s: *const c_char) {
    let terminator_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let c_str = unsafe {
        assert!(!s.is_null());

        CStr::from_ptr(s)
    };
    let mut bla = String::from(c_str.to_str().unwrap());
    let r_str = bla.as_mut_str();

    terminator_view_model.append_data_to_terminator(r_str);
}