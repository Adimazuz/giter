use mockall::*;
use mockall::predicate::*;


#[automock]
pub(crate) trait ITreeViewModelFFI {
    fn tree_view_model_notify_new_line(&self, input: usize);
    fn tree_view_model_notify_clear_the_tree(&self);
    fn tree_view_model_notify_tree_enablement(&self, enabled: bool);
    fn tree_view_model_notify_tree_selection(&self);
    fn tree_view_model_copy_paste_was_pressed(&self);
}

pub struct TreeViewModelFFI {}

impl TreeViewModelFFI {
    pub(crate) fn new() -> TreeViewModelFFI {
        TreeViewModelFFI {}
    }
}

impl ITreeViewModelFFI for TreeViewModelFFI {
    fn tree_view_model_notify_new_line(&self, input: usize) {
        unsafe {
            tree_view_model_notify_new_line(input);
        }
    }

    fn tree_view_model_notify_clear_the_tree(&self) {
        unsafe {
            tree_view_model_notify_clear_the_tree();
        }
    }

    fn tree_view_model_notify_tree_enablement(&self, enabled: bool) {
        unsafe {
            tree_view_model_notify_tree_enablement(enabled);
        }
    }

    fn tree_view_model_notify_tree_selection(&self) {
        unsafe {
            tree_view_model_notify_tree_selection();
        }
    }

    fn tree_view_model_copy_paste_was_pressed(&self) {
        unsafe {
            tree_view_model_copy_paste_was_pressed();
        }
    }
}

extern "C" {
    fn tree_view_model_notify_new_line(input: usize);
}

extern "C" {
    fn tree_view_model_notify_clear_the_tree();
}

extern "C" {
    fn tree_view_model_notify_tree_enablement(enabled: bool);
}

extern "C" {
    fn tree_view_model_notify_tree_selection();
}

extern "C" {
    fn tree_view_model_copy_paste_was_pressed();
}