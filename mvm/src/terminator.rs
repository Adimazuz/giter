use crate::terminator_colorizer::TerminatorColorizer;
use crate::terminator_colorizer::ColorString;
use std::cell::RefCell;
use std::rc::Rc;
use std::env;
use crate::model::pipe::ipipe::IPipe;
use winapi::um::winnt::{PHANDLE, HANDLE};
use winapi::um::minwinbase::{SECURITY_ATTRIBUTES, LPSECURITY_ATTRIBUTES};
use winapi::um::processthreadsapi::{STARTUPINFOW, LPSTARTUPINFOW, LPPROCESS_INFORMATION};
use winapi::um::processthreadsapi::PROCESS_INFORMATION;
use std::ptr::null_mut;
use winapi::_core::mem;
use winapi::um::winbase::{STARTF_USESHOWWINDOW, STARTF_USESTDHANDLES, CREATE_NEW_CONSOLE, WAIT_OBJECT_0};
use winapi::um::winuser::SW_HIDE;
use std::ffi::OsStr;
use std::os::windows::ffi::OsStrExt;
use winapi::_core::iter::once;
use winapi::shared::minwindef::{LPDWORD, LPVOID};
use winapi::_core::cmp::min;
use mockall::*;
use mockall::predicate::*;
use crate::model::lib_giter::ilib_giter::ILibGiter;


#[automock]
pub(crate) trait ITerminator {
    fn execute_terminator_command(&self, data: &str) -> Vec<ColorString>;
    fn execute_shell_command(&self, data: &str) -> Vec<String>;
    fn get_string_tree(&self) -> String;
}

pub struct Terminator {
    m_pipe: Box<dyn IPipe>,
    m_lib_giter: Rc<RefCell<dyn ILibGiter>>,
    m_terminator_colorizer: Rc<RefCell<TerminatorColorizer>>,
}

impl Terminator {
    pub(crate) fn new(pipe: Box<dyn IPipe>, lib_giter: Rc<RefCell<dyn ILibGiter>>, terminator_colorizer: Rc<RefCell<TerminatorColorizer>>) -> Terminator {
        Terminator {
            m_pipe: pipe,
            m_lib_giter: lib_giter,
            m_terminator_colorizer: terminator_colorizer,
        }

//        let res = Terminator {
//            m_pipe: pipe,
//            m_sheller: sheller,
//            m_lib_giter: lib_giter,
//            m_terminator_colorizer: terminator_colorizer,
//        };
//
//        let bla = res.talk_to_the_shell("sdfsd");
//
//        res
    }

    fn talk_to_the_shell(&self, data: &str) -> String {
        let mut h_pipe_read: HANDLE = null_mut();
        let mut h_pipe_write: HANDLE = null_mut();
        let mut sa_attr = SECURITY_ATTRIBUTES {
            nLength: mem::size_of::<SECURITY_ATTRIBUTES>() as u32,
            lpSecurityDescriptor: null_mut(),
            bInheritHandle: true as i32,
        };
        if self.m_pipe.create_pipe(&mut h_pipe_read as PHANDLE, &mut h_pipe_write as PHANDLE, &mut sa_attr as LPSECURITY_ATTRIBUTES, 0) == false {
            panic!("Panic! Could not create a pipe for shell execution!");
        }

        let mut si = STARTUPINFOW {
            cb: mem::size_of::<STARTUPINFOW>() as u32,
            lpReserved: null_mut(),
            lpDesktop: null_mut(),
            lpTitle: null_mut(),
            dwX: 0,
            dwY: 0,
            dwXSize: 0,
            dwYSize: 0,
            dwXCountChars: 0,
            dwYCountChars: 0,
            dwFillAttribute: 0,
            dwFlags: 0,
            wShowWindow: 0,
            cbReserved2: 0,
            lpReserved2: null_mut(),
            hStdInput: null_mut(),
            hStdOutput: null_mut(),
            hStdError: null_mut(),
        };
        si.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
        si.hStdOutput = h_pipe_write;
        si.hStdError = h_pipe_write;
        si.wShowWindow = SW_HIDE as u16;

        let mut pi = PROCESS_INFORMATION {
            hProcess: null_mut(),
            hThread: null_mut(),
            dwProcessId: 0,
            dwThreadId: 0,
        };

        let mut wide: Vec<u16> = OsStr::new(data).encode_wide().chain(once(0)).collect();
        if self.m_pipe.create_process(null_mut(), wide.as_mut_ptr(), null_mut(), null_mut(), 1,
                                      CREATE_NEW_CONSOLE, null_mut(), null_mut(), &mut si as LPSTARTUPINFOW, &mut pi as LPPROCESS_INFORMATION) == false {
            self.m_pipe.close_handle(h_pipe_write);
            self.m_pipe.close_handle(h_pipe_read);
            if data.is_empty() == true {
                return String::new();
            } else {
                return "'".to_string() + data + "'" + " is not recognized as an internal or external command, operable program or batch file.\n";
            }
        }

        let mut res: String = "".to_string();
        let mut has_process_ended = false;
        while !has_process_ended {
            has_process_ended = self.m_pipe.wait_for_single_object(pi.hProcess, 50) == WAIT_OBJECT_0;

            // Even if process exited - we continue reading, if there is some data available over pipe.
            loop {
                let mut avail: u32 = 0;
                if self.m_pipe.peek_named_pipe(h_pipe_read, null_mut(), 0, null_mut(), &mut avail as LPDWORD, null_mut()) == false {
                    break;
                }

                if avail == 0 {
                    // No data available, return.
                    break;
                }

                let mut read: u32 = 0;
                //let mut bla: [u8; 1024];
                let mut buf: Vec<u8> = vec![0; 1024];
                if self.m_pipe.read_file(h_pipe_read, buf.as_mut_ptr() as LPVOID, min((buf.len() - 1) as u32, avail), &mut read as LPDWORD, null_mut()) == false || read == 0 {
                    // Error, the child process might ended.
                    panic!("Panic! Read_file() failed!");
                }

                res = res + &String::from_utf8(buf[0..(read as usize)].to_owned()).unwrap();
            }
        }

        self.m_pipe.close_handle(h_pipe_write);
        self.m_pipe.close_handle(h_pipe_read);
        self.m_pipe.close_handle(pi.hProcess);
        self.m_pipe.close_handle(pi.hThread);

        res
    }
}

impl ITerminator for Terminator {
    fn execute_terminator_command(&self, data: &str) -> Vec<ColorString> {
        let mut res = self.execute_shell_command(data);

        res.push("\n".to_string());
        res.push(env::current_dir().unwrap().to_str().unwrap().to_string() + " (" + &self.m_lib_giter.borrow_mut().get_current_branch() + ")");
        res.push("\n".to_string());
        res.push("> ".to_string());

        self.m_terminator_colorizer.borrow_mut().colorize(&res)
    }

    fn execute_shell_command(&self, data: &str) -> Vec<String> {
        //let data = self.m_sheller.talk_to_the_shell(data);
        let data = self.talk_to_the_shell(data);

        // Break into lines.
        let mut res = vec![];
        let lines = data.lines();
        for line in lines {
            res.push(line.to_string() + "\n")
        }
        //res.push("\n".to_string());
        res
    }

    fn get_string_tree(&self) -> String {
        //self.m_sheller.talk_to_the_shell("git log --oneline --graph --no-abbrev-commit --all")
        self.talk_to_the_shell("git log --no-color --oneline --graph --no-abbrev-commit --all")
    }
}