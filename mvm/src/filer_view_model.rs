use std::rc::Rc;
use std::cell::RefCell;
use crate::terminator::ITerminator;
use crate::terminator_colorizer::{ColorString, TerminatorColorizer};
use crate::tree_line_parser::TreeLineParser;
use std::os::raw::c_char;
use std::ffi::{CString, CStr};
use std::path::PathBuf;
use git2::IntoCString;
use crate::filer_view_model_ffi::IFilerViewModelFFI;
use crate::model::lib_giter::ilib_giter::ILibGiter;


struct HistoryLine {
    m_commit_message: String,
    m_date: String,
    m_sha1: String,
}

pub struct FilerViewModel {
    m_lib_giter: Rc<RefCell<dyn ILibGiter>>,
    m_terminator: Rc<RefCell<dyn ITerminator>>,
    m_terminator_colorizer: Rc<RefCell<TerminatorColorizer>>,
    m_ffi: Rc<RefCell<dyn IFilerViewModelFFI>>,
    m_history: Vec<HistoryLine>,
    m_diff: Vec<ColorString>,
}

impl FilerViewModel {
    pub(crate) fn new(lib_giter: Rc<RefCell<dyn ILibGiter>>, terminator: Rc<RefCell<dyn ITerminator>>, terminator_colorizer: Rc<RefCell<TerminatorColorizer>>, filer_view_model_ffi: Rc<RefCell<dyn IFilerViewModelFFI>>) -> FilerViewModel {
        FilerViewModel {
            m_lib_giter: lib_giter,
            m_terminator: terminator,
            m_terminator_colorizer: terminator_colorizer,
            m_ffi: filer_view_model_ffi,
            m_history: vec![],
            m_diff: vec![],
        }
    }

    fn get_history(&mut self, file_path: &str) -> &Vec<HistoryLine> {
        self.m_history = vec![];

        let data = self.m_terminator.borrow().execute_shell_command(("git log --oneline --no-color --graph --no-abbrev-commit --all -- ".to_owned() + file_path).as_str());
        if data.len() == 0 {
            let history_line = HistoryLine {
                m_commit_message: "Path not in repo!".to_string(),
                m_date: "N/A".to_string(),
                m_sha1: "N/A".to_string(),
            };
            self.m_history.push(history_line);
        } else {
            for mut datum in data {
                datum.pop();
                if datum == "...".to_string() {
                    continue;
                }

                let sha1 = TreeLineParser::new(&datum).get_sha1_from_line();
                if sha1.is_empty() == true {
                    continue;
                }

                let commit_message = self.m_lib_giter.borrow().get_commit_message(sha1.as_str());
                let date = self.m_lib_giter.borrow().get_date(sha1.as_str());
                let history_line = HistoryLine {
                    m_commit_message: commit_message,
                    m_date: date,
                    m_sha1: sha1,
                };
                self.m_history.push(history_line);
            }
        }

        &self.m_history
    }

    fn get_diff_text(&mut self, sha1: &str, sha1_previous: &str, file_path: &str) -> &Vec<ColorString> {
        let data;
        if sha1_previous.is_empty() == true {
            self.m_diff.clear();

            self.m_diff.push(ColorString::new("Its the last item in the list. Its impossible to show git diff for it - it has no parents.",
                                              self.m_terminator_colorizer.borrow().get_diff_normal_color(), self.m_terminator_colorizer.borrow().get_diff_background_normal_color()));

            return &self.m_diff;
        } else {
            data = self.m_terminator.borrow().execute_shell_command(String::from(
                "git -c diff.submodule=short -c diff.noprefix=false diff --no-color --unified=3 -M -C ".to_string() + sha1_previous + " "
                    + sha1 + " -- " + /*".gitignore"*/ file_path).as_str());
        }

        self.m_diff = self.m_terminator_colorizer.borrow().colorize_single_diff(&data);

        if self.m_diff.is_empty() == true {
            self.m_diff.clear();

            self.m_diff.push(ColorString::new("There was nothing to show here. Its a bug!",
                                              self.m_terminator_colorizer.borrow().get_diff_normal_color(), self.m_terminator_colorizer.borrow().get_diff_background_normal_color()));
            return &self.m_diff;
        }

        &self.m_diff
    }

    fn get_repo_path(&self) -> PathBuf {
        PathBuf::from(self.m_lib_giter.borrow().get_repo_path())
    }

    pub(crate) fn keyboard_shortcut_was_pressed(&mut self, was_ctrl_pressed: bool, key_code: usize) {
        if was_ctrl_pressed == true && key_code == 0x52 { // The letter R.
            self.m_ffi.borrow().filer_view_model_notify_refresh();
        }
    }
}

#[no_mangle]
pub extern fn filer_view_model_get_history(ptr: *mut FilerViewModel, s: *const c_char) {
    let filer_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let c_str = unsafe {
        assert!(!s.is_null());
        CStr::from_ptr(s)
    };
    let r_str = c_str.to_str().unwrap();

    filer_view_model.get_history(r_str);
}

#[no_mangle]
pub extern fn filer_view_model_history_amount(ptr: *mut FilerViewModel) -> usize {
    let filer_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    filer_view_model.m_history.len()
}

#[no_mangle]
pub extern fn filer_view_model_history_commit_message(ptr: *mut FilerViewModel, line_index: usize) -> *const c_char {
    let filer_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

//filer_view_model.m_history[line_index].m_commit_message
    let commit_message = &filer_view_model.m_history[line_index].m_commit_message;

    let s = CString::new(commit_message.as_str()).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn filer_view_model_history_date(ptr: *mut FilerViewModel, line_index: usize) -> *const c_char {
    let filer_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

//filer_view_model.m_history[line_index].m_commit_message
    let commit_message = &filer_view_model.m_history[line_index].m_date;

    let s = CString::new(commit_message.as_str()).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn filer_view_model_history_sha1(ptr: *mut FilerViewModel, line_index: usize) -> *const c_char {
    let filer_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

//filer_view_model.m_history[line_index].m_commit_message
    let commit_message = &filer_view_model.m_history[line_index].m_sha1;

    let s = CString::new(commit_message.as_str()).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn filer_view_model_get_diff(ptr: *mut FilerViewModel, sha1: *const c_char, sha1_previous: *const c_char, file_path: *const c_char) {
    let filer_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let c_str = unsafe {
        assert!(!sha1.is_null());
        CStr::from_ptr(sha1)
    };
    let mut bla = String::from(c_str.to_str().unwrap());
    let sha1 = bla.as_mut_str();

    let c_str = unsafe {
        assert!(!sha1_previous.is_null());
        CStr::from_ptr(sha1_previous)
    };
    let mut bla = String::from(c_str.to_str().unwrap());
    let sha1_previous = bla.as_mut_str();

    let c_str = unsafe {
        assert!(!file_path.is_null());
        CStr::from_ptr(file_path)
    };
    let mut bla = String::from(c_str.to_str().unwrap());
    let file_path = bla.as_mut_str();

    filer_view_model.get_diff_text(sha1, sha1_previous, file_path);
}

#[no_mangle]
pub extern fn filer_view_model_get_diff_amount(ptr: *mut FilerViewModel) -> usize {
    let filer_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    filer_view_model.m_diff.len()
}

#[no_mangle]
pub extern fn filer_view_model_get_diff_data(ptr: *mut FilerViewModel, index: usize) -> *const c_char {
    let filer_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let res = filer_view_model.m_diff[index].data.as_str();

    let s = CString::new(res).unwrap();
    s.into_raw()
}

#[no_mangle]
pub extern fn filer_view_model_get_diff_red(ptr: *mut FilerViewModel, index: usize) -> usize {
    let filer_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    filer_view_model.m_diff[index].color.red
}

#[no_mangle]
pub extern fn filer_view_model_get_diff_green(ptr: *mut FilerViewModel, index: usize) -> usize {
    let filer_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    filer_view_model.m_diff[index].color.green
}

#[no_mangle]
pub extern fn filer_view_model_get_diff_blue(ptr: *mut FilerViewModel, index: usize) -> usize {
    let filer_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    filer_view_model.m_diff[index].color.blue
}

#[no_mangle]
pub extern fn filer_view_model_get_diff_background_red(ptr: *mut FilerViewModel, index: usize) -> usize {
    let filer_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    filer_view_model.m_diff[index]._background_color.red
}

#[no_mangle]
pub extern fn filer_view_model_get_diff_background_green(ptr: *mut FilerViewModel, index: usize) -> usize {
    let filer_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    filer_view_model.m_diff[index]._background_color.green
}

#[no_mangle]
pub extern fn filer_view_model_get_diff_background_blue(ptr: *mut FilerViewModel, index: usize) -> usize {
    let filer_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    filer_view_model.m_diff[index]._background_color.blue
}

#[no_mangle]
pub extern fn filer_view_model_get_repo_path(ptr: *mut FilerViewModel) -> *const c_char {
    let filer_view_model = unsafe {
        assert!(!ptr.is_null());
        &mut *ptr
    };

    let res = filer_view_model.get_repo_path();

    res.into_c_string().unwrap().into_raw()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::terminator::ITerminator;
    use crate::terminator::Terminator;
    use crate::terminator::MockITerminator;
    //use crate::lib_giter::MockILibGiter;
    use crate::model::lib_giter::ilib_giter::MockILibGiter;
    use crate::filer_view_model_ffi::MockIFilerViewModelFFI;


    fn setup(mock_lib_giter: MockILibGiter, mock_terminator: MockITerminator, mock_ffi: MockIFilerViewModelFFI) -> FilerViewModel {
        let terminator_colorizer: Rc<RefCell<TerminatorColorizer>> = Rc::new(RefCell::new(TerminatorColorizer::new()));

        FilerViewModel::new(Rc::new(RefCell::new(mock_lib_giter)), Rc::new(RefCell::new(mock_terminator)), terminator_colorizer, Rc::new(RefCell::new(mock_ffi)))
    }

    #[test]
    fn get_diff_empty() {
        let mut mock_terminator = MockITerminator::new();
        mock_terminator.expect_execute_shell_command().return_const(vec![]);

        let mut filer_view_model = setup(MockILibGiter::new(), mock_terminator, MockIFilerViewModelFFI::new());

        let res = filer_view_model.get_diff_text("some sha1 goes here", "some sha1 goes here", "some_file_path_goes_here");

        assert_eq!(res[0].data, "There was nothing to show here. Its a bug!".to_string());
    }

    fn get_history_string_test_data() -> Vec<String> {
        let mut res = vec![];
        res.push("...\n".to_string());
        res.push("| * 1b5b4e0aa7ba4d55ebc86309b0b6640ef9d5b458 Revert Revert Merge branch feature-make\n".to_string());
        res.push("...\n".to_string());
        res.push("| | * 187d2d7eebfd226acf6a9f0191cd1a60987e2b1c Update .gitignore\n".to_string());
        res.push("...\n".to_string());
        res.push("| * | | 8075badba2d597a8fcea6e4805efa9e5dbe6b23c changed value in used USB\n".to_string());
        res.push("...\n".to_string());
        res.push("| | | * 2b179178548e3eecaafffd43e108a6cde7602401 Revert Revert Merge branch feature-make2\n".to_string());
        res.push("| |_|/\n".to_string());
        res.push("|/| |\n".to_string());
        res.push("...\n".to_string());
        res.push("| * | | 8e2c7c75379843519b70170aec948b9fc51cb0dd Fix release build\n".to_string());

        res
    }

    #[test]
    fn get_history_not_repo_file() {
        let mut mock_terminator = MockITerminator::new();
        mock_terminator.expect_execute_shell_command().return_const(vec![]);
        let mut filer_view_model = setup(MockILibGiter::new(), mock_terminator, MockIFilerViewModelFFI::new());

        let res = filer_view_model.get_history("some file path goes here");

        assert_eq!(res.len(), 1);

        assert_eq!(res[0].m_commit_message, "Path not in repo!".to_string());
        assert_eq!(res[0].m_date, "N/A".to_string());
        assert_eq!(res[0].m_sha1, "N/A".to_string());
    }

    #[test]
    fn get_history() {
        let mut mock_lib_giter = MockILibGiter::new();
        mock_lib_giter.expect_get_date().return_const("42-42-4242 42:42:42");
        mock_lib_giter.expect_get_commit_message().return_const("some commit message goes here");
        let mut mock_terminator = MockITerminator::new();
        mock_terminator.expect_execute_shell_command().return_const(get_history_string_test_data());
        let mut filer_view_model = setup(mock_lib_giter, mock_terminator, MockIFilerViewModelFFI::new());

        let res = filer_view_model.get_history("some file path goes here");

        assert_eq!(res.len(), 5);

        assert_eq!(res[0].m_commit_message, "some commit message goes here".to_string());
        assert_eq!(res[0].m_date, "42-42-4242 42:42:42".to_string());
        assert_eq!(res[0].m_sha1, "1b5b4e0aa7ba4d55ebc86309b0b6640ef9d5b458".to_string());

        assert_eq!(res[1].m_commit_message, "some commit message goes here".to_string());
        assert_eq!(res[1].m_date, "42-42-4242 42:42:42".to_string());
        assert_eq!(res[1].m_sha1, "187d2d7eebfd226acf6a9f0191cd1a60987e2b1c".to_string());

        assert_eq!(res[2].m_commit_message, "some commit message goes here".to_string());
        assert_eq!(res[2].m_date, "42-42-4242 42:42:42".to_string());
        assert_eq!(res[2].m_sha1, "8075badba2d597a8fcea6e4805efa9e5dbe6b23c".to_string());

        assert_eq!(res[3].m_commit_message, "some commit message goes here".to_string());
        assert_eq!(res[3].m_date, "42-42-4242 42:42:42".to_string());
        assert_eq!(res[3].m_sha1, "2b179178548e3eecaafffd43e108a6cde7602401".to_string());

        assert_eq!(res[4].m_commit_message, "some commit message goes here".to_string());
        assert_eq!(res[4].m_date, "42-42-4242 42:42:42".to_string());
        assert_eq!(res[4].m_sha1, "8e2c7c75379843519b70170aec948b9fc51cb0dd".to_string());
    }

    #[test]
    fn keyboard_shortcut_was_pressed_ctrl_plus_r() {
        let mut mock_ffi = MockIFilerViewModelFFI::new();
        mock_ffi.expect_filer_view_model_notify_refresh().times(1).return_const(());
        let mut filer_view_model = setup(MockILibGiter::new(), MockITerminator::new(), mock_ffi);

        filer_view_model.keyboard_shortcut_was_pressed(true, 0x52 /* The letter R*/);
    }
}