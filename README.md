[![pipeline status](https://gitlab.com/refaelsh/giter/badges/master/pipeline.svg)](https://gitlab.com/refaelsh/giter/commits/master)

# Giter - the philosophy

This project is about yet another GUI client for Git, but different.
It will mix elements from both CLI and GUI.
It will have as minimalistic features set as possible, because, well, I don't like `feature-bloat`.
I've seen many Git GUI clients over the years, and, IMHO, they all suffer from one big problem: they are all too complex.
Git is complex enough, and it takes time and effort to learn it.
If you add on top of it learning the quirks of some GUI, it becomes too much.
The GUI should be a simple wrapper around Git CLI. For example, I've once seen a GUI that had a `pull` button, but what it was doing in the background is something like `git pull --all --force --modules --init` and etc., and the user had no clue. It forced the user to learn the quirks of that specific GUI and not Git in general. Giter, would not have such a button, in fact, Giter would almost have no buttons at all. And the ones it will have will be very simple and will reflect their action in a terminal window that is embedded in Giter.

# Cross Platformability

This project is intended to be cross platform: winblows and Linux.
In first stage it will be only winblows (alas, the IT of my work place does not allows Linux).

# Contributions guide

Of course contributions are always welcome. Please feel free to open a Merge Request and/or
open an issue.

# Notes on compiling

Nothing special goes here. Just rebuild every project in the solution and you are good to go.
