#pragma once
#pragma warning( push )
#pragma warning( disable : 4267)
#include "MainFrameBase.h"
#pragma warning(pop)
#include "TerminatorControl.h"
#include "TreeControl.h"
#include "StatuserControl.h"
#include "FilerControl.h"
#include "ViewModelRustFFI.h"
#include <apiquery2.h>


class MainFrame final : public MainFrameBase
{
public:
	/**
	 * \brief Constructs the main window of Giter.
	 * \param parent The parent window.
	 * \param view_model An instance of the ViewModel class.
	 */
	explicit MainFrame(wxWindow* parent, VIEW_MODEL_T* view_model_ffi);

	void Main_frame_on_char_hook(wxKeyEvent& event) override;

private:

	VIEW_MODEL_T* m_view_model_ffi;

	TerminatorControl* m_terminator_control{};

	TreeControl* m_tree_control{};

	StatuserControl* m_statuser_control{};
	FilerControl* m_filer_control{};

	void Init_terminator_control();
	void Init_tree_control();
	void Init_statuser_control();
	void Init_filer_control();
	void Init_status_bar() const;
};
