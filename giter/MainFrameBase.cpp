///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "MainFrameBase.h"

///////////////////////////////////////////////////////////////////////////

MainFrameBase::MainFrameBase(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxFrame(
	parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);

	wxBoxSizer* box_sizer;
	box_sizer = new wxBoxSizer(wxHORIZONTAL);

	m_splitter1 = new wxSplitterWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D);
	m_splitter1->Connect(wxEVT_IDLE, wxIdleEventHandler(MainFrameBase::m_splitter1OnIdle), NULL, this);

	m_splitter1->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_MENU));

	m_panel1 = new wxPanel(m_splitter1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	m_panel1->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_MENU));

	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer(wxHORIZONTAL);

	m_notebook1 = new wxNotebook(m_panel1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxNB_TOP);
	m_notebook1->SetForegroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
	m_notebook1->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_MENU));

	m_tree_panel = new wxPanel(m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	m_tree_panel->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_MENU));

	m_notebook1->AddPage(m_tree_panel, wxT("Forester"), true);
	m_statuser_panel = new wxPanel(m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	m_statuser_panel->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_MENU));

	m_notebook1->AddPage(m_statuser_panel, wxT("Statuser"), false);
	m_panel111 = new wxPanel(m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	wxBoxSizer* bSizer132;
	bSizer132 = new wxBoxSizer(wxVERTICAL);

	m_staticText1 = new wxStaticText(m_panel111, wxID_ANY, wxT("CTRL+S: executes `git status` and refreshes the Statuser screen."), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText1->Wrap(-1);
	bSizer132->Add(m_staticText1, 0, wxALL, 5);

	m_staticText2 = new wxStaticText(m_panel111, wxID_ANY, wxT("CTRL+R: executes `git log` and regreshes everthing, all tabs, all screens, including the Tree."), wxDefaultPosition,
	                                 wxDefaultSize, 0);
	m_staticText2->Wrap(-1);
	bSizer132->Add(m_staticText2, 0, wxALL, 5);

	m_staticText3 = new wxStaticText(m_panel111, wxID_ANY, wxT("CTRL+G: sets the focus to the Terminator."), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText3->Wrap(-1);
	bSizer132->Add(m_staticText3, 0, wxALL, 5);

	m_staticText4 = new wxStaticText(m_panel111, wxID_ANY, wxT("CTRL+T: will set focus to the Tree when the Tree tab is selected."), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText4->Wrap(-1);
	bSizer132->Add(m_staticText4, 0, wxALL, 5);

	m_staticText5 = new wxStaticText(m_panel111, wxID_ANY, wxT("CTRL+C: will copy/paste from the Tree when the Tree tab is selected."), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText5->Wrap(-1);
	bSizer132->Add(m_staticText5, 0, wxALL, 5);

	m_filer_panel = new wxPanel(m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	m_notebook1->AddPage(m_filer_panel, wxT("Filer"), false);

	m_panel111->SetSizer(bSizer132);
	m_panel111->Layout();
	bSizer132->Fit(m_panel111);
	m_notebook1->AddPage(m_panel111, wxT("Helper"), false);

	bSizer4->Add(m_notebook1, 1, wxEXPAND | wxALL, 5);

	m_panel1->SetSizer(bSizer4);
	m_panel1->Layout();
	bSizer4->Fit(m_panel1);
	m_terminator_panel = new wxPanel(m_splitter1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	m_terminator_panel->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_MENU));

	m_splitter1->SplitHorizontally(m_panel1, m_terminator_panel, 494);
	box_sizer->Add(m_splitter1, 1, wxEXPAND, 5);


	this->SetSizer(box_sizer);
	this->Layout();
	m_status_bar = this->CreateStatusBar(1, wxSTB_DEFAULT_STYLE, wxID_ANY);

	this->Centre(wxBOTH);

	// Connect Events
	this->Connect(wxEVT_CHAR_HOOK, wxKeyEventHandler(MainFrameBase::Main_frame_on_char_hook));
}

MainFrameBase::~MainFrameBase()
{
	// Disconnect Events
	this->Disconnect(wxEVT_CHAR_HOOK, wxKeyEventHandler(MainFrameBase::Main_frame_on_char_hook));
}
