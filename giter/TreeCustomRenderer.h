#pragma once
#include "wx/dataview.h"
#include "wx/splitter.h"
//#include "Tree.h"
#include "TreeViewModelRustFFI.h"


class TreeCustomRenderer final : public wxDataViewCustomRenderer
{
public:
	explicit TreeCustomRenderer(TREE_VIEW_MODEL_T* tree_view_model_ffi);
	~TreeCustomRenderer() = default;

	bool SetValue(const wxVariant& value) override;
	bool GetValue(wxVariant& value) const override;
	[[nodiscard]] wxSize GetSize() const override;
	bool Render(wxRect cell, wxDC* dc, int state) override;

private:
	TREE_VIEW_MODEL_T* m_view_model_ffi;
	/**
	 * \brief Holds the current line index that the Render() method will render.
	 */
	unsigned int m_line_index{};

	//ITreeViewModel* m_view_model;

	/**
	 * \brief Holds all the possible colors for the graph.
	 */
	std::vector<wxColor> m_colors;

	/**
	 * \brief Holds the radius for the circle of the commit in the GUI.
	 */
	const int m_radius = 5;
};
