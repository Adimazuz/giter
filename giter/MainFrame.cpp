#pragma warning( push )
#pragma warning( disable : 4267)
#include <future>
#pragma warning(pop)
#include "MainFrame.h"
#include "ColoredTextCustomRenderer.h"
#include "TreeControl.h"
#include <wx/button.h>


MainFrame::MainFrame(wxWindow* parent, VIEW_MODEL_T* view_model_ffi): MainFrameBase(parent), m_view_model_ffi(view_model_ffi)
{
	Init_terminator_control();

	Init_tree_control();

	Init_statuser_control();

	Init_filer_control();

	Init_status_bar();

	// Make sure the Statuser tab is selected.
	this->m_notebook1->SetSelection(1);

	this->wxTopLevelWindowMSW::Show();
	this->wxTopLevelWindowMSW::Maximize();
}

void MainFrame::Init_terminator_control()
{
	auto* terminator_sizer = new wxBoxSizer(wxHORIZONTAL);

	m_terminator_control = new TerminatorControl(view_model_get_terminator_view_model(m_view_model_ffi), m_terminator_panel, wxID_ANY, wxEmptyString, wxDefaultPosition,
	                                             wxDefaultSize,wxTE_AUTO_URL | wxBORDER_SUNKEN | wxHSCROLL | wxVSCROLL | wxWANTS_CHARS);

	// ReSharper disable once CppCStyleCast
	terminator_sizer->Add((wxWindow*)m_terminator_control, 1, wxEXPAND | wxALL, 5); // NOLINT(bugprone-suspicious-enum-usage)

	m_terminator_panel->SetSizer(terminator_sizer);
	m_terminator_panel->Layout();
	terminator_sizer->Fit(m_terminator_panel);
}

void MainFrame::Init_tree_control()
{
	auto* tree_sizer = new wxBoxSizer(wxVERTICAL);

	m_tree_control = new TreeControl(view_model_get_tree_view_model(m_view_model_ffi), m_tree_panel, wxID_ANY, wxDefaultPosition,
	                                 wxSize(-1, -1),wxDV_ROW_LINES | wxDV_SINGLE);

	// ReSharper disable once CppCStyleCast
	tree_sizer->Add((wxWindow*)m_tree_control, 1, wxEXPAND, 5);

	m_tree_panel->SetSizer(tree_sizer);
	tree_sizer->Fit(m_tree_panel);
	m_tree_panel->Layout();
}

void MainFrame::Init_statuser_control()
{
	auto* statuser_sizer = new wxBoxSizer(wxVERTICAL);

	m_statuser_control = new StatuserControl(view_model_get_statuser_view_model(m_view_model_ffi), m_statuser_panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D);

	// ReSharper disable once CppCStyleCast
	statuser_sizer->Add((wxWindow*)m_statuser_control, 1, wxEXPAND, 5);

	m_statuser_panel->SetSizer(statuser_sizer);
	statuser_sizer->Fit(m_statuser_panel);
	m_statuser_panel->Layout();
}

void MainFrame::Init_filer_control()
{
	auto* statuser_sizer = new wxBoxSizer(wxVERTICAL);

	m_filer_control = new FilerControl(view_model_get_filer_view_model(m_view_model_ffi), m_filer_panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D);

	// ReSharper disable once CppCStyleCast
	statuser_sizer->Add((wxWindow*)m_filer_control, 1, wxEXPAND, 5);

	m_filer_panel->SetSizer(statuser_sizer);
	statuser_sizer->Fit(m_filer_panel);
	m_filer_panel->Layout();
}

void MainFrame::Init_status_bar() const
{
	this->m_status_bar->SetFieldsCount(3);

	const int widths[]{200, 200, 200};
	this->m_status_bar->SetStatusWidths(3, widths);
}

void MainFrame::Main_frame_on_char_hook(wxKeyEvent& event)
{
	const auto was_ctrl_pressed = wxGetKeyState(WXK_CONTROL);
	const unsigned int key_code = event.GetKeyCode();

	// ReSharper disable once CppCStyleCast
	const bool is_tree_selected = this->m_notebook1->GetSelection() == 0 && ((wxWindowBase*)m_terminator_control)->HasFocus() == false;

	view_model_keyboard_shortcut_was_pressed(m_view_model_ffi, was_ctrl_pressed, key_code, is_tree_selected);

	event.Skip();
}
