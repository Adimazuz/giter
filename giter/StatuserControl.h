#pragma once
#include <wx/splitter.h>
#include <wx/dataview.h>
#include <wx/richtext/richtextctrl.h>
#include "StatuserViewModelFFI.h"
#include "DifferControl.h"


class StatuserControl : protected wxSplitterWindow
{
public:
	StatuserControl(STATUSER_VIEW_MODEL_T* view_model_ffi, wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
	                long style = wxSP_3D, const wxString& name = wxT("splitter"));

	~StatuserControl();

private:

	//IStatuserViewModel* m_view_model;
	STATUSER_VIEW_MODEL_T* m_view_model_ffi;

	wxButton* m_button_stage{};
	wxButton* m_button_reset{};
	wxButton* m_button_unstage{};
	wxDataViewListCtrl* m_list_unstaged{};
	wxDataViewListCtrl* m_list_staged{};
	DifferControl* m_differ{};
	wxButton* m_button_commit{};
	wxButton* m_button_push{};
	wxRichTextCtrl* m_rich_text_commit_message{};

	void Init_gui();

	void Init_events();

	void Subscribe_to_observables();

	void Refresh_diff_view(const wxDataViewEvent& event) const;
	std::string Get_file_path(const wxDataViewEvent& event) const;
	std::string Get_file_path_of_selected_row() const;

	// Events.
	void On_button_click_m_button_stage(wxCommandEvent& event);
	void On_button_click_m_button_reset(wxCommandEvent& event);
	void On_button_click_m_button_unstage(wxCommandEvent& event);

	void List_item_activated_unstaged(wxDataViewEvent& event);
	void List_selection_changed_unstaged(wxDataViewEvent& event);

	void List_item_activated_staged(wxDataViewEvent& event);
	void List_selection_changed_staged(wxDataViewEvent& event);

	void On_button_click_m_button_commit(wxCommandEvent& event);
	void On_button_click_m_button_push(wxCommandEvent& event);
};
