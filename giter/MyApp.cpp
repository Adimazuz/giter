#include "MyApp.h"
#include <sstream>


MyApp::MyApp()
{
	m_main_frame = nullptr;
}

bool MyApp::OnInit()
{
	std::filesystem::path path_to_repo;
	// // Repo path can be the current dir.
	// if (argc != 2)
	// {
	// 	path_to_repo = std::filesystem::current_path();
	// }
	// else // Or it can be some path passed in a CLI argument.
	// {
	// 	path_to_repo = std::filesystem::path(static_cast<std::string>(argv.GetArguments()[1]));
	// }

	path_to_repo = std::filesystem::current_path();

	Compose_the_root(path_to_repo);

	Init_the_main_window();

	return true;
}

void MyApp::Compose_the_root(const std::filesystem::path& path_to_repo)
{
	m_view_model_ffi = view_model_new(path_to_repo.string().c_str());
}

void MyApp::Init_the_main_window()
{
	wxInitAllImageHandlers();
	m_main_frame = new MainFrame(nullptr, m_view_model_ffi);
	this->SetTopWindow(m_main_frame);
}
