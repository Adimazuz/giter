#pragma once
#include <wx/splitter.h>
#include <wx/dataview.h>
#include <wx/richtext/richtextctrl.h>
#include "wx/generic/dirctrlg.h"
#include "DifferControl.h"
#include "FilerViewModelFFI.h"
#include "MyGenericDirCtrl.h"
#include "wx/sizer.h"


class FilerControl : protected wxSplitterWindow
{
public:
	FilerControl(FILER_VIEW_MODEL_T* view_model_ffi, wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
	             long style = wxSP_3D, const wxString& name = wxT("splitter"));

	~FilerControl();

private:

	FILER_VIEW_MODEL_T* m_view_model_ffi;

	MyGenericDirCtrl* m_dir_ctrl{};
	wxDataViewListCtrl* m_list_ctrl{};
	wxStaticBoxSizer* m_static_box_sizer{};
	DifferControl* m_differ{};

	void Init_gui();

	void Subscribe_to_observables();

	void Init_file_history_list() const;

	void Init_events();

	void Refresh_history_list() const;
	
	void List_selection_changed(wxDataViewEvent& event);
	void Dir_ctrl_selection_changed(wxTreeEvent& event);

	void Refresh_diff_view(const std::string& sha1, const std::string& sha1_previous, const std::string& file_path) const;
};
