///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/panel.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/stattext.h>
#include <wx/sizer.h>
#include <wx/notebook.h>
#include <wx/splitter.h>
#include <wx/statusbr.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class MainFrameBase
///////////////////////////////////////////////////////////////////////////////
class MainFrameBase : public wxFrame
{
private:

protected:
	wxSplitterWindow* m_splitter1;
	wxPanel* m_panel1;
	wxPanel* m_tree_panel;
	wxPanel* m_panel111;
	wxStaticText* m_staticText1;
	wxStaticText* m_staticText2;
	wxStaticText* m_staticText3;
	wxStaticText* m_staticText4;
	wxStaticText* m_staticText5;
	wxPanel* m_terminator_panel;

	// Virtual event handlers, overide them in your derived class
	virtual void Main_frame_on_char_hook(wxKeyEvent& event) { event.Skip(); }


public:
	wxNotebook* m_notebook1;
	wxPanel* m_statuser_panel;
	wxStatusBar* m_status_bar;
	wxPanel* m_filer_panel;

	MainFrameBase(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Giter"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(979, 680),
	              long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL);

	~MainFrameBase();

	void m_splitter1OnIdle(wxIdleEvent&)
	{
		m_splitter1->SetSashPosition(494);
		m_splitter1->Disconnect(wxEVT_IDLE, wxIdleEventHandler(MainFrameBase::m_splitter1OnIdle), NULL, this);
	}
};
