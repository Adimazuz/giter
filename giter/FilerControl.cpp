#include "FilerControl.h"
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/panel.h>
#include <wx/dataview.h>
#include <wx/statbox.h>
#include <wx/button.h>
#include <wx/richtext/richtextctrl.h>
#include "ColoredTextCustomRenderer.h"
#include "TerminatorViewModelFFI.h"


FilerControl::FilerControl(FILER_VIEW_MODEL_T* view_model_ffi, wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size,
                           long style, const wxString& name):
	wxSplitterWindow(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D), m_view_model_ffi(view_model_ffi) //, m_view_model(view_model)
{
	Init_gui();

	Subscribe_to_observables();

	Init_file_history_list();

	Init_events();
}

FilerControl::~FilerControl()
{
	m_list_ctrl->Disconnect(wxEVT_COMMAND_DATAVIEW_SELECTION_CHANGED, wxDataViewEventHandler(FilerControl::List_selection_changed), nullptr, this);
	m_dir_ctrl->Disconnect(wxEVT_DIRCTRL_FILEACTIVATED, wxTreeEventHandler(FilerControl::Dir_ctrl_selection_changed), nullptr, this);
}

void FilerControl::Init_gui()
{
	this->SetSashPosition(300);

	wxPanel* left_panel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	auto* left_sizer = new wxBoxSizer(wxVERTICAL);
	this->SetSashPosition(225);
	left_panel->SetSizer(left_sizer);

	const auto path_to_repo = filer_view_model_get_repo_path(m_view_model_ffi);
	m_dir_ctrl = new MyGenericDirCtrl(left_panel, path_to_repo);
	left_sizer->Add(m_dir_ctrl, 1, wxEXPAND, 5);
	//left_sizer->Add(top_static_sizer, 1, wxEXPAND, 5);

	wxPanel* right_panel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	auto* right_sizer = new wxBoxSizer(wxVERTICAL);
	wxSplitterWindow* right_splitter_window = new wxSplitterWindow(right_panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D);
	right_splitter_window->SetSashPosition(307);
	right_sizer->Add(right_splitter_window, 1, wxEXPAND, 0);
	right_panel->SetSizer(right_sizer);

	wxPanel* middle_panel = new wxPanel(right_splitter_window, wxID_ANY);
	auto* middle_sizer = new wxBoxSizer(wxVERTICAL);
	m_static_box_sizer = new wxStaticBoxSizer(new wxStaticBox(middle_panel, wxID_ANY, wxT("Nothing selected")), wxVERTICAL);
	m_list_ctrl = new wxDataViewListCtrl(middle_panel, wxID_ANY, wxDefaultPosition, wxSize(-1, -1), wxDV_ROW_LINES | wxDV_SINGLE);
	m_static_box_sizer->Add(m_list_ctrl, 1, wxEXPAND, 5);
	middle_sizer->Add(m_static_box_sizer, 1, wxEXPAND, 5);
	middle_panel->SetSizer(middle_sizer);

	wxPanel* buttom_right_panel = new wxPanel(right_splitter_window, wxID_ANY);
	auto* buttom_right_sizer = new wxBoxSizer(wxHORIZONTAL);
	auto* sizer_for_buttons = new wxBoxSizer(wxVERTICAL);
	m_differ = new DifferControl(buttom_right_panel);
	buttom_right_sizer->Add(sizer_for_buttons, 0, wxALIGN_CENTER, 5);
	buttom_right_sizer->Add((wxWindow*)m_differ, 1, wxEXPAND, 5);
	buttom_right_panel->SetSizer(buttom_right_sizer);

	right_splitter_window->SplitVertically(middle_panel, buttom_right_panel, 400);

	this->wxSplitterWindow::SplitVertically(left_panel, right_panel, 300);
}

void FilerControl::Subscribe_to_observables()
{
	refresh.subscribe([this]()
	{
		Refresh_history_list();
	});
}

void FilerControl::Init_file_history_list() const
{
	m_list_ctrl->AppendTextColumn("Commit message")->SetWidth(wxCOL_WIDTH_AUTOSIZE);
	m_list_ctrl->AppendTextColumn("Date")->SetWidth(wxCOL_WIDTH_AUTOSIZE);
	m_list_ctrl->AppendTextColumn("SHA1")->SetWidth(wxCOL_WIDTH_AUTOSIZE);

	//m_main_frame->m_data_view_list_ctrl->SetRowHeight(22);
}

void FilerControl::Init_events()
{
	m_list_ctrl->Connect(wxEVT_COMMAND_DATAVIEW_SELECTION_CHANGED, wxDataViewEventHandler(FilerControl::List_selection_changed), nullptr, this);
	m_dir_ctrl->Connect(wxEVT_DIRCTRL_FILEACTIVATED, wxTreeEventHandler(FilerControl::Dir_ctrl_selection_changed), nullptr, this);
}

void FilerControl::Refresh_history_list() const
{
	m_differ->Clear_diff_text();
	
	m_list_ctrl->DeleteAllItems();

	filer_view_model_get_history(m_view_model_ffi, m_dir_ctrl->GetPath().ToStdString().c_str());
	const auto amount = filer_view_model_history_amount(m_view_model_ffi);
	for (unsigned int i = 0; i < amount; i++)
	{
		wxVector<wxVariant> item;

		item.push_back(filer_view_model_history_commit_message(m_view_model_ffi, i));
		item.push_back(filer_view_model_history_date(m_view_model_ffi, i));
		item.push_back(filer_view_model_history_sha1(m_view_model_ffi, i));

		m_list_ctrl->AppendItem(item);
	}

	m_static_box_sizer->GetStaticBox()->SetLabel(m_dir_ctrl->GetPath());
}

// ReSharper disable once CppMemberFunctionMayBeConst
void FilerControl::List_selection_changed(wxDataViewEvent& event)
{
	// Get selected SHA1.
	wxVariant variant;
	event.GetModel()->GetValue(variant, event.GetItem(), 2);
	const auto sha1 = variant.GetString().ToStdString();

	// Get previous SHA1.
	const auto selected_index = m_list_ctrl->ItemToRow(event.GetItem());
	if (selected_index + 1 >= m_list_ctrl->GetItemCount())
	{
		Refresh_diff_view(sha1, "", m_dir_ctrl->GetPath().ToStdString());
	}
	else
	{
		const auto item = m_list_ctrl->RowToItem(selected_index + 1);
		m_list_ctrl->GetModel()->GetValue(variant, item, 2);
		const auto sha1_previous = variant.GetString().ToStdString();

		Refresh_diff_view(sha1, sha1_previous, m_dir_ctrl->GetPath().ToStdString());
	}
}

void FilerControl::Refresh_diff_view(const std::string& sha1, const std::string& sha1_previous, const std::string& file_path) const
{
	filer_view_model_get_diff(m_view_model_ffi, sha1.c_str(), sha1_previous.c_str(), file_path.c_str());
	std::vector<COLOR_STRING> diff_string;
	const auto amount = filer_view_model_get_diff_amount(m_view_model_ffi);
	for (unsigned int i = 0; i < amount; ++i)
	{
		const char* data = filer_view_model_get_diff_data(m_view_model_ffi, i);
		const unsigned int red = filer_view_model_get_diff_red(m_view_model_ffi, i);
		const unsigned int green = filer_view_model_get_diff_green(m_view_model_ffi, i);
		const unsigned int blue = filer_view_model_get_diff_blue(m_view_model_ffi, i);
		const unsigned int background_red = filer_view_model_get_diff_background_red(m_view_model_ffi, i);
		const unsigned int background_green = filer_view_model_get_diff_background_green(m_view_model_ffi, i);
		const unsigned int background_blue = filer_view_model_get_diff_background_blue(m_view_model_ffi, i);
		diff_string.push_back(COLOR_STRING{data, COLOR{red, green, blue}, COLOR{background_red, background_green, background_blue}});
	}

	m_differ->Clear_diff_text();
	m_differ->Set_diff_text(diff_string);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void FilerControl::Dir_ctrl_selection_changed(wxTreeEvent& event)
{
	Refresh_history_list();
}
