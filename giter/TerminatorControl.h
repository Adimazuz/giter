#pragma once
#pragma warning(push)
#pragma warning( disable : 4267)
#include <wx/richtext/richtextctrl.h>
#include "TerminatorViewModelFFI.h"
#pragma warning(pop)


class TerminatorControl : protected wxRichTextCtrl
{
public:

	TerminatorControl(TERMINATOR_VIEW_MODEL_T* view_model, wxWindow* parent, wxWindowID id = -1, const wxString& value = wxEmptyString, const wxPoint& pos = wxDefaultPosition,
	                  const wxSize& size = wxDefaultSize,
	                  long style = wxRE_MULTILINE);
	~TerminatorControl();

private:

	TERMINATOR_VIEW_MODEL_T* m_view_model_ffi;

	void Init_gui_stuff();

	void Subscribe_to_observables();

	void Enter_was_pressed_event(wxKeyEvent& event);

	void Rich_text_on_key_down(wxKeyEvent& event);

	void On_left_down(wxMouseEvent& event);
};
