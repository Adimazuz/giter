#include "DifferControl.h"


DifferControl::DifferControl(wxWindow* parent): wxRichTextCtrl(parent, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize,
                                                               wxTE_READONLY | wxBORDER_SUNKEN | wxHSCROLL | wxVSCROLL | wxWANTS_CHARS)
{
}

void DifferControl::Set_diff_text(std::vector<COLOR_STRING>& diff_string)
{
	// View stuff.
	wxRichTextAttr attr = this->GetBasicStyle();
	attr.SetLineSpacing(7);
	attr.SetFontSize(11);

	// Print the diff text.
	for (const auto& line : diff_string)
	{
		attr.SetBackgroundColour(wxColour(line.background_color.red, line.background_color.green, line.background_color.blue));
		attr.SetTextColour(wxColour(line.color.red, line.color.green, line.color.blue));

		this->BeginStyle(attr);
		this->WriteText(line.data);
		this->EndStyle();
	}
}

void DifferControl::Clear_diff_text()
{
	this->Clear();
}
