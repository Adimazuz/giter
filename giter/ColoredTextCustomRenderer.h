#pragma once

#include "wx/dataview.h"
#include "wx/splitter.h"

//#include "Tree.h"


// ReSharper disable once CppInconsistentNaming
struct INDEX_INFO
{
	// ReSharper disable CppInconsistentNaming
	std::string file_path;
	unsigned int red;
	unsigned int green;
	unsigned int blue;
	// ReSharper restore CppInconsistentNaming	
};

class ColoredTextCustomRenderer final : public wxDataViewCustomRenderer
{
public:
	explicit ColoredTextCustomRenderer(/*std::shared_ptr<ViewModel> view_model*/);
	~ColoredTextCustomRenderer() = default;

	bool SetValue(const wxVariant& value) override;
	bool GetValue(wxVariant& value) const override;
	wxSize GetSize() const override;
	bool Render(wxRect cell, wxDC* dc, int state) override;

private:

	//std::shared_ptr<ViewModel> m_view_model;

	INDEX_INFO* m_index_info;
};
