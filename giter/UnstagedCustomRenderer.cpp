#include "ColoredTextCustomRenderer.h"
#include <wx/wx.h>


ColoredTextCustomRenderer::ColoredTextCustomRenderer(/*std::shared_ptr<ViewModel> view_model*/)
	: wxDataViewCustomRenderer("void*", wxDATAVIEW_CELL_INERT, wxALIGN_CENTER), m_unstaged_info(nullptr)
{
}

bool ColoredTextCustomRenderer::SetValue(const wxVariant& value)
{
	if (value.IsNull() == true)
	{
		return false;
	}

	m_unstaged_info = static_cast<UNSTAGED_INFO*>(value.GetVoidPtr());

	return true;
}

bool ColoredTextCustomRenderer::GetValue(wxVariant& value) const
{
	//value = wxVariant(m_node);

	return true;
}

wxSize ColoredTextCustomRenderer::GetSize() const
{
	return wxSize(42, 42);
}

bool ColoredTextCustomRenderer::Render(wxRect cell, wxDC* dc, int state)
{
	wxCoord height = 0;
	wxCoord width = 0;

	const std::string text_to_draw = m_unstaged_info->data;
	dc->GetTextExtent(text_to_draw, &width, &height);
	dc->SetTextForeground(wxColour(m_unstaged_info->red, m_unstaged_info->green, m_unstaged_info->blue));
	dc->DrawText(text_to_draw, cell.GetX() + 5, cell.GetY() + cell.height / 2 - height / 2);

	return true;
}
