#include "StatuserControl.h"
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/panel.h>
#include <wx/dataview.h>
#include <wx/statbox.h>
#include <wx/button.h>
#include <wx/richtext/richtextctrl.h>
#include "ColoredTextCustomRenderer.h"
#include "TerminatorViewModelFFI.h"


StatuserControl::StatuserControl(/*IStatuserViewModel* view_model, */STATUSER_VIEW_MODEL_T* view_model_ffi, wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size,
                                                                     long style, const wxString& name):
	wxSplitterWindow(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D), m_view_model_ffi(view_model_ffi) //, m_view_model(view_model)
{
	Init_gui();

	Init_events();

	Subscribe_to_observables();
}

StatuserControl::~StatuserControl()
{
	m_button_stage->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(StatuserControl::On_button_click_m_button_stage), nullptr, this);
	m_button_reset->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(StatuserControl::On_button_click_m_button_reset), nullptr, this);
	m_button_unstage->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(StatuserControl::On_button_click_m_button_unstage), nullptr, this);

	m_list_unstaged->Disconnect(wxEVT_COMMAND_DATAVIEW_ITEM_ACTIVATED, wxDataViewEventHandler(StatuserControl::List_item_activated_unstaged), nullptr, this);
	m_list_unstaged->Disconnect(wxEVT_COMMAND_DATAVIEW_SELECTION_CHANGED, wxDataViewEventHandler(StatuserControl::List_selection_changed_unstaged), nullptr, this);

	m_list_staged->Disconnect(wxEVT_COMMAND_DATAVIEW_ITEM_ACTIVATED, wxDataViewEventHandler(StatuserControl::List_item_activated_staged), nullptr, this);
	m_list_staged->Disconnect(wxEVT_COMMAND_DATAVIEW_SELECTION_CHANGED, wxDataViewEventHandler(StatuserControl::List_selection_changed_staged), nullptr, this);

	m_button_commit->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(StatuserControl::On_button_click_m_button_commit), nullptr, this);
	m_button_push->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(StatuserControl::On_button_click_m_button_push), nullptr, this);
}

void StatuserControl::Init_gui()
{
	this->SetSashPosition(300);

	wxPanel* left_panel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	auto* left_sizer = new wxBoxSizer(wxVERTICAL);
	wxSplitterWindow* left_splitter_window = new wxSplitterWindow(left_panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D);
	this->SetSashPosition(225);
	left_sizer->Add(left_splitter_window, 1, wxEXPAND, 0);
	left_panel->SetSizer(left_sizer);

	wxPanel* top_left_panel = new wxPanel(left_splitter_window, wxID_ANY);
	auto* top_left_sizer = new wxBoxSizer(wxVERTICAL);
	auto* top_left_buttons_sizer = new wxBoxSizer(wxHORIZONTAL);
	m_button_stage = new wxButton(top_left_panel, wxID_ANY, wxT("Stage all"), wxDefaultPosition, wxDefaultSize, 0);
	m_button_reset = new wxButton(top_left_panel, wxID_ANY, wxT("Reset selection"), wxDefaultPosition, wxDefaultSize, 0);
	auto* top_static_sizer = new wxStaticBoxSizer(new wxStaticBox(top_left_panel, wxID_ANY, wxT("Unstaged")), wxVERTICAL);
	m_list_unstaged = new wxDataViewListCtrl(top_static_sizer->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, wxDV_ROW_LINES | wxDV_SINGLE);
	top_static_sizer->Add(m_list_unstaged, 1, wxEXPAND, 5);
	// top_left_sizer->Add(m_button_stage, 0, wxALL, 5);
	// top_left_sizer->Add(m_button_reset_selection, 0, wxALL, 5);
	top_left_buttons_sizer->Add(m_button_stage, 0, wxALL, 5);
	top_left_buttons_sizer->Add(m_button_reset, 0, wxALL, 5);
	top_left_sizer->Add(top_left_buttons_sizer, 0, wxEXPAND, 5);
	top_left_sizer->Add(top_static_sizer, 1, wxEXPAND, 5);
	top_left_panel->SetSizer(top_left_sizer);

	wxPanel* buttom_left_panel = new wxPanel(left_splitter_window, wxID_ANY);
	auto* buttom_left_sizer = new wxBoxSizer(wxVERTICAL);
	m_button_unstage = new wxButton(buttom_left_panel, wxID_ANY, wxT("Unstage all"), wxDefaultPosition, wxDefaultSize, 0);
	auto* buttom_static_sizer = new wxStaticBoxSizer(new wxStaticBox(buttom_left_panel, wxID_ANY, wxT("Staged")), wxVERTICAL);
	m_list_staged = new wxDataViewListCtrl(buttom_static_sizer->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, wxDV_ROW_LINES | wxDV_SINGLE);
	buttom_static_sizer->Add(m_list_staged, 1, wxEXPAND, 5);
	buttom_left_sizer->Add(m_button_unstage, 0, wxALL, 5);
	buttom_left_sizer->Add(buttom_static_sizer, 1, wxEXPAND, 5);
	buttom_left_panel->SetSizer(buttom_left_sizer);

	left_splitter_window->SplitHorizontally(top_left_panel, buttom_left_panel, 225);

	wxPanel* right_panel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	auto* right_sizer = new wxBoxSizer(wxVERTICAL);
	wxSplitterWindow* right_splitter_window = new wxSplitterWindow(right_panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D);
	right_splitter_window->SetSashPosition(307);
	right_sizer->Add(right_splitter_window, 1, wxEXPAND, 0);
	right_panel->SetSizer(right_sizer);

	wxPanel* top_right_panel = new wxPanel(right_splitter_window, wxID_ANY);
	auto* top_right_sizer = new wxBoxSizer(wxVERTICAL);
	m_differ = new DifferControl(top_right_panel);
	top_right_sizer->Add((wxWindow*)m_differ, 1, wxEXPAND | wxALL, 5);
	top_right_panel->SetSizer(top_right_sizer);

	wxPanel* buttom_right_panel = new wxPanel(right_splitter_window, wxID_ANY);
	auto* buttom_right_sizer = new wxBoxSizer(wxHORIZONTAL);
	auto* sizer_for_buttons = new wxBoxSizer(wxVERTICAL);
	m_button_commit = new wxButton(buttom_right_panel, wxID_ANY, wxT("Commit"), wxDefaultPosition, wxDefaultSize, 0);
	m_button_push = new wxButton(buttom_right_panel, wxID_ANY, wxT("Push"), wxDefaultPosition, wxDefaultSize, 0);
	m_rich_text_commit_message = new wxRichTextCtrl(buttom_right_panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize,
	                                                wxBORDER_SUNKEN | wxHSCROLL | wxVSCROLL | wxWANTS_CHARS);
	buttom_right_sizer->Add(sizer_for_buttons, 0, wxALIGN_CENTER, 5);
	sizer_for_buttons->Add(m_button_commit, 0, wxALL | wxALIGN_CENTER, 5);
	sizer_for_buttons->Add(m_button_push, 0, wxALL | wxALIGN_CENTER, 5);
	buttom_right_sizer->Add(m_rich_text_commit_message, 1, wxEXPAND | wxALL, 5);
	buttom_right_panel->SetSizer(buttom_right_sizer);

	right_splitter_window->SplitHorizontally(top_right_panel, buttom_right_panel, 380);

	this->wxSplitterWindow::SplitVertically(left_panel, right_panel, 300);
}

void StatuserControl::Init_events()
{
	m_button_stage->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(StatuserControl::On_button_click_m_button_stage), nullptr, this);
	m_button_reset->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(StatuserControl::On_button_click_m_button_reset), nullptr, this);
	m_button_unstage->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(StatuserControl::On_button_click_m_button_unstage), nullptr, this);

	m_list_unstaged->Connect(wxEVT_COMMAND_DATAVIEW_ITEM_ACTIVATED, wxDataViewEventHandler(StatuserControl::List_item_activated_unstaged), nullptr, this);
	m_list_unstaged->Connect(wxEVT_COMMAND_DATAVIEW_SELECTION_CHANGED, wxDataViewEventHandler(StatuserControl::List_selection_changed_unstaged), nullptr, this);

	m_list_staged->Connect(wxEVT_COMMAND_DATAVIEW_ITEM_ACTIVATED, wxDataViewEventHandler(StatuserControl::List_item_activated_staged), nullptr, this);
	m_list_staged->Connect(wxEVT_COMMAND_DATAVIEW_SELECTION_CHANGED, wxDataViewEventHandler(StatuserControl::List_selection_changed_staged), nullptr, this);

	m_button_commit->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(StatuserControl::On_button_click_m_button_commit), nullptr, this);
	m_button_push->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(StatuserControl::On_button_click_m_button_push), nullptr, this);
}

void StatuserControl::Subscribe_to_observables()
{
	// Unstaged.
	auto* unstaged_custom_renderer_file_name = new ColoredTextCustomRenderer();
	wxDataViewColumn* unstaged_column_file_name = new wxDataViewColumn("File name", unstaged_custom_renderer_file_name, 0, 210);
	this->m_list_unstaged->AppendColumn(unstaged_column_file_name);
	auto* unstaged_custom_renderer_status = new ColoredTextCustomRenderer();
	wxDataViewColumn* unstaged_column_status = new wxDataViewColumn("Status", unstaged_custom_renderer_status, 1);
	this->m_list_unstaged->AppendColumn(unstaged_column_status);

	// Staged.
	auto* staged_custom_renderer_file_name = new ColoredTextCustomRenderer();
	wxDataViewColumn* staged_column_file_name = new wxDataViewColumn("File name", staged_custom_renderer_file_name, 0, 210);
	this->m_list_staged->AppendColumn(staged_column_file_name);
	auto* staged_custom_renderer_status = new ColoredTextCustomRenderer();
	wxDataViewColumn* staged_column_status = new wxDataViewColumn("Status", staged_custom_renderer_status, 1);
	this->m_list_staged->AppendColumn(staged_column_status);

	// Subscribe to the unstaged subject.
	// m_view_model->unstaged.subscribe([this](const std::vector<std::pair<std::string, std::string>>& unstaged)
	// {
	// 	this->m_list_unstaged->DeleteAllItems();
	//
	// 	const auto color = wxColour(169, 9, 54);
	//
	// 	wxVector<wxVariant> item;
	// 	for (unsigned int i = 0; i < unstaged.size(); ++i)
	// 	{
	// 		item.push_back(wxVariant(new INDEX_INFO{(unstaged)[i].second, color.Red(), color.Green(), color.Blue()}));
	// 		item.push_back(wxVariant(new INDEX_INFO{(unstaged)[i].first, color.Red(), color.Green(), color.Blue()}));
	// 		this->m_list_unstaged->AppendItem(item);
	// 		item.clear();
	// 	}
	// });

	// Subscribe to the unstaged subject.
	unstaged.subscribe([this]()
	{
		this->m_list_unstaged->DeleteAllItems();

		const auto color = wxColour(169, 9, 54);

		const auto amount = statuser_view_model_get_unstaged_amount(m_view_model_ffi);

		wxVector<wxVariant> item;
		for (unsigned int i = 0; i < amount; ++i)
		{
			item.push_back(wxVariant(new INDEX_INFO{statuser_view_model_get_unstaged_file_name(m_view_model_ffi, i), color.Red(), color.Green(), color.Blue()}));
			item.push_back(wxVariant(new INDEX_INFO{statuser_view_model_get_unstaged_file_status(m_view_model_ffi, i), color.Red(), color.Green(), color.Blue()}));
			this->m_list_unstaged->AppendItem(item);
			item.clear();
		}
	});

	// // Subscribe to the untracked subject.
	// m_view_model->untracked.subscribe([this](const std::vector<std::pair<std::string, std::string>>& untracked)
	// {
	// 	const auto color = wxColour(32, 134, 168);
	//
	// 	wxVector<wxVariant> item;
	// 	for (unsigned int i = 0; i < untracked.size(); ++i)
	// 	{
	// 		item.push_back(wxVariant(new INDEX_INFO{(untracked)[i].second, color.Red(), color.Green(), color.Blue()}));
	// 		item.push_back(wxVariant(new INDEX_INFO{(untracked)[i].first, color.Red(), color.Green(), color.Blue()}));
	// 		this->m_list_unstaged->AppendItem(item);
	// 		item.clear();
	// 	}
	// });

	// Subscribe to the untracked subject.
	untracked.subscribe([this]()
	{
		const auto color = wxColour(32, 134, 168);

		const auto amount = statuser_view_model_get_untracked_amount(m_view_model_ffi);

		wxVector<wxVariant> item;
		for (unsigned int i = 0; i < amount; ++i)
		{
			item.push_back(wxVariant(new INDEX_INFO{statuser_view_model_get_untracked_file_name(m_view_model_ffi, i), color.Red(), color.Green(), color.Blue()}));
			item.push_back(wxVariant(new INDEX_INFO{statuser_view_model_get_untracked_file_status(m_view_model_ffi, i), color.Red(), color.Green(), color.Blue()}));
			this->m_list_unstaged->AppendItem(item);
			item.clear();
		}
	});

	// // Subscribe to the staged subject.
	// m_view_model->staged.subscribe([this](const std::vector<std::pair<std::string, std::string>>& staged)
	// {
	// 	this->m_list_staged->DeleteAllItems();
	//
	// 	auto color = wxColour(116, 166, 18);
	//
	// 	wxVector<wxVariant> item;
	// 	for (unsigned int i = 0; i < staged.size(); ++i)
	// 	{
	// 		item.push_back(wxVariant(new INDEX_INFO{(staged)[i].second, color.Red(), color.Green(), color.Blue()}));
	// 		item.push_back(wxVariant(new INDEX_INFO{(staged)[i].first, color.Red(), color.Green(), color.Blue()}));
	// 		this->m_list_staged->AppendItem(item);
	// 		item.clear();
	// 	}
	//
	// 	this->m_list_unstaged->UnselectAll();
	// 	this->m_list_staged->UnselectAll();
	//
	// 	if (this->m_list_unstaged->GetItemCount() > 0)
	// 	{
	// 		// This will select the first row.
	// 		this->m_list_unstaged->SelectRow(0);
	//
	// 		// This will generate a selection changed event to display the diff view of the first row.
	// 		const wxDataViewItem first_item = this->m_list_unstaged->RowToItem(0);
	// 		wxDataViewEvent event = wxDataViewEvent(wxNewEventType(), this->m_list_unstaged, first_item);
	// 		this->Refresh_diff_view(event);
	// 	}
	// 	else if (this->m_list_staged->GetItemCount() > 0)
	// 	{
	// 		// This will select the first row.
	// 		this->m_list_staged->SelectRow(0);
	//
	// 		// This will generate a selection changed event to display the diff view of the first row.
	// 		const wxDataViewItem first_item = this->m_list_staged->RowToItem(0);
	// 		wxDataViewEvent event = wxDataViewEvent(wxNewEventType(), this->m_list_staged, first_item);
	// 		this->Refresh_diff_view(event);
	// 	}
	// });

	// // Subscribe to the staged subject.
	staged.subscribe([this]()
	{
		this->m_list_staged->DeleteAllItems();

		auto color = wxColour(116, 166, 18);

		const auto amount = statuser_view_model_get_staged_amount(m_view_model_ffi);

		wxVector<wxVariant> item;
		for (unsigned int i = 0; i < amount; ++i)
		{
			item.push_back(wxVariant(new INDEX_INFO{statuser_view_model_get_staged_file_name(m_view_model_ffi, i), color.Red(), color.Green(), color.Blue()}));
			item.push_back(wxVariant(new INDEX_INFO{statuser_view_model_get_staged_file_status(m_view_model_ffi, i), color.Red(), color.Green(), color.Blue()}));
			this->m_list_staged->AppendItem(item);
			item.clear();
		}

		this->m_list_unstaged->UnselectAll();
		this->m_list_staged->UnselectAll();

		if (this->m_list_unstaged->GetItemCount() > 0)
		{
			// This will select the first row.
			this->m_list_unstaged->SelectRow(0);

			// This will generate a selection changed event to display the diff view of the first row.
			const wxDataViewItem first_item = this->m_list_unstaged->RowToItem(0);
			wxDataViewEvent event = wxDataViewEvent(wxNewEventType(), this->m_list_unstaged, first_item);
			this->Refresh_diff_view(event);
		}
		else if (this->m_list_staged->GetItemCount() > 0)
		{
			// This will select the first row.
			this->m_list_staged->SelectRow(0);

			// This will generate a selection changed event to display the diff view of the first row.
			const wxDataViewItem first_item = this->m_list_staged->RowToItem(0);
			wxDataViewEvent event = wxDataViewEvent(wxNewEventType(), this->m_list_staged, first_item);
			this->Refresh_diff_view(event);
		}
	});

	// m_view_model->commit_message_set_text.subscribe([this](const std::string& data)
	// {
	// 	m_rich_text_commit_message->SetValue(data);
	// });

	commit_message_clear.subscribe([this]()
	{
		m_rich_text_commit_message->SetValue("");
	});
}

void StatuserControl::Refresh_diff_view(const wxDataViewEvent& event) const
{
	// Get file path from the wxDataViewEvent (`event`) object.
	const std::string file_path = Get_file_path(event);

	// // Get the diff text.
	// std::vector<COLOR_STRING> diff_string = m_view_model->Get_diff(file_path);
	statuser_view_model_get_diff(m_view_model_ffi, file_path.c_str());
	std::vector<COLOR_STRING> diff_string;
	const unsigned int amount = statuser_view_model_get_diff_amount(m_view_model_ffi);
	for (unsigned int i = 0; i < amount; ++i)
	{
		const char* data = statuser_view_model_get_diff_data(m_view_model_ffi, i);
		const unsigned int red = statuser_view_model_get_diff_red(m_view_model_ffi, i);
		const unsigned int green = statuser_view_model_get_diff_green(m_view_model_ffi, i);
		const unsigned int blue = statuser_view_model_get_diff_blue(m_view_model_ffi, i);
		const unsigned int background_red = statuser_view_model_get_diff_background_red(m_view_model_ffi, i);
		const unsigned int background_green = statuser_view_model_get_diff_background_green(m_view_model_ffi, i);
		const unsigned int background_blue = statuser_view_model_get_diff_background_blue(m_view_model_ffi, i);
		diff_string.push_back(COLOR_STRING{data, COLOR{red, green, blue}, COLOR{background_red, background_green, background_blue}});
	}

	// Clear the diff view.
	m_differ->Clear_diff_text();

	m_differ->Set_diff_text(diff_string);
}

// ReSharper disable once CppMemberFunctionMayBeStatic
std::string StatuserControl::Get_file_path(const wxDataViewEvent& event) const
{
	if (event.GetItem() == nullptr)
	{
		// Nothing was selected.
		return "";
	}

	// Get the wxVariant of the row.
	wxVariant variant;
	event.GetModel()->GetValue(variant, event.GetItem(), 0);

	// Convert the variant from above to INDEX_INFO.
	const auto index_info = static_cast<INDEX_INFO*>(variant.GetVoidPtr());

	return index_info->file_path;
}

std::string StatuserControl::Get_file_path_of_selected_row() const
{
	const wxDataViewItem data_view_item = m_list_unstaged->GetSelection();

	if (data_view_item.IsOk() == false)
	{
		return "";
	}

	// Get the wxVariant of the row.
	wxVariant variant;
	m_list_unstaged->GetModel()->GetValue(variant, data_view_item, 0);

	// Convert the variant from above to INDEX_INFO.
	const auto index_info = static_cast<INDEX_INFO*>(variant.GetVoidPtr());

	return index_info->file_path;
}

// ReSharper disable once CppMemberFunctionMayBeConst
void StatuserControl::On_button_click_m_button_stage(wxCommandEvent& event)
{
	//m_view_model->Stage_button_was_clicked();
	statuser_view_model_stage_button_was_clicked(m_view_model_ffi);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void StatuserControl::On_button_click_m_button_reset(wxCommandEvent& event)
{
	const std::string file_path = Get_file_path_of_selected_row();

	//m_view_model->Reset_button_was_clicked(file_path);
	statuser_view_model_reset_button_was_clicked(m_view_model_ffi, file_path.c_str());
}

// ReSharper disable once CppMemberFunctionMayBeConst
void StatuserControl::On_button_click_m_button_unstage(wxCommandEvent& event)
{
	//m_view_model->Unstage_button_was_clicked();
	statuser_view_model_unstage_button_was_clicked(m_view_model_ffi);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void StatuserControl::List_item_activated_unstaged(wxDataViewEvent& event)
{
	const std::string file_path = Get_file_path(event);

	//m_view_model->Unstaged_double_click(file_path);
	statuser_view_model_unstaged_double_click(m_view_model_ffi, file_path.c_str());
}

// ReSharper disable once CppMemberFunctionMayBeConst
void StatuserControl::List_selection_changed_unstaged(wxDataViewEvent& event)
{
	Refresh_diff_view(event);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void StatuserControl::List_item_activated_staged(wxDataViewEvent& event)
{
	const std::string file_path = Get_file_path(event);

	//m_view_model->Staged_double_click(file_path);
	statuser_view_model_staged_double_click(m_view_model_ffi, file_path.c_str());
}

// ReSharper disable once CppMemberFunctionMayBeConst
void StatuserControl::List_selection_changed_staged(wxDataViewEvent& event)
{
	Refresh_diff_view(event);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void StatuserControl::On_button_click_m_button_commit(wxCommandEvent& event)
{
	//m_view_model->Commit_button_was_clicked(m_rich_text_commit_message->GetValue().ToStdString());
	const char* bla = m_rich_text_commit_message->GetValue().ToStdString().c_str();
	statuser_view_model_commit_button_was_clicked(m_view_model_ffi, m_rich_text_commit_message->GetValue().ToStdString().c_str());
}

// ReSharper disable once CppMemberFunctionMayBeConst
void StatuserControl::On_button_click_m_button_push(wxCommandEvent& event)
{
	//m_view_model->Push_button_was_clicked();
	statuser_view_model_push_button_was_clicked(m_view_model_ffi);
}
