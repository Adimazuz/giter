#include "TextDrawer.h"


TextDrawer::TextDrawer(TREE_VIEW_MODEL_T* view_model_ffi, const unsigned int index, wxDC* dc, const unsigned y) :
	m_view_model_ffi(view_model_ffi), m_index(index), m_dc(dc), m_y(y)
{
	m_x_offset = tree_view_model_get_commit_message_offset(m_view_model_ffi, m_index);
}

void TextDrawer::Draw_all_the_text() const
{
	const unsigned int commit_message_offset = Draw_all_refs();
	const auto commit_message = tree_view_model_get_commit_message(m_view_model_ffi, m_index);
	Draw_commit_message(commit_message, m_x_offset + commit_message_offset);
}

unsigned int TextDrawer::Draw_all_refs() const
{
	// Local refs.
	std::vector<std::string> local_refs; // = line.Get_local_refs();
	//const auto local_refs_amount = line.Get_local_refs_amount();
	const auto local_refs_amount = tree_view_model_get_local_refs_amount(m_view_model_ffi, m_index);
	for (unsigned int i = 0; i < local_refs_amount; ++i)
	{
		//local_refs.push_back(line.Get_local_ref(i));
		local_refs.emplace_back(tree_view_model_get_local_ref(m_view_model_ffi, m_index, i));
	}

	m_dc->SetTextForeground(wxColour(145, 10, 16));
	m_dc->SetPen(wxPen(wxColour(145, 10, 16)));
	m_dc->SetBrush(*wxTRANSPARENT_BRUSH);
	const wxCoord width_local_ref = Draw_rectenglized_text(local_refs, m_x_offset);

	// Remote refs.
	std::vector<std::string> remote_refs; // = line.Get_remote_refs();
	//const auto remote_refs_amount = line.Get_remote_refs_amount();
	const auto remote_refs_amount = tree_view_model_get_remote_refs_amount(m_view_model_ffi, m_index);
	for (unsigned int i = 0; i < remote_refs_amount; ++i)
	{
		//remote_refs.push_back(line.Get_remote_ref(i));
		remote_refs.emplace_back(tree_view_model_get_remote_ref(m_view_model_ffi, m_index, i));
	}

	m_dc->SetTextForeground(wxColour(47, 139, 16));
	m_dc->SetPen(wxPen(wxColour(47, 139, 16)));
	m_dc->SetBrush(*wxTRANSPARENT_BRUSH);
	const wxCoord width_remote_ref = Draw_rectenglized_text(remote_refs, m_x_offset + width_local_ref);

	// Tag refs.
	std::vector<std::string> tag_refs; // = line.Get_tag_refs();
	//const auto tag_refs_amount = line.Get_tag_refs_amount();
	const auto tag_refs_amount = tree_view_model_get_tag_refs_amount(m_view_model_ffi, m_index);
	for (unsigned int i = 0; i < tag_refs_amount; ++i)
	{
		//tag_refs.push_back(line.Get_tag_ref(i));
		tag_refs.emplace_back(tree_view_model_get_tag_ref(m_view_model_ffi, m_index, i));
	}

	m_dc->SetTextForeground(wxColour(20, 12, 146));
	m_dc->SetPen(wxPen(wxColour(20, 12, 146)));
	m_dc->SetBrush(*wxTRANSPARENT_BRUSH);
	const wxCoord width_tag_ref = Draw_rectenglized_text(tag_refs, m_x_offset + width_local_ref + width_remote_ref);

	return width_local_ref + width_remote_ref + width_tag_ref;
}

void TextDrawer::Draw_commit_message(const std::string& get_commit_message, const unsigned int commit_message_offset) const
{
	wxCoord width_commit_message = 0;
	wxCoord height = 0;
	m_dc->GetTextExtent(get_commit_message, &width_commit_message, &height);
	m_dc->SetTextForeground(wxColour("black"));
	m_dc->DrawText(get_commit_message, commit_message_offset, m_y);
}

unsigned int TextDrawer::Draw_rectenglized_text(std::vector<std::string> data, const unsigned x) const
{
	if (data.empty() == true)
	{
		return 0;
	}

	unsigned int ref_length = 0;
	for (auto& ref : data)
	{
		ref.insert(0, "   ");
		ref.append("   ");

		ref_length += m_dc->GetTextExtent(ref).GetWidth();
	}

	unsigned int x_offset = x;
	for (auto& ref : data)
	{
		std::string original_ref = ref;

		ref = ref.erase(0, 1);
		ref = ref.erase(ref.length() - 1, 1);

		const wxSize ref_size = m_dc->GetTextExtent(ref);

		const wxRect rect = wxRect(static_cast<int>(x_offset), static_cast<int>(m_y), ref_size.GetWidth(), ref_size.GetHeight());
		m_dc->DrawRoundedRectangle(rect, 2);

		m_dc->DrawLabel(ref, rect, wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL);

		x_offset += m_dc->GetTextExtent(original_ref).GetWidth();
	}

	return ref_length;
}
