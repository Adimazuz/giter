#include "ColoredTextCustomRenderer.h"
#include <wx/wx.h>


ColoredTextCustomRenderer::ColoredTextCustomRenderer(/*std::shared_ptr<ViewModel> view_model*/)
	: wxDataViewCustomRenderer("void*", wxDATAVIEW_CELL_INERT, wxALIGN_CENTER), m_index_info(nullptr)
{
}

bool ColoredTextCustomRenderer::SetValue(const wxVariant& value)
{
	if (value.IsNull() == true)
	{
		return false;
	}

	m_index_info = static_cast<INDEX_INFO*>(value.GetVoidPtr());

	return true;
}

bool ColoredTextCustomRenderer::GetValue(wxVariant& value) const
{
	return true;
}

wxSize ColoredTextCustomRenderer::GetSize() const
{
	return wxSize(999, 999);
}

bool ColoredTextCustomRenderer::Render(wxRect cell, wxDC* dc, int state)
{
	dc->SetTextForeground(wxColor(m_index_info->red, m_index_info->green, m_index_info->blue));

	RenderText(m_index_info->file_path, 0, cell, dc, state);

	return true;
}
