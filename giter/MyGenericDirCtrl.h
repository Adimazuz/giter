#pragma once
#include <wx/richtext/richtextctrl.h>
#include "wx/generic/dirctrlg.h"
#include <filesystem>


class MyGenericDirCtrl : public wxGenericDirCtrl
{
public:
	MyGenericDirCtrl(wxWindow* parent, const std::filesystem::path& path_to_repo);

	void SetupSections() override;

private:
	std::filesystem::path m_path_to_repo;

	wxTreeItemId m_tree_item;
};
