#pragma once
#include "wx/richtext/richtextctrl.h"
#include "TerminatorViewModelFFI.h"


class DifferControl : wxRichTextCtrl
{
public:
	DifferControl(wxWindow* parent);
	~DifferControl() = default;

	void Set_diff_text(std::vector<COLOR_STRING>& diff_string);
	void Clear_diff_text();
};
