#include "TreeCustomRenderer.h"
#include <wx/wx.h>
#include "TextDrawer.h"


TreeCustomRenderer::TreeCustomRenderer(TREE_VIEW_MODEL_T* tree_view_model_ffi)
	: wxDataViewCustomRenderer(/*"void*",*/ "long", wxDATAVIEW_CELL_INERT, wxALIGN_CENTER), m_view_model_ffi(tree_view_model_ffi)
{
	// Blue.
	m_colors.emplace_back(wxColor(0, 0, 255));
	// Pink.
	m_colors.emplace_back(wxColor(240, 36, 117));
	// Light blue.
	m_colors.emplace_back(wxColor(120, 180, 255));
	// Green.
	m_colors.emplace_back(wxColor(46, 204, 113));
	// Purple.
	m_colors.emplace_back(wxColor(142, 68, 173));
	// Orange
	m_colors.emplace_back(wxColor(231, 76, 60));
	// Black.
	m_colors.emplace_back(wxColor(40, 40, 40));
	// Light green.
	m_colors.emplace_back(wxColor(26, 188, 156));
	// Yellow.
	m_colors.emplace_back(wxColor(241, 196, 15));
}

bool TreeCustomRenderer::SetValue(const wxVariant& value)
{
	if (value.IsNull() == true)
	{
		return false;
	}
	//
	// Line* line = static_cast<Line*>(value.GetVoidPtr());
	//
	// m_line = Line(*line);

	m_line_index = value.GetInteger();

	return true;
}

bool TreeCustomRenderer::GetValue(wxVariant& value) const
{
	// // ReSharper disable once CppCStyleCast
	// value = wxVariant((void*)&m_line);
	value = wxVariant(static_cast<int>(m_line_index));

	return true;
}

wxSize TreeCustomRenderer::GetSize() const
{
	return wxSize(42, 42);
}

bool TreeCustomRenderer::Render(const wxRect cell, wxDC* dc, int state)
{
	const unsigned int nodes_amount = tree_view_model_get_nodes_amount(m_view_model_ffi, m_line_index);
	for (unsigned int i = 0; i < nodes_amount; ++i)
	{
		const int cell_width = 4;
		const int y_offset = cell.height / 2;
		const int y_center = cell.GetY() + y_offset;
		const int x_center = tree_view_model_get_column(m_view_model_ffi, m_line_index, i) * cell_width / 2 * m_radius;

		// Build a TextDrawer instance.
		const auto y_coordinate = static_cast<unsigned int>(static_cast<float>(cell.GetY()) + static_cast<float>(cell.height) / 2 - cell.height * 0.68 / 2);
		TextDrawer text_drawer(m_view_model_ffi, m_line_index, dc, y_coordinate);

		// Use the above built TextDrawer to draw all refs and the commit message.
		text_drawer.Draw_all_the_text();

		wxColor color = m_colors[tree_view_model_get_color(m_view_model_ffi, m_line_index, i) % m_colors.size()];
		dc->SetPen(wxPen(color, 2));

		if (tree_view_model_get_node_type(m_view_model_ffi, m_line_index, i) == COMMIT)
		{
			if (tree_view_model_get_is_first_line(m_view_model_ffi, m_line_index) == true)
			{
				// If it is the first commit, draw only half of a vertical line.
				dc->DrawLine(x_center, tree_view_model_is_tip(m_view_model_ffi, m_line_index, i) == true ? y_center : y_center - cell.height / 2, x_center, y_center);
			}
			else
			{
				// Otherwise, draw the full length of the vertical line.
				dc->DrawLine(x_center, tree_view_model_is_tip(m_view_model_ffi, m_line_index, i) == true ? y_center : y_center - cell.height / 2, x_center,
				             y_center + cell.height / 2);
			}

			if (tree_view_model_has_at_least_one_ref(m_view_model_ffi, m_line_index) == true)
			{
				// If its NOT a ref, draw rectangle instead of a circle.
				wxRect rect = wxRect(x_center - m_radius - 1, y_center - m_radius - 1, m_radius * 2 + 2, m_radius * 2 + 2);

				dc->SetBrush(wxBrush(color));
				dc->DrawRectangle(rect);

				dc->SetPen(wxPen("black", 2));
				dc->DrawRectangle(rect);

				// Restore the color back after drawing all the rectangles for a ref.
				dc->SetPen(wxPen(color, 2));
			}
			else
			{
				// If its a ref, draw a circle.
				dc->SetBrush(wxBrush(color));
				dc->DrawCircle(wxPoint(x_center, y_center), m_radius);
			}
		}

		if (tree_view_model_get_node_type(m_view_model_ffi, m_line_index, i) == LINE)
		{
			dc->DrawLine(x_center, y_center - cell.height / 2, x_center, y_center + cell.height / 2);
		}

		if (tree_view_model_get_node_type(m_view_model_ffi, m_line_index, i) == HORIZONTAL)
		{
			dc->DrawLine(x_center - cell_width / 2 * m_radius, y_center + cell_width / 2 * m_radius, x_center + cell_width / 2 * m_radius, y_center + cell_width / 2 * m_radius);
		}

		if (tree_view_model_get_node_type(m_view_model_ffi, m_line_index, i) == LEFT_TO_RIGHT)
		{
			wxPoint points[4];
			points[0] = wxPoint(x_center - cell_width / 2 * m_radius, y_center - cell.height / 2);
			points[1] = wxPoint(x_center - cell_width / 4 * m_radius, y_center);
			points[2] = wxPoint(x_center + cell_width / 4 * m_radius, y_center);
			points[3] = wxPoint(x_center + cell_width / 2 * m_radius, y_center + cell.height / 2);

			dc->DrawSpline(4, points);
		}

		if (tree_view_model_get_node_type(m_view_model_ffi, m_line_index, i) == LEFT_TO_RIGHT_NARROW)
		{
			dc->DrawLine(x_center - cell_width / 2 * m_radius, y_center - cell.height / 2, x_center, y_center + cell.height / 2);
		}

		if (tree_view_model_get_node_type(m_view_model_ffi, m_line_index, i) == RIGHT_TO_LEFT)
		{
			wxPoint points[4];
			points[0] = wxPoint(x_center + cell_width / 2 * m_radius, y_center - cell.height / 2);
			points[1] = wxPoint(x_center + cell_width / 4 * m_radius, y_center);
			points[2] = wxPoint(x_center - cell_width / 4 * m_radius, y_center);
			points[3] = wxPoint(x_center - cell_width / 2 * m_radius, y_center + cell.height / 2);

			dc->DrawSpline(4, points);
		}

		if (tree_view_model_get_node_type(m_view_model_ffi, m_line_index, i) == RIGHT_TO_LEFT_NARROW)
		{
			dc->DrawLine(x_center, y_center - cell.height / 2, x_center - cell_width / 2 * m_radius, y_center + cell.height / 2);
		}
	}

	return true;
}
