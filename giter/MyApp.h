#pragma once
#include <wx/wx.h>
#include "MainFrame.h"
#include <filesystem>


class MyApp final : public wxApp
{
public:
	MyApp();

	bool OnInit() override;

	void Compose_the_root(const std::filesystem::path& path_to_repo);

private:
	MainFrame* m_main_frame;

	// Composition root.
	VIEW_MODEL_T* m_view_model_ffi;
	//TERMINATOR_VIEW_MODEL_T* m_terminator_view_model_ffi;

	void Init_the_main_window();
};
