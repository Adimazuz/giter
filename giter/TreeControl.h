#pragma once
#include "TreeCustomRenderer.h"
#include "TreeViewModelRustFFI.h"


class TreeControl : protected wxDataViewListCtrl
{
public:

	TreeControl(TREE_VIEW_MODEL_T* tree_view_model_ffi, wxPanel* panel3, wxStandardID wx_standard_id, const wxPoint& wx_point,
	            const wxSize& wx_size, int i);

	~TreeControl();

private:
	TREE_VIEW_MODEL_T* m_view_model_ffi;

	//ITreeViewModel* m_view_model;

	TreeCustomRenderer* m_tree_custom_renderer{};
	//std::vector<std::shared_ptr<Line>> m_lines;

	wxMenu* m_menu;

	void Create_the_menu(unsigned int row_index);

	void Init_gui_stuff();

	void Subscribe_to_observables();

	void On_popup_click(wxCommandEvent& command);
	void Item_right_clicked(wxDataViewEvent& command);
	void Clipboard_it(const char* text);
};
