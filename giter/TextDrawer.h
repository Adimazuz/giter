#pragma once
#include "TreeViewModelRustFFI.h"
#include "wx/wx.h"

//#include "Line.h"


class TextDrawer
{
public:
	TextDrawer(TREE_VIEW_MODEL_T* view_model_ffi, unsigned int index, wxDC* dc, const unsigned int y);

	void Draw_all_the_text() const;

	~TextDrawer() = default;

private:

	TREE_VIEW_MODEL_T* m_view_model_ffi;
	unsigned int m_index;
	wxDC* m_dc;
	unsigned int m_x_offset;
	const unsigned int m_y;

	[[nodiscard]] unsigned int Draw_all_refs() const;

	void Draw_commit_message(const std::string& get_commit_message, const unsigned int commit_message_offset) const;

	[[nodiscard]] unsigned int Draw_rectenglized_text(std::vector<std::string> data, const unsigned int x) const;
};
