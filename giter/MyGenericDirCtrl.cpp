#include "MyGenericDirCtrl.h"


MyGenericDirCtrl::MyGenericDirCtrl(wxWindow* parent, const std::filesystem::path& path_to_repo) :
	m_path_to_repo(path_to_repo.c_str())
{
	wxGenericDirCtrl::Init();
	Create(parent);

	this->wxGenericDirCtrl::ShowHidden(true);

	this->wxGenericDirCtrl::GetTreeCtrl()->Expand(m_tree_item);
}

void MyGenericDirCtrl::SetupSections()
{
	m_tree_item = AddSection(m_path_to_repo.c_str(), m_path_to_repo.c_str(), wxFileIconsTable::folder_open);
}
