#include "TreeControl.h"
#include "TreeCustomRenderer.h"
#include "wx/menu.h"
#include <Windows.Globalization.h>


TreeControl::TreeControl(TREE_VIEW_MODEL_T* tree_view_model_ffi, wxPanel* panel3, const wxStandardID wx_standard_id, const wxPoint& wx_point, const wxSize& wx_size, const int i):
	wxDataViewListCtrl(panel3, wx_standard_id, wx_point, wx_size, i), m_view_model_ffi(tree_view_model_ffi), m_menu(nullptr)
{
	Init_gui_stuff();

	Subscribe_to_observables();

	this->Connect(wxEVT_COMMAND_DATAVIEW_ITEM_CONTEXT_MENU, wxDataViewEventHandler(TreeControl::Item_right_clicked), nullptr, this);

	tree_view_model_update_the_tree(m_view_model_ffi);
}

TreeControl::~TreeControl()
{
	this->Disconnect(wxEVT_COMMAND_DATAVIEW_ITEM_CONTEXT_MENU, wxDataViewEventHandler(TreeControl::Item_right_clicked), nullptr, this);
}

void TreeControl::Init_gui_stuff()
{
	// Column for the tree.
	m_tree_custom_renderer = new TreeCustomRenderer(m_view_model_ffi);
	wxDataViewColumn* column = new wxDataViewColumn("Forest", m_tree_custom_renderer, 0);
	column->SetWidth(700);

	this->wxDataViewListCtrl::AppendColumn(column);
	this->AppendTextColumn("Date")->SetWidth(wxCOL_WIDTH_AUTOSIZE);
	this->AppendTextColumn("SHA1")->SetWidth(wxCOL_WIDTH_AUTOSIZE);

	//m_main_frame->m_data_view_list_ctrl->SetRowHeight(22);
}

void TreeControl::Subscribe_to_observables()
{
	clear_the_tree.subscribe([this]()
	{
		this->DeleteAllItems();
	});

	new_line.subscribe([this](const unsigned int line)
	{
		wxVector<wxVariant> item;

		item.push_back(wxVariant(static_cast<int>(line)));
		item.push_back(wxVariant(tree_view_model_get_date(m_view_model_ffi, line)));
		item.push_back(wxVariant(tree_view_model_get_sha1(m_view_model_ffi, line)));

		this->AppendItem(item);
	});

	tree_enablement.subscribe([=](const bool should_enable)
	{
		this->Enable(should_enable);
	});

	tree_selection.subscribe([this]()
	{
		const auto item = this->GetTopItem();

		this->Select(item);
		this->SetFocus();
	});

	copy_paste_was_pressed.subscribe([this]()
	{
		const auto item = this->GetTopItem();
		const std::shared_ptr<wxDataViewEvent> command = std::make_shared<wxDataViewEvent>(wxNewEventType(), static_cast<wxDataViewCtrlBase*>(this), item);
		command->SetInt(42); // A marker to say that it came from ctrl+c and not from right click.
		Item_right_clicked(*command);
	});
}

void TreeControl::Item_right_clicked(wxDataViewEvent& command)
{
	const auto row_index = this->GetSelectedRow();

	Create_the_menu(row_index);

	// If true, it was from ctrl+c and not from right click.
	if (command.GetInt() == 42)
	{
		// Just some nice and round numbers.
		const auto x = this->GetSize().x / 2 - 300;
		const auto y = this->GetSize().y / 2 - 25;

		if (m_menu != nullptr)
		{
			this->PopupMenu(m_menu, x, y);
		}
	}
	else
	{
		if (m_menu != nullptr)
		{
			this->PopupMenu(m_menu);
		}
	}
}

void TreeControl::Create_the_menu(const unsigned int row_index)
{
	if (m_menu != nullptr)
	{
		m_menu->Disconnect(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(TreeControl::On_popup_click), nullptr, this);
	}

	const auto sha1 = tree_view_model_get_sha1(m_view_model_ffi, row_index);
	if (std::string(sha1).empty() == true)
	{
		m_menu = nullptr;
		return;
	}

	const auto commit_message = tree_view_model_get_commit_message(m_view_model_ffi, row_index);
	const auto short_sha1 = std::string(sha1).substr(0, 7);

	m_menu = new wxMenu();
	m_menu->Append(0, short_sha1);
	m_menu->Append(1, commit_message);
	m_menu->Append(2, sha1);
	m_menu->Connect(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(TreeControl::On_popup_click), nullptr, this);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void TreeControl::On_popup_click(wxCommandEvent& command)
{
	const auto clicked_index = command.GetId();

	const auto bla = m_menu->GetLabelText(clicked_index).ToStdString();
	const char* text = bla.c_str();

	Clipboard_it(text);
}

void TreeControl::Clipboard_it(const char* text)
{
	const size_t len = strlen(text) + 1;
	const HGLOBAL h_mem = GlobalAlloc(GMEM_MOVEABLE, len);
	memcpy(GlobalLock(h_mem), text, len);
	GlobalUnlock(h_mem);
	OpenClipboard(nullptr);
	EmptyClipboard();
	SetClipboardData(CF_TEXT, h_mem);
	CloseClipboard();
}
