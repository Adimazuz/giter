#include "TerminatorControl.h"


TerminatorControl::TerminatorControl(TERMINATOR_VIEW_MODEL_T* view_model, wxWindow* parent, const wxWindowID id, const wxString& value, const wxPoint& pos, const wxSize& size,
                                     const long style): wxRichTextCtrl(parent, id, value, pos, size, style), m_view_model_ffi(view_model)
{
	Init_gui_stuff();

	Subscribe_to_observables();

	// This will make sure that terminal will show an initial line or something.
	auto bla = wxKeyEvent();
	bla.m_keyCode = 13;
	this->Enter_was_pressed_event(bla);

	this->Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(TerminatorControl::Rich_text_on_key_down), nullptr, this);
	//this->Connect(wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler(TerminatorControl::Enter_was_pressed_event), nullptr, this);
	this->Connect(wxEVT_CHAR_HOOK, wxKeyEventHandler(TerminatorControl::Enter_was_pressed_event));
	this->Connect(wxEVT_LEFT_DOWN, wxMouseEventHandler(TerminatorControl::On_left_down), NULL, this);
}

TerminatorControl::~TerminatorControl()
{
	this->Disconnect(wxEVT_KEY_DOWN, wxKeyEventHandler(TerminatorControl::Rich_text_on_key_down), nullptr, this);
	//this->Disconnect(wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler(TerminatorControl::Enter_was_pressed_event), nullptr, this);
	this->Disconnect(wxEVT_CHAR_HOOK, wxKeyEventHandler(TerminatorControl::Enter_was_pressed_event));
}

void TerminatorControl::Init_gui_stuff()
{
	this->wxRichTextCtrl::SetBackgroundColour(wxColour(0, 0, 0));

	// Set the font for the Terminator.
	wxFont font(wxFontInfo(14).FaceName("Consolas"));
	font.SetNativeFontInfo("1;14;-17;0;0;0;400;0;0;0;1;0;0;5;48;Consolas");
	this->wxRichTextCtrl::SetFont(font);

	// Configure the Rich Text Widget of the Terminator.
	wxRichTextAttr attr = this->wxRichTextCtrl::GetBasicStyle();
	attr.SetBackgroundColour(wxColour(0, 0, 0));

	//const COLOR normal_color = m_view_model->Get_normal_terminator_color();
	const COLOR normal_color = COLOR{
		terminator_view_model_get_normal_terminator_color_red(m_view_model_ffi),
		terminator_view_model_get_normal_terminator_color_red(m_view_model_ffi),
		terminator_view_model_get_normal_terminator_color_red(m_view_model_ffi)
	};
	attr.SetTextColour(wxColour(normal_color.red, normal_color.green, normal_color.blue));

	attr.SetLineSpacing(7);
	this->wxRichTextCtrl::SetBasicStyle(attr);

	this->wxRichTextCtrl::SetFocus();
}

void TerminatorControl::Subscribe_to_observables()
{
	// m_view_model->terminal_data.subscribe([=](const std::vector<COLOR_STRING>& data)
	// {
	// 	if (data.empty() == false)
	// 	{
	// 		this->SetInsertionPointEnd();
	//
	// 		for (unsigned int i = 0; i < data.size(); ++i)
	// 		{
	// 			if (data[i].data.empty() == false)
	// 			{
	// 				this->BeginTextColour(wxColour(data[i].color.red, data[i].color.green, data[i].color.blue));
	// 				this->WriteText(data[i].data);
	// 				this->EndTextColour();
	// 			}
	// 		}
	//
	// 		// This if for auto scrolling.
	// 		this->ScrollIntoView(this->GetCaretPosition() + 1, WXK_PAGEDOWN);
	//
	// 		this->Refresh();
	// 		this->Update();
	// 	}
	// });
	terminal_data.subscribe([=](/*const unsigned int line_number*/)
	{
		const auto res_amount = terminator_view_model_get_res_amount(m_view_model_ffi);
		if (terminator_view_model_get_res_amount(m_view_model_ffi) > 0)
		{
			this->SetInsertionPointEnd();

			for (unsigned int i = 0; i < res_amount; ++i)
			{
				const char* data = terminator_view_model_get_res(m_view_model_ffi, i);
				if (data != nullptr && std::string(data).empty() == false)
				{
					this->BeginTextColour(wxColour(
						terminator_view_model_get_res_red(m_view_model_ffi, i),
						terminator_view_model_get_res_green(m_view_model_ffi, i),
						terminator_view_model_get_res_blue(m_view_model_ffi, i)));
					this->WriteText(std::string(data));
					this->EndTextColour();
				}
			}

			// This if for auto scrolling.
			this->ScrollIntoView(this->GetCaretPosition() + 1, WXK_PAGEDOWN);

			this->Refresh();
			this->Update();
		}
	});

	//m_view_model->terminator_enablement.subscribe([=](const bool should_enable)
	terminator_enablement.subscribe([=](const bool should_enable)
	{
		this->Enable(should_enable);
	});

	terminator_set_focus.subscribe([this]()
	{
		this->SetFocus();
	});

	clear_input_line.subscribe([this]()
	{
		this->SetInsertionPointEnd();
		const auto end_position = this->GetInsertionPoint();

		long col = 0;
		long line = 0;
		this->PositionToXY(end_position, &col, &line);

		const auto start_position = this->XYToPosition(0, line);

		this->Replace(start_position, end_position, "> ");
	});
}

// ReSharper disable once CppMemberFunctionMayBeConst
void TerminatorControl::Enter_was_pressed_event(wxKeyEvent& event)
{
	if (event.m_keyCode == 13 || event.m_keyCode == 370)
	{
		// Get the number of lines.
		const long number_of_lines = this->GetNumberOfLines();

		// Get the text the user typed in.
		const wxString res = this->GetLineText(number_of_lines - 1);

		//m_view_model->Press_enter(res.ToStdString());
		terminator_view_model_press_enter(m_view_model_ffi, res.ToStdString().c_str());
	}
	else
	{
		event.Skip();
	}
}

// ReSharper disable once CppMemberFunctionMayBeConst
void TerminatorControl::Rich_text_on_key_down(wxKeyEvent& event)
{
	const long position = this->GetCaretPosition();

	long x = 0;
	long y = 0;
	this->PositionToXY(position, &x, &y);

	// This will prevent typing beyond the last line.
	if (y != this->GetNumberOfLines() - 1)
	{
		return;
	}

	// Left arrow or backspace.
	if (event.m_keyCode == 0x13A || event.m_keyCode == 0x8)
	{
		if (x == 1)
		{
			return;
		}
	}

	// Up arrow.
	if (event.m_keyCode == 0x13B) // || event.m_keyCode == 0x13D)
	{
		//m_view_model->Go_earlier_in_history();
		terminator_view_model_go_earlier_in_history(m_view_model_ffi);

		return;
	}

	// Down arrow.
	if (event.m_keyCode == 0x13D)
	{
		//m_view_model->Go_later_in_history();
		terminator_view_model_go_later_in_history(m_view_model_ffi);

		return;
	}

	// Tab.
	if (event.m_keyCode == 0x9)
	{
		// Get the number of lines.
		const long number_of_lines = this->GetNumberOfLines();

		// Get the text the user typed in.
		const wxString res = this->GetLineText(number_of_lines - 1);

		//m_view_model->Append_data_to_terminator(res.ToStdString());
		terminator_view_model_append_data_to_terminator(m_view_model_ffi, res.ToStdString().c_str());

		return;
	}

	// In all the rest of cases, skip, let wxWidgets handle the event.
	event.Skip();
}

void TerminatorControl::On_left_down(wxMouseEvent& event)
{
	// const long position = this->GetCaretPosition();
	//
	// long x = 0;
	// long y = 0;
	// this->PositionToXY(position, &x, &y);
	//
	// const auto res = this->XYToPosition(2, this->GetNumberOfLines() - 1);
	// this->SetCaretPosition(res);

	event.Skip();
}
