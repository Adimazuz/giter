---
layout: post
title: "The philosophy behind Giter"
date: 2019-08-10 16:25:01 -0700
comments: true
---

This project is about yet another GUI client for Git, but different.
It will mix elements from both CLI and GUI.
It will have as minimalistic features set as possible, because, well, I don't like `feature-bloat`.
I've seen many Git GUI clients over the years, and, IMHO, they all suffer from one big problem: they are all too complex.
Git is complex enough, and it takes time and effort to learn it.
If you add on top of it learning the quirks of some GUI, it becomes too much.
The GUI should be a simple wrapper around Git CLI. For example, I've once seen a GUI that had a `pull` button, but what it was doing in the background is something like `git pull --all --force --modules --init` and etc., and the user had no clue. It forced the user to learn the quirks of that specific GUI and not Git in general. Giter, would not have such a button, in fact, Giter would almost have no buttons at all. And the ones it will have will be very simple and will reflect their action in a terminal window that is embedded in Giter.
